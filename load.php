<?php

//CURRENT DIRECTORY
$dr = dirname(__FILE__);
global $dr;

//CONFIG
require('etc/config.php');

//CONTENT DIRECTORY
define("CONTENTDIR",$dr . '/content/');

//CORE CLASSES
if ($handle = opendir($dr . '/inc/classes')) {
    while (false !== ($entry = readdir($handle))) {
	if (is_file($dr . '/inc/classes/' . $entry)) require($dr . '/inc/classes/' . $entry);
    }
    closedir($handle);
}

//SMARTY TEMPLATE ENGINE SETUP
require($dr . "/inc/smarty/libs/Smarty.class.php");
global $smarty;
$smarty = new Smarty();
$smarty->caching = false;
$smarty->template_dir = $dr . '/inc/smarty/this.site/templates';
$smarty->compile_dir = $dr . '/inc/smarty/this.site/templates_c';
$smarty->cache_dir = $dr . '/inc/smarty/this.site/cache';
$smarty->config_dir = $dr . '/inc/smarty/this.site/configs';
//SMARTY CUSTOM UTILS
$smarty->assign("smartyutil",$SMARTYUTIL);

//TIMEZONE
date_default_timezone_set('GMT');

//CACHE SPANNER
$spanner = date('U');
$smarty->assign("spanner",$spanner);

//JAVASCRIPT
$GLOBALS['js'] = array();
if ($handle = opendir($dr . '/inc/js')) {
    while (false !== ($entry = readdir($handle))) {
	if (is_file($dr . '/inc/js/' . $entry)) $GLOBALS['js'][] = $entry;
    }
    closedir($handle);
}
$smarty->assign("js",$GLOBALS['js']);

//CSS
$GLOBALS['css'] = array();
if ($handle = opendir($dr . '/inc/css')) {
    while (false !== ($entry = readdir($handle))) {
	if (is_file($dr . '/inc/css/' . $entry)) $GLOBALS['css'][] = $entry;
    }
    closedir($handle);
}
$smarty->assign("css",$GLOBALS['css']);

//LOAD SESSION
if (isset($_SERVER)) {
	$SECURITY->session();
	$_SESSION['me'] = (isset($_SESSION['me'])) ? $_SESSION['me'] : false;
	$smarty->assign("me",$_SESSION['me']);
}

//MOBILE BYPASS
if (isset($_REQUEST['mobilemain']) and $_REQUEST['mobilemain'] > 0) {
	if (!isset($_SESSION['mobilemain'])) $_SESSION['mobilemain'] = true;
} else if (isset($_REQUEST['mobilemain']) and $_REQUEST['mobilemain'] < 1) {
	if ($_SESSION['mobilemain']) $_SESSION['mobilemain'] = false;
}
	
if (isset($_SESSION['mobilemain'])) {
	$smarty->assign("is_mobile_main",$_SESSION['mobilemain']);
} else {
	$smarty->assign("is_mobile_main",false);

}

//CHECK IF MOBILE
$is_mobile = $UTIL->is_mobile();
if (isset($_SESSION['mobilemain']) and $_SESSION['mobilemain']) {
	$is_mobile = false;
} else if (isset($_SESSION['mobilemain']) and !$_SESSION['mobilemain']) {
	$is_mobile = true;
}	
global $is_mobile;
//Disable mobile interface
$is_mobile = false;
//Ends
$smarty->assign("is_mobile",$is_mobile);

//LOAD CONFIG
$config = $UTIL->config();
global $config;
$smarty->assign("config",$config);

//URI INSTRUCTION
$path = $UTIL->path();
global $path;
$smarty->assign("path",$path);

//LOAD TEXTS AND LANGUAGES
$language = $UTIL->language(1);
$all_language = $UTIL->language(0);
$text = $UTIL->text();

//PUT POST REQUEST STACK INTO SMARTY
if (!empty($_POST)) {
	$post = $_POST;
} else {
	$post = false;		
}
$smarty->assign("post",$post);

//SET LANGUAGE ON CLICK
if (isset($_GET['l'])) {
	$_SESSION['language'] = $_GET['l'];
	if (isset($_SESSION['me']->id)) $UTIL->user_language($_SESSION['me']->id,$_SESSION['language']);
	//LANGUAGE COOKIE
	setcookie("language",$_GET['l'],time()+60*60*24*30,"/","speaktalkchat.com");
} 

//IF COOKIE EXISTS, BUT NO SESSION LANGUAGE, SET SESSION TO COOKIE
if (isset($_COOKIE['language']) and !isset($_SESSION['language'])) {
	if ($_COOKIE['language'] == 'ir') $_COOKIE['language'] = 'en';
	$_SESSION['language'] = $_COOKIE['language'];
} elseif (!isset($_SESSION['language'])) {
	$_SESSION['language'] = 'en';
}

//SET LANGUAGE COOKIE IF IT DOESN'T EXIST
if (!isset($_COOKIE['language'])) {
	setcookie("language",$_SESSION['language'],time()+60*60*24*30,"/","speaktalkchat.com");
}

//VANITY URLS
include('vanity.php');

$smarty->assign("selected_language",$_SESSION['language']);
global $language;
global $text;
$smarty->assign("language",$language);
$smarty->assign("all_language",$all_language);
$smarty->assign("text",$text);

//SYSTEM LANGUAGES
$system_languages = array();
foreach ($language as $k => $v) {
	$system_languages[$v->language_id] = $v->language_name;
}
$smarty->assign('system_languages',$system_languages);

//PROFILE LANGUAGES
$profile_languages = array();
foreach ($all_language as $k => $v) {
	if ($v->language_profile > 0) {
		$profile_languages[$v->language_id] = $v->language_name;
	}
}
$smarty->assign('profile_languages',$profile_languages);

//LANGUAGE LEVELS
$levels = array($text[$_SESSION['language']]['user_data_op']['beginner']->text,$text[$_SESSION['language']]['user_data_op']['competent']->text,$text[$_SESSION['language']]['user_data_op']['fluent']->text);
$levels_en = array('Beginner','Competent','Fluent');
$smarty->assign('levels',$levels);
$smarty->assign('levels_en',$levels_en);

//FACEBOOK
require($dr . '/inc/classes/imported/facebook.php');
$facebook = new Facebook(array(
	'appId'  => $config['facebook']->data->login_app_id->value,
	'secret' => $config['facebook']->data->login_app_secret->value,
	'cookie' => true
));
global $facebook;

//HEARTBEAT STATE
$smarty->assign("heartbeat_state","0");

//DEFAULT FOR INDICATING IF USER IS IN CONVERSATION
$in_conversation = false;
$smarty->assign("in_conversation",$in_conversation);

//NEW PRIVATE MESSAGE COUNT
if (isset($_SESSION['me']->id)) {
	$pmcount = $UTIL->pmcount($_SESSION['me']->id);
} else {	
	$pmcount = false;
}
$smarty->assign("pmcount",$pmcount);

//NEW CONVERSATION COUNT
if (isset($_SESSION['me']->id)) {
	$concount = $UTIL->concount($_SESSION['me']->id);
} else {	
	$concount = false;
}
$smarty->assign("concount",$concount);

//MENUS
$CONTENT->static_menu();

//NONAV MARKER FOR CONVERSATION SCREEN
$nonav = false;
$smarty->assign("nonav",$nonav);

//CHROME WARNING (REQUIRES TRANSLATION)
$is_chrome = false;
if (!isset($_SESSION['chrome_banner_displayed']) and $_SERVER['REQUEST_URI'] != "/" and $_SERVER['REQUEST_URI'] != "/loggedout") {
	
	if (strpos($_SERVER['HTTP_USER_AGENT'], 'Chrome') !== false) {
		
	
		$_SESSION['chrome_banner_displayed'] = true;
		$is_chrome = true;
	}
}
//SWITCH OFF
$is_chrome = false;
$smarty->assign("is_chrome",$is_chrome);

//ANDROID WARNING (REQUIRES TRANSLATION)
$is_android = false;
if ($is_mobile) {	
	if (strpos($_SERVER['HTTP_USER_AGENT'], 'Android') !== false and strpos($_SERVER['HTTP_USER_AGENT'], 'Firefox') === false) {
		$_SESSION['android_banner_displayed'] = true;
		$is_android = true;
	}
	
}
$smarty->assign("is_android",$is_android);


//ADMINISTRATOR
$is_admin = false;
if (isset($_SESSION['me']->id)) {
	$_SESSION['me']->is_admin = false;	
	if ($_SESSION['me']->id === "1") {
		$_SESSION['me']->is_admin = true;
		$is_admin = true;
	}
}
$smarty->assign("is_admin",$is_admin);

//UNDER 18 FROM FACEBOOK
$u18 = false;
if (isset($_SESSION['u18'])) {
	$u18 = true;	
}
$smarty->assign("u18",$u18);
?>
