<?php

include('load.php');

$ajax = false;

switch ($path[0]) {

	case 'start':
		
		//DISABLE BACK BUTTON AFTER LOGOUT
		if (!$_SESSION['me']) $UTIL->redirect("/");

		switch ($path[1]) {

			case 'skype':

				$START->skype_start();
				$ajax = true;
				exit;
				break;

			break;			
			
			case 'data':

				$START->data($path[2],$path[3],$path[4]);
				$ajax = true;
				exit;
				break;

			break;

			case 'key':

				$START->key($path[2],$path[3]);
				$ajax = true;
				exit;
				break;

			break;

			case 'feedback':

				$START->feedback();
				$ajax = true;
				exit;
				break;

			break;

			case 'review':

				$START->review($path[2]);

			break;

			default:

				$nonav = array("id" => $path[1]);
				$smarty->assign("nonav",$nonav);
				$START->launch($path[1]);			
		
		}
	break;

	case 'faq':

		$CONTENT->faq();

	break;

	case 'member':

		//DISABLE BACK BUTTON AFTER LOGOUT
		if (!$_SESSION['me']) $UTIL->redirect("/");

		if ($path[2]) {

			switch ($path[2]) {

				case 'req':

					$MEMBER->request();
					$ajax = true;
					exit;

				break;

				case 'c':

					$MEMBER->conversation($path[1],$path[3],$path[4]);
					$smarty->display('member_page.tpl');

				break;
				
				case 'result':
					
					$MEMBER->page($path[1]);
					$smarty->display('member_page.tpl');
					
				break;

			}

		} else {

			$MEMBER->page($path[1]);
			$smarty->display('member_page.tpl');

		}

	break;

	case 'user':

		$path[1] = (isset($_SESSION['me'])) ? $path[1] : 'not_logged_in';

		$_SESSION['me'] = (isset($_SESSION['me'])) ? $_SESSION['me'] : false;

		//DISABLE BACK BUTTON AFTER LOGOUT
		if (!$_SESSION['me'] and $path[1] != 'password') $UTIL->redirect("/");

		$smarty->assign("me",$_SESSION['me']);

		switch ($path[1]) {

			case 'password':

				switch ($path[2]) {

					case 'reset':

						$USER->password_reset();

					break;

					default:

					//Visual confirmation
					$SECURITY->do_captcha();

					$new = true;
					$smarty->assign("new",$new);
					$smarty->display("user_password.tpl");
				}

			break;
			

			case 'conversations':

				//CHECK DATA PROFILE FOR REQUIRED DATA BEFORE USE
				$USER->check_profile();

				switch ($path[2]) {

					case 'get':
						$conversations = $USER->conversations();
						$ajax = true;
						exit;
					break;

					default:

					$smarty->assign("code",$path[2]);
					$smarty->display("user_conversations.tpl");
				}

			break;

			case 'directory':

				//CHECK DATA PROFILE FOR REQUIRED DATA BEFORE USE
				$USER->check_profile();

				switch ($path[2]) {

					case 'get':
						$USER->get_directory();
						$ajax = true;
						exit;
					break;

					case 'update':

						$USER->update_directory($path[4],$path[3]);
						$ajax = true;
						exit;

					break;

					default:

					$smarty->display("user_directory.tpl");

				}

			break;

			case 'heartbeat':

				$USER->heartbeat();
				$ajax = true;
				exit;

			break;

			case 'search':

				//CHECK DATA PROFILE FOR REQUIRED DATA BEFORE USE
				$USER->check_profile();

				$USER->search();

				switch ($path[2]) {

					case 'detail':
						$ajax = true;
						exit;
					break;	
					case 'results':
						$ajax = true;
						exit;
					break;


					default:		
						$smarty->display("user_search.tpl");

				}

			break;

			case 'dashboard':

				//CHECK DATA PROFILE FOR REQUIRED DATA BEFORE USE
				$USER->check_profile();

				$USER->dashboard();	
				$USER->profile();

				switch ($path[2]) {
					case '':

					break;

					default:
				}

				$smarty->display("user_dashboard.tpl");

			break;

			case 'account':

				$USER->account();	

				switch ($path[2]) {
					case '':

					break;

					default:
				}

				$smarty->display("user_account.tpl");

			break;

			case 'rate':

				$USER->rate();
				$ajax = true;
				exit;

			break;

			case 'profile':

				$avatar_processed = false;
				$avail_processed = false;
				$lang_processed = false;
				$profile_processed = false;
				
				$USER->profile();	
				$required = false;
				
				switch ($path[2]) {
					
					case 'required':
						$required = true;
					break;
					
					case 'update_offset':
						$ajax = true;
						exit;
					break;	
											
					default:
				}

				$smarty->assign("required",$required);
				$smarty->assign("avatar_processed",$avatar_processed);
				$smarty->assign("avail_processed",$avail_processed);				
				$smarty->assign("lang_processed",$lang_processed);				
				$smarty->assign("profile_processed",$profile_processed);				
				
				$smarty->display("user_profile.tpl");

			break;

			case 'groups':

				//CHECK DATA PROFILE FOR REQUIRED DATA BEFORE USE
				$USER->check_profile();

				$statuses = array('public'=>$text[$_SESSION['language']]['statuses']['public']->text,'private'=>$text[$_SESSION['language']]['statuses']['private']->text);
				$smarty->assign('statuses',$statuses);

				$GROUP->mod_required();
				$GROUP->requests();

				$members = false;
				$is_member = false;
				$discussions = false;
				$discussion = false;
				$posts = false;
				$thread_no = 0;
				$groupdata = false;
				$group = false;

				switch ($path[2]) {

					case 'members':

						switch ($path[3]) {

							case 'delete':

								$GROUP->delete_member();
								$ajax = true;
								exit;


							break;

							default:

								if (is_numeric($path[3])) {
									$members = $GROUP->members($path[3]);	
									$groupdata = $GROUP->groupdata($path[3]);
									$group = $path[3];
									$is_member = $GROUP->check_membership($path[3]);
									$discussions = $GROUP->discussions($path[3],5);	
								}

						}
		
					break;

					case 'forum':

						//BLOCK IF NOT A GROUP MEMBER OR ADMIN
						if (!$_SESSION['me']->is_admin) {
							if (!$GROUP->check_membership($path[3])) $UTIL->redirect('/user/groups/' . $path[3]);
						}

						$group = $path[3];
						$groupdata = $GROUP->groupdata($path[3]);

						switch ($path[4]) {

							case 'discussion':

								switch ($path[5]) {

									default:

										if (is_numeric($path[5])) {
											$discussion = $path[5];
											if (isset($path[6])) $thread_no = $path[6];	
										} else if ($path[5] == 'posts') {
											$GROUP->posts();
											$ajax = true;
											exit;
										} else if ($path[5] == 'submitpost') {
											$GROUP->submit_post();
											$ajax = true;
											exit;
										} else if ($path[5] == 'flagpost') {
											$GROUP->flag_post();
											$ajax = true;
											exit;
										} else if ($path[5] == 'submitdiscussion') {
											$GROUP->submit_discussion();
											$ajax = true;
											exit;
										} else if ($path[5] == 'editpost') {
											$GROUP->edit_post();
											$ajax = true;
											exit;
										} else if ($path[5] == 'notifypost') {
											$GROUP->notify_post();
											$ajax = true;
											exit;
										} else if ($path[5] == 'deldiscussion') {
											$GROUP->delete_discussion();
											$ajax = true;
											exit;
										} else if ($path[5] == 'delpost') {
											$GROUP->delete_post();
											$ajax = true;
											exit;
										} else if ($path[5] == 'togglediscussion') {
											$GROUP->toggle_discussion();
											$ajax = true;
											exit;
										}

								}
		
							break;

							case 'discussions':

								$GROUP->discussions();
								$ajax = true;
								exit;

							break;

							default:

								if (is_numeric($path[3])) {
									$discussions = $GROUP->discussions($path[3]);	
								}


						}
		
					break;

					case 'create':

						$GROUP->create();
						$ajax = true;
						exit;

					break;

					case 'get':

						$GROUP->get();
						$ajax = true;
						exit;

					break;

					case 'action':

						$GROUP->action();
						$ajax = true;
						exit;

					break;

					default:

						$selector = $path[2];

				}

				if (empty($selector)) $selector = 'my';

				$smarty->assign('selector',$selector);
				$smarty->assign('members',$members);
				$smarty->assign('is_member',$is_member);
				$smarty->assign('discussions',$discussions);
				$smarty->assign('discussion',$discussion);
				$smarty->assign('thread_no',$thread_no);
				$smarty->assign('groupdata',$groupdata);
				$smarty->assign('group',$group);

				$smarty->display("user_group.tpl");

			break;

			case 'messages':

				//CHECK DATA PROFILE FOR REQUIRED DATA BEFORE USE
				$USER->check_profile();

				$to = false;
				$subject = false;
				$thread = false;
				$group = false;

				if ($path[2] and !is_numeric($path[2])) {

					switch ($path[2]) {

						case 'getmod':

							$MESSAGE->getmod();
							$ajax = true;
							exit;

						break;

						case 'actionmod':

							$MESSAGE->actionmod();
							$ajax = true;
							exit;

						break;

						case 'mark':

							$MESSAGE->mark();
							$ajax = true;
							exit;

						break;

						case 'send':

							$MESSAGE->send();
							$ajax = true;
							exit;

						break;

						case 'delete':

							$MESSAGE->delete();
							$ajax = true;
							exit;

						break;

						case 'data':

							$MESSAGE->recipients();
							$ajax = true;
							exit;

						break;

						case 'sendto':

							$MESSAGE->inbox($path[2]);
							$to = $path[3];
							

						break;

						case 'sendtogroup':

							$group = $path[3];
							$MESSAGE->inbox($path[2]);	
							$to = $GROUP->groupdata($path[3])->group_name;

						break;

						default:

							$MESSAGE->message();
	
					}

				} else {

					$MESSAGE->inbox($path[2]);

				}

				$smarty->assign("to",$to);
				$smarty->assign("thread",$thread);
				$smarty->assign("group",$group);
				$smarty->display('user_messages.tpl');			
			break;

			default:		

			$smarty->display("user_not_logged_in.tpl");

		}

	break;

	//PROCESS THE SESSION DELETION AND REDIRECT
	case 'logout':

		$USER->logout();
		$UTIL->redirect("/loggedout");

	break;
	
	//FACEBOOK LOGIN IS NOT RE_ATTEMPTED AT THIS URL
	case 'loggedout':
		
		if ($is_mobile) {
			$page = 'mobile_home';
		} else {
			$page = 'home';
		}
		$CONTENT->page($page);

	break;

	case 'login':

		$USER->login();
		
		//ALLOW MOBILE ACCESS /login URL
		$smarty->display("user_login_mobile.tpl");

	break;

	case 'invalidlogin':

		$process_message = $text[$_SESSION['language']]['messages']['invalid_login']->text;
		$smarty->assign("process_message",$process_message);
		if ($is_mobile) {
			$smarty->display("user_login_mobile.tpl");
		} else {
			$CONTENT->page('home');
		}


	break;

	case 'register':

		switch ($path[1]) {
			case 'account':

				//Test Facebook and process form if submitted
				$me = $USER->register();
				//Visual confirmation
				$SECURITY->do_captcha();
				//If registration has been processed, or login by Facebook, redirect
				if (!empty($_SESSION['me']->redirect)) {
					$UTIL->redirect($_SESSION['me']->redirect);
				} else {
				//else, repeat
					$smarty->display("user_register_account.tpl");
				}

			break;

			default:

				$smarty->display("user_register_intro.tpl");
		
		}
	break;

	case 'admin':

		if ($path[1] != 'login') {
			if ($_SESSION['me']->id != 1) $UTIL->redirect("/admin/login");
		}

		switch ($path[1]) {

			case 'login':

				$ADMIN->login();
				$smarty->display("admin_login.tpl");

			break;

			case 'dashboard':

				$ADMIN->dashboard();
				$smarty->display("admin_dashboard.tpl");

			break;

			case 'faq':

				$ADMIN->faq();
				$smarty->display("admin_faq.tpl");

			break;

			case 'conversation':

				$ADMIN->conversation($path[2]);
				$smarty->display("admin_conversation.tpl");

			break;
			
			case 'conversations':


				switch ($path[2]) {

					case 'get':
						$conversations = $USER->conversations();
						$ajax = true;
						exit;
					break;

					default:
						
					$nickname = $UTIL->nickname_convert($path[2]);

					$smarty->assign("nickname",$nickname);
					$smarty->display("admin_conversations.tpl");
				}

			break;			

		
			case 'logout':

				$USER->logout();
				$UTIL->redirect("/admin/login");

			break;

			case 'ad':
				switch ($path[2]) {
					case 'update':
						$ADMIN->admin_ad_update();
						$smarty->display("admin_ad_update.tpl");
					break;

					default:
				}

			break;
			
			case 'groups':
				switch ($path[2]) {
					case 'update':
						$ADMIN->admin_groups_update();
						$smarty->display("admin_groups_update.tpl");
					break;

					default:
				}

			break;

			case 'search':

				$USER->search();

				switch ($path[2]) {

					case 'detail':
						$ajax = true;
						exit;
					break;	
					case 'results':
						$ajax = true;
						exit;
					break;


					default:		
						$smarty->display("admin_user_search.tpl");

				}

			break;

			case 'config':
				switch ($path[2]) {
					case 'update':
						$ADMIN->admin_config_update();
						$smarty->display("admin_config_update.tpl");
					break;

					default:
				}
			break;
			case 'text':
				switch ($path[2]) {
					case 'update':
						$ADMIN->admin_text_update();
						$smarty->display("admin_text_update.tpl");
					break;

					default:
				}
			break;
			case 'language':
				switch ($path[2]) {
					case 'update':
						$ADMIN->admin_language_update();
						$smarty->display("admin_language_update.tpl");
					break;

					default:
				}
			break;
			case 'data':
				switch ($path[2]) {
					case 'update':
						switch ($path[3]) {

							case 'delete':
								$ADMIN->admin_data_update_delete($path[4]);


							break;
							default:
								$ADMIN->admin_data_update($path[3]);
				
						}
						$smarty->display("admin_data_update.tpl");
					break;
					case 'delete':
						$ADMIN->admin_data_update($path[3]);
						$smarty->display("admin_data_update.tpl");
						
					break;


					default:
				}
			break;

			case 'content':
				switch ($path[2]) {
					case 'update':
						$ADMIN->admin_content_update($path[3],$path[4]);
						$smarty->display("admin_content_update.tpl");
						
					break;

					default:
				}
			break;

			case 'user':
				switch ($path[2]) {
					case 'profile':
						$ADMIN->user_profile($path[3]);
					break;
					case 'account':
						$ADMIN->user_account($path[3]);
					break;

					default:
				}
			break;

			case 'messages':

				$to = false;
				$subject = false;
				$thread = false;
				$group = false;
				$message_id = false;

				if ($path[2] and !is_numeric($path[2])) {

					switch ($path[2]) {

						case 'getmod':

							$MESSAGE->getmod();
							$ajax = true;
							exit;

						break;

						case 'actionmod':

							$MESSAGE->actionmod();
							$ajax = true;
							exit;

						break;

						case 'mark':

							$MESSAGE->mark();
							$ajax = true;
							exit;

						break;

						case 'send':

							$MESSAGE->send();
							$ajax = true;
							exit;

						break;

						case 'delete':

							$MESSAGE->delete();
							$ajax = true;
							exit;

						break;

						case 'data':

							$MESSAGE->recipients();
							$ajax = true;
							exit;

						break;

						case 'sendto':

							$MESSAGE->inbox($path[2]);
							$to = $path[3];

						break;

						case 'sendtogroup':

							$group = $path[3];
							$MESSAGE->inbox($path[2]);	
							$to = $GROUP->groupdata($path[3])->group_name;

						break;
						
						case 'm':
							
							$MESSAGE->inbox();
							$smarty->assign("message_id",$path[3]);
							
						break;

						default:

							$MESSAGE->message();
	
					}

				} else {

					$MESSAGE->inbox($path[2]);

				}

				$smarty->assign("to",$to);
				$smarty->assign("subject",$subject);
				$smarty->assign("thread",$thread);
				$smarty->assign("group",$group);
				$smarty->display('admin_messages.tpl');			
			break;



			default:		
		}
	break;
	
	default:

	//PAGE
	$path[0] = (!empty($path[0])) ? $path[0] : 'home';
	if ($path[0] == 'home') {
		$path[0] = ($is_mobile) ? 'mobile_' . $path[0] : $path[0];
	}
	$CONTENT->page($path[0]);
	
}

if ($ajax) exit;

?>
