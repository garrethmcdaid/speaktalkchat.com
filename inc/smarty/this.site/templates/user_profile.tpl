{include file="header.tpl"}

{if ($required)}

<div style="height:14px;"></div>

<span class="highlight"><b>{$text.$selected_language.messages.profile_data_required->text}</b></span>

<div style="height:10px;"></div>

{/if}

<div style="height:18px;">
	<div class="highlight2 process_message">
	{if isset($process_message)}
		{$process_message}
	{/if}
	</div>
</div>

<div class="thickgreybar"></div>
{include file="user_profile_form.tpl"}

<div style="height:10px;"></div>

<span style="font-size:0.9em;"><a class="highlight clickable_nd" href="/user/account">{$text.$selected_language.links.link_to_account_from_profile->text}</a></span>

<div class="hidden_block">
{foreach item=v key=k from=$text.$selected_language.messages}
	<input type="hidden" id="{$v->text_tag}" value="{$v->text}">
{/foreach}
</div>

{if ($required)}
	{include file="user_conversion_tracker.tpl"}
{/if}

{include file="footer.tpl"}
