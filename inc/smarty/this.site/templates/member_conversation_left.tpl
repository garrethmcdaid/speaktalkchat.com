{if $is_mobile}
<div style="height:14px;"></div>
{/if}

<div id="member_conversation_left">

	<h3>{$user->nickname} {if ($user->loggedin)}<img src="/images/greenlight.png">{else}<img src="/images/redlight.png">{/if} {if $user->skype}<img src="/images/skype.png">{/if} <a href="/user/messages/sendto/{$user->nickname}"> <img src="/images/envelope.jpg"></a>
	{if $path.2 == 'result'}
		| <a href="/user/search/back" class="highlight clickable_nd" style="font-size:0.8em;">{$text.$selected_language.links.back_to_search_results->text}</a>
	{/if}
	</h3>
	
	{if $user->rating.on > 0}
	
		<div id="member_rating">
	
			<div>
				<div id="member_rating_stars">
				{for $v=1 to 10}
		
					{if ($v == $user->rating.user*2)}
						<input name="r1" type="radio" class="star {literal}{split:2}{/literal}" disabled="disabled" checked="checked" value="{$v}" />
					{else}
						<input name="r1" type="radio" class="star {literal}{split:2}{/literal}" disabled="disabled" value="{$v}" />
					{/if}
			
				{/for}
				</div>		
				<div id="member_rating_no">
				({$user->rating.count})
				</div>
			<br style="clear:both;">
			</div>

			<br style="clear:both;">
			<div style="height:10px;"></div>

		</div>

			
	{else}
	
			{$text.$selected_language.messages.ratings_not_allowed->text}
			<div style="height:10px;"></div>
	
	{/if}
	
	<div id="member_stats">
	
		<div id="member_stats_avatar">
			{if ($user->avatar > 0)}
				<img src="/content/{$user->id}/avatar/avatar.jpg">
			{else}
				<img src="/content/default/avatar/avatar.jpg">
			{/if} 
		</div>
		<div id="member_stats_data">
	
			{if isset($user->statistics)}
	
				<b>{$text.$selected_language.headings.user_conversations->text}</b><br> <span class="highlight">{$user->statistics.data.total_conversations}</span> ({$user->statistics.data.total_conversations_28} {$text.$selected_language.headings.last_28_days->text})
				<br>
				<b>{$text.$selected_language.headings.chattime_heading->text}</b><br> <span class="highlight">{$smartyutil->sec_convert($user->statistics.data.total_duration)}</span> ({$smartyutil->sec_convert($user->statistics.data.total_duration_28)} {$text.$selected_language.headings.last_28_days->text})
	
			{/if}
	
		</div>
		
		<div style="height:10px;clear:both;"></div>
	
	</div>

	

	
	<input type="button" class="directory_add" id="{$user->id}" value="{$text.$selected_language.forms.add_to_directory_prompt->text}" {if ($user->in_directory)}style="display:none;"{/if}>

	<input type="button" class="directory_del" id="{$user->id}" value="{$text.$selected_language.forms.del_from_directory_prompt->text}" {if (!$user->in_directory)}style="display:none;"{/if}>

	<div style="height:10px;"></div>

	<b>{$user->languages.field_label}</b>

	<div style="height:10px;"></div>

	{foreach item=v key=k from=$user->languages.data}

		{$text.$selected_language.languages.{$v->language_name|strtolower}->text} ({$text.$selected_language.user_data_op.{{$v->level_en}|strtolower}->text}) <br>

	{/foreach}

	{if !$is_mobile}

		<div style="height:10px;"></div>
	
		<b>{$user->groups.field_label}</b>
	
		<div style="height:10px;"></div>
	
		{foreach item=v key=k from=$user->groups.data}
	
			<a href="/user/groups/{$v->id}">{$v->group_name}</a> ({$v->user_status})<br>
	
		{/foreach}
	
		<div style="height:6px;"></div>
	
		<span id="invite_link" class="reveal invite_form fadeIn clickable">{$text.$selected_language.links.invite_user->text}</span>
	
		<div style="height:6px;"></div>
	
		<div id="invite_form" style="display:none;">
			<form id="invite_user_form_fields">
			<div class="ui-widget">
			{$text.$selected_language.forms.group_name_prompt->text}:<br>
			<input type="text" name="groupname" id="groupname" class="required"><br>
			<input user_id="{$user->id}" group="" type="button" id="invite" class="user_group" action="invite" value="{$text.$selected_language.forms.invite_prompt->text}"> 		
			<span id="group_message_0"></span>
			</div>
			</form>
		</div>
	
	{/if}

	<div style="height:10px;"></div>

	<div id="availability_section">

	<b>{$user->availability.field_label}</b>

	<div style="height:10px;"></div>

	{if ($user->availability.data->always > 0)}
		{$text.$selected_language.messages.availability_always->text}<br>
		{$user->availability.data->zone} (GMT{if ($user->availability.data->offset) != 0} {$user->availability.data->offset}{/if})
	{else}
		{$user->availability.data->zone} (GMT{if ($user->availability.data->offset) != 0} {$user->availability.data->offset}{/if})<br>
		<span id="start1">{$user->availability.data->start1|date_format:"%H:%M"}</span> - <span id="end1">{$user->availability.data->end1|date_format:"%H:%M"}</span><br>
		{if ($user->availability.data->start2 != '00:00:00' or $user->availability.data->end2 != '00:00:00')} 
			<span id="start2">{$user->availability.data->start2|date_format:"%H:%M"}</span> - <span id="end2">{$user->availability.data->end2|date_format:"%H:%M"}</span><br>
		{/if}
	{/if}

	</div>

	<div style="height:14px;"></div>

	{if isset($user->fields)}

		{foreach item=v key=k from=$user->fields}

			<b>{$text.$selected_language.user_data.{$v->tag}->text}</b><br>
			{if $v->data_type == 'vc'}
				{$v->value_vc|stripslashes}<br>
			{else}
				{if ($v->data_type == 'opts')}
					{if !empty($v->value_opts)}
						{foreach item=q key=p from=$v->value_opts name=s}
							{$q}
							{if !$smarty.foreach.s.last} | {/if}
						{/foreach}
					{/if}
					<br>
				{else if ($v->data_type == 'opt')}
					{$v->value_opt|ucfirst}<br>
				{else}
					{$v->options.{$user.{$v->id}->value_opt}|ucfirst}<br>
				{/if}
			{/if}
	
		{/foreach}

	{/if}

</div>
