{include file="admin_header.tpl"}

<div id='admin_container'>

	<b>Conversation Details</b>: {$c->code}<br>
	<div style='height:20px;'></div>

	Proposing user: <a href="/admin/user/profile/{$c->user1_nickname}">{$c->user1_nickname}</a> (ID = {$c->user1})<br>
	Proposing user browser: {$c->user1_agent}
	<div class="greybar"></div>
	Accepting user: <a href="/admin/user/profile/{$c->user2_nickname}">{$c->user2_nickname}</a> (ID = {$c->user2})<br>
	Accepting user browser: {$c->user2_agent}<br>
	<div class="greybar"></div>
	GMT Appointment time: {$c->start}<br>
	GMT Conversation started at: {$c->started}<br>
	Duration: {$smartyutil->sec_convert($c->duration)}<br>

</div>


{include file="admin_footer.tpl"}
