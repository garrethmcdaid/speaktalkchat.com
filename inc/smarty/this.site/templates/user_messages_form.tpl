{$text.$selected_language.forms.recipient_prompt->text}<br>

<form id="message_form_fields">

<div class="ui-widget">
<input type="text" name="recipient" id="recipient" class="required" style="width:200px;" value="{if ($to)}{$to}{/if}">
</div>
{if ($group)}{$text.$selected_language.messages.message_to_info->text}{/if}

<div style="height:14px;"></div>

{$text.$selected_language.forms.subject_prompt->text}<br>
<input type="text" name="subject" id="subject" class="required" style="width:200px;" value="{if ($subject)}{$subject}{/if}">

<div style="height:14px;"></div>

{$text.$selected_language.forms.message_prompt->text}<br>
<textarea name="the_message" class="required" id="the_message"></textarea>

<input type="hidden" id="thread" name="thread" value="{if ($thread)}{$thread}{else}0{/if}">

<input type="hidden" id="group" name="group" value="{if ($group)}{$group}{else}0{/if}">

</form>

<div style="height:14px;"></div>

<input type="button" id="send_message" name="send_message" value="{$text.$selected_language.forms.send_prompt->text}">

<input type="button" class="reveal message_form slideDown" id="cancel_message" value="{$text.$selected_language.forms.cancel_prompt->text}"> <span id="transaction_update"></span>


