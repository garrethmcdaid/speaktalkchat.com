{include file="header.tpl"}

{if !$is_mobile}

	<h3>{$text.$selected_language.headings.user_dashboard->text}</h3>
	
	<div style="float:left;width:290px;height:auto;padding-right:3%;">
	
	<b>{$text.$selected_language.headings.user_profile->text}</b> {$me->profile->nickname}
	
	<br style="clear:both;">
	
	{if $me->profile->rating.on > 0}
	
		{for $v=1 to 10}
	
			{if ($v == $me->profile->rating.user*2)}
				<input name="r1" type="radio" class="star {literal}{split:2}{/literal}" disabled="disabled" checked="checked" value="{$v}" />
			{else}
				<input name="r1" type="radio" class="star {literal}{split:2}{/literal}" disabled="disabled" value="{$v}" />
			{/if}
	
		{/for}
		&nbsp;({$me->profile->rating.count})
	
	{else}
	
			{$text.$selected_language.messages.ratings_not_visible->text}
	
	{/if}
	
	<div style="height:4px;"></div>
	
	<span style="font-size:0.9em;"><a class="highlight"  href="/user/profile">{$text.$selected_language.links.edit_profile->text}</a> | <a class="highlight"  href="/user/account">{$text.$selected_language.links.edit_account->text}</a> | <a class="highlight"  href="/user/conversations">{$text.$selected_language.headings.user_conversations->text}</a></span>
	
	<div style="height:10px;"></div>
	
	<div style="float:left;width:auto;height:auto;">
		{if ($avatar_exists)}
			<img src="/content/{$me->id}/avatar/avatar.jpg?cache={$spanner}"><br>
		{else}
			<img src="/content/default/avatar/avatar.jpg?cache={$spanner}"><br>
		{/if}
	</div>
	<div style="float:left;width:156px;margin-left:8px;height:auto;font-size:0.8em;">
		{if isset($me->profile->statistics)}
	
			<b>{$text.$selected_language.headings.user_conversations->text}</b><br> <span class="highlight">{$me->profile->statistics.data.total_conversations}</span> ({$me->profile->statistics.data.total_conversations_28} {$text.$selected_language.headings.last_28_days->text})
			<br>
			<b>{$text.$selected_language.headings.chattime_heading->text}</b><br> <span class="highlight">{$smartyutil->sec_convert($me->profile->statistics.data.total_duration)}</span> ({$smartyutil->sec_convert($me->profile->statistics.data.total_duration_28)} {$text.$selected_language.headings.last_28_days->text})
	
		{/if}
	</div>
	
	<div style="height:14px;clear:both;"></div>
	
	<b>{$text.$selected_language.headings.languages_heading->text}</b>
	<div style="height:10px;"></div>

	
	{if isset($selected_languages)}
	
		{foreach item=v key=k from=$selected_languages}
	
			{$text.$selected_language.languages.{$profile_languages.$v|strtolower}->text} ({$text.$selected_language.user_data_op.{{$language_levels.$v}|strtolower}->text}) <br>
		
		{/foreach}
	
	
	{/if}
	
	
	<div style="height:10px;"></div>
	
	<b>{$text.$selected_language.headings.availability_heading->text}</b>
	
	<div style="height:10px;"></div>
	
	<input type="hidden" id="myoffset" value="{$me->profile->availability.data->offset}">
	<input type="hidden" id="myautodetect" value="{$me->profile->availability.data->autodetect}">
	
	{if ($me->profile->availability.data->always > 0)}
		{$text.$selected_language.messages.availability_always->text}
		<div style="height:8px;"></div>
		{$text.$selected_language.messages.availability_zone->text}:<br> {$me->profile->availability.data->zone} (GMT{if ($me->profile->availability.data->offset) != 0} {$me->profile->availability.data->offset}{/if})
	{else}
		
		<span id="start1">{$me->profile->availability.data->start1|date_format:"%H:%M"}</span> - <span id="end1">{$me->profile->availability.data->end1|date_format:"%H:%M"}</span><br>
		{if ($me->profile->availability.data->start2 != '00:00:00' or $me->profile->availability.data->end2 != '00:00:00')} 
			<span id="start2">{$me->profile->availability.data->start2|date_format:"%H:%M"}</span> - <span id="end2">{$me->profile->availability.data->end2|date_format:"%H:%M"}</span><br>
		{/if}
		<div style="height:8px;"></div>
		{$text.$selected_language.messages.availability_zone->text}:<br> {$me->profile->availability.data->zone} (GMT{if ($me->profile->availability.data->offset) != 0} {$me->profile->availability.data->offset}{/if})
	{/if}
	
	{if ($me->profile->availability.data->autodetect < 1)}
		<div style="height:14px;"></div>
				
		<img src="/images/yellowwarn.png"><span style="font-size:0.9em;"> {$text.$selected_language.messages.tz_auto_warning->text}</span>
		
		<div style="height:8px;"></div>
		
		<span style="font-size:0.9em;" class="highlight">{$text.$selected_language.words.autodetected->text|ucfirst}: </span><span style="font-size:0.9em;" id="browser_timezone"></span><br>
		
		<a style="font-size:0.9em;" class="highlight clickable_nd" href="/user/profile">{$text.$selected_language.links.change_tz_settings->text}</a>
		
	{/if}
	
	<div style="height:10px;"></div>
	
	{include file="user_tzupdate.tpl"}

	{include file="user_tzcalc.tpl"}
		
	<div style="height:14px;"></div>
	<div id="ad_dashboard" style="width:360px;height:60px;">{$smartyutil->ad('Profile - User Dashboard')}</div>
	<div style="height:10px;"></div>
	
	</div>
	<div style="float:left;width:280px;height:auto;padding-right:3%;border-right:1px ">
	
	<b>{$text.$selected_language.headings.suggested_partners->text}</b>
	
	<div style="height:14px;"></div>
	
	<div id="user_search_results"></div>
	<img id="ajax-loader" src="/images/ajax-loader.gif">
	<div id="user_search_result_detail"></div>
	
	<div style="height:14px;"></div>
	
	<input type="button" id="user_suggest" name="user_suggest" value="{$text.$selected_language.forms.refresh_prompt->text}">
	
	<script>js_userSuggest();</script>
	
	</div>
	
	<div style="float:left;width:308px;height:auto;padding-right:3%;">

{/if}

<b>{$text.$selected_language.headings.conversations->text}</b>

<div style="height:14px;"></div>

{$no_results = true}

{if (!empty($conversations))}

	{foreach item=v key=k from=$conversations}

		{if !$v->obsolete}

			{$no_results = false}

			<div id="{$v->code}"{if ($v->status == 'declined' || $v->status == 'cancelled')}class="inactive"{/if}>
			
				<div class="conversation_list_data">
					<div class="conversation_list_data_inner">
						<div class="conversation {$v->css_status}" style="float:left;padding-left:4px;">
						<span class="enhance">
						<a href="/member/{$v->with_name}/c/{$v->code}">{$v->with_name} | {$v->status}</a>
						</span>
						<br>
						<span class="dehance">
						{$v->start|date_format:"%H:%M %a, %b %e, %Y"}
						</span>
						</div>
						<br>
					</div>
				</div>
					 
				<div class="conversation_list_avatar small_avatar">
				{if ($v->avatar_exists)}
					<img src="/content/{$v->with_id}/avatar/avatar.jpg?cache={$spanner}"><br>
				{else}
					<img src="/content/default/avatar/avatar.jpg?cache={$spanner}"><br>
				{/if}
				
				</div>
				
				
			
				<div class="conversation_list_status {if $is_mobile}{$v->css_status}{/if}">
				{if ($v->new > 0)}
					{$text.$selected_language.words.new->text}<br>
				{elseif ($v->updated_by != $me->id)}
					{$text.$selected_language.words.updated->text}<br>
				{/if}
			
				{if ($v->delay == 5)}
					<img src="/images/redwarn.png">
				{elseif ($v->delay == 30)}
					<img src="/images/yellowwarn.png">
				{/if}
				</div>
								
				<br style="clear:both;">
				
				
			</div>
			<div style="margin-bottom:2px;"></div>

		{/if}

	{/foreach}

{/if}

{if !$no_results}

		<label><input type="checkbox" id="active_only" class="margin-left: 0px;"><span style="font-size:0.9em;">{$text.$selected_language.links.show_active_only->text}</span></label><br>

{else}

	{$text.$selected_language.messages.no_appointments_found->text}

{/if}

{if !$is_mobile}
	</div>
{/if}

<br style="clear:both;">
<div style="height:14px;"></div>

<div style="width:auto;height:auto;padding-right:3%;">

{if isset($blogfeed)}

	<b>{$text.$selected_language.headings.latest_news->text}</b>

	<div style="height:14px;"></div>


	{foreach item=v key=k from=$blogfeed->items}
		
		<a href="{$v.link}" target="_blank">{$v.title}</a><br>
		{$v.summary}
		<div style="height:14px;"></div>

	{/foreach}


{/if}

</div>
<br style="clear:left;">

<div class="hidden_block">
{foreach item=v key=k from=$text.$selected_language.links}
	<input type="hidden" id="{$v->text_tag}" value="{$v->text}">
{/foreach}
</div>

<div class="hidden_block">
{foreach item=v key=k from=$text.$selected_language.messages}
	<input type="hidden" id="{$v->text_tag}" value="{$v->text}">
{/foreach}
</div>

{include file="footer.tpl"}


