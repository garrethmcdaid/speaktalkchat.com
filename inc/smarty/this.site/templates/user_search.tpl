{include file="header.tpl"}

<h3>{$text.$selected_language.headings.user_search->text}</h3>

<div style="height:20px;"></div>

{include file="user_search_form.tpl"}

<div style="height:10px;"></div>

<div id="ajax_submit_error" class="highlight">{$text.$selected_language.messages.user_search_incomplete->text}</div>

{if isset($process_message)}
	{$process_message}
{/if}

<div style="height:14px;"></div>

<div id="filters" style="display:none;">
<input type="checkbox" filter="skype_only" class="search_result_filter">
{$text.$selected_language.forms.skype_only_prompt->text}
<div style="height:14px;"></div>
</div>

<div class="user_search_results_pagination_t"></div>
<div id="user_search_results"></div>
<div id="user_search_result_detail"></div>
<div class="user_search_results_pagination_b"></div>

<div class="hidden_block">
{foreach item=v key=k from=$text.$selected_language.links}
	<input type="hidden" id="{$v->text_tag}" value="{$v->text}">
{/foreach}
</div>

<div class="hidden_block">
{foreach item=v key=k from=$text.$selected_language.messages}
	<input type="hidden" id="{$v->text_tag}" value="{$v->text}">
{/foreach}
</div>

<input type="hidden" id="current_page" value="1">

{if $path.2 == 'back'}

	<script>js_backToResults({$last_search});</script>
{/if}

{include file="footer.tpl"}
