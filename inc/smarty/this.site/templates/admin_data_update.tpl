{include file="admin_header.tpl"}

<DIV ID='admin_container'>

	<b>Data management</b><br>
	<div class="greybar"></div>
	<div style='height:20px;'></div>

	<div style="float:left;width:380px;height:auto;margin-right:36px;">

	{if !$field}

		{if isset($process_message)}
			{$process_message}
		{/if}

		{foreach item=tag key=id from=$fields}
			{if $tag->sys == 0}
			<a href="/admin/data/update/{$tag->tag}">{$text.$selected_language.user_data.{$tag->tag}->text}</a> <span id="{$tag->tag}" class="delete_data clickable">click here to delete</span> <span id="delete_{$tag->tag}" style="display:none;"><a href="/admin/data/update/delete/{$tag->tag}">click here to confirm deletion</a> <span id="cancel_{$tag->tag}" class="cancel_delete clickable">cancel</span> </span><br>
			{/if}
		{/foreach}
		<div style='height:20px;'></div>
		Please note that deleting a data option will affect all user accounts

	{else}

		<a href="/admin/data/update">Back</a>		

	{/if}

	<DIV STYLE='height:20px;'></DIV>

	</div>
	<div style="float:left;width:380px;height:auto;margin-right:36px;">

	{include file="admin_data_update_form.tpl"}

	<div style='height:20px;'></div>

	</div>


</DIV>

{include file="admin_footer.tpl"}
