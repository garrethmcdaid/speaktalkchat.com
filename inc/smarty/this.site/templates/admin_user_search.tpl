{include file="admin_header.tpl"}

<DIV ID='admin_container'>

	<b>Data management</b><br>
	<div class="greybar"></div>
	<div style='height:20px;'></div>


	{include file="admin_user_search_form.tpl"}

	<div id="ajax_submit_error">{$text.$selected_language.messages.user_search_incomplete->text}</div>

	{if isset($process_message)}
		{$process_message}
	{/if}

	<div style="height:14px;"></div>
	<div class="user_search_results_pagination_t"></div>
	<div id="user_search_results"></div>
	<div id="user_search_result_detail"></div>
	<div class="user_search_results_pagination_b"></div>

	<div class="hidden_block">
	{foreach item=v key=k from=$text.$selected_language.links}
		<input type="hidden" id="{$v->text_tag}" value="{$v->text}">
	{/foreach}
	</div>
	
	<input type="hidden" id="current_page" value="1">


</div>

<div class="hidden_block">
{foreach item=v key=k from=$text.$selected_language.messages}
	<input type="hidden" id="{$v->text_tag}" value="{$v->text}">
{/foreach}
</div>

{include file="admin_footer.tpl"}
