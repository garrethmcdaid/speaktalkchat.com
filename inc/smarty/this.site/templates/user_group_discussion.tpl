<div class="dir_header"><a href="/user/groups/" style="color:#ffffff;">{$text.$selected_language.links.all_groups->text}</a> - <a href="/user/groups/{$group}" style="color:#ffffff;">{$groupdata->group_name}</a> - <a href="/user/groups/forum/{$group}" style="color:#ffffff;">{$text.$selected_language.links.forum->text}</a> - <span id="subject"></span></div>
<div style="height:8px;"></div>

<div id="post_list_pagination_t" style="text-align:right;"></div>
<div style="height:8px;"></div>
<div id="post_list"></div>

<div class="discussion_summary" id="new_reply" style="display:none;">

	<div class="post_header">
		<div id="poster_post_new" style="width:auto;float:left;"><b>{$text.$selected_language.headings.reply_to_discussion->text}</b></div>
		<div style="margin-left:100px;text-align:right;"></div><br>
	</div>

	<textarea name="post_reply_text" id="post_reply_text_new" style="width:99%;padding:0.5%;height:120px;" class="post_reply_text"></textarea>
	
	<div style="text-align:right;">
		<input type="button" class="submit_post" post="new" value="{$text.$selected_language.forms.post_prompt->text}">
	</div>

</div>

<div class="discussion_summary" id="notification_panel" style="display:none;">

	<div class="post_header" style="margin-bottom:0px;">
		<div style="width:auto;text-align:right;">
			<label><b>{$text.$selected_language.forms.notify_me_prompt->text}</b></label>
			<input type="checkbox" class="notify_new_post" id="notify_setting">
		</div>
	</div>

</div>


<div style="height:8px;"></div>
<div id="post_list_pagination_b" style="text-align:right;"></div>

<input type="hidden" id="discussion_id" value="{$discussion}">
<script>js_getPosts({$discussion},1,{$thread_no});</script>

