{include file="header.tpl"}

{if (!$me && $path.0 == 'login') || $path.0 == 'invalidlogin'}

	<form name="user_register_form" id="user_register_form" method="post" class="validation_required" action="/login">
	
	<div style="height:46px;"></div>
	
	<div id="facebook_mobile_login" class="highlight clickable_nd" scope="email,birthday" style="height:22px;width:auto;margin:auto;font-size:1.2em;"><img src="/images/facebook_32.png" align="absmiddle" style="margin-right:8px;">{$text.$selected_language.forms.login_with_facebook->text}</div><br>
	
	<div style="height:14px;"></div>
	
	<div style="height:70px;">
	
		<div>
		{$text.$selected_language.forms.email_prompt->text}<br>
		<input type="text" name="me[email]" id="email_address" class="required email" style="width:200px;margin-bottom:6px;"> 
		</div>
	
	</div>
	
	<div style="height:70px;">
	
		<div>
		{$text.$selected_language.forms.password_prompt->text}<br>
		<input type="password" name="me[password]" id="password" class="required" style="width:200px;margin-bottom:6px;"> 
		</div>
	
	</div>
	
	</form>
	
	<div style="height:10px;"></div>
	
	<div style="font-size:1.4em;">
	<span id="submit_login" class="clickable_nd highlight">{$text.$selected_language.forms.login_prompt->text}</span>						
	</div>
			
	<div style="height:20px;"></div>
	
	<div>
	<a href="/user/password">{$text.$selected_language.forms.forgot_password->text}</a>
	</div>
	
	<div style="height:10px;"></div>
	
	<div style="height:0px;clear:both;font-size:0.9em;" class="highlight">
		{if isset($process_message)}
			{$process_message}
		{/if}
	</div>	
	
	<div style="height:10px;"></div>
	
	<input type="hidden" name="logged_in" id="logged_in" value="0">
	
	<div class="hidden_block">
	{foreach item=v key=k from=$text.$selected_language.messages}
		<input type="hidden" id="{$v->text_tag}" value="{$v->text}">
	{/foreach}
	</div>
	
	

{/if}

{include file="footer.tpl"}

