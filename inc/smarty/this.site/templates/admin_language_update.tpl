{include file="admin_header.tpl"}

<div id='admin_container'>

<b>Language management</b><br>
<div class="greybar"></div>
<div style='height:20px;'></div>

<b>Site enabled languages</b><br>
<div style='height:10px;'></div>

<form id='admin_language_update' name='admin_language_update' method='post' class="validation_required">

{foreach item=v key=k from=$all_language}

	<input type="checkbox" name="enabled_languages[]" id="{$v->language_suffix}" value="{$v->language_id}" {if ($v->language_active gt 0)}checked{/if}>{$v->language_name	}

{/foreach}

<div style='height:20px;'></div>

<input type='submit' name='language_update' value='Update'>

</form>

<div style='height:20px;'></div>

<b>Profile enabled languages</b><br>
<div style='height:10px;'></div>

<form id='admin_profile_language_update' name='admin_profile_language_update' method='post' class="validation_required">

{foreach item=v key=k from=$all_language}

	<input type="checkbox" name="profile_languages[]" id="profile_{$v->language_suffix}" value="{$v->language_id}" {if ($v->language_profile gt 0)}checked{/if}>{$v->language_name	}

{/foreach}

<div style='height:20px;'></div>

<input type='submit' name='profile_language_update' value='Update'>

</form>

<div style='height:20px;'></div>


<b>Add a language</b><br>
<div style='height:10px;'></div>

<form id='admin_language_add' name='admin_language_add' method='post' class="validation_required">

Language name:<br>
<input type="text" name="language_name" id="language_name"><br>

Language suffix (max. 3 characters):<br>
<input type="text" name="language_suffix" id="language_suffix"><br>

<div style='height:20px;'></div>

<input type='submit' name='language_add' value='Add'>

</form>

<div style='height:20px;'></div>

{$message}

</div>

{include file="admin_footer.tpl"}
