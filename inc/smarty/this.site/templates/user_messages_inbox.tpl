{if $is_mobile}
	<div class="greybar"></div>
{/if}

<b style="font-size:1.4em;">{$text.$selected_language.headings.inbox_heading->text}</b>

<span style="display:none;" class="clickable_nd" message="" id="inbox_back"> | <span class="highlight" style="font-size:1.6em;"><<<</span></span>

{if $is_mobile}
	<div class="greybar"></div>
{else}
	<div style="height:7px;"></div>
{/if}
	
{if ($pagination)}
	<span {if $is_mobile}style="font-size:1.2em;"{/if}>{$pagination}</span>
{/if}

<div style="height:7px;"></div>

<table border="0" cellpadding="0" cellspacing="0" class="inbox">

	{foreach item=v key=k from=$inbox}
	<tr id="{$v->id}" class="clickable_nd inbox_row" {if ($v->status == "new")}style="font-weight:bold;"{/if}>
		<td style="white-space:nowrap;">
		<span style="display:none;" id="sent_date_{$v->id}">{$v->sent|date_format:"%H:%M, %b %e, %Y"}</span>
		{if ($v->today)}
			{$v->sent|date_format:"%H:%M"}
		{else}
			{if $is_mobile}
				{$v->sent|date_format:"%d/%m"}
			{else}
				{$v->sent|date_format:"%b %e, %Y"}
			{/if}
		{/if}
		</td>
		<td>
		<span class="enhance highlight" id="subject_{$v->id}">{$v->subject}</span>
		</td>
		<td>
		<span id="sender_{$v->id}">{$v->sender_name}</span>
		</td>
		{if !$is_mobile}
			<td style="width:200px;">
			<span id="x_{$v->id}" class="reveal delete_message_x_{$v->id} fadeIn delete_something clickable_nd">X</span>
			<span message="{$v->id}" class="confirm_delete_message delete_something clickable_nd" id="delete_message_x_{$v->id}" style="display:none;">{$text.$selected_language.forms.confirm_prompt->text}</span>
			<input type="hidden" id="x_{$v->id}_thread" value="{$v->thread}">
			</td>
		{/if}
	</tr>
	{/foreach}
</table>

<table border="0" cellpadding="0" cellspacing="0" class="inbox">
{foreach item=v key=k from=$inbox}

	<tr class="inbox_message enhance" id="message_{$v->id}">
		<td>
		{$v->sent|date_format:"%H:%M, %b %e, %Y"} | <span message="{$v->id}" class="inbox_reply clickable">{$text.$selected_language.forms.reply_prompt->text}</span> | <a  href="/member/{$v->sender_name}">{$text.$selected_language.links.connect->text}</a><br><br>
		<div style="height:260px;overflow:auto;border-top:1px solid #e0e0e0;border-bottom:1px solid #e0e0e0;padding-top:10px;">
		<span id="message_content_{$v->id}">{$v->message|nl2br}</span>
		</div>
		{if $is_mobile}
			<div style="font-size:1.7em;">
			<span id="x_{$v->id}" class="reveal delete_message_x_{$v->id} fadeIn delete_something clickable_nd">X</span>
			<span message="{$v->id}" class="confirm_delete_message delete_something clickable_nd" id="delete_message_x_{$v->id}" style="display:none;">{$text.$selected_language.forms.confirm_prompt->text}</span>
			</div>
		{/if}	
		</td>
	</tr>


{/foreach}
</table>

