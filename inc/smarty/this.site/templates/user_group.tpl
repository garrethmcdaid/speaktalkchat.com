{include file="header.tpl"}

<h3>{$text.$selected_language.headings.user_groups->text}</h3>

<input type="hidden" id="group" value="{$group}">

<input type="hidden" id="edited" value="{$text.$selected_language.messages.edited_time->text|lcfirst}">
<input type="hidden" id="closed" value="{$text.$selected_language.messages.closed->text|lcfirst}">

<input type="hidden" id="open" value="{$text.$selected_language.forms.open_prompt->text|lcfirst}">
<input type="hidden" id="close" value="{$text.$selected_language.forms.close_prompt->text|lcfirst}">
<input type="hidden" id="confirm" value="{$text.$selected_language.forms.confirm_prompt->text|lcfirst}">
<input type="hidden" id="edit" value="{$text.$selected_language.forms.edit_prompt->text|lcfirst}">
<input type="hidden" id="flag" value="{$text.$selected_language.forms.flag_prompt->text|lcfirst}">
<input type="hidden" id="reply" value="{$text.$selected_language.forms.reply_prompt->text|lcfirst}">
<input type="hidden" id="cancel" value="{$text.$selected_language.forms.cancel_prompt->text|lcfirst}">
<input type="hidden" id="delete" value="{$text.$selected_language.forms.delete_prompt->text|lcfirst}">
<input type="hidden" id="post" value="{$text.$selected_language.forms.post_prompt->text|lcfirst}">

{if $path[4] == 'discussion'}
	{include file="user_group_discussion.tpl"}
{elseif $path[2] == 'forum'}
	{include file="user_group_forum.tpl"}
{elseif $group}
	{include file="user_group_members.tpl"}
{else}
	{include file="user_group_intro.tpl"}
{/if}

<input type="hidden" id="join" value="{$text.$selected_language.forms.join_prompt->text}">
<input type="hidden" id="leave" value="{$text.$selected_language.forms.leave_prompt->text}">
<input type="hidden" id="delete" value="{$text.$selected_language.forms.delete_prompt->text}">
<input type="hidden" id="group" value="{$text.$selected_language.words.group->text}">

<div class="hidden_block">
{foreach item=v key=k from=$text.$selected_language.messages}
	<input type="hidden" id="{$v->text_tag}" value="{$v->text}">
{/foreach}
</div>

{if isset($process_message)}
	{$process_message}
{/if}

<br>

{include file="footer.tpl"}
