{include file="header.tpl"}

<h3>{$text.$selected_language.headings.user_account->text}</h3>

<div style="height:18px;">
	<div class="highlight process_message">
	{if isset($process_message)}
		{$process_message}
	{/if}
	</div>
</div>

<div style="height:6px;"></div>

{include file="user_account_form.tpl"}

{include file="footer.tpl"}
