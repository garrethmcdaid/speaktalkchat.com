{include file="admin_header.tpl"}

<h3>{$text.$selected_language.headings.user_profile->text}: {$user.nickname}</h3>

<div class="thickgreybar"></div>

<b>Account details:</b><br>
Email: {$user.account->email}<br>
Joined: {$user.account->joined}<br>
Rating: {$user.account->rating}<br>
Rating on: {$user.account->rating_on}<br>
Default language: {$user.account->language}<br>

<div style="height:14px;"></div>

{if isset($user.statistics)}

	<b>{$text.$selected_language.headings.user_conversations->text}</b><br> <span class="highlight">{$user.statistics.data.total_conversations}</span> ({$user.statistics.data.total_conversations_28} {$text.$selected_language.headings.last_28_days->text})
	<br>
	<b>{$text.$selected_language.headings.chattime_heading->text}</b><br> <span class="highlight">{$smartyutil->sec_convert($user.statistics.data.total_duration)}</span> ({$smartyutil->sec_convert($user.statistics.data.total_duration_28)} {$text.$selected_language.headings.last_28_days->text})

{/if}

<div style="height:14px;"></div>

<b><span class="reveal feedback_list slideDown clickable" id="feedback_link">Click here for Feedback/Reports</span></b><br>

<div id="feedback_list" style="display:none;">
{foreach item=v key=k from=$user.feedback}
	<div class="greybar"></div>
	<span class="highlight">{if $v->sender == $user.account->id}Sender{else}Respondant{/if}</span><br>
	{$v->message}<br>	
{/foreach}
</div>

<div style="height:14px;"></div>

<b><a href="/admin/conversations/{$user.account->id}">Click here for Conversation history</a></b><br>

<div style="height:14px;"></div>

{if ($required)}

<div style="height:14px;"></div>

<span class="highlight"><b>{$text.$selected_language.messages.profile_data_required->text}</b></span>

<div style="height:10px;"></div>

<a href="/user/dashboard">{$text.$selected_language.links.proceed_to_dashboard->text}</a>

<div style="height:10px;"></div>

{/if}

{if isset($process_message)}
	<span class="highlight">{$process_message}</span>
{/if}

<div class="thickgreybar"></div>

{include file="user_profile_form.tpl"}

{include file="user_avatar_form.tpl"}

<br>

{include file="admin_footer.tpl"}
