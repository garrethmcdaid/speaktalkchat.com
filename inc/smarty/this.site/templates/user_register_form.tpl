{if ($account === 3)}

	{$text.$selected_language.messages.register_child_message->text}

{/if}

<form name="user_register_form" id="user_register_form" method="post" class="validation_required">

{$text.$selected_language.forms.email_prompt->text}<br>
<input type="text" name="me[email]" id="email_address" class="required email" value="{if $post}{$post.me.email}{/if}"><br>
{$text.$selected_language.forms.password_prompt->text}<br>
<input type="password" name="me[password]" id="password" class="required" value="{if $post}{$post.me.password}{/if}"><br>

{$text.$selected_language.forms.skype_prompt->text}<br>
<div class="flash-suspend">
<span class="highlight" style="font-size:0.8em;">{$text.$selected_language.messages.skype_required_warning->text}</span><br>
</div>
<input type="text" name="me[skype]" id="skype" class="not-required" value="{if $post}{$post.me.skype}{/if}"><br>

{if ($account === 1)}
	{$text.$selected_language.forms.familyname_prompt->text}<br>
	<input type="text" name="me[family_name]" id="familyname" class="required" value="{if $post}{$post.me.family_name}{/if}"><br>
{/if}
{if ($account === 2)}
	{$text.$selected_language.forms.parentemail_prompt->text}<br>
	<input type="text" name="me[parent_email]" id="parentemail" class="required" value="{if $post}{$post.me.parent_email}{/if}"><br>
{/if}
{if ($account === 3)}
	{$text.$selected_language.forms.parentemail_prompt->text}<br>
	<input type="text" name="me[parent_email]" id="parentemail" class="required" value="{if $post}{$post.me.parent_email}{/if}"><br>
	{$text.$selected_language.forms.familycode_prompt->text}<br>
	<input type="password" name="me[family_code]" id="familycode" class="required" value="{if $post}{$post.me.family_code}{/if}"><br>
{/if}

<input type="hidden" name="me[account]" id="account" value="{$account}" />

{$text.$selected_language.forms.captcha_prompt->text}<br>
<img src='/images/captcha/{$captchaimage}'><br>
<input type="text" name="me[captcha]" id="captcha" class="required"><br>
<input type="hidden" name="me[hash]" id="hash" value="{$captchahash}"/>

<div style="height:10px;"></div>

{$text.$selected_language.forms.terms_prompt->text} <a href="/terms_and_conditions" target="_blank">{$text.$selected_language.links.terms_link->text}</a> 
<input type="checkbox" name="me[terms]" value="1" class="required" {if $post.me.terms}checked{/if}><br> 

<div style="height:10px;"></div>

<input type="submit" name="submit" value="{$text.$selected_language.forms.register_prompt->text}">

</form>

{if $path.3 == 'error'}
	<div style="height:12px;"></div>
	<span class="highlight">
	{$text.$selected_language.messages.reregister_error_message->text}
	<div style="height:12px;"></div>
	<a href="/">{$text.$selected_language.forms.login_prompt->text}</a>
	</span>
{/if}

<div class="hidden_block">
{foreach item=v key=k from=$text.$selected_language.messages}
	<input type="hidden" id="{$v->text_tag}" value="{$v->text}">
{/foreach}
</div>
