{include file="header.tpl"}

<h3>{$text.$selected_language.headings.user_directory->text}</h3>

<div class="dir_letter directory_select" id="all" style="width:auto;">{$text.$selected_language.words.all->text}</div>{$smartyutil->directory('dir_letter directory_select')} 

<br style="clear:both;">

<img id="ajax-loader" src="/images/ajax-loader.gif">

<div style="height:14px;"></div>

<div style="height:24px;">
<input type="button" id="directory_refresh" value="{$text.$selected_language.forms.refresh_prompt->text}">
</div>

<div style="height:14px;"></div>

<div id="directory_results"></div>
<div id="directory_no_results">{$text.$selected_language.messages.no_results->text}</div>

<script>js_getDirectory();</script>

{include file="footer.tpl"}


