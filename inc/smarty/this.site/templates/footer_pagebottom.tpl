	<div class="hidden_block">
	{foreach item=v key=k from=$text.$selected_language.notifications}
		<input type="hidden" id="{$v->text_tag}" value="{$v->text}">
	{/foreach}
	</div>

	<div id="notifier">
		<div id="close" class="clear_heartbeat clickable" style="text-align:right;">{if $is_mobile}<span style="font-size:1.2em;">{$text.$selected_language.forms.dismiss_prompt->text}</span>{else}X{/if}</div>
		<div id="notifier_inner"></div>
	</div>

	{if ($me && !$in_conversation)}

		<audio id="ping" style="display: none;"></audio>
		<input type="hidden" id="heartbeat_state" value="{$heartbeat_state}">
		<script>heartbeat()</script>
	{/if}

	</div>

</div>

<div id="pagebottom">
	<div id="intbottom">
		{if ((($me) && $path.0 != '') && !$is_mobile)}
		    <div style="height:16px;clear:both;"></div>
		    <div style="float: right;">
			{include file="social_buttons.tpl"}			
		    </div>
		{/if}

		{if $is_mobile}{include file="mobile_static_menu.tpl"}{/if}
		<div style="height:16px;clear:both;"></div>
		{include file="footer_static_menu.tpl"}
		{if !$is_mobile}
			<div style="height:5px;clear:both;"></div>
			<div style="text-align:right;color:#ffffff;font-size:0.8em;margin-right:0px;">Version: 00015</div>
		{/if}
	</div>
</div>

{if $is_mobile}
	</div>
{/if}


{if !$is_mobile}

	<div style="display:none;"><img src="/images/logo.png"></div>

	<div id="chrome_banner_message" class="banner_message">
		<img src="/images/chrome.jpg" align="left">
		{$text.$selected_language.messages.chrome_warning->text} <span class="dismiss_banner clickable highlight">{$text.$selected_language.forms.dismiss_prompt->text}</span>
	</div>	
	
	{if $is_chrome}
		<script>$("#chrome_banner_message").slideDown('slow');</script>
	{/if}
	

{/if}

<div id="ad_analytics">{$smartyutil->ad('Template - Analytics')}</div>
