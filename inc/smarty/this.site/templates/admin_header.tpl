<!DOCTYPE html> 
<html lang="en"> 
<head> 
<meta charset="utf-8" />
<title>{$text.$selected_language.info.site_title->text}</title>
<meta name="description" content="{$text.$selected_language.info.meta_tag_description->text}"/>

<script src="/inc/js/jquery-1.7.2.min.js?v=00004" language="javascript" type="text/javascript"></script>
<script src="/inc/js/main.js?v=00004" language="javascript" type="text/javascript"></script>
<script src="/inc/js/init.js?v=00004" language="javascript" type="text/javascript"></script>
<script src="/inc/js/admin.js?v=00004" language="javascript" type="text/javascript"></script>

<script type="text/javascript" src="/inc/js/tinymce/jscripts/tiny_mce/jquery.tinymce.js"></script>
<script type="text/javascript" src="/inc/js/jcrop/js/jquery.Jcrop.min.js"></script>
<script type="text/javascript" src="/inc/js/datepick/jquery.datepick.min.js"></script>
<script type="text/javascript" src="/inc/js/starrating/jquery.rating.pack.js"></script>
<script type="text/javascript" src="/inc/js/starrating/jquery.MetaData.js"></script>
<script type="text/javascript" src="/inc/js/validate/jquery.validate.min.js"></script>
<script type="text/javascript" src="/inc/js/ui/jquery-ui-1.8.23.custom.min.js"></script>

{include file="header_css.tpl"}

</head>

<body>

<div class="ajax-loader"><img src="/images/ajax-loader.gif"></div>

<div id="admin_panel">

	<h3>SpeakTalkChat Site Administration</h3>

	{if ($me)}

	<div id="admin_navigation">

		<a href="/admin/dashboard">Dashboard</a> |
		<a href="/admin/search">User Search</a> |
		<a href="/admin/messages">Messages{if ($pmcount)} ({$pmcount}){/if}</a> |
		<a href="/admin/text/update">Text translations</a> |
		<a href="/admin/language/update">Language management</a> |  
		<a href="/admin/config/update">Site configuration</a> | 
		<a href="/admin/data/update">Data management</a> | 
		<a href="/admin/content/update">Static content management</a> |
		<a href="/admin/faq/update">FAQ management</a> |
		<a href="/admin/ad/update">Ad management</a> |
		<a href="/admin/groups/update">Groups management</a> |
		<a href="/blog/wp-admin" target="_blank">Blog management</a> |    
		<a href="/admin/logout">Logout</a>

	</div>

	{/if}

	<div style="height:16px;"></div>


