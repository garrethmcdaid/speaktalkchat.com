{if ($mod_required)}

	<div id="mod_required">

	<div style="height:8px;"></div>

	<div class="dir_header">{$text.$selected_language.headings.group_message_mod_heading->text}</div>

	{foreach item=v key=k from=$mod_required}

		<div class="mod_message" id="mod_message_{$v->thread}" style="border-bottom:#e0e0e00;height:auto;" ><a href="/member/{$v->nickname}">{$v->nickname}</a> 
	| {$v->group_name}
	| <span id="view_message_{$v->thread}" thread="{$v->thread}" class="clickable view_mod_message">{$v->subject}</span> 
	| {$v->sent} 
	| <span id="confirm_{$v->thread}" thread="{$v->thread}" sender="{$v->sender}" action="confirm" class="clickable action_mod_message">{$text.$selected_language.forms.confirm_prompt->text}</span>  
	| <span id="decline_{$v->thread}" thread="{$v->thread}" sender="{$v->sender}" action="decline" class="clickable action_mod_message">{$text.$selected_language.forms.decline_prompt->text}</span> 
	<span id="mod_update_{$v->thread}"></span></div>

	{/foreach}

	<div style="height:16px;"></div>

	<div style="border-top:1px solid #e0e0e0;border-bottom:1px solid #e0e0e0;padding:10px 0 10px 0;">
		<div id="view_message" style="display:none;"></div>
	</div>

	<div style="height:16px;"></div>

	</div>

{/if}

{if ($requests)}

	<div id="group_requests">

	<div style="height:8px;"></div>

	<div class="dir_header">{$text.$selected_language.headings.join_requests_heading->text}</div>

	{foreach item=v key=k from=$requests}

		<div class="join_request" id="request_{$v->request_id}" style="border-bottom:#e0e0e00;height:auto;" ><a href="/member/{$v->nickname}">{$v->nickname}</a> 
	| {$v->group_name} 
	| @{$v->when} 
	| <span id="confirm_{$v->request_id}" group="{$v->id}" user_id="{$v->user_id}" request_id="{$v->request_id}" action="confirm" class="clickable user_group">{$text.$selected_language.forms.confirm_prompt->text}</span>  
	| <span id="decline_{$v->request_id}" user_id="{$v->user_id}" request_id="{$v->request_id}" action="decline" group="{$v->id}" class="clickable user_group">{$text.$selected_language.forms.decline_prompt->text}</span> 
	<span id="request_message_{$v->request_id}"></span></div>

	{/foreach}

	<div style="height:16px;"></div>

	</div>


{/if}


<div class="dir_letter group_select" id="my" style="width:auto;">{$text.$selected_language.links.my_groups->text}</div>{$smartyutil->directory('dir_letter group_select')} 

<br style="clear:both;">

<div style="height:14px;"></div>

<span class="reveal group_create_form slideDown clickable" id="compose_message">{$text.$selected_language.forms.create_group_prompt->text}</span>

<div style="height:14px;"></div>

<div id="group_create_form" style="display:none;">

{include file="user_group_create_form.tpl"}

</div>

<div id="group_detail"></div>

<div id="group_results"></div>

<script>js_userGroupSelect('{$selector}');</script>


