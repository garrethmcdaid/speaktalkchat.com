{$in_conversation = true}

<div id="start">

{if $c->mode == 'flash' || $c->mode == ''}

	{$review_display = "none"}

	<span style="font-size:3em;"><a class="clickable_nd" style="text-decoration:underline;" id="openchat" href="/start/{$startcode}">{$text.$selected_language.links.start->text} <<</a></span>

{elseif $skype != '' && $me->skype != ''}

	{if $c->user1 == $me->id}

		{$review_display = "none"}
	
		<span style="font-size:3em;"><a class="clickable_nd" code="{$startcode}" id="openskype" href="skype:{$skype}?call">{$text.$selected_language.links.start->text} <<</a></span>
		
		<div style="height:20px"></div>
		<div id="retry_skype" class="highlight" style="display:none;height:20px;">{$text.$selected_language.messages.retry_skype->text}</div>
		<div style="height:20px"></div>
		<input type="hidden" id="code" value="{$startcode}">
		<input type="hidden" id="started" value="0">
		<script>js_startSkype();</script>

	{else}
	
		{$review_display = "block"}
	
		<div class="highlight" style="height:20px;">{$text.$selected_language.messages.please_wait_skype->text}</div>
		<div style="height:14px;"></div>
	
	{/if}
	
{else}

	<div class="highlight" style="height:20px;">{$text.$selected_language.messages.one_user_no_skype->text}</div>
	<div style="height:14px;"></div>
	
{/if}

</div>


<div id="conversation_start">

<div style="height:20px;"></div>

<div id="advice">

<b>{$text.$selected_language.headings.conversation_advice_heading->text}</b>
<div style="height:20px;"></div>

{if $c->mode == 'flash' || $c->mode == ''}

	{$text.$selected_language.advice.flash_version->text}
	<div class="greybar"></div>
	{$text.$selected_language.advice.broadband_speed->text}
	<div class="greybar"></div>
	{$text.$selected_language.advice.quiet_room->text}
	<div class="greybar"></div>
	{$text.$selected_language.advice.use_headset->text}
	<div class="greybar"></div>
	{$text.$selected_language.advice.stay_still->text}
	<div class="greybar"></div>
	{$text.$selected_language.advice.other_apps->text}
	<div class="greybar"></div>

{else}

	{$text.$selected_language.advice.broadband_speed->text}
	<div class="greybar"></div>
	{$text.$selected_language.advice.quiet_room->text}
	<div class="greybar"></div>
	{$text.$selected_language.advice.use_headset->text}
	<div class="greybar"></div>
	{$text.$selected_language.advice.stay_still->text}
	<div class="greybar"></div>


{/if}

</div>

<div style="height:14px;"></div>

<div style="display:{$review_display};" id="review_prompt">
<span style="font-size:1.4em"><a href="/start/review/{$c->code}">{$text.$selected_language.links.conversation_exit->text}</a></span>
</div>

</div>
