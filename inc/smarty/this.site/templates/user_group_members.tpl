<div class="dir_header"><a href="/user/groups/" style="color:#ffffff;">{$text.$selected_language.links.all_groups->text}</a> - {$groupdata->group_name}</div>

<div style="height:8px;"></div>
{$groupdata->group_description|nl2br}
<div style="height:8px;"></div>

	{$disable = false}

	
	{if !$disable}


<div class="thickgreybar"></div>	


<div><b>{$text.$selected_language.headings.recent_discussions_heading->text}</b> | 


	{if $is_member || $is_admin}
		<a class="highlight" href="/user/groups/forum/{$group}">{$text.$selected_language.links.enter_forum->text}</a></div>
<div style="height:8px;"></div>
	{else}
		<a class="highlight" href="/user/groups/{$group}">{$text.$selected_language.links.join_forum->text}</a></div>
<div style="height:8px;"></div>
	{/if}

	<div class="discussion_summary">
		<b>
		<div class="discussion_summary_col" style="width:540px;padding-left:0px;">{$text.$selected_language.headings.subject_heading->text}</div>
		<div class="discussion_summary_col" style="width:70px;">{$text.$selected_language.headings.posts_heading->text}</div>
		<div class="discussion_summary_col" style="width:140px;">{$text.$selected_language.headings.created_by->text}</div>
		<div class="discussion_summary_col" style="width:140px;">{$text.$selected_language.headings.last_post->text}</div>
		<br style="clear:both;">
		</b>

	</div>

	{foreach item=v key=k from=$discussions}
		<div class="discussion_summary">
			<div class="discussion_summary_col" style="width:540px;padding-left:0px;">{$v->subject}</div>
			<div class="discussion_summary_col" style="width:70px;">{$v->no_posts}</div>
			<div class="discussion_summary_col" style="width:140px;"><span class="highlight">{$v->created_by_nick}</span></div>
			<div class="discussion_summary_col" style="width:140px;">{$v->last_post}</div>
			<br style="clear:both;">
		</div>
	{/foreach}
	{/if}

<div class="thickgreybar"></div>	


{if (is_array($members))}

	<div><b>Members</b></div>

	<div class="dir_letter member_select" id="all" style="width:auto;">{$text.$selected_language.words.all->text}</div>{$smartyutil->directory('dir_letter member_select')}

	<br style="clear:both;">

	<div style="height:8px;"></div>

	{foreach item=v key=k from=$members}
		<div id="member_{$v->user_id}"class="member_{$v->nickname|substr:0:1|strtoupper} member" style="margin-bottom:2px;width:120px;height:35px;float:left;">
			<div style="float:left;margin-right:4px;width:auto;" class="extra_small_avatar">
			<a href="/member/{$v->nickname}">
			{if ($v->avatar)}
				<img align="top" src="/content/{$v->user_id}/avatar/avatar.jpg">
			{else}
				<img align="top" src="/content/default/avatar/avatar.jpg">
			{/if}
			</a>
			</div>
			<div style="float:left;height:auto;width:auto;padding-right:0px;">
			<a href="/member/{$v->nickname}">{$v->nickname}</a><br>
			{if $is_owner}
				<span id="delete_{$v->user_id}" class="reveal delete_member_x_{$v->user_id} fadeIn delete_something clickable_nd">X</a></span> <span class="confirm_delete_member delete_something clickable_nd" id="delete_member_x_{$v->user_id}" user_id="{$v->user_id}" group_id="{$v->group_id}" style="display:none;">{$text.$selected_language.forms.confirm_prompt->text}</span>
				
			{/if}
			</div>
		</div>
	{/foreach} 

		<br style="clear:both;">

	<div style="height:14px;"></div>

	

{/if}
