{include file="header.tpl"}

<div style="font-size:1.3em;">

{$text.$selected_language.messages.register_account_types->text}

<div style="height:10px;"></div>

<a href="/register/account/adult" class="highlight">{$text.$selected_language.links.register_adult_link->text}</a>
<div style="height:10px;"></div>
<a href="/register/account/family" class="highlight">{$text.$selected_language.links.register_family_link->text}</a>
<div style="height:10px;"></div>
<a href="/register/account/teenager" class="highlight">{$text.$selected_language.links.register_teen_link->text}</a>
<div style="height:10px;"></div>
<a href="/register/account/child" class="highlight">{$text.$selected_language.links.register_child_link->text}</a>
<div style="height:10px;"></div>
<a href="/register/account/educator" class="highlight">{$text.$selected_language.links.register_educator_link->text}</a>

</div>

{include file="footer.tpl"}
