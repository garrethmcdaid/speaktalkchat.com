{if $menu}

	{if ($nonav)}

		<div style="height:14px;"></div>

		<div id="nav_menu">
		<b>{$text.$selected_language.words.conversation->text} {$nonav.id}</b>		
		</div>


	{else}

		
		<div style="height:23px;clear:both;"></div>
		
		<div>

		<div id="nav_menu" style="float:left;">

		{foreach item=v key=k from=$menu}
			{if ($v->static_menu == 'top') && $v->static_menu_on > 0}
				<a href="/{$v->static_tag}">{$v->static_title}</a> <img src="/images/sep.png">
			{/if}

		{/foreach}

		{if (isset($me->logout_url))}

			<a href="/user/dashboard">{$text.$selected_language.headings.user_dashboard->text}</a> <img src="/images/sep.png">  <a href="/user/search">{$text.$selected_language.headings.user_search->text}</a> <img src="/images/sep.png"> <a href="/user/directory">{$text.$selected_language.headings.user_directory->text}</a> <img src="/images/sep.png"> <a href="/user/messages">{$text.$selected_language.headings.user_messages->text} {if ($pmcount)}({$pmcount}){/if}</a> <img src="/images/sep.png"><a href="/user/groups">{$text.$selected_language.headings.user_groups->text}</a> <img src="/images/sep.png"> <a href="/faq">{$text.$selected_language.headings.faq_short_heading->text}</a> <img src="/images/sep.png"> <a href="{$me->logout_url}">{$text.$selected_language.forms.logout_prompt->text}</a>

		{else}

			<a href="/faq">{$text.$selected_language.headings.faq_short_heading->text}</a> <img src="/images/sep.png">  <a href="/register">{$text.$selected_language.headings.register_heading->text}</a>

		{/if}
		
			

		</div>
		<div style="float:right;display:block;">
			<a href="http://twitter.com/speaktalkchat" target="_blank"><img src="/images/template/twitter.png"></a>						<a href="http://facebook.com/speaktalkchat" target="_blank"><img src="/images/template/facebook.png"></a>						<a href="/blog" target="_blank"><img src="/images/template/rss.png"></a>


		</div>
		
		{if !($me)}
		    <div style="float: right; margin-right: 10px;">
			{include file="social_buttons.tpl"}			
		    </div>
		{/if}
			
		</div>

	{/if}

{/if}
