{if $is_mobile}	
	<div id="page">
	<div id="pagetop">
	<a href="/"><img src="/images/template/mobile_logo.png"></a>	
	</div>
	<div style="height:56px;"></div>	
{else}

	<div id="pagetop">
		<div id="inttop">
			
			{if !($me) && $path.0 != 'register'}  
				{include file="user_login.tpl"}				
			{else}
				<br style="clear:both;">
				<div style="height:103px;"></div>	
			{/if}
	
			<div style="width:auto;height:auto;">
				<div style="width:auto;float:none;height:auto;text-align:right;color:#ffffff;">
				{if (isset($me->logout_url))}
					<span><a href="/user/messages/sendto/admin/?subject=bugreport" class="highlight clickable_nd">{$text.$selected_language.forms.report_bug_prompt->text}</a> | </span>
				{/if}
				{foreach item=v key=k from=$all_language}
					{if ($v->language_active gt 0)}
					<a href="?l={$v->language_suffix}" title="{$v->language_name}">{$v->language_suffix|strtolower}</a> |
					{/if}
				{/foreach}
				
				</div>
				
			</div>
			{include file="header_static_menu.tpl"}
			</div>
		</div>
	</div>	

{/if}

	<div id="pagemiddle">
		<div id="intmiddle">
	
		<div style="height:14px;"></div>
		
		{if $is_mobile}
			<div style="height:48px;"></div>
		{/if}
		
		{if $is_android}
			<div id="android_banner_message" style="font-size:1.2em;">
			{$text.$selected_language.messages.android_warning->text}<br>
			<a class="highlight" href="market://details?id=org.mozilla.firefox&hl=en" target="_blank">{$text.$selected_language.forms.download_prompt->text|ucfirst}</a>
			<div style="height:16px;"></div>
			</div>
		{/if}
