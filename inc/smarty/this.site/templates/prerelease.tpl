<!DOCTYPE html> 
<html lang="en"> 
<head> 
<meta charset="utf-8" />
<title>SpeakTalkChat.com</title>
<meta name="description" content=""/>

<link href="/inc/css/layout.css" type="text/css" rel="stylesheet">
<link href="/inc/css/style.css" type="text/css" rel="stylesheet">

</head>

<body>


<div id="content">

<div id="pagetop">
<div style="height:18px;"></div>

</div>
<div id="pagemiddle" style="text-align:center;padding:0 130px 0 130px;">

<div style="height:30px;"></div>

<img src="/inc/css/images/logo.png">

<div style="height:30px;"></div>

<b>An Ghaeltacht ar Líne</b><br>
SpeakTalkChat.com/Gaeltacht
<div style="height:20px;"></div>
 
Seolfar leagan Beta An Ghaeltacht ar Líne le SpeakTalkChat.com an mhí
seo chugainn. Tugann an suíomh nua seo deis do dhaoine clárú agus
labhairt le daoine eile i nGaeilge a bhfuil na caitheamh aimsire
céanna acu. 
<div style="height:20px;"></div>
Beidh sé de rogha ag an úsáideoir clárú do go leor
caithimh aimsire agus mar thosnaitheoir nó duine atá réasúnta líofa nó
líofa i nGaeilge. Ag tabhairt an Ghaeilge chun suntais, is é
SpeakTalkChat an chéad suíomh dá leithéid.
<div style="height:20px;"></div> 
Chomh maith leis an nGaeilge beidh sé de rogha agat clárú agus nascadh
le daoine i go leor teangacha eile. Tá sé feiliúnach do dhaoine fásta,
scoileanna, déagóirí agus páistí agus tá cosaintí i bhfeidhm go
speisialta d’úsáideoirí óga.

<div style="height:40px;"></div>

<b>The Online Gaeltacht</b><br>
SpeakTalkChat.com/Gaeltacht

<div style="height:20px;"></div>

Next month sees the Beta launch of The Online Gaeltacht, <b>speaktalkchat.com/gaeltacht</b>, which allows people to sign up and chat through Irish with others who have similar interests.
<div style="height:20px;"></div>
You can sign up to a wide variety of interests and as a fluent, competent or beginner speaker.
<div style="height:20px;"></div>
speaktalkchat.com/gaeltacht is the first 'social chatting media' and has been designed with Gaeilge to the fore.
<div style="height:20px;"></div>
As well as chatting through Irish, you can sign up to chat through other languages.  It is suitable for adults, families, schools and adolescents and has stringent safeguards built into the system for underage users.
<div style="height:20px;"></div>
</div>

<div id="pagebottom">
<div style="height:90px;"></div>
			
	

</div>

</div>

</body>
</html>
