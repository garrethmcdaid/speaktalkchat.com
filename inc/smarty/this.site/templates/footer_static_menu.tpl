{if (!$nonav)}

	{if $menu}
		{foreach item=v key=k from=$menu_bot name="links"}
			{if $v->static_menu_on > 0}
				<a href="/{$v->static_tag}">{$v->static_title}</a>
				{if !$smarty.foreach.links.last} | {/if}
			{/if}
		{/foreach}
	{/if}
	
	{if $is_mobile_main}
		| <a href="/?mobilemain=0">{$text.$selected_language.links.mobile_site->text}</a>
	{else if !$is_mobile_main && $is_mobile}
		| <a href="/?mobilemain=1">{$text.$selected_language.links.main_site->text}</a>
	{/if}

{/if}
