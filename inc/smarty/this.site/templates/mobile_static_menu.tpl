{if $menu}

	{if ($nonav)}

		<div style="height:38px;"></div>

		<div id="nav_menu">
		<b>{$text.$selected_language.words.conversation->text} {$nonav.id}</b>		
		</div>


	{else}

		
		<div id="nav_menu">

		{foreach item=v key=k from=$menu}
			{if $v->static_menu == 'top' && $v->static_menu_on > 0}
				<a href="/{$v->static_tag}"><img align="absmiddle" src="/images/{$v->static_tag}.png"></a>
			{/if}

		{/foreach}

		{if (isset($me->logout_url)) }

			<a href="/user/dashboard"><img align="absmiddle" src="/images/dashboard.png"></a> <a href="/user/messages"><img align="absmiddle" src="/images/messages.png"> {if ($pmcount)}({$pmcount}){/if}</a> <a href="/faq"><img align="absmiddle" src="/images/faq.png"></a> <a href="{$me->logout_url}"><img align="absmiddle" src="/images/logout.png"></a>

		{else}
			<a href="/login"><img align="absmiddle" src="/images/login.png"></a>
			<a href="/faq"><img align="absmiddle" src="/images/faq.png"></a>

		{/if}

		</div>
		
		<div id="mobile_lang_nav">
		
		{foreach item=v key=k from=$all_language}
			{if ($v->language_active gt 0)}
				<a href="?l={$v->language_suffix}" title="{$v->language_name}">{$v->language_suffix|strtolower}</a> |
			{/if}
		{/foreach}
		
		</div>

	{/if}

{/if}
