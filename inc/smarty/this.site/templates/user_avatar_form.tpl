<form name="user_avatar_form" id="user_avatar_form" method="post" enctype="multipart/form-data">

<span id="avatar_selector_link" class="reveal avatar_selector slideDown clickable_nd"><b>{$text.$selected_language.headings.avatar_heading->text}</b></span>
<div style="height:10px;"></div>

<div id="avatar_selector" {if !$avatar_processed}style="display:none;"{/if}>

{if isset($avatar_exists)}
	{if ($me->id > 1)}
		<img src="/content/{$me->id}/avatar/avatar.jpg?cache={$spanner}"><br>
	{else}
		<img src="/content/{$user.id}/avatar/avatar.jpg?cache={$spanner}"><br>
	{/if}
{/if}

{if isset($avatar)}
	<div style="width:70px;height:70px;overflow:hidden;">
		{if ($me->id > 1)}
			<img src="/content/{$me->id}/avatar/avatar_orig.jpg?cache={$spanner}" id="avatar_preview">
		{else}
			<img src="/content/{$user.id}/avatar/avatar_orig.jpg?cache={$spanner}" id="avatar_preview">
		{/if}
	</div>
<input type="submit" name="add_user_avatar" value="{$text.$selected_language.forms.update_prompt->text}"><br>
	{$text.$selected_language.forms.avatar_crop_prompt->text}<br>

	{if ($me->id > 1)}
		<img src="/content/{$me->id}/avatar/avatar_orig.jpg?cache={$spanner}" id="avatar_orig">
	{else}
		<img src="/content/{$user.id}/avatar/avatar_orig.jpg?cache={$spanner}" id="avatar_orig">
	{/if}

	<input type="hidden" name="coords[x1]" id="x1" value="0">
	<input type="hidden" name="coords[y1]" id="y1" value="0">
	<input type="hidden" name="coords[x2]" id="x2" value="0">
	<input type="hidden" name="coords[y2]" id="y2" value="0">
	<input type="hidden" name="coords[w]" id="w" value="0">
	<input type="hidden" name="coords[h]" id="h" value="0">
	<input type="hidden" name="orig_w" id="orig_w" value="{$avatar.0}">
	<input type="hidden" name="orig_h" id="orig_h" value="{$avatar.1}">

{else}
	{$text.$selected_language.forms.image_prompt->text}<br>
	<input type="file" name="avatar" id="avatar" class="required"><br>
	<div style="height:10px;"></div>
	<input type="submit" name="add_user_avatar" value="{$text.$selected_language.forms.update_prompt->text}"><br>

{/if}

</form>

</div>

<div style="height:14px;"></div>

