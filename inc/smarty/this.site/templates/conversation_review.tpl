{include file="header.tpl"}

<b>{$text.$selected_language.headings.conversation_review_heading->text}</b>

<div style="height:14px;"></div>

<input type="hidden" id="code" value="{$code}">

{if $rating.on > 0}

	<div id="member_review_rating">

	{$text.$selected_language.forms.user_rating->text}:<br>
	{$rating.user = $rating.user*2}
	<input type="hidden" id="user_id" value="{$users.user_id}">
	
	<div id="member_review_stars1">

	{for $v=1 to 10}

		{if ($v == $rating.user)}
			<input name="r1" type="radio" class="star {literal}{split:2}{/literal}" disabled="disabled" checked="checked" value="{$v}" />
		{else}
			<input name="r1" type="radio" class="star {literal}{split:2}{/literal}" disabled="disabled" value="{$v}" />
		{/if}
	
	{/for}
	
	</div>

	<div style="clear:both;height:14px;"></div>
	{$text.$selected_language.forms.previous_rating->text}:<br>
	<input type="hidden" id="viewer_id" value="{$users.viewer_id}">
	
	<div id="member_review_stars2">

	{for $v=1 to 5}

		{if ($v == $rating.viewer)}
			<input name="r2" type="radio" class="star rating_submit" checked="checked" value="{$v}" />
		{else}
			<input name="r2" type="radio" class="star rating_submit" value="{$v}" />
		{/if}


	{/for}
	
	</div>

	<div style="clear:both;height:14px;"></div>

	{$text.$selected_language.forms.rate_prompt->text}

	<div class="highlight" id="rating_response" style="height:auto;"></div>
	
	{if $is_mobile}<div class="greybar"></div>{/if}

	<div style="clear:both;height:14px;"></div>

	</div>
	
{/if}

<div id="member_review_feedback">

<div id="member_review_feedback_input">

<span class="highlight" style="font-size:1.2em;">{$text.$selected_language.forms.review_feedback_prompt->text}:</span><br>

<div style="clear:both;height:14px;"></div>

<form name="review_feedback" id="review_feedback">

<textarea id="feedback" name="feedback" style="height:130px;" class="required"></textarea><br>
{$text.$selected_language.messages.review_feedback_message->text}

<div style="clear:both;height:14px;"></div>

<input type="button" id="submit_review_feedback" name="submit_review_feedback" value="{$text.$selected_language.forms.send_prompt->text}"> 

</div>

<span class="highlight" id="feedback_response"></span>

</form>

</div>

{if isset($process_message)}
	{$process_message}
{/if}

<div class="hidden_block">
{foreach item=v key=k from=$text.$selected_language.messages}
	<input type="hidden" id="{$v->text_tag}" value="{$v->text}">
{/foreach}
</div>

<br>

{include file="footer.tpl"}
