<div style="float:left;width:240px;height:auto;margin-right:30px;">

<form name="user_account_email_form" id="user_account_email_form" method="post" class="validation_required">

<b>{$text.$selected_language.forms.email_prompt->text}</b>
<div style="height:14px;"></div>

{$text.$selected_language.forms.new_email_prompt->text}<br>
<input type="text" style="width:200px;" name="me[email]" id="email_address" class="required email" value=""><br>

{$text.$selected_language.forms.confirm_email_prompt->text}<br>
<input type="text" style="width:200px;" name="me[confirm_email]" id="confirm_email_address" class="required email" value=""><br>

<input type="submit" name="update_user_account_email" value="{$text.$selected_language.forms.update_prompt->text}"><br>

</form>

<div style="height:14px;"></div>

{$text.$selected_language.words.current->text}:<br>
<span class="highlight">{$me->email}</span>


{if $is_admin}

	<div style="height:10px;"></div>
	
	<form name="user_account_suspend_form" id="user_account_suspend_form" method="post">
	
	Suspend Account:
	<input type="checkbox" name="account_suspend" {if $suspend > 0}checked{/if}>
	
	<div style="height:10px;"></div>
	
	<input type="submit" name="update_user_account_suspend" value="{$text.$selected_language.forms.update_prompt->text}"><br>
	
	</form>

{/if}

</div>

<div style="float:left;width:174px;height:auto;margin-right:30px;">

<form name="user_account_password_form" id="user_account_password_form" method="post" class="validation_required">

<b>{$text.$selected_language.forms.password_prompt->text}</b>
<div style="height:14px;"></div>

{$text.$selected_language.forms.new_password_prompt->text}<br>
<input style="width:120px" type="password" name="me[password]" id="password" class="required"><br>
{$text.$selected_language.forms.confirm_password_prompt->text}<br>
<input style="width:120px;" type="password" name="me[confirm_password]" id="confirm_password" class="required"><br>

<input type="submit" name="update_user_account_password" value="{$text.$selected_language.forms.update_prompt->text}"><br>

</form>

</div>

<div style="float:left;width:210px;height:auto;margin-right:30px;">

<form name="user_account_skype_form" id="user_account_skype_form" method="post" class="validation_required">

<b>{$text.$selected_language.forms.skype_prompt->text}</b>
<div style="height:14px;"></div>

{$text.$selected_language.forms.skype_prompt->text}<br>
<input type="text" name="me[skype]" id="skype" class="required skype" value=""><br>

{$text.$selected_language.forms.confirm_skype_prompt->text}<br>
<input type="text" name="me[confirm_skype]" id="confirm_skype" class="required skype" value="">

{if $me->skype != ''}

	<div class="flash-suspend">

	<div style="height:10px;"></div>
	<span style="font-size:0.9em;">{$text.$selected_language.words.or->text}</span>
	<div style="height:10px;"></div>
	
	<input type="checkbox" name="me[remove_skype]" id="remove_skype">
	<span class="highlight">{$text.$selected_language.forms.delete_prompt->text|lcfirst}</span><br><br>

	</div>

{/if}

<div style="height:10px;"></div>

<input type="submit" name="update_user_account_skype" value="{$text.$selected_language.forms.update_prompt->text}"><br>

<div class="flash-suspend">
<span class="highlight" style="font-size:0.8em;">{$text.$selected_language.messages.skype_required_warning->text}</span>
</div>

<div style="height:14px;"></div>

{$text.$selected_language.words.current->text}:<br>
<span class="highlight">{$me->skype}</span>

</form>

</div>

<div style="float:left;width:230px;height:auto;margin-right:30px;">

<b>{$text.$selected_language.forms.user_rating->text}</b>
<div style="height:14px;"></div>

{$text.$selected_language.forms.allow_ratings_prompt->text}

<div style="height:6px;"></div>

<form name="user_account_rating_on_form" id="user_account_rating_on_form" method="post" class="validation_required">

{if $me->profile->rating.on > 0}
	<input type="radio" name="rating_on" value="1" checked> {$text.$selected_language.words.yes->text} <input type="radio" name="rating_on" value="0"> {$text.$selected_language.words.no->text}
{else}
	<input type="radio" name="rating_on" value="1"> {$text.$selected_language.words.yes->text} <input type="radio" name="rating_on" value="0" checked> {$text.$selected_language.words.no->text}
{/if}

<div style="height:6px;"></div>

<input type="submit" name="update_user_account_rating_on" value="{$text.$selected_language.forms.update_prompt->text}"><br>

<div style="height:6px;"></div>

{for $v=1 to 10}

	{if ($v == $me->profile->rating.user*2)}
		<input name="r1" type="radio" class="star {literal}{split:2}{/literal}" disabled="disabled" checked="checked" value="{$v}" />
	{else}
		<input name="r1" type="radio" class="star {literal}{split:2}{/literal}" disabled="disabled" value="{$v}" />
	{/if}

{/for}
&nbsp;({$me->profile->rating.count})

<br style="clear:both;">


</form>

</div>

<div style="height:14px;clear:left;"></div>

<div class="hidden_block">
{foreach item=v key=k from=$text.$selected_language.messages}
	<input type="hidden" id="{$v->text_tag}" value="{$v->text}">
{/foreach}
</div>

