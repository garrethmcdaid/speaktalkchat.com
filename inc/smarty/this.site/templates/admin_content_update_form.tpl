	<form name="admin_content_update_form" id="admin_content_update_form" method="post">

	{if $content}
		{$menu = $content->static_menu}
		<b>Language:</b><br>
		{$content->language_name}<br>
		<input type="hidden" name="content[id]" value="{$content->id}">
		<input type="hidden" name="content[static_tag]" value="{$content->static_tag}">
	{else}
		{$menu = 'bot'}		
	{/if}


	<b>Page title:</b><br>
	<input type="text" id="static_title" name="content[static_title]" value="{if $content}{$content->static_title|stripslashes}{/if}"><br>
	<b>Title display:</b><br>
	<select name="content[static_title_on]" id="static_title_on">
	{if $content->static_title_on < 1}
		<option value="0" selected>Hide title
		<option value="1">Display title
	{else}
		<option value="0">Hide title
		<option value="1" selected>Display title
	{/if}
	</select>
	<br>
	<b>Include in menu:</b><br>
	<select name="content[static_menu_on]" id="static_menu_on">
	{if $content->static_menu_on < 1}
		<option value="1">Include
		<option value="0" selected>Hide
	{else}	
		<option value="1" selected>Include
		<option value="0">Hide
	{/if}
	</select>
	<br><br>
	<b>Menu:</b><br>
	{html_radios name="content[static_menu]" options=$default_menus selected=$menu}<br><br>
	<b>Page content:</b><br>
	<textarea class="tinymce" style="width:1000px;height:500px;" name="content[static_content]">{if $content}{$content->static_content|stripslashes}{/if}</textarea><br>
	
	<b>Status:</b><br>
	<select name="content[static_status]" id="static_status">
	{if $content->static_status < 1}
		<option value="0" selected>Published
		<option value="1">Unpublished
	{else}
		<option value="0">Published
		<option value="1" selected>Unpublished
	{/if}
	</select>
	<br><br>
	
	<b>Menu Order (0 = first in order of preference):</b><br>
	<input style="width:30px;text-align:right;" type="text" name="content[static_order]" id="static_order" value="{if $content}{$content->static_order}{else}1{/if}">
	
	<br><br>


	<input type="hidden" name="content[{if $content}update{else}add{/if}]">
	<input type="submit" name="admin_content_update_submit" value="{if $content}Update{else}Add{/if}">	


	</form>
