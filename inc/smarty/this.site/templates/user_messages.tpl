{include file="header.tpl"}

<h3>{$text.$selected_language.headings.user_messages->text}</h3>

<span class="reveal message_form slideDown clickable enhance" id="compose_message">{$text.$selected_language.forms.compose_prompt->text}</span>

<div style="height:14px;"></div>

<div id="message_form" {if (!$to)}style="display:none;"{/if}>

{include file="user_messages_form.tpl"}

</div>

<div style="height:14px;"></div>

<div id="inbox">

{include file="user_messages_inbox.tpl"}

</div>

<div style="height:14px;"></div>

{if isset($process_message)}
	{$process_message}
{/if}

<div class="hidden_block">
{foreach item=v key=k from=$text.$selected_language.messages}
	<input type="hidden" id="{$v->text_tag}" value="{$v->text}">
{/foreach}
</div>

{include file="footer.tpl"}
