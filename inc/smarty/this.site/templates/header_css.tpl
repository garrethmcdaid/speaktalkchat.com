{if $is_mobile}
	<link rel="stylesheet" href="/inc/css/mobile_layout.css?v=10" type="text/css" />
{else}	
	<link rel="stylesheet" href="/inc/css/layout.css?v=10" type="text/css" />	
{/if}
<link rel="stylesheet" href="/inc/css/style.css" type="text/css" />
<link rel="stylesheet" href="/inc/css/jquery.ui.autocomplete.css" type="text/css" />
<link rel="stylesheet" href="/inc/js/jcrop/css/jquery.Jcrop.css" type="text/css" />
<link rel="stylesheet" href="/inc/js/datepick/jquery.datepick.css" type="text/css" />
<link rel="stylesheet" href="/inc/js/starrating/jquery.rating.css" type="text/css" />
<link rel="stylesheet" href="/inc/js/social_likes/social-likes_classic.css" type="text/css" />

