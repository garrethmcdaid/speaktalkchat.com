<input type="hidden" class="auto_timezone" name="auto_timezone" value="">
<input type="hidden" class="auto_timezone_offset" name="auto_timezone_offset" value="">
<input type="hidden" id="update_timezone_offset" value="">
<input type="hidden" id="update_timezone" value="">


<script>
var tz = jstz.determine();
var offset = (new Date()).getTimezoneOffset();
offset = (offset*-1)/60;

if (offset != ($("#myoffset").val()*1) && ($("#myautodetect").val()*1) > 0) {
	$("#tz_update_form").slideDown();
}

if ($("#browser_timezone").length) {
	$("#browser_timezone").empty();
	$("#browser_timezone").append(tz.name());
}

$(".auto_timezone").val(tz.name());
$(".auto_timezone_offset").val(offset);
$("#update_timezone").val(tz.name());
$("#update_timezone_offset").val(offset);


</script>
