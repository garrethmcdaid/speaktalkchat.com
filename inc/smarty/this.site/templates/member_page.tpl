{include file="header.tpl"}

{if $is_mobile}
	{include file="member_conversation_right.tpl"}
	{include file="member_conversation_left.tpl"}
{else}
	{include file="member_conversation_left.tpl"}
	{include file="member_conversation_right.tpl"}
{/if}


<br style="clear:both;">
<div style="height:14px;"></div>

{if isset($process_message)}
	{$process_message}
{/if}

<div class="hidden_block">
{foreach item=v key=k from=$text.$selected_language.messages}
	<input type="hidden" id="{$v->text_tag}" value="{$v->text}">
{/foreach}
</div>

{include file="footer.tpl"}
