<form name="create_group" id="group_create_form_fields" style="width:406px;">

{$text.$selected_language.forms.group_name_prompt->text}<br>
<input type="text" name="group_name" id="group_name" class="required" value="">

<div style="height:14px;"></div>

{$text.$selected_language.forms.group_description_prompt->text}<br>
<textarea name="group_description" class="required" id="group_description" style="height:80px;width:400px;"></textarea>

<div style="height:14px;"></div>

{$text.$selected_language.forms.group_status_prompt->text}<br>
{html_radios name="group_status" id="group_status" options=$statuses selected='public'}

</form>

<div style="height:14px;"></div>

<input type="button" id="create_group" name="create_group" value="{$text.$selected_language.forms.create_prompt->text}">

<input type="button" class="reveal group_create_form slideDown" id="cancel_message" value="{$text.$selected_language.forms.cancel_prompt->text}"> <span id="transaction_update"></span>

<div style="height:14px;"></div>

