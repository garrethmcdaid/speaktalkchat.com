<!DOCTYPE html> 
<html lang="en" prefix="og: http://ogp.me/ns/fb#"> 
<head> 
<meta charset="utf-8" />
<title>{$text.$selected_language.info.site_title->text}</title>
<meta name="description" content="{$text.$selected_language.info.meta_tag_description->text}"/>

<meta property="og:title" content="SpeakTalkChat" />
<meta property="og:type" content="website" />
<meta property="og:url" content="http://speaktalkchat.com" />
<meta property="og:image" content="http://speaktalkchat.com/images/template/mobile_logo.png"/>

{include file="header_js.tpl"}
{include file="header_css.tpl"}

{if $is_mobile}
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
{/if}

</head>

<body>

{if (!$me)}

	<script src="https://connect.facebook.net/en_US/all.js"></script> 
	
	<div id="fb-root"></div>
	
	{if !$is_mobile}
	
	<script type="text/javascript"> 
	 
		FB.init({
		appId : "{$config.facebook->data->login_app_id->value}",
		status : true,
		cookie : true,
		xfbml : true,
		oauth : true
		});
		{literal}
		
		FB.Event.subscribe('auth.login', function (response) {			
		  	if (response.status == 'connected') {
				window.location = "/register/account";
				$(".ajax-loader").show();
			}	
		});
		
		(function(d){
	
		   var js, id = 'facebook-jssdk'; if (d.getElementById(id)) {return;}
		   js = d.createElement('script'); js.id = id; js.async = true;
		   js.src = "//connect.facebook.net/en_US/all.js";
		   d.getElementsByTagName('head')[0].appendChild(js);
		 }(document));
	
		{/literal}

	</script>
	
	{else}
	
	<script type="text/javascript"> 
 
		{literal}
		FB.init({
		{/literal}
		appId : "{$config.facebook->data->login_app_id->value}",
		status : true,
		cookie : true,
		xfbml : true
		{literal}		
		});
		{/literal}
	
		FB.Event.subscribe('auth.statusChange', js_statusChange);

		</script>
	
	{/if}

{/if}

<div class="ajax-loader"><img src="/images/ajax-loader.gif"></div>

{if $is_admin}
	<a href="?text_hint_on">Text hint on</a> | <a href="?text_hint_off">Text hint off</a>
{/if}

{include file="header_pagetop.tpl"}
