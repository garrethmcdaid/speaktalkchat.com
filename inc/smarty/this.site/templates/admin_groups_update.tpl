{include file="admin_header.tpl"}

<div id='admin_container'>

<b>Groups management</b><br>
<div class="greybar"></div>
<div style='height:20px;'></div>


{foreach item=v key=k from=$groups}

	{$k} | {$v->owner_nickname} | <span id="delete_group_link_{$v->id}" class="reveal delete_group_{$v->id} fadeIn clickable_nd highlight">Delete</span> <span style="display:none;" id="delete_group_{$v->id}"><a  class="clickable_nd highlight" href="/admin/groups/update/delete/{$v->id}">Confirm</a></span><br>

{/foreach}


<div style='height:20px;'></div>

{$message}

</div>

{include file="admin_footer.tpl"}
