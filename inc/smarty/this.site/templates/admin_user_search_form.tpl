<form name="user_search_form" id="user_search_form" method="get">

<input type="hidden" id="admin_search" value="1">

<div id="directory">
<b>{$text.$selected_language.user_data.nickname->text}</b><br>
{$smartyutil->directory('dir_letter user_search_data')} <br style="clear:both;">
</div>
{$text.$selected_language.messages.nickname_search_letters->text}<span id="nickname_search"></span>
<div style="height:10px;"></div>
{$text.$selected_language.words.or->text}
<div style="height:10px;"></div>
{$text.$selected_language.forms.nickname_search_prompt->text}<br>
<input type="text" name="nickname" id="nickname" class="user_search_data">
<div style="height:20px;"></div>

<b>{$text.$selected_language.headings.languages_heading->text}</b>
<div style="height:10px;"></div>

{foreach item=v key=k from=$profile_languages}
        <div id="{$k}" style="float:left;width:100px;margin-bottom:10px;height:50px;">
	<label><input style="margin-left:1px;" type="checkbox" name="language" class="language_checkbox user_search_data" value="{$k}">{$text.$selected_language.languages.{$v|replace:' ':'_'|strtolower}->text}</label><br>
        {if !isset($language_levels.$k)}{$language_levels.$k = 0}{/if}
	{html_options level_id="{$k}" name="language_level" class="user_search_data" id="level_{$k}" values=$levels_en output=$levels}
        </div>
{/foreach}

<div style="height:20px;clear:both;"></div>

<span id="show_data_options" class="reveal data_search_options slideDown clickable">{$text.$selected_language.links.advanced_search_link->text}</span>

<div style="height:20px;"></div>

<div id="data_search_options" style="display:none;">

{if isset($fields)}

	{foreach item=v key=k from=$fields}

		<div id="{$v->id}" class="user_search_input">

		<b>{$text.$selected_language.user_data.{$v->tag}->text}</b><br>
		{if $v->entry_method == 'text'}
			<input type="text" name="me[{$v->id}]" id="{$v->tag}" value="{if isset($user.{$v->id}->value_vc)}{$user.{$v->id}->value_vc|stripslashes}{/if}" class="{if ($v->required gt 0)}required{/if}"><br>
		{elseif $v->entry_method == 'select'}
			{html_options name="me[{$v->id}]" id="{$v->tag}" options=$v->options selected={$user.{$v->id}->value_opt}}<br>
		{elseif $v->entry_method == 'select_mul'}
			{html_options name="me[{$v->id}]" id="{$v->tag}" options=$v->options selected={$user.{$v->id}->value_opt}}<br>
		{elseif $v->entry_method == 'checkbox'}
			{html_checkboxes name="{$v->id}" class="user_search_data" values=$v->options_keys output=$v->options}<br>
		{elseif $v->entry_method == 'radio'}
			{html_radios name="me[{$v->id}]" id="{$v->tag}" class="user_search_data {$v->tag}_radio" options=$v->options selected={$user.{$v->id}->value_opt}} | <span class="highlight clickable_nd" onClick="js_clearRadios('{$v->tag}_radio');">{$text.$selected_language.forms.clear_prompt->text}</span><br>
		{/if}

		</div>
		<div class="greybar"></div>
	
	{/foreach}

{/if}

</div>

<div style="height:20px;"></div>
<input type="button" class="submit_search" id="user_search" name="user_search" value="{$text.$selected_language.forms.search_prompt->text}">

<div id="the_json"></div>

</form>

