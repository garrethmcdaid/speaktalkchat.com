<span id="profile_selector_link" class="reveal profile_selector slideDown clickable_nd"><b>{$text.$selected_language.headings.user_profile->text}</b></span>

<div id="profile_selector" {if !$required && !$profile_processed}style="display:none;"{/if}>

<form name="user_profile_form" id="user_profile_form" method="post">

<div style="height:14px;"></div>

{if isset($fields)}

	{foreach item=v key=k from=$fields name="datafields"}

		<b>{$text.$selected_language.user_data.{$v->tag}->text}</b>{if ($v->required gt 0)}<span class="highlight">*</span>{/if} 
		{if $v->entry_method == 'checkbox'}
		| <span id="expand_profile_options_{$v->id}" class="reveal {$v->tag}_options slideDown clickable_nd highlight">>>></span><br>
		{/if}
		{if $v->entry_method == 'text'}
			<input type="text" name="me[{$v->id}]" id="{$v->tag}" value="{if isset($user.{$v->id}->value_vc)}{$user.{$v->id}->value_vc|stripslashes}{/if}" class="{if ($v->required gt 0)}required{/if}">
			<div style="height:14px;"></div>
		{elseif $v->entry_method == 'select'}
			{html_options name="me[{$v->id}]" id="{$v->tag}" options=$v->options selected={$user.{$v->id}->value_opt}}<br>
		{elseif $v->entry_method == 'select_mul'}
			{html_options name="me[{$v->id}]" id="{$v->tag}" options=$v->options selected={$user.{$v->id}->value_opt}}<br>
		{elseif $v->entry_method == 'checkbox'}
			<div style="display:none;padding-top:6px;" id="{$v->tag}_options">	
			{if isset($user.{$v->id}->value_opts)}
				{html_checkboxes name="me[{$v->id}]" id="{$v->tag}" values=$v->options_keys output=$v->options selected=$user.{$v->id}->value_opts}<br>
			{else}
				{html_checkboxes name="me[{$v->id}]" id="{$v->tag}" values=$v->options_keys output=$v->options}<br>
			{/if}
				</div>
		{elseif $v->entry_method == 'radio'}
			{html_radios name="me[{$v->id}]" id="{$v->tag}" class="{$v->tag}_radio" options=$v->options selected={$user.{$v->id}->value_opt}} | <span class="highlight clickable_nd" onClick="js_clearRadios('{$v->tag}_radio');">{$text.$selected_language.forms.clear_prompt->text}</span>
			<div style="height:14px;"></div>

		{/if}
		{if !$smarty.foreach.datafields.last}<div class="greybar"></div>{/if}
	
	{/foreach}

{/if}

<div style="height:14px;"></div>

{include file="user_tzcalc.tpl"}

<input type="submit" name="add_user_data" value="{$text.$selected_language.forms.update_prompt->text}"><br>

</div>

<div style="height:14px;"></div>

<div class="thickgreybar"></div>

</form>

<form name="user_language_form" id="user_language_form" method="post">

<span id="language_selector_link" class="reveal language_selector slideDown clickable_nd">
<b>{$text.$selected_language.headings.languages_heading->text}</b></span>

<span class="highlight">({$text.$selected_language.messages.one_language_selected->text})</span>
<div style="height:14px;"></div>

<div id="language_selector" {if !$required && !$lang_processed}style="display:none;"{/if}>
{foreach item=v key=k from=$profile_languages}
        <div style="float:left;width:100px;margin-bottom:10px;height:50px;">
	{if in_array($k,$selected_languages)}{$c='checked'}{else}{$c=''}{/if}
	<label><input style="margin-left:1px;" type="checkbox" name="languages[]" value="{$k}" {$c}>{$text.$selected_language.languages.{$v|replace:' ':'_'|strtolower}->text}</label><br>
        {if !isset($language_levels.$k)}{$language_levels.$k = 0}{/if}
        {html_options name="levels[$k]" id="level_{$k}" values=$levels_en output=$levels selected=$language_levels.$k}
        </div>
{/foreach}

<div style="height:14px;clear:both;"></div>

<input type="submit" name="add_user_language" value="{$text.$selected_language.forms.update_prompt->text}"><br>

</div>

<div style="height:14px;"></div>

</form>

<div class="thickgreybar"></div>

{include file="user_avatar_form.tpl"}

<div class="thickgreybar"></div>

<span id="availability_selector_link" class="reveal availability_selector slideDown clickable_nd"><b>{$text.$selected_language.headings.availability_heading->text}</b></span>

<div id="availability_selector" {if !$avail_processed}style="display:none;"{/if}>

<form name="user_availability_form" id="user_availability_form" method="post">

<div style="height:8px;"></div>
<div class="greybar"></div>
<div style="height:8px;"></div>

{$text.$selected_language.forms.timezone->text}<br>

<div style="height:8px;"></div>

{$smartyutil->html_timezone({{$user.availability->zone}})}

<div style="height:8px;"></div>

<span class="highlight">{$text.$selected_language.words.autodetected->text|ucfirst}: </span><span id="browser_timezone"></span>

{include file="user_tzcalc.tpl"}

<div style="height:8px;"></div>
<span class="highlight" style="font-size:0.9em">{$text.$selected_language.messages.tz_use_warning->text}</span>
<div style="height:8px;"></div>

<div style="height:8px;"></div>
<div class="greybar"></div>
<div style="height:8px;"></div>

{$text.$selected_language.headings.availability_heading->text}<br>

<div style="height:14px;"></div>

<input type="checkbox" name="times[always]" value="1" {if ($user.availability->always > 0)}checked{/if}><label>{$text.$selected_language.messages.always_available->text}</label><br>

<div style="height:8px;"></div>

{$text.$selected_language.words.or->text}

<div style="height:8px;"></div>

{$text.$selected_language.forms.period_one->text}<br>
{html_select_time use_24_hours=true display_seconds=false time="{$user.availability->start1}" field_array="times[period1_start]"}
{$text.$selected_language.words.to->text}
{html_select_time use_24_hours=true display_seconds=false time="{$user.availability->end1}" field_array="times[period1_end]"}<br>

{$text.$selected_language.forms.period_two->text}<br>
{html_select_time use_24_hours=true display_seconds=false time="{$user.availability->start2}" field_array="times[period2_start]"}
{$text.$selected_language.words.to->text}
{html_select_time use_24_hours=true display_seconds=false time="{$user.availability->end2}" field_array="times[period2_end]"}<br>

<div style="height:14px;"></div>


<input type="submit" name="add_user_data" value="{$text.$selected_language.forms.update_prompt->text}"><br>

</form>

</div>

<div style="height:14px;"></div>

<div class="thickgreybar"></div>

