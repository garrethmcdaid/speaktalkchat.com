{include file="admin_header.tpl"}

<div id='admin_container'>

	<b>Site content management</b><br>
	<div class="greybar"></div>

	<div style='height:20px;'></div>

	{if $contents}

		{foreach item=v key=k from=$contents}

			{$v.en->static_title}
			{foreach item=vv key=kk from=$v}
				<a href="/admin/content/update/{$kk}/{$vv->static_tag}">{$kk}</a> 
			{/foreach}
			<br>

		{/foreach} 

	{else}

		<a href="/admin/content/update">Back</a>

	{/if}

	<div style='height:20px;'></div>

	{include file="admin_content_update_form.tpl"}

	<div style='height:20px;'></div>

</div>

{include file="admin_footer.tpl"}
