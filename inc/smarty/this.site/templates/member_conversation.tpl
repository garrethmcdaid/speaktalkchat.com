{if (!$c)}

	{$display = false}
	{$no_skype = false}
	
	{if $is_mobile}
		{if ($user->loggedin && $user->account_type < 3)} 
			{$display = true}
			{if !$user->skype or !$me->skype}
				{$no_skype = true}
			{/if}
		{/if}		
	{else}
		{$display = true}
	{/if}

	{if $display && !$no_skype}
	
		<h3>{$text.$selected_language.headings.request_conversation->text}</h3>

		<div id="now_area">
		<div class="flash-suspend">

		{if $is_mobile}
			<input type="hidden" name="mode" id="mode" value="skype">	
		{else}
			{if $user->skype && $me->skype}
				<div style="height:14px;"></div>
				{$text.$selected_language.forms.conversation_mode_prompt->text}<br>
				<select name="mode" id="mode">
					<option value="skype">Skype
					<option value="flash">Flash
				</select>

			{else}
				<input type="hidden" name="mode" id="mode" value="flash">		
			{/if}
		{/if}

		</div>

		<div class="greybar"></div>
		<div style="height:14px;"></div>

		{if ($user->loggedin && $user->account_type < 3)}	
			{$text.$selected_language.messages.user_logged_in->text}<br>
			<span class="clickable" id="conversation_now">{$text.$selected_language.links.request_conversation_now->text}</span>
			{if !$is_mobile}
			<div style="height:14px;"></div>
			<span class="highlight">{$text.$selected_language.words.or->text}</span><br>
			<div style="height:14px;"></div>
			{/if}
		{/if}	
		</div>
	
	{/if}
	{if $display && $no_skype}
		{$text.$selected_language.messages.skype_required_for_mobile_call->text}
	{/if}
	

{else}

	<h3>{$text.$selected_language.headings.review_conversation->text} {$c->code}</h3>
	{if ($c->updated_by == $me->id)}
		{$u = $text.$selected_language.words.you->text}
	{else}
		{if ($c->updated_by == $c->user1)}
			{$u = $c->user1_nickname}
		{else}
			{$u = $c->user2_nickname}
		{/if}
	{/if}
	{$text.$selected_language.words.status->text|ucfirst}: {$c->status} {$text.$selected_language.words.by->text} {$u}

	{if $c->past && $c->started != '0000-00-00 00:00:00'}
	
	<div style="height:4px;"></div>
	{$text.$selected_language.words.start->text|ucfirst}: {$c->started} (GMT)
	<div style="height:4px;"></div>
	{$text.$selected_language.headings.chattime_heading->text|ucfirst}: {$smartyutil->sec_convert($c->duration)}

	{/if}

	<div style="height:14px;"></div>
	{if ($user->loggedin && $c->start_now_ok && $c->status_en == 'confirmed')}
		<span style="font-size:1.4em;"><a class="clickable" href="/member/{$user->nickname}/c/now/{$c->code}">{$text.$selected_language.links.start_now->text}</a></span>
		<div style="height:14px;"></div>
	{/if}

{/if}

{if ($c && !$c->past)}

	<div id="confirm_area">

	<input type="hidden" id="selected_option" value="1">

	{if ($c->status_en == 'proposed' or $c->status_en == 'amended')}

		<div id="protime1">
		{if ($c->protime2 != "0000-00-00 00:00:00")}
			{$text.$selected_language.forms.select_option_prompt->text}
			<div style="height:14px;"></div>
			<span class="highlight">{$text.$selected_language.words.option->text|ucfirst} 1:</span> {if ($me->id != $c->updated_by)}<span class="select_option clickable" id="option1">{$text.$selected_language.forms.select->text}</span>{/if}<br>
		{/if}
		{$text.$selected_language.statuses.{$c->status_en}->text|ucfirst}: {$c->my_protime1|date_format:"%Y-%m-%d"} {$c->my_protime1|date_format:"%H:%M"} | {$text.$selected_language.words.mode->text}: <span class="highlight">{$c->mode}</span>
		</div>

		{if ($c->protime2 != "0000-00-00 00:00:00")}
			<div id="protime2">
			<span class="highlight">{$text.$selected_language.words.option->text|ucfirst} 2:</span> {if ($me->id != $c->updated_by)}<span class="select_option clickable" id="option2">{$text.$selected_language.forms.select->text}</span>{/if}<br>
			{$text.$selected_language.statuses.{$c->status_en}->text|ucfirst}: {$c->my_protime2|date_format:"%Y-%m-%d"} {$c->my_protime2|date_format:"%H:%M"}  | {$text.$selected_language.words.mode->text}: <span class="highlight">{$c->mode}</span>
			</div>
		{/if}

	{else}

		{$text.$selected_language.statuses.{$c->status_en}->text|ucfirst}: {$c->start|date_format:"%Y-%m-%d"} {$c->start|date_format:"%H:%M"}  | {$text.$selected_language.words.mode->text}: <span class="highlight">{$c->mode}</span>

	{/if}

	<div style="height:14px;"></div>

	{if ($me->id == $c->user1)}
		{if {$c->user2_message|strlen} > 0}
		    {$text.$selected_language.words.message->text|ucfirst}:<br>
	    {/if}
		{$c->user2_message}
	{else}
		{if {$c->user1_message|strlen} > 0}
		    {$text.$selected_language.words.message->text|ucfirst}:<br>
	    {/if}
		{$c->user1_message}
	{/if}

	{if ($c->status_en != 'declined' && $c->status_en != 'cancelled')}

		<div style="height:14px;"></div>
		{$text.$selected_language.forms.reminder->text}<br>

		{if ($c)}
			{if ($me->id != $c->updated_by && $c->status_en != 'confirmed')}
				{if ($me->id == $c->user1)}
					{$r = $c->user1_remind}
					<input type="hidden" id="user2_c_remind" name="user2_c_remind" value="{$c->user2_remind}">
					<select name="user1_c_remind" id="user1_c_remind">
				{else}
					{$r = $c->user1_remind}
					<input type="hidden" id="user1_c_remind" name="user1_c_remind" value="{$c->user1_remind}">
					<select name="user2_c_remind" id="user2_c_remind">
				{/if}

				{section name=remind start=10 loop=130 step=10}
				<option value="{$smarty.section.remind.index}" {if ($smarty.section.remind.index == $r)}selected{/if}>{$smarty.section.remind.index}
				{/section}
				</select>
				{$text.$selected_language.forms.reminder_label->text}

			{else}
			
				{if ($me->id == $c->user1)}
					{$c->user1_remind}
				{else}
					{$c->user2_remind}
				{/if}
				{$text.$selected_language.forms.reminder_label->text}

			{/if}
		{/if}

		<div style="height:14px;"></div>
		{if ($c->updated_by == $me->id)}
			{if ($c->updated_by == $c->user1)}
				{$u = $c->user2_nickname}
			{else}
				{$u = $c->user1_nickname}
			{/if}
			{if ($c->status_en != 'confirmed')}
				{$text.$selected_language.messages.awaiting_review_by->text} {$u}
			{/if}
		{else}
			{if ($c->status_en != 'confirmed')}
				{$text.$selected_language.forms.proposed_conversation_message->text}<br>
				<textarea name="confirm_message" id="confirm_message" style="width:300px;"></textarea>
					<div style="height:14px;"></div>
	
			{/if}	
				
			{if ($c->status_en != 'confirmed')}
				<input type="submit" id="conversation_decline" value="{$text.$selected_language.forms.decline_prompt->text}">
				<input type="submit" id="conversation_confirm" value="{$text.$selected_language.forms.confirm_prompt->text}" {if ($c->protime2 != "0000-00-00 00:00:00")}style="display:none"{/if}>
			{else}
				{*<input type="submit" id="conversation_cancel" value="{$text.$selected_language.forms.cancel_prompt->text}">*}
			{/if}
		{/if}
		{if !$is_mobile}
		<div style="height:14px;"></div>
		<span id="conversation_amend" class="clickable">{$text.$selected_language.forms.propose_alternative_datetime->text}</span>
		<div style="height:14px;"></div>
		{/if}


	{/if}

	</div>


{/if}

<input type="hidden" value="{if ($c)}{$c->code}{else}0{/if}" id="code">
<input type="hidden" value="{if ($c)}{$c->user2}{else}{$user->id}{/if}" id="user2">
<input type="hidden" value="{$user->nickname}" id="user2_nickname">
<input type="hidden" value="{$user->availability.data->offset}" id="user2_offset">
<input type="hidden" value="{$user->account_type}" id="user_account_type">
<input type="hidden" value="{$me->profile->account_type}" id="me_account_type">



{if !$is_mobile}

<div id="review_area" {if ($c)}style="display:none;"{/if}>

<div id="request_area">
	
	{if $c}
		{if $user->skype && $me->skype}
			<div style="height:14px;"></div>
			{$text.$selected_language.forms.conversation_mode_prompt->text}<br>
			<select name="mode" id="mode">
				<option value="skype" {if $c->mode == 'skype'}selected{/if}>Skype
				<option value="flash" {if $c->mode == 'flash'}selected{/if}>Flash
			</select>
			<div class="greybar"></div>
			<div style="height:14px;"></div>
		{else}
			<input type="hidden" name="mode" id="mode" value="flash">		
		{/if}
	{/if}

	<form id="proposed_date" name="proposed_date">
	{$text.$selected_language.forms.proposed_start1->text}<br>
	<input type="text" name="proposed_date1" id="proposed_date1" class="datefield required" value="{if ($c)}{$c->protime1|date_format:"%Y-%m-%d"}{/if}"><br>
	</form>

	{if ($c)}
		{$t = $c->protime1|date_format:"%H:%M"}
	{else}
		{if ($user->availability.data->always < 1)}
			{$t = $user->availability.data->start1}
		{else}
			{$t = "+2 hours"|date_format:"%H:00"}
		{/if}
	{/if}

	{html_select_time hour_extra='id="p1_hour" class="time_propose"' minute_extra='id="p1_min" class="time_propose"' use_24_hours=true display_seconds=false time="{$t}" field_array=""}

	{$dif = $user->availability.data->offset - $me->profile->availability.data->offset}

	({$smartyutil->timezone_diff({{$t}},{{$dif}},1)} <span style="display:none;" id="p1_nextday">{$text.$selected_language.messages.next_day->text}</span>
	<span style="display:none;" id="p1_previousday">{$text.$selected_language.messages.previous_day->text}</span> {$text.$selected_language.messages.in_your_zone->text})<br>
	<span style="display:none;" id="p1_invalid_time">{$text.$selected_language.messages.invalid_time->text}</span>
	<span style="display:none;" id="p1_past_time">{$text.$selected_language.messages.past_time->text}</span>

	<div style="height:14px;"></div>
	{$text.$selected_language.words.or->text}		
	<div style="height:14px;"></div>

	{$text.$selected_language.forms.proposed_start2->text}<br>
	<input type="text" name="proposed_date2" id="proposed_date2" class="datefield" value="{if ($c)}{if ($c->protime2 != "0000-00-00 00:00:00")}{$c->protime2|date_format:"%Y-%m-%d"}{/if}{/if}"><br>

	{if ($c)}
		{if ($c->protime2 != "0000-00-00 00:00:00")}
			{$t = $c->protime2|date_format:"%H:%M"}
		{else}
			{$t = $user->availability.data->start1}
		{/if}
	{else}
		{if ($user->availability.data->always < 1)}
			{if ($user->availability.data->start2 != "00:00:00")}
				{$t = $user->availability.data->start2}
			{else}
				{$t = $user->availability.data->start1}
			{/if}
		{else}
			{$t = "+2 hours"|date_format:"%H:00"}

		{/if}
	{/if}

	{html_select_time hour_extra='id="p2_hour" class="time_propose"' minute_extra='id="p2_min" class="time_propose"' use_24_hours=true display_seconds=false time="{$t}" field_array=""}

	{$dif = $user->availability.data->offset - $me->profile->availability.data->offset}

	({$smartyutil->timezone_diff({{$t}},{{$dif}},2)} <span style="display:none;" id="p2_nextday">{$text.$selected_language.messages.next_day->text}</span>
	<span style="display:none;" id="p2_previousday">{$text.$selected_language.messages.previous_day->text}</span> {$text.$selected_language.messages.in_your_zone->text})<br>
	<span style="display:none;" id="p2_invalid_time">{$text.$selected_language.messages.invalid_time->text}</span>
	<span style="display:none;" id="p2_past_time">{$text.$selected_language.messages.past_time->text}</span>

	<input type="hidden" id="dif" value="{$dif}">

	<div style="height:14px;"></div>
	{$text.$selected_language.forms.reminder->text}<br>

	{if ($c)}
		{if ($me->id == $c->user1)}
			{$r = $c->user1_remind}
			<input type="hidden" id="user2_remind" name="user2_remind" value="{$c->user2_remind}">
			<select name="user1_remind" id="user1_remind">
		{else}
			{$r = $c->user1_remind}
			<input type="hidden" id="user1_remind" name="user1_remind" value="{$c->user1_remind}">
			<select name="user2_remind" id="user2_remind">
		{/if}
	{else}
		{$r = 20}
		<input type="hidden" name="user2_remind" value="{$r}">
		<select name="user1_remind" id="user1_remind">
	{/if}

	{section name=remind start=10 loop=130 step=10}
	<option value="{$smarty.section.remind.index}" {if ($smarty.section.remind.index == $r)}selected{/if}>{$smarty.section.remind.index}
	{/section}
	</select>
	{$text.$selected_language.forms.reminder_label->text}

	<div style="height:14px;"></div>
	{$text.$selected_language.forms.proposed_conversation_message->text}<br>
	<textarea name="request_message" id="request_message" style="width:300px;"></textarea>

	<div style="height:14px;"></div>

	<input type="submit" id="conversation_request" value="{$text.$selected_language.forms.request_prompt->text}">
	{if $c}
		<input type="submit" id="conversation_cancel" value="{$text.$selected_language.forms.cancel_prompt->text}">
	{/if}


</div>

</div>

{/if}

<div id="response_area" style="display:none;"></div>

<div class="hidden_block" id="stccalendar">
{foreach item=v key=k from=$text.$selected_language.calendar}
	<input type="hidden" id="{$v->text_tag}" value="{$v->text}">
{/foreach}
</div>
