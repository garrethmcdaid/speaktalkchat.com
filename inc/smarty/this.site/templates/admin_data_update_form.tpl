	<form name="add_user_data_form" id="add_user_data_form" method="post">

	Field tag:<br>
	<input type="text" name="field[tag]" id="tag" value="{if $field}{$field->tag}{/if}"><br>

	{if $field}
	<input type="hidden" name="field_id" value="{$field->id}">
	{/if}

	Display caption:<br>
	<input type="text" name="field[caption]" id="tag" value="{if $field}{$text.$selected_language.user_data.{$field->tag}->text}{/if}"><br>

	Account type:<br>
	{html_options id=account name="field[account]" options=$account_types selected={$field->account}}
	<br>	

	Entry method:<br>
	{html_options id=entry_method name="field[entry_method]" options=$entry_method_select selected={$field->entry_method}}
	<br>	

	{if !empty($field->options)}
	<div id="entry_method_options">
	{else}
	<div id="entry_method_options" style="display:none;">
	{/if}
		<div id="entry_method_options_list">
			{if isset($field->options)}
				{if $field->options|is_array}
					{foreach item=v key=k from=$field->options}
						<div id="option_{$k}">
						<span id="option_text">Option: </span><input type="text" name="field[options][{$k}]" id="entry_method_options_{$k}" class="entry_method_option" value="{$text.$selected_language.user_data_op.{$v}->text}">
						<a class="clickable delete_option" onClick="js_deleteOption({$k});">delete</a>
						<br id="option_nl">
						</div>
					{/foreach}
					<input type="hidden" name="field[deleted_options]" id="deleted_options" value="">
				{/if}
			{else}
			<span id="option_text">Option: </span> <input type="text" name="field[options][0]" id="entry_method_options_0" class="entry_method_option"><br id="option_nl">
			Option: <input type="text" name="field[options][1]" id="entry_method_options_1" class="entry_method_option"><br>
			{/if}

		</div>
		<span class="clickable" id="user_data_add_input" onClick="js_addInput('entry_method_options_list');">Add option</span><br>
	</div>

	Data type:<br>
	{html_options name="field[data_type]" options=$data_type_select selected={$field->data_type}}
	<br>	

	{if $field}
		{$p = $field->priority}
	{else}
		{$p = 10}
	{/if}

	Priority order:<br>
	{html_options name="field[priority]" options=$priority selected={$p}}
	<br>	

	Entry required:<br>
	{if $field}
		{html_radios name="field[required]" options=$default_radios selected={$field->required}}
	{else}
		{html_radios name="field[required]" options=$default_radios selected=0}
	{/if}
	<br>

	Unique required:<br>
	{if $field}
		{html_radios name="field[uniq]" options=$default_radios selected={$field->uniq}}
	{else}
		{html_radios name="field[uniq]" options=$default_radios selected=0}
	{/if}
	<br>

	Include in User search function:<br>
	{if $field}
		{html_radios name="field[search]" options=$default_radios selected={$field->search}}
	{else}
		{html_radios name="field[search]" options=$default_radios selected=0}
	{/if}
	<br>

	Hidden from other users:<br>
	{if $field}
		{html_radios name="field[hidden]" options=$default_radios selected={$field->hidden}}
	{else}
		{html_radios name="field[hidden]" options=$default_radios selected=0}
	{/if}


	<div style="height:20px;"></div>

	<input type="submit" name="add_user_data" value="{if $field}Update{else}Add{/if}"><br>

	{if ($message)}
		<b>{$message}</b>
	{/if}		

	</form>

