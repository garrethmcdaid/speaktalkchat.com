{include file="admin_header.tpl"}

<div id='admin_container'>

	<b>Dashboard</b><br>
	<div class="greybar"></div>
	<div style='height:14px;'></div>

	<div style="float:left;height:auto;width:400px;">
	<b>Current Statistics</b><br>
	<div class="greybar"></div>
	<div style='height:20px;'></div>
	<b>Total number of registered users:</b> {$s.total_users}<br>
	<b>Total number of registered users with Skype:</b> {$s.total_users_skype}<br><br>

	<b>Total number of registered users in the last 7 days:</b> {$s.total_users_7_days}<br>
	<b>Total number of users currently logged in:</b> {$s.total_users_logged_in}<br><br>
	
	<b>Conversations scheduled in the next 24 hours:</b> {$s.conversations_next_24_hours}<br>
	<b>Conversations scheduled in the next 24 hours (Skype):</b> {$s.conversations_next_24_hours_skype}<br><br>

	<b>Conversations scheduled in the next 4 weeks:</b> {$s.conversations_next_4_weeks}<br>
	<b>Conversations scheduled in the next 4 weeks (Skype):</b> {$s.conversations_next_4_weeks_skype}<br><br>

	<b>Conversations held in the last 24 hours:</b> {$s.conversations_last_24_hours}<br>
	<b>Conversations held in the last 24 hours (Skype):</b> {$s.conversations_last_24_hours_skype}<br><br>

	<b>Conversations held in the last 4 weeks:</b> {$s.conversations_last_4_weeks}<br>
	<b>Conversations held in the last 4 weeks (Skype):</b> {$s.conversations_last_4_weeks_skype}<br><br>
	
	<b>Conversations currently in progress:</b> {$s.conversations_current}<br>
	<b>Conversations currently in progress (Skype):</b> {$s.conversations_current_skype}<br><br>
	

	</div>

	<div style="float:left;height:auto;width:500px;margin-left:40px;">
	<b>Feedback Reports</b><br>
	<div class="greybar"></div>
	<div style='height:14px;'></div>

	<table border="0" cellpadding="0" cellspacing="0" class="inbox">

		{foreach item=v key=k from=$reports}
		<tr id="{$v->id}">
			<td>
			<span style="display:none;" id="sent_date_{$v->id}">{$v->sent|date_format:"%H:%M, %b %e, %Y"}</span>
			<a href="/admin/messages/m/{$v->id}">
			{if ($v->today)}
				{$v->sent|date_format:"%H:%M"}
			{else}
				{$v->sent|date_format:"%b %e, %Y"}
			{/if}
			</a>
			</td>
			<td>
			<span id="sender_{$v->id}"><span class="highlight">Sender: </span>{$v->sender_name}</span> | <span class="highlight">Respondent: </span> {$v->res_name}
			</td>
		</tr>
		{/foreach}
	</table>

	</div>
	<div style='height:20px;clear:both;'></div>

	<div style="float:left;height:auto;width:400px;">
	<b>Recent Conversations</b><br>
	<div class="greybar"></div>
	<div style='height:14px;'></div>

	{foreach item=v key=k from=$recents}

		{$v->started}: <a href="/admin/conversation/{$v->code}">{$v->code}</a> ({$smartyutil->sec_convert($v->duration)})<br>

	{/foreach}	

	</div>
	<div style='height:20px;clear:both;'></div>


</div>

<div style='height:20px;'></div>

{if isset($process_message)}
	{$process_message}
{/if}


{include file="admin_footer.tpl"}
