{include file="header.tpl"}

{if $content->static_title_on > 0}

<h2>{$content->static_title}</h2>

{/if}

<div class="static_content">

{$content->static_content}

</div>

<div class="hidden_block">
{foreach item=v key=k from=$text.$selected_language.messages}
	<input type="hidden" id="{$v->text_tag}" value="{$v->text}">
{/foreach}
</div>

{include file="footer.tpl"}
