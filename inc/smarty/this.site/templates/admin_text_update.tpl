{include file="admin_header.tpl"}

<div id='admin_container'>

<b>Text translations</b><br>
<div class="greybar"></div>
<div style='height:20px;'></div>

<b>Add a new text</b><br>

<div class="greybar"></div>

<div style="float:left;width:380px;height:auto;margin-right:36px;">

<form id='admin_text_add' name='admin_text_add' method='post' class="validation_required">

<b>Group:</b><br>
{if ($text['en'])}
	<select name="text_group">
	<option value=''>Select:
	{foreach item=g key=id from=$text['en']}
		<option value='{$id}'>{$id}
	{/foreach}
	</select><br>
{/if}

New text group:<br>
<input type="text" name="new_text_group" id="new_text_group">

<div style='height:20px;'></div>

<B>Description:</B><br>
<input type='text' name='text_description' style="width:200px;" value='' class="required"><br>
<B>Tag:</B><br>
<input type='text' name='text_tag' style="width:200px;" value='' class="required"><br>

<div style='height:32px;'></div>

<B>Text:</B><br>
<textarea class="mceNone" name="text" style="width:380px;height:150px;padding:2px;" class="required"></textarea>

<div style='height:20px;'></div>

<input type='submit' name='add_text' value='Save'>

</form>

<div style='height:20px;'></div>

<b>Update an existing text</b><br>

<div class="greybar"></div>

<form id='admin_text_update' name='admin_text_update' method='post'>

Reference Language:<br>
<select name='ref_language_suffix' style='width:200px;'>

	{foreach item=l key=id from=$all_language}

		{if ($ref_language_suffix == $id)}
			<option value='{$id}' selectED>{$l->language_name}
		{else}
			<option value='{$id}'>{$l->language_name}
		{/if}

	{/foreach}

</select>
<div style='height:20px;'></div>

Translation Language:<br>
<select name='trans_language_suffix' style='width:200px;'>

	{foreach item=l key=id from=$all_language}

		{if ($trans_language_suffix == $id)}
			<option value='{$id}' selected>{$l->language_name}
		{else}
			<option value='{$id}'>{$l->language_name}
		{/if}

	{/foreach}

</select>
<div style='height:20px;'></div>

<INPUT TYPE='submit' name='update' value='Select'>

</div>
<div style="float:left;width:auto;height:auto;margin-right:16px;">


{if ($ref_language_suffix)}

{if ($req_tag)}

	<B>Edit your text below</B>
	<div style='height:16px;'></div>

	<B>Text group:</B><br>
	{$text.$ref_language_suffix.$req_group.$req_tag->text_group}<br><br>
	<B>Description:</B><br>
	{$text.$ref_language_suffix.$req_group.$req_tag->text_description}<br><br>
	<B>Tag:</B><br>
	{$text.$ref_language_suffix.$req_group.$req_tag->text_tag}<br><br>
	<B>Text to translate:</B><br>
	{$text.$ref_language_suffix.$req_group.$req_tag->text}
	<input type="hidden" name="text_id" value="{$text.$trans_language_suffix.$req_group.$req_tag->text_id}">

	<div style='height:20px;'></div>

	<div id='texts_text'>
		<TEXTAREA style='width:380px;height:150px;padding:2px;' CLASS='{$text.$trans_language_suffix.$req_group.$req_tag->text_type}' name='text'>{$text.$trans_language_suffix.$req_group.$req_tag->text}</TEXTAREA>
	</div>

	<div style='height:20px;'></div>

	<INPUT TYPE='submit' name='update_text' value='Save'>

	<div style="height:16px;"></div>

{/if}

<b>Select text from text to edit</b><br>

{foreach item=g key=id from=$text[$ref_language_suffix]}
	{$id}<br>
	<select id="text_group_select_{$id}" class="text_group_select" name='req_tag[{$id}]'">
	<option value='0'>Select:
	{foreach item=t key=id from=$g}
		{if ($t->text_tag == $req_tag)}
			<option value='{$t->text_tag}' selected>{$t->text_id}:{$t->text_description}
		{else}
			<option value='{$t->text_tag}'>{$t->text_id}:{$t->text_description}
		{/if}
	{/foreach}
	</select><br>
{/foreach}

<div style='height:20px;'></div>

{/if}

<div class="hidden_block">
{foreach item=v key=k from=$text.$selected_language.messages}
	<input type="hidden" id="{$v->text_tag}" value="{$v->text}">
{/foreach}
</div>

</div>

</div>

<br style='clear:both;'>

{include file="admin_footer.tpl"}
