{include file="admin_header.tpl"}

<div id='admin_container'>

<b>Ad management</b><br>
<div class="greybar"></div>
<div style='height:20px;'></div>

<b>Upload an asset</b>
<div class="greybar"></div>
<div style='height:10px;'></div>

<form name="ad_asset_upload" method="post" enctype="multipart/form-data">

<div style="float:left;width:540px;height:auto;margin-right:0px;">
	Select file:<br>
	<input type="file" name="asset" class="required">
	<div style='height:20px;'></div>
	<input type="submit" name="ad_asset_submit" id="ad_asset_submit" value="Upload">
	</form>
</div>
<div style="float:left;width:540px;height:auto;margin-left:30px;">
	<b>Current assets:</b><br>
	<select name="select_asset" id="select_asset">
	<option value="">Select asset:
	{foreach item=v key=k from=$assets}
		{if ($v == $fn)}
			<option id="asset_{$k}" asset="{$v}" selected>{$v}
		{else}
			<option id="asset_{$k}" asset="{$v}">{$v}
		{/if}
	{/foreach}
	</select>
	<div style='height:10px;'></div>
	Link for this asset (copy and paste where required):<br>
	<input type="text" id="asset_link" style="width:240px;" value="" onClick="$(this).select();">
	{if ($fn)}<script>$("#asset_link").val('<img src="/images/ads/{$fn}">');</script>{/if}
	<div id="asset_display">{if ($fn)}<img src="/images/ads/{$fn}">{/if}</div>
</div>

<div style='height:20px;clear:both;'></div>

<b>Current ads</b>
<div class="greybar"></div>

{foreach item=v key=k from=$ads}

	<form name="ad_update_{$k}_form" id="ad_update_{$v->id}_form" method="post">

		<div style="float:left;width:520px;height:auto;margin-right:0px;display:inline-block;">

		<input type="hidden" name="ad[id]" id="ad_id_{$v->id}" value="{$v->id}">

		<b>{$v->ad_tag}</b>

		<div style='height:10px;'></div>

		Width:{$v->ad_width}px Height:{$v->ad_height}px

		<div style='height:10px;'></div>

		<textarea class="tinymce" style="height:130px;width:520px !important;" name="ad[ad_content]" id="ad_content_{$v->id}">{$v->ad_content|stripslashes}</textarea>

		<div style='height:10px;'></div>

		<select name="ad[ad_status]" id="ad_status_{$v->id}" style="width:60px;">

			{if ($v->ad_status > 0)}
				<option value="0">Off
				<option value="1" selected>On
			{else}
				<option value="0" selected>Off
				<option value="1">On
			{/if}

		</select>

	</div>

	<div style="float:left;width:340px;height:auto;margin-left:20px;">

		<b>Displayed as:</b>

		<div style='height:10px;'></div>

		{$v->ad_content}

	</div>

	<div style='height:10px;clear:both;'></div>

	<input type="submit" name="ad_update_submit" id="ad_input_submit_{$v->id}" value="Update">

	</form>
	<div class="greybar"></div>

{/foreach}

{include file="admin_footer.tpl"}
