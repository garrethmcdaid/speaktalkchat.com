{include file="header.tpl"}

<h3>{$text.$selected_language.headings.faq_heading->text}</h3>

<div style="height:16px;"></div>

<div class="static_content">

{foreach item=v key=k from=$categories}

	<b><span class="highlight">{$v}</span></b>

	<div class="greybar"></div>

	{foreach item=vv key=kk from=$faq}

		{if ($data.$kk == $k)}

			<div id="{$vv.tag}" class="reveal {$vv.tag}_answer slideDown clickable"><b>{$vv.q|nl2br}</b></div>
			<div style="height:10px;"></div>
			<div id="{$vv.tag}_answer" style="display:none;" class="highlight">{$vv.a|nl2br}</div>
			<div style="height:10px;"></div>

		{/if}

	{/foreach}

{/foreach}

</div>

{include file="footer.tpl"}
