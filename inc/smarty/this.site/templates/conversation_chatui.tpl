<script type="text/javascript" src="/inc/js/swfobject/swfobject.js"></script>

<input type="hidden" id="code" value="{$c->code}">
<input type="hidden" id="viewer" value="{$c->viewer}">
<input type="hidden" id="language" value="{$c->language}">
<input type="hidden" id="isfirst" value="{$c->isfirst}">
<input type="hidden" id="timeout" value="0">

<div id="wait_message" style="display:none;font-size:1.2em;text-align:center;">
	<div style="height:14px;"></div>
	{$text.$selected_language.messages.wait_other_user->text}
	</div>
<div id="cancel_message" style="display:none;text-align:center;">
	<div style="height:14px;"></div>
	{$text.$selected_language.messages.user_not_joined->text}
	<div style="height:14px;"></div>
	<a onclick="opener.location.href='/user/dashboard';window.close( );" href="#">{$text.$selected_language.links.conversation_exit->text}</a>
</div>

<div id="flash_interface">

	<div id="flash_screen" style="height:auto;width:508px;margin:4px 4px 14px 4px;">

		<div id="flashcontent" style="display:none;">
			<p>
				To view this page ensure that Adobe Flash Player version 11.2.0 or greater is installed.
				</p>
		</div>

	</div>

	<div id="flash_extra" style="height:auto;width:516px;border:0px solid #e0e0e0;display:none;text-align:center;">

	<span style="font-size:1.4em"><a onclick="opener.location.href='/start/review/{$c->code}';window.close( );" href="#">{$text.$selected_language.links.conversation_exit->text}</a></span>

	</div>

</div>
<script type="text/javascript">js_checkChat();</script>

