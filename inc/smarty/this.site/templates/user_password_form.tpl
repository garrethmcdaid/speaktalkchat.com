<form name="user_password_form" id="user_password_form" method="post" class="validation_required" action="/user/password/reset">

{if ($new)}

	{$text.$selected_language.forms.password_reset_prompt->text}
	<div style="height:14px;"></div>
	{$text.$selected_language.forms.email_prompt->text}<br>
	<input type="text" name="me[email]" id="email_address" class="required email" style="width:200px;"> 

	<div style="height:14px;"></div>

	{$text.$selected_language.forms.captcha_prompt->text}<br>
	<img src='/images/captcha/{$captchaimage}'><br>
	<input type="text" name="me[captcha]" id="captcha" class="required" style="width:200px;"><br>
	<input type="hidden" name="me[hash]" id="hash" value="{$captchahash}"/>

	<div style="height:14px;"></div>

	<input type="submit" name="submit" value="{$text.$selected_language.forms.submit_prompt->text}">

	</form>

{/if}

<div class="hidden_block">
{foreach item=v key=k from=$text.$selected_language.messages}
	<input type="hidden" id="{$v->text_tag}" value="{$v->text}">
{/foreach}
</div>

<div style="height:14px;"></div>

{if isset($process_message)}
	<span class="highlight">{$process_message}</span>
{/if}
