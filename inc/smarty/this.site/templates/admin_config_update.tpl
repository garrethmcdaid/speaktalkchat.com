{include file="admin_header.tpl"}

<div id='admin_container'>

	<b>Site configuration</b><br>
	<div class="greybar"></div>
	<div style='height:20px;'></div>

	<form id='settings_form' name='settings_form' method='post' action='/admin/config/update'>

	<div class='submit' style='width;auto;text-align:left;margin-left:0px;'><input type="submit" class="button-primary" name="settings_submit" id="wp-submit" VALUE="Submit"></div>

	<div style='height:20px;'></div>


	{foreach item=tag key=id from=$config}

		<b>{ucfirst($id)}</b><br>

		{foreach item=setting key=i from=$tag->data}

			{$setting->display}

			{if ($setting->input eq 'text') }
				<input name="config[{$id}][data][{$i}][value]" type="text" value="{$setting->value}">
			{elseif ($setting->input eq 'textarea') }
				<textarea name="config[{$id}][data][{$i}][value]">{$setting->value}</textarea>
			{/if}
			<br>

		{/foreach}

	{/foreach}

	<div style='height:20px;'></div>

	<div class='submit' style='width;auto;text-align:left;margin-left:0px;'><input type="submit" class="button-primary" name="settings_submit" id="wp-submit" VALUE="Submit"></div>

	</form>

</div>

{include file="admin_footer.tpl"}
