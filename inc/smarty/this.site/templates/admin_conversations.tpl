{include file="admin_header.tpl"}

<h3>{$text.$selected_language.headings.user_conversations->text}: <a href="/admin/user/profile/{$nickname}" class="highlight">{$nickname}</a></h3>

{if (empty($code))}

	<span class="clickable" id="today_filter">{$text.$selected_language.links.show_today_only->text}</span> | <span class="clickable" id="not_today_filter">{$text.$selected_language.links.show_all->text}</span>

	<div style="height:14px;"></div>

{/if}

<input type="hidden" id="user_id" value="{$path.2}">

<div class="conversations_pagination_t"></div>
<div style="height:14px;"></div>
<div id="conversation_results"></div>
<div class="conversations_pagination_b"></div>

<input type="hidden" id="highlight" value="{$code}">

<script>js_conversations(1);</script>

{if isset($process_message)}
	{$process_message}
{/if}

<br>

{include file="admin_footer.tpl"}
