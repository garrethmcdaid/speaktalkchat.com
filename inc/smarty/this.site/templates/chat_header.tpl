<!DOCTYPE html> 
<html lang="en"> 
<head> 
<meta charset="utf-8" />
<title>{$text.$selected_language.info.site_title->text}</title>
<meta name="description" content="{$text.$selected_language.info.meta_tag_description->text}"/>

{if ($js)}
{foreach item=file key=id from=$js}
<script src="/inc/js/{$file}" language="javascript" type="text/javascript"></script>
{/foreach}
{/if}

{if ($css)}
{foreach item=file key=id from=$css}
<link href="/inc/css/{$file}" type="text/css" rel="stylesheet">
{/foreach}
{/if}

</head>

<body style="background:#ffffff;">
