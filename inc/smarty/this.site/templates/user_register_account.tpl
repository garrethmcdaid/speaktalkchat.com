{include file="header.tpl"}

{if !$u18}

	{if ($account == 'adult' or $account == 'family' or $account == 'educator')}
		<div class="fb-login-button" scope="email,user_birthday">{$text.$selected_language.forms.login_with_facebook->text}</div><br>
		<span class="highlight" style="font-size:0.8em;">{$text.$selected_language.messages.skype_facebook_info->text}</span>
		<div style="height:10px;"></div>
		<div class="greybar"></div>
		<div style="height:10px;"></div>
	{/if}

{/if}

{include file="user_register_form.tpl"}

<div style="height:10px;"></div>

{if isset($process_message)}
	<div class="highlight">
	{$process_message}
	</div>
{/if}



{include file="footer.tpl"}
