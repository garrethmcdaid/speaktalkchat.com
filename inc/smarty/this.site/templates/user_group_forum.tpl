<div class="dir_header"><a href="/user/groups/" style="color:#ffffff;">{$text.$selected_language.links.all_groups->text}</a> - <a href="/user/groups/members/{$group}" style="color:#ffffff;">{$groupdata->group_name}</a> - {$text.$selected_language.links.forum->text}</div>
<div style="height:8px;"></div>

<div class="discussion_summary">

	<div class="post_header" style="margin-bottom:0px;">
		<div id="poster_discussion_new" style="width:auto;float:left;" class="reveal new_discussion_form slideDown clickable_nd"><b>{$text.$selected_language.headings.create_new_discussion->text}</b></div>
		<div style="margin-left:100px;text-align:right;"></div><br>
	</div>
	<div id="new_discussion_form" style="display:none;">
		<b>{$text.$selected_language.forms.subject_prompt->text}:</b>
		<input type="text" style="width:99%;padding:0.5%;" name="new_discussion_subject" id="new_discussion_subject"><br>
		<b>{$text.$selected_language.forms.enter_post_prompt->text}:</b><br>
		<textarea name="post_content_new" id="post_content_new" style="width:99%;padding:0.5%;height:120px;" class="post_reply_text"></textarea>
	
		<div style="text-align:right;">
			<input type="button" id="submit_discussion" group="{$group}" value="{$text.$selected_language.forms.create_prompt->text}">
		</div>
	</div>
	

</div>

<div id="discussion_list_pagination_t" style="text-align:right;"></div>
<div style="height:8px;"></div>

<div class="discussion_summary">
	<b>
	<div class="discussion_summary_col" style="width:540px;padding-left:0px;">{$text.$selected_language.headings.subject_heading->text}</div>
	<div class="discussion_summary_col" style="width:70px;">{$text.$selected_language.headings.posts_heading->text}</div>
	<div class="discussion_summary_col" style="width:140px;">{$text.$selected_language.headings.created_by->text}</div>
	<div class="discussion_summary_col" style="width:140px;">{$text.$selected_language.headings.last_post->text}</div>
	<br style="clear:both;">
	</b>

</div>

<div id="discussion_list"></div>

<div style="height:8px;"></div>
<div id="discussion_list_pagination_b" style="text-align:right;"></div>
<script>js_getDiscussions({$group},1);</script>
