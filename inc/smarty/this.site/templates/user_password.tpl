{include file="header.tpl"}

<h3>{$text.$selected_language.headings.password_reset_heading->text}</h3>

{include file="user_password_form.tpl"}

{include file="footer.tpl"}
