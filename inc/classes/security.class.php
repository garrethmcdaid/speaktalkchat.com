<?php

//##################################################################################
//START OF CLASS
//##################################################################################

class SECURITY {


//##################################################################################
//START SECURE SESSION
//##################################################################################
function session($l=1) {

	session_start();

	//ANTI-FIXATION MECHANISM
	if (!isset($_SESSION['initiated'])) {
		session_regenerate_id();
		$_SESSION['initiated'] = true;
	}

	//BASIC ANTI-HIJACK METHOD
	if (isset($_SESSION['HTTP_USER_AGENT'])) {
		if ($_SESSION['HTTP_USER_AGENT'] != md5($_SERVER['HTTP_USER_AGENT'])) {
			die();
		} else {
			$_SESSION['HTTP_USER_AGENT'] = md5($_SERVER['HTTP_USER_AGENT']);
		}
	}
}
//##################################################################################
//CAPTCHA
//##################################################################################
function do_captcha() {
	global $DB, $smarty, $config;

	$md5 = md5(microtime() * mktime());
	$string = substr($md5,0,10);
	$string = str_replace("0","7",$string);
	$string = str_replace("o","p",$string);
	$string = str_replace("O","P",$string);

	$ir = rand(1,100);

	$i = "captcha_user_" . $ir . ".png";
	$image_filename = $_SERVER['DOCUMENT_ROOT'] ."/images/captcha/" . $i;
	$font = $_SERVER['DOCUMENT_ROOT'] . "/inc/font/" . $config['font']->data->font_file->value;

	$captcha = imagecreatefrompng($_SERVER['DOCUMENT_ROOT'] . "/images/captcha/captcha.png");
	$fc = imagecolorallocate($captcha, 255, 255, 255);
	$line = imagecolorallocate($captcha,255,255,255); 
	imageline($captcha,0,0,39,29,$fc);
	imageline($captcha,40,0,64,29,$fc);
	imageline($captcha,10,0,29,42,$fc);
	imageline($captcha,90,0,87,31,$fc);
	imageline($captcha,60,10,57,61,$fc);


	$l = rand(2,40);
	$b = rand(15,20);

	imagettftext($captcha,15,0,$l,$b,$fc,$font,$string);

	$u = $_SERVER['HTTP_USER_AGENT'] . $_SERVER['REMOTE_ADDR'] . rand(1,5000);
	$hash = md5($u);

	$q = "INSERT INTO captcha (code,hash) values ('" . $string . "','" . $hash . "')";
	$result = $DB->query($q); 

	$this->wave_area($image_filename,$captcha,0,0,140,26);

	$smarty->assign("captchahash",$hash);
	$smarty->assign("captchaimage",$i);

}

function wave_area($image_filename, $img, $x, $y, $width, $height, $amplitude = 8, $period = 10){

	// Make a copy of the image twice the size

	$height2 = $height * 2;

	$width2 = $width * 2;

	$img2 = imagecreatetruecolor($width2, $height2);

	imagecopyresampled($img2, $img, 0, 0, $x, $y, $width2, $height2, $width, $height);

	if($period == 0) $period = 1;

	// Wave it

	for($i = 0; $i < ($width2); $i += 2)

	imagecopy($img2, $img2, $x + $i - 2, $y + sin($i / $period) * $amplitude, $x + $i, $y, 2, $height2);

	// Resample it down again

	imagecopyresampled($img, $img2, $x, $y, 0, 0, $width, $height, $width2, $height2);

	imagepng($img,$image_filename);

	imagedestroy($img2);

} 

//##################################################################################
//END OF CLASS
//##################################################################################
}

//##################################################################################
//CONSTRUCT
//##################################################################################

$SECURITY = new SECURITY();

?>
