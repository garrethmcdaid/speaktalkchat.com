<?php

//##################################################################################
//START
//##################################################################################

class GROUP {

##################################################################################
//DELETE A GROUP MEMBER
//##################################################################################
function delete_member() {
	global $DB;

	$s = json_decode(stripslashes($_REQUEST['json']));

	$q = "SELECT id FROM groups WHERE id = " . $s->group_id . " AND group_owner = " . $_SESSION['me']->id;
	$group_id = $DB->get_var($q);

	if ($group_id) {

		$q = "DELETE FROM user_to_group WHERE user_id = " . $s->user_id . " AND group_id = " . $s->group_id;
		$DB->query($q);

		$response['data'] = $s->user_id;
		$response['flag'] = 'group_member_delete';

		echo json_encode($response); 

	}

}
//##################################################################################
//OBTAIN MESSAGES REQUIRING MODERATION
//##################################################################################
function mod_required($id=false) {
	global $DB, $smarty, $UTIL;

	$q = "SELECT DISTINCT thread,subject,group_id,group_name,sender,date_format(sent,'%Y-%m-%d') AS sent FROM message LEFT JOIN groups ON groups.id = message.group_id WHERE message.group_owner = " . $_SESSION['me']->id . " AND moderated < 1 ORDER BY sent DESC";
	$mod_required = $DB->get_results($q);

	foreach ($mod_required as $k=>$v) {
		$mod_required[$k]->nickname = $UTIL->nickname_convert($v->sender);
	}

	$smarty->assign('mod_required',$mod_required);

}
//##################################################################################

//##################################################################################
//GET GROUP DATA
//##################################################################################
function groupdata($id=false) {
	global $DB;

	$q = "SELECT group_name, group_description FROM groups WHERE id = " . $id;
	return $DB->get_result($q);

}
//##################################################################################
//GROUP MEMBERS
//##################################################################################
function members($id=false,$mail=false) {
	global $DB, $UTIL, $smarty;

	$q = "SELECT group_owner FROM groups WHERE id = " . $id;
	$owner = $DB->get_var($q);

	$is_owner = false;
	if ($_SESSION['me']->id == $owner) $is_owner = true;
	$smarty->assign("is_owner",$is_owner);
	
	if ($is_owner or $mail or $this->check_membership($id)) {

		$q = "SELECT user_id,status FROM user_to_group WHERE group_id = " . $id . " AND status != 'requested' AND user_id != " . $_SESSION['me']->id;
		$members = $DB->get_results($q);

		foreach ($members as $k=>$v) {
			$members[$k]->nickname = $UTIL->nickname_convert($v->user_id);
			$members[$k]->avatar = $UTIL->avatar_exists($v->user_id);
			$members[$k]->group_id = $id;
		}

		return $members;


	} else {
		return true;
	}

}
//##################################################################################
//CHECK MEMBERSHIP OF A GROUP
//##################################################################################
function check_membership($group=false) {
	global $DB;

	$q = "SELECT user_id FROM user_to_group WHERE user_id = " . $_SESSION['me']->id . " AND group_id = " . $group;
	return $DB->get_var($q);

}
//##################################################################################
//GROUP JOIN REQUESTS
//##################################################################################
function requests() {
	global $DB, $text, $MESSAGE, $UTIL, $smarty;

	$requests = false;

	$q = "SELECT groups.*,user_to_group.id as request_id,user_to_group.user_id,user_to_group.status,user_to_group.joined FROM groups LEFT JOIN user_to_group ON groups.id = user_to_group.group_id WHERE group_owner = " . $_SESSION['me']->id . " AND user_to_group.status = 'requested'";

	$requests = $DB->get_results($q);

	foreach ($requests as $k=>$v) {

		$requests[$k]->nickname = $UTIL->nickname_convert($v->user_id);
		$requests[$k]->status_text = $text[$_SESSION['language']]['statuses']['requested']->text;
		if (date("Y-m-d",strtotime($v->joined)) == date("Y-m-d")) {	
			$requests[$k]->when = date("H:i");
		} else {
			$requests[$k]->when = date("Y-m-d");
		}

	}

	$smarty->assign("requests",$requests);

}
//##################################################################################
//GROUP ACTIONS
//##################################################################################
function action() {
	global $DB, $text, $MESSAGE, $UTIL;

	$pm = false;

	$response = array();

	$s = json_decode(stripslashes($_REQUEST['json']));

	switch ($s->action) {

		case 'join':

			$q = "INSERT INTO user_to_group (user_id,group_id,status,joined) VALUES (" . $_SESSION['me']->id . "," . $s->group_id . ",'requested',now())";
			$DB->query($q); 

			$response['data']['action'] = $s->action;
			$response['data']['group_id'] = $s->group_id;
			$response['data']['button'] = 'leave';
			$response['data']['the_message'] = $text[$_SESSION['language']]['messages']['group_joined']->text;

			//SEND PRIVATE MESSAGE
			$q = "SELECT group_owner FROM groups WHERE id = " . $s->group_id;
			$o = $DB->get_var($q);

			$ul = $UTIL->user_language($o);
			$user = $_SESSION['me']->profile->nickname;

			$link = addslashes('<a href="/user/groups/' . $s->group_id . '">' . $user . '</a>');

			$s = '{"thread":"0","recipient":"' . $o . '","sender":"' . $_SESSION['me']->id . ' ","subject":"' . $text[$ul]['messages']['group_join_request_subject']->text . '","the_message":"' . $text[$ul]['messages']['group_join_request_message']->text . "<br><br>" . $link . '"}';

			$MESSAGE->send($s);

		break;

		case 'leave':

			$q = "DELETE FROM user_to_group WHERE user_id = " . $_SESSION['me']->id . " AND group_id = " . $s->group_id;
			$DB->query($q); 

			$response['data']['action'] = $s->action;
			$response['data']['group_id'] = $s->group_id;
			$response['data']['button'] = 'join';
			$response['data']['the_message'] = $text[$_SESSION['language']]['messages']['group_left']->text;

		break;

		case 'invite':

			$response['data']['action'] = $s->action;
			$response['data']['group_id'] = $s->group_id;
			$response['data']['the_message'] = $text[$_SESSION['language']]['messages']['group_invited']->text;

			//SEND PRIVATE MESSAGE
			$q = "SELECT group_name FROM groups WHERE id = " . $s->group_id;
			$name = $DB->get_var($q);

			$ul = $UTIL->user_language($s->user_id);

			$link = addslashes('<a href="/user/groups/' . $s->group_id . '">' . $name . '</a>');

			$s = '{"thread":"0","recipient":"' . $s->user_id . '","sender":"' . $_SESSION['me']->id . ' ","subject":"' . $text[$ul]['messages']['group_join_invite_subject']->text . '","the_message":"' . $text[$ul]['messages']['group_invite_message']->text . "<br><br>" . $link . '"}';

			$MESSAGE->send($s);

		break;

		case 'confirm':

			$q = "SELECT id FROM groups WHERE id = " . $s->group_id . " AND group_owner = " . $_SESSION['me']->id;
			$group_id = $DB->get_var($q);

			if ($group_id) {

				$q = "UPDATE user_to_group SET status = 'member' WHERE user_id = " . $s->user_id . " AND group_id = " . $group_id;
				$DB->query($q);

				$response['data']['action'] = $s->action;
				$response['data']['request_id'] = $s->request_id;
				$response['data']['the_message'] = $text[$_SESSION['language']]['messages']['group_join_confirmed']->text;

				//SEND PRIVATE MESSAGE
				$q = "SELECT group_name FROM groups WHERE id = " . $s->group_id;
				$name = $DB->get_var($q);

				$ul = $UTIL->user_language($s->user_id);

				#$link = addslashes('<a href="/user/groups/' . $s->group_id . '">' . $name . '</a>');
				$link = '<a href="/user/groups/' . $s->group_id . '">' . $text[$_SESSION['language']]['headings']['user_groups']->text . '</a>';
				
				$s = '{"thread":"0","recipient":"' . $s->user_id . '","sender":"' . $_SESSION['me']->id . ' ","subject":"' . $text[$ul]['messages']['group_join_update_subject']->text . '","the_message":"' . $text[$ul]['messages']['group_join_ok_message']->text . "<br><br>" . addslashes($link) . '"}';

				$MESSAGE->send($s);

			}

		break;

		case 'decline':

			$q = "SELECT id FROM groups WHERE id = " . $s->group_id . " AND group_owner = " . $_SESSION['me']->id;
			$group_id = $DB->get_var($q);

			if ($group_id) {

				$q = "DELETE FROM user_to_group WHERE user_id = " . $s->user_id . " AND group_id = " . $s->group_id;
				$DB->query($q);

				$response['data']['action'] = $s->action;
				$response['data']['request_id'] = $s->request_id;
				$response['data']['the_message'] = $text[$_SESSION['language']]['messages']['group_join_declined']->text;

				//SEND PRIVATE MESSAGE
				$q = "SELECT group_name FROM groups WHERE id = " . $s->group_id;
				$name = $DB->get_var($q);

				$ul = $UTIL->user_language($s->user_id);

				$link = addslashes('<a href="/user/groups/' . $s->group_id . '">' . $name . '</a>');

				$s = '{"thread":"0","recipient":"' . $s->user_id . '","sender":"' . $_SESSION['me']->id . ' ","subject":"' . $text[$ul]['messages']['group_join_update_subject']->text . '","the_message":"' . $text[$ul]['messages']['group_join_declined_message']->text . "<br><br>" . $link . '"}';

				$MESSAGE->send($s);

			}


		break;


	}

	$response['flag'] = 'user_group';

	echo json_encode($response); 	

}
//##################################################################################
//GET GROUPS
//##################################################################################
function get() {
	global $DB, $text;

	$response = array();

	//FOR AUTOCOMPLETE WIDGET
	if (isset($_REQUEST['name_startsWith'])) {
		$q = "SELECT groups.group_name,groups.id FROM groups WHERE group_name LIKE '" . $_REQUEST['name_startsWith'] . "%' AND (group_status = 'public' OR group_owner = " . $_SESSION['me']->id . ")";
		$r = $DB->get_results($q);

		echo json_encode($r);
		return;
	}

	$s = json_decode(stripslashes($_REQUEST['json']));

	if ($s->l == 'my') {
		$q = "SELECT groups.*,user_to_group.status FROM groups LEFT JOIN user_to_group ON groups.id = user_to_group.group_id WHERE user_to_group.user_id = " . $_SESSION['me']->id . " ORDER BY groups.group_name";
	} else if (is_numeric($s->l)) {
		$q = "SELECT * FROM groups WHERE id = " . $s->l;
	} else {
		$q = "SELECT * FROM groups WHERE group_name LIKE '" . $s->l . "%' AND (group_status = 'public' OR group_owner = " . $_SESSION['me']->id . ")";
	}

	$groups = $DB->get_results($q);

	foreach ($groups as $k=>$v) {

		$q = "SELECT count(*) FROM user_to_group WHERE group_id = " . $v->id . " AND status != 'requested'";
		$nm = $DB->get_var($q);

		$q = "SELECT status FROM user_to_group WHERE user_id = " . $_SESSION['me']->id . " AND group_id = " . $v->id;
		
		$my_status = $DB->get_var($q);

		if ($my_status and $my_status == 'owner') $groups[$k]->owner = true;

		if ($my_status) $my_status = $text[$_SESSION['language']]['statuses'][$my_status]->text;

		$groups[$k]->my_status = $my_status;
		$groups[$k]->nm = $nm;
		$groups[$k]->group_status = $text[$_SESSION['language']]['statuses'][$v->group_status]->text;
		
	}

	$response['data'] = $groups;
	$response['flag'] = 'group_results';

	echo json_encode($response); 	

}
//##################################################################################
//CREATE A GROUP
//##################################################################################
function create() {
	global $DB, $text;

	$response = array();

	$s = json_decode(stripslashes($_REQUEST['json']));

	//CHECK THAT USER IS AN ADULT ACCOUNT

	//CHECK FOR GROUP NAME
	$q = "SELECT group_name FROM groups WHERE group_name = '" . $DB->escape($s->group_name) . "'";
	$r = $DB->get_var($q);

	if ($r) {
		$response['data'] = $text[$_SESSION['language']]['messages']['group_name_exists']->text;
		$response['flag'] = 'group_not_created';
	} else {
		$q = "INSERT INTO groups (group_name,group_status,group_created,group_owner,group_description) VALUES ('" . $DB->escape($s->group_name) . "','" . $s->group_status . "',now()," . $_SESSION['me']->id . ",'" . $DB->escape($s->group_description) . "')";
		$group_id = $DB->query($q);
		$q = "INSERT INTO user_to_group (user_id,group_id,status,joined) VALUES (" . $_SESSION['me']->id . "," . $group_id . ",'owner',now())";
		$DB->query($q); 
		$response['data'] = $text[$_SESSION['language']]['messages']['group_created']->text . ": " . $s->group_name;
		$response['flag'] = 'group_created';
	}

	$response = json_encode($response);

	echo $response;

}
//##################################################################################
//GET A LIST OF DISCUSSIONS
//##################################################################################
function discussions($group_id=false,$limit=false) {
	global $DB;

	if (isset($_REQUEST['json'])) {

		$s = json_decode(stripslashes($_REQUEST['json']));

		$q = "SELECT count(*) FROM discussion WHERE group_id = " . $s->group;
		$c = $DB->get_var($q);

		if ($s->page > 1) {
			$start = ($s->page * 10) - 10;
		} else {
			$start = 0;
		}

		$q = "SELECT * FROM discussion WHERE group_id = " . $s->group . " ORDER BY last_post DESC LIMIT " . $start . ",10";
		$discussions = $DB->get_results($q);


		$response['data'] = $discussions;
		$response['data']['total'] = $c;
		$response['data']['page'] = $s->page;
		$response['data']['is_admin'] = $_SESSION['me']->is_admin;

		$response['flag'] = 'discussion_list';

		$response = json_encode($response);

		echo $response;

	} else {
	
		$q = "SELECT * FROM discussion WHERE group_id = " . $group_id . " ORDER BY last_post DESC";
		if ($limit) $q .= " LIMIT " . $limit;
		$discussions = $DB->get_results($q);

		return $discussions;

	}

}
//##################################################################################
//GET A LIST OF POSTS
//##################################################################################
function posts() {
	global $DB,$UTIL;

	$s = json_decode(stripslashes($_REQUEST['json']));

	//TOTAL POSTS
	$q = "SELECT count(*) FROM post WHERE discussion_id = " . $s->discussion . " AND status < 1";
	$c = $DB->get_var($q);

	//TOTAL POSTS UP TO THREAD NO (TO ACCOUNT FOR DELETED POSTS)
	$q = "SELECT count(*) FROM post WHERE discussion_id = " . $s->discussion . " AND status < 1 AND thread_no <=" . $s->thread_no;
	$thread_no_c = $DB->get_var($q);

	//NOTIFICATION SETTING
	$q = "SELECT user_id FROM notification WHERE purpose = 'new_post' and ref_id = " . $s->discussion;
	$notify = $DB->get_var($q);

	if ($s->page > 1) {
		$start = ($s->page * 10) - 10;
	} else {
		$start = 0;
	}

	if ($thread_no_c > 10) {
		$m = ($thread_no_c % 10);
		if ($m < 1) $m = 10;
		$start = ($thread_no_c - $m);
		$finish = $start + 10;
		$s->page = $finish/10;
	} 

	$q = "SELECT subject,status FROM discussion WHERE id = " . $s->discussion;
	$dinfo = $DB->get_result($q); 
	

	$q = "SELECT * FROM post WHERE discussion_id = " . $s->discussion . " AND status < 1 ORDER by thread_no LIMIT " . $start . ",10";

	$posts = array();

	foreach ($DB->get_results($q) as $k => $v) {

		$v->edit_content = $v->content;

		$v->reply_content = $this->strip_quote($v->content,'[quote]','[/quote]');

		//EXTRACT NAME OF QUOTE POSTER
		$qposter = $UTIL->poster_name($v->content);

		$v->content = str_replace('[quote:' . $qposter . ']','<div class="quote"><b>' . $qposter . ':</b><br>',$v->content);
		//$v->content = str_replace('[quote]','<div class="quote"><b>' . $v->poster . ':</b><br>',$v->content);
		$v->content = str_replace('[/quote]','</div>',$v->content);

		$v->content = str_replace("\n\n","<div style='height:10px;'></div>",$v->content);
		$v->content = $UTIL->auto_link_text($v->content);
		$v->content = nl2br($v->content);
		if ($v->poster_id == $_SESSION['me']->id) {
			$v->is_poster = true;
		} else {
			$v->is_poster = false;
		}
		$posts[] = $v;	

	}

	$response['data'] = $posts;
	$response['data']['thread_no'] = $s->thread_no;
	$response['data']['total'] = $c;
	$response['data']['page'] = $s->page;
	$response['data']['subject'] = $dinfo->subject;
	$response['data']['status'] = $dinfo->status;
	$response['data']['notify'] = $notify;
	$response['data']['is_admin'] = $_SESSION['me']->is_admin;
	$response['flag'] = 'post_list';

	$response = json_encode($response);

	echo $response;

}
//##################################################################################
//FLAG A POST
//##################################################################################
function flag_post() {
	global $DB,$UTIL,$MESSAGE;

	$s = json_decode(stripslashes($_REQUEST['json']));

	$q = "UPDATE post SET flagged = 1 WHERE discussion_id = " . $s->discussion_id . " AND id = " . $s->post;
	$DB->query($q);

	$response['data']['p'] = $s->post;
	$response['data']['update'] = '<img src="/images/redwarn.png" align="absbottom">';
	$response['flag'] = 'post_flagged';

	$response = json_encode($response);

	//ALERT ADMIN
	$link = addslashes('<a href="http://' . $_SERVER['SERVER_NAME'] . '/user/groups/forum/' . $s->group  . '/discussion/' . $s->discussion_id . '/' . $s->thread_no . '">Link to post</a>');

	$s = '{"thread":"0","recipient":"1","sender":"' . $_SESSION['me']->id . '","subject":"Flagged discussion post","the_message":"The following post has been flagged:<br><br>' . $link . '","admin":false}';

	$MESSAGE->send($s);

	echo $response;
		
}
//##################################################################################
//EDIT A POST
//##################################################################################
function edit_post() {
	global $DB,$UTIL,$MESSAGE;

	$s = json_decode(stripslashes($_REQUEST['json']));

	$s->content = strip_tags($s->content);

	$q = "UPDATE post SET content = '" . $s->content . "',edited = now() WHERE id = " . $s->post . " AND discussion_id = " . $s->discussion_id . " AND poster_id = " . $_SESSION['me']->id;
	$DB->query($q);

	if ($s->thread_no > 10) {
		$m = ($s->thread_no % 10);
		if ($m < 1) $m = 10;
		$start = ($s->thread_no - $m);
		$finish = $start + 10;
		$page = $finish/10;
	} else {
		$page = 1;
	}

	$response['data']['p'] = $page;
	$response['data']['d'] = $s->discussion_id;
	$response['data']['t'] = $s->thread_no;
	$response['flag'] = 'post_edited';

	$response = json_encode($response);

	echo $response;
		
}
//##################################################################################
//DELETE A DISCUSSION
//##################################################################################
function delete_discussion() {
	global $DB,$UTIL,$MESSAGE;

	$s = json_decode(stripslashes($_REQUEST['json']));

	if ($_SESSION['me']->is_admin) {

		$q = "DELETE FROM post WHERE discussion_id = " . $s->discussion_id;
		$DB->query($q);
		$q = "DELETE FROM discussion WHERE id = " . $s->discussion_id;
		$DB->query($q);

		$response['data']['g'] = $s->group;
		$response['flag'] = 'discussion_deleted';

		$response = json_encode($response);

		echo $response;

	}

		
}
//##################################################################################
//DELETE A POST
//##################################################################################
function delete_post() {
	global $DB,$UTIL,$MESSAGE;

	$s = json_decode(stripslashes($_REQUEST['json']));

	if ($_SESSION['me']->is_admin) {

		$q = "DELETE FROM post WHERE id = " . $s->post . " AND discussion_id = " . $s->discussion_id;
		$DB->query($q);
		$q = "UPDATE discussion SET no_posts = no_posts - 1 WHERE id = " . $s->discussion_id;
		$DB->query($q);

		$response['data']['p'] = $s->post;
		$response['flag'] = 'post_deleted';

		$response = json_encode($response);

		echo $response;

	}

		
}
//##################################################################################
//CHANGE NOTIFICATION SETTING FOR A DISCUSSION
//##################################################################################
function notify_post() {
	global $DB;

	$s = json_decode(stripslashes($_REQUEST['json']));

	$q = "SELECT * FROM notification WHERE user_id = " . $_SESSION['me']->id . " AND purpose = 'new_post' AND ref_id = " . $s->discussion_id;
	$n = $DB->get_result($q);

	if ($n) {
		$q = "DELETE FROM notification WHERE user_id = " . $_SESSION['me']->id . " AND purpose = 'new_post' AND ref_id = " . $s->discussion_id;
	} else {
		$q = "INSERT INTO notification (user_id,purpose,ref_id) VALUES (" . $_SESSION['me']->id . ",'new_post'," . $s->discussion_id . ")";
	}

	$DB->query($q);
		
}

//##################################################################################
//TOGGLE DISCUSSION STATUS
//##################################################################################
function toggle_discussion() {
	global $DB,$UTIL,$MESSAGE;

	$s = json_decode(stripslashes($_REQUEST['json']));

	if ($_SESSION['me']->is_admin) {

		$q = "UPDATE discussion SET status = " . $s->status . " WHERE id = " . $s->discussion_id;
		$DB->query($q);

		$response['data']['g'] = $s->group;
		$response['flag'] = 'discussion_toggled';

		$response = json_encode($response);

		echo $response;

	}

		
}
//##################################################################################
//CREATE A NEW POST
//##################################################################################
function submit_post() {
	global $DB,$UTIL,$MESSAGE;

	$s = json_decode(stripslashes($_REQUEST['json']));

	//CHECK IF DISCUSSION IS OPEN
	$q = "SELECT status FROM discussion WHERE id = " . $s->discussion_id;
	$status = $DB->get_var($q);

	if ($status > 0) return;

	$q = "SELECT max(thread_no) FROM post WHERE discussion_id = " . $s->discussion_id;
	$thread_no = $DB->get_var($q) + 1;

	$s->content = strip_tags($s->content);

	$q = "INSERT INTO post (discussion_id,created,poster_id,poster,thread_no,content) values (" . $s->discussion_id . ",now()," . $_SESSION['me']->id . ",'" . $_SESSION['me']->profile->nickname . "'," . $thread_no . ",'" . $DB->escape($s->content) . "')";
	$DB->query($q);

	$q = "UPDATE discussion SET no_posts = no_posts + 1, last_post = now(), last_by_nick = '" . $_SESSION['me']->profile->nickname . "',last_post_no = " . $thread_no . " WHERE id = " . $s->discussion_id;
	$DB->query($q);

	if ($thread_no > 10) {
		$m = ($thread_no % 10);
		if ($m < 1) $m = 10;
		$start = ($thread_no - $m);
		$finish = $start + 10;
		$page = $finish/10;
	} else {
		$page = 1;
	}

	//SEND NOTIFICATIONS
	$q = "SELECT subject FROM discussion WHERE id = " . $s->discussion_id;
	$subject = $DB->get_var($q);

	$q = "SELECT user_id FROM notification WHERE purpose = 'new_post' AND ref_id = " . $s->discussion_id;
	$users = $DB->get_col($q);

	$link = addslashes('<a href="http://' . $_SERVER['SERVER_NAME'] . '/user/groups/forum/' . $s->group  . '/discussion/' . $s->discussion_id . '/' . $thread_no . '">' . $subject . '</a>');

	if ($users) {
		foreach ($users as $user_id) {
			if ($user_id !== $_SESSION['me']->id) {
				$ss = '{"thread":"0","recipient":"' . $user_id . '","sender":"admin","subject":"New discussion post","the_message":"A new post has been made in the following discussion:<br><br>' . $link . '","admin":"false"}';
				$MESSAGE->send($ss);
			}
		}
	}


	$response['data']['d'] = $s->discussion_id;
	$response['data']['p'] = $page;
	$response['data']['t'] = $thread_no;
	$response['flag'] = 'new_post';

	$response = json_encode($response);

	echo $response;

}
//##################################################################################
//CREATE A NEW DISCUSSION
//##################################################################################
function submit_discussion() {
	global $DB,$UTIL;

	$s = json_decode(stripslashes($_REQUEST['json']));

	$thread_no = 1;

	//NO XSS
	$s->content = strip_tags($s->content);
	$s->subject = strip_tags($s->subject);

	$q = "INSERT INTO discussion (group_id,created,created_by,created_by_nick,subject,last_post,last_by_nick,no_posts) values (" . $s->group . ",now()," . $_SESSION['me']->id . ",'" . $_SESSION['me']->profile->nickname . "','" . $DB->escape($s->subject) . "',now(),'" . $_SESSION['me']->profile->nickname . "',1)";
	$id = $DB->query($q);

	$UTIL->log($q);

	if ($id) {

		$q = "INSERT INTO post (discussion_id,created,poster_id,poster,thread_no,content) values (" . $id . ",now()," . $_SESSION['me']->id . ",'" . $_SESSION['me']->profile->nickname . "'," . $thread_no . ",'" . $DB->escape($s->content) . "')";
		$DB->query($q);

		$page = 1;

		$response['data']['d'] = $id;
		$response['data']['p'] = $page;
		$response['data']['t'] = $thread_no;
		$response['data']['g'] = $s->group;
		$response['flag'] = 'new_discussion';

		$response = json_encode($response);

		echo $response;
	}

}
//##################################################################################
//STRIP QUOTE FROM CONTENT OF POST
//##################################################################################
function strip_quote($string, $start, $end) {

	while (true) {
		$ini = strpos($string,$start);
		if ($ini === false) return $string;
		$len = strpos($string,$end,$ini) + strlen($end) - $ini;
		$sub = substr($string,$ini,$len);
		$string = str_replace($sub,"",$string);
		$string = ltrim($string);
	}
	return $string;
}
//##################################################################################
//END OF CLASS
//##################################################################################
}

//##################################################################################
//CONSTRUCT
//##################################################################################

$GROUP = new GROUP();

?>
