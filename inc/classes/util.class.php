<?php

//##################################################################################
//START OF CLASS
//##################################################################################

class UTIL {

//##################################################################################
//CONTRUCT THE PATH ARRAY
//##################################################################################
function path() {

	$path = array();

	if (isset($_GET['string'])) {
		if (strstr($_GET['string'],"?") !== false) {
			$pos = 0;
			$tailstr = substr($_GET['string'],$pos);
			$endpos = strpos($tailstr, "?");
			$endpos = $endpos + strlen("?");
			$string = substr($_GET['string'],$pos,$endpos);
		} else {
			$string = $_GET['string'];
		}

		$path = explode("/",$_GET['string']);

	}

	for ($i=0;$i<5;$i++) {
		if (!isset($path[$i])) $path[$i] = null; 
	}

	return $path;

}
//##################################################################################
//LOAD THE SITE CONFIG
//##################################################################################
function config() {
	global $DB;

	$config = array();

	$q = "select * from config where active > 0";
	$result = $DB->get_results($q);

	if ($result) {
		foreach ($result as $c) {
			$c->data = json_decode($c->data);
			$config[$c->tag] = $c;
		}
		return $config;
	} else {
		return;
	}

}
//##################################################################################
//LOAD THE LANGUAGE OBJECT
//##################################################################################
function language($s) {
	global $DB, $UTIL;

	$language = array();

	$q = "SELECT * FROM language WHERE language_active = " . $s . " OR language_active = 1";
	$result = $DB->get_results($q);

	if ($result) {
		foreach ($result as $c) {
			$language[$c->language_suffix] = $c;
		}
		return $language;
	} else {
		return;
	}

}
//##################################################################################
//MAKE OBJECT FROM XML
//##################################################################################
function getxml($src) {
	global $dr;

	require($dr . "/inc/classes/imported/magpierss/rss_fetch.inc");
	$feed = fetch_rss($src);
	return $feed;

}
//##################################################################################
//LOAD THE TEXT OBJECT
//##################################################################################
function text() {
	global $DB, $UTIL, $path;
	
	$language = array();

	$q = ($path[0] == 'admin') ? "select * from text order by text_id" : "select * from text";

	$result = $DB->get_results($q);

	if ($result) {
		foreach ($result as $c) {
			if (isset($_REQUEST['text_hint_on']) and $_SESSION['me']->is_admin) {
				$c->text .= '<span class="text_hint">|' . $c->text_group . '|' . $c->text_id . '</span>';
			} 
			$text[$c->language_suffix][$c->text_group][$c->text_tag] = $c;
		}
		return $text;
	} else {
		return;
	}

}
//##################################################################################
//UPDATE USER LANGUAGE OF CHOICE
//##################################################################################
function user_language($id,$l=false) {
	global $DB;

	if ($l) {
		$q = "UPDATE user SET language = '" . $_SESSION['language'] . "' WHERE id = " . $_SESSION['me']->id;
		$DB->query($q); 
		return;
	} else {
		$q = "SELECT language FROM user WHERE id = " . $id;
		$result = $DB->get_var($q);
		return $result;
	}

}
//##################################################################################
//WRITE TO LOG
//##################################################################################
function log($message) {

	$message = date("Y-m-d H:i:s") . " " . $_SERVER['REQUEST_URI'] . " " . $message . "\n"; 
	$log = fopen("/tmp/app.log","a");
	fwrite($log,$message);
	fclose($log);
}
//##################################################################################
//REDIRECT THE USER
//##################################################################################
function redirect($path) {

	header("Location: " . $path );

}
//##################################################################################
//GET POSTER NAME FROM QUOTE ENCLOSURE
//##################################################################################
function poster_name($s) {

	preg_match('@^(?:\[quote\:)?([^\]]+)@i',$s, $qposter);

	if (isset($qposter[1])) return $qposter[1]; 

	return false;

}
//##################################################################################
//CONVERT NICKNAME TO ID AND VICE VERSA
//##################################################################################
function nickname_convert($n) {
	global $DB;

	if (!is_numeric($n)) {
		$q = "SELECT user_id FROM user_data LEFT JOIN user_data_field ON user_data_field.id = user_data.field_id WHERE tag = 'nickname' and value_vc = '" . $n . "'";
		$n = $DB->get_var($q);
	} else {
		$q = "SELECT value_vc FROM user_data LEFT JOIN user_data_field ON user_data_field.id = user_data.field_id WHERE tag = 'nickname' and user_id = " . $n;
		$n = $DB->get_var($q);
	}

	return $n;

}
//##################################################################################
//CHECK THAT ACCOUNT TYPES OF 2 USERS CAN COMMUNICATE WITH EACH OTHER
//##################################################################################
function valid_connect($user_id=false) {
	global $DB;

	if ($_SESSION['me']->is_admin) return true;
	
	//ADULT AND FAMILY ACCOUNTS
	if ($_SESSION['me']->profile->account_type < 2) {
		$q = "SELECT user_id FROM user_data WHERE field_id = 1 AND user_id = " . $user_id . " AND value_opt < 2";
	//EDUCATOR ACCOUNT
	} else if ($_SESSION['me']->profile->account_type === 4) {
		$q = "SELECT user_id FROM user_data WHERE field_id = 1 AND user_id = " . $user_id . " AND value_opt = 4";
	//TEEN AND MINOR ACCOUNTS
	} else {
		$q = "SELECT user_id FROM user_data WHERE field_id = 1 AND user_id = " . $user_id . " AND value_opt = " . $_SESSION['me']->profile->account_type;
	}

	$u = $DB->get_var($q);

	if ($u) return true;

	return false;

}
//##################################################################################
//GET USERS EMAIL ADDRESS
//##################################################################################
function get_email($n) {
	global $DB;

	if (!is_numeric($n)) {
		$q = "SELECT user_id FROM user_data LEFT JOIN user_data_field ON user_data_field.id = user_data.field_id WHERE tag = 'nickname' and value_vc = '" . $n . "'";
		$n = $DB->get_var($q);
	}

	$q = "SELECT email FROM user WHERE id = " . $n;
	
	return $DB->get_var($q);	

}
//##################################################################################
//GET USERS PARENT EMAIL ADDRESS
//##################################################################################
function get_parent_email($n) {
	global $DB;

	if (!is_numeric($n)) {
		$q = "SELECT user_id FROM user_data LEFT JOIN user_data_field ON user_data_field.id = user_data.field_id WHERE tag = 'nickname' and value_vc = '" . $n . "'";
		$n = $DB->get_var($q);
	}

	$q = "SELECT value_vc FROM user_data LEFT JOIN user_data_field ON user_data_field.id = user_data.field_id WHERE tag = 'parent_email' and user_data.user_id = '" . $n . "'";

	return $DB->get_var($q);	

}
//##################################################################################
//EXTRACT FAQ NUMBER
//##################################################################################
function faq_no($s=false) {

	$a = explode("_",$s);
	return $a[1];

}
//##################################################################################
//GET RATING FOR USER AND VIEWING USER
//##################################################################################
function rating($n) {
	global $DB;

	$rating = array();

	if (!is_numeric($n)) {
		$q = "SELECT user_id FROM user_data LEFT JOIN user_data_field ON user_data_field.id = user_data.field_id WHERE tag = 'nickname' and value_vc = '" . $n . "'";
		$n = $DB->get_var($q);
	}

	$q = "SELECT rating_on FROM user WHERE id = " . $n;
	$rating['on'] = $DB->get_var($q);
	$q = "SELECT rating FROM user WHERE id = " . $n;
	$rating['user'] = $DB->get_var($q);
	$q = "SELECT RATING FROM user_rating WHERE rating_user = " . $_SESSION['me']->id;
	$rating['viewer'] = $DB->get_var($q);
	$q = "SELECT count(*) FROM user_rating WHERE user_id = " . $n;
	$rating['count'] = $DB->get_var($q);

	return $rating;	

}
//##################################################################################
//IS THE USER LOGGED IN
//##################################################################################
function loggedin($u=false) {
	global $DB;

	if ($u) {

		if (!is_numeric($u)) {
			$q = "SELECT user_id FROM user_data LEFT JOIN user_data_field ON user_data_field.id = user_data.field_id WHERE tag = 'nickname' and value_vc = '" . $u . "'";
			$u = $DB->get_var($q);
		} 

		$q = "SELECT user_id FROM heartbeat WHERE stamp > DATE_SUB(now(), interval 20 second) AND user_id = " . $u;
		$result = $DB->get_var($q);

		if ($result) return true;

		return false;

	}

	return false;

}
//##################################################################################
//HAS USER ENTERED SKYPE USERNAME
//##################################################################################
function skype($u=false) {
	global $DB;

	if ($u) {

		if (!is_numeric($u)) {
			$q = "SELECT user_id FROM user_data LEFT JOIN user_data_field ON user_data_field.id = user_data.field_id WHERE tag = 'nickname' and value_vc = '" . $u . "'";
			$u = $DB->get_var($q);
		} 

		$q = "SELECT skype FROM user WHERE id = " . $u;
		$result = $DB->get_var($q);

		if ($result != '') return $result;

		return false;

	}

	return false;

}
//##################################################################################
//IS THE USER ALREADY IN MY DIRECTORY
//##################################################################################
function in_directory($u=false) {
	global $DB;

	if (!isset($_SESSION['me']->id)) die();

	if ($u) {

		if (!is_numeric($u)) {
			$q = "SELECT user_id FROM user_data LEFT JOIN user_data_field ON user_data_field.id = user_data.field_id WHERE tag = 'nickname' and value_vc = '" . $u . "'";
			$u = $DB->get_var($q);
		} 

		$q = "SELECT members FROM directory WHERE user_id = " . $_SESSION['me']->id;
		$d = $DB->get_var($q);

		if ($d) {

			$d = json_decode($d);

			if (in_array($u,$d)) return true;

		}

		return false;

	}

	return false;

}
//##################################################################################
//WRAP A STRING
//##################################################################################
function wrap($s) {
	$s = "'" . $s . "'";
	return $s;
}
//##################################################################################
//SYTEMIZE A TAG
//##################################################################################
function systag($s) {
	$s = str_replace(" ", "_", $s);
	$s = str_replace("'", "", $s);
	return strtolower($s);
}
//##################################################################################
//AUTHKEY GENERATOR
//##################################################################################
function authkey() {

	return substr(md5(uniqid($_SERVER['HTTP_USER_AGENT'], true)),0,14);

}
//##################################################################################
//NEW PASSWORD GENERATOR
//##################################################################################
function password() {

	return substr(md5(uniqid($_SERVER['HTTP_USER_AGENT'], true)),0,8);

}
//##################################################################################
//FAMILYCODE GENERATOR
//##################################################################################
function familycode() {

	return substr(md5(uniqid($_SESSION['HTTP_USER_AGENT'], true)),0,8);

}
//##################################################################################
//VERIFY FAMILY CODE
//##################################################################################
function codeok($submitted_code,$email) {
	global $DB;

	$q = "SELECT user_id,value_vc FROM user_data LEFT JOIN user_data_field ON user_data.field_id = user_data_field.id LEFT JOIN user ON user_data.user_id = user.id WHERE tag = 'family_code' AND email = '" . $email . "'";

	$u = $DB->get_result($q);

	if ($submitted_code == $u->value_vc) return $u->user_id;

	return false;
	
}
//##################################################################################
//NEW PM MESSAGE COUNT
//##################################################################################
function pmcount($id) {
	global $DB;

	$q = "SELECT count(*) FROM message WHERE recipient = " . $id . " AND status = 'new' AND moderated > 0";
	return $DB->get_var($q);
}
##################################################################################
//NEW CONVERSATOIN COUNT
//##################################################################################
function concount($id) {
	global $DB;

	$q = "SELECT count(*) FROM conversation WHERE user2 = " . $id . " AND new > 0";
	return $DB->get_var($q);
}
//##################################################################################
//SEND AN EMAIL
//##################################################################################
function sendemail($message) {
	global $config, $dr;

	//PHPMailer
	require_once($dr . "/inc/classes/imported/PHPMailer_5.2.1/class.phpmailer.php");
	$mail = new PHPMailer(); 

	$mail->CharSet = 'UTF-8';
	$mail->SetFrom($config['email']->data->admin_address->value, $config['email']->data->admin_address->value);
	$mail->AddReplyTo($config['email']->data->admin_address->value, $config['email']->data->admin_address->value);
	$mail->AddAddress($message['to'],$message['to']);
	$mail->Subject = $message['subject'];
	$mail->Body = $message['message'];

	if(!$mail->Send()) {
	  $this->log("Mailer Error: " . $mail->ErrorInfo);
	  return false;
	} 

	return true;

}
//##################################################################################
//AVATAR CROP
//##################################################################################
function crop_avatar($user_id=false) {

	//ADMIN UPDATE
	if ($user_id and $_SESSION['me']->id < 2) {
		$_SESSION['me']->id = $user_id;
	}

	$targ_w = $targ_h = 70;
	$jpeg_quality = 90;

	$src = CONTENTDIR . '/' . $_SESSION['me']->id . '/avatar/avatar_orig.jpg';
	$dst = CONTENTDIR . '/' . $_SESSION['me']->id . '/avatar/avatar.jpg';
	$img_r = imagecreatefromjpeg($src);
	$dst_r = ImageCreateTrueColor( $targ_w, $targ_h );

	imagecopyresampled($dst_r,$img_r,0,0,$_REQUEST['coords']['x1'],$_REQUEST['coords']['y1'],
	    $targ_w,$targ_h,$_REQUEST['coords']['w'],$_REQUEST['coords']['h']);

	imagejpeg($dst_r, $dst, $jpeg_quality);

	if ($user_id) $_SESSION['me']->id = 1;

}
//##################################################################################
//CONVERT TIME TO USER TIMEZONE
//##################################################################################
function minor_check($id=false) {
	global $DB;

	$q = "SELECT parent_id FROM user WHERE id = " . $id;
	$DB->get_var($q);

	return $DB->get_var($q);

}
//##################################################################################
//CONVERT TIME TO USER TIMEZONE
//##################################################################################
function gmt_convert($d,$user_id) {
	global $DB;

	if ($d == "0000-00-00 00:00:00") return $d;

	$q = "SELECT offset FROM availability WHERE user_id = " . $user_id;
	$offset = $DB->get_var($q);

	$d = date("Y-m-d H:i:s",strtotime($d)+($offset*3600));

	return $d;

}
//##################################################################################
//AVATAR EXISTS
//##################################################################################
function avatar_exists($id=false) {
	global $DB;

	if (!is_numeric($id)) {
		$q = "SELECT user_id FROM user_data LEFT JOIN user_data_field ON user_data_field.id = user_data.field_id WHERE tag = 'nickname' and value_vc = '" . $id . "'";
		$id = $DB->get_var($q);
	} 

	if (file_exists(CONTENTDIR . $id . '/avatar/avatar.jpg')) {
		return true;
	}

	return false;

}
//##################################################################################
//CONVERT A URL TO A HYPERLINK
//By JOHN GRUBER
//http://daringfireball.net/2009/11/liberal_regex_for_matching_urls
//##################################################################################
function auto_link_text($text) {

   $pattern  = '#\b(([\w-]+://?|www[.])[^\s()<>]+(?:\([\w\d]+\)|([^[:punct:]\s]|/)))#';
   $callback = create_function('$matches', '
       $url       = array_shift($matches);
       $url_parts = parse_url($url);

       $text = parse_url($url, PHP_URL_HOST) . parse_url($url, PHP_URL_PATH);
       $text = preg_replace("/^www./", "", $text);

       $last = -(strlen(strrchr($text, "/"))) + 1;
       if ($last < 0) {
           $text = substr($text, 0, $last) . "&hellip;";
       }

       return sprintf(\'<a rel="nowfollow" href="%s">%s</a>\', $url, $text);
   ');

   return preg_replace_callback($pattern, $callback, $text);
}
//##################################################################################
//AVATAR STORE
//##################################################################################
function avatar($user_id=false) {
	global $config;

	//ADMIN UPDATE
	if ($user_id and $_SESSION['me']->id < 2) {
		$_SESSION['me']->id = $user_id;
	}

	$_FILES['avatar']['name'] = $this->systag($_FILES['avatar']['name']);

	if (!is_dir(CONTENTDIR . $_SESSION['me']->id)) mkdir(CONTENTDIR . $_SESSION['me']->id,0777); 

	if (!is_dir(CONTENTDIR . $_SESSION['me']->id . "/avatar")) mkdir(CONTENTDIR . $_SESSION['me']->id . "/avatar",0777);

	move_uploaded_file($_FILES['avatar']['tmp_name'],CONTENTDIR . $_SESSION['me']->id . "/avatar/avatar_orig.jpg");

	$ext = end(explode('.',$_FILES['avatar']['name']));

	#VERY LARGE IMAGE RESIZE
	$di = getimagesize(CONTENTDIR . $_SESSION['me']->id . "/avatar/avatar_orig.jpg");
	if ($di[0] > 300) {

		if ($di[0] >= $di[1]) {
			$w = $config['image']->data->max_image_width->value;
			$h = ($config['image']->data->max_image_width->value / $di[0]) * $di[1];
		} else {
			$h = $config['image']->data->max_image_height->value;
			$w = ($config['image']->data->max_image_height->value / $di[1]) * $di[0];
		}

		$di[0] = $w;
		$di[1] = $h;

		$this->resize_image(CONTENTDIR . $_SESSION['me']->id . "/avatar/avatar_orig.jpg",CONTENTDIR . $_SESSION['me']->id . "/avatar/avatar_orig.jpg",$ext,$w,$h);

	} 
	
	if (file_exists(CONTENTDIR . $_SESSION['me']->id . "/avatar/avatar_orig.jpg")) {

		if ($user_id) $_SESSION['me']->id = 1;
		return $di;

	} else {
		if ($user_id) $_SESSION['me']->id = 1;
		return false;

	}

}
//##################################################################################
//IMAGE RESIZE FUNCTION
//##################################################################################
function resize_image($src_image,$dst_image,$ext,$xsize,$ysize)
{

list($w,$h) = getimagesize($src_image);

if ($w > $h) {

   if ($h < $ysize) {

      if ($w < $xsize) {

	      $new_width = $w;
	      $new_height = $h;

      } else {
 
	      $new_width = $xsize;
	      $new_height = ($xsize / $w) * $h;

      }

   } else {

      if ($w <= $xsize) $new_width = $w;
      if ($w >= $xsize) $new_width = $xsize;
      if ($h <= $ysize) $new_height = $h;
      if ($h >= $ysize) $new_height = $ysize;

   }

   if ($ext == 'png') {
      $image = imagecreatefrompng($src_image);
   } else {
      $image = imagecreatefromjpeg($src_image);
   }
   $crop = imagecreatetruecolor($new_width,$new_height);
   imagecopyresampled ($crop, $image, 0, 0, 0, 0, $new_width, $new_height, $w, $h);
   imagejpeg($crop,$dst_image,100);

} elseif ($h > $w) {

   if ($w < $xsize) {

      if ($h < $ysize) {

	      $new_height = $h;
	      $new_width = $w;
	      
      } else {

	      $new_width = ($ysize / $h) * $w;
	      $new_height = $ysize;

      }

   } else {

      if ($h <= $ysize) $new_height = $h;
      if ($h >= $ysize) $new_height = $ysize;
      if ($w <= $xsize) $new_width = $w;
      if ($w >= $xsize) $new_width = ($ysize / $h) * $w;

   }

   if ($ext == 'png') {
      $image = imagecreatefrompng($src_image);
   } else {
      $image = imagecreatefromjpeg($src_image);
   }
   $crop = imagecreatetruecolor($new_width,$new_height);
   imagecopyresampled ($crop, $image, 0, 0, 0, 0, $new_width, $new_height, $w, $h);
   imagejpeg($crop,$dst_image,100);
  
} elseif ($h == $w) {

   $new_width = $ysize;
   $new_height = $ysize;

   $x=$w;
   $y=$h;

   if ($ext == 'png') {
      $image = imagecreatefrompng($src_image);
   } else {
      $image = imagecreatefromjpeg($src_image);
   }
   $crop = imagecreatetruecolor($new_width,$new_height);
   imagecopyresampled ($crop, $image, 0, 0, 0, 0, $new_width, $new_height, $w, $h);

}

imagejpeg($crop,$dst_image,100);
imagedestroy($image);

}
//#################################################################################
//MOBILE DETECTION
//##################################################################################
function is_mobile() {

	$is_mobile = false;

	$useragent=$_SERVER['HTTP_USER_AGENT'];

	if(preg_match('/android|avantgo|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|e\-|e\/|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(di|rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|xda(\-|2|g)|yas\-|your|zeto|zte\-/i',substr($useragent,0,4))) {

		$is_mobile = true;

	} 

	return $is_mobile;

}
//##################################################################################
//END OF CLASS
//##################################################################################
}

//##################################################################################
//CONSTRUCT
//##################################################################################

$UTIL = new UTIL();

?>
