<?php

//##################################################################################
//START OF CLASS
//##################################################################################

class MEMBER {

//##################################################################################
//MEMBER PAGE
//##################################################################################
function page($nickname=false) {
	global $DB, $USER, $UTIL, $smarty;

	$user = $USER->getprofile($nickname);
	
	//DON'T LET THE USER LAND ON THEIR OWN PAGE
	//if ($_SESSION['me']->id === $user->id) {
		//$UTIL->redirect("/user/dashboard");
		//return;
	//}	

	//CHECK DATA PROFILE OF VIEWING USER FOR REQUIRED DATA
	$q = "SELECT * FROM user_data_field WHERE required > 0 AND id NOT IN (SELECT field_id FROM user_data WHERE user_id = " . $_SESSION['me']->id . ") AND (account = '" . $_SESSION['me']->account['account_type'] . "' OR account = 'all') ORDER BY priority";

	$fields = $DB->get_results($q);
	if (!empty($fields)) {
		$UTIL->redirect('/user/profile/required');
	}

	//CHECK THAT VIEWING USER IS ALLOWED VIEW ACCOUNT TYPE
	if (!$UTIL->valid_connect($user->id)) {

		$user = false;
		$UTIL->redirect("/");
		return;

	} else {

		//AVATAR?
		if ($UTIL->avatar_exists($nickname)) {
			$user->avatar = true;
		} else {
			$user->avatar = false;
		}

		//SKYPE?
		$user->skype = $UTIL->skype($nickname);

		//LOGGED IN?
		$user->loggedin = $UTIL->loggedin($nickname);

		//IN DIRECTORY?
		$user->in_directory = $UTIL->in_directory($nickname);

	}

	$smarty->assign("user",$user);
	$smarty->assign("c",false);

}
//##################################################################################
//CONVERSATION REQUEST
//##################################################################################
function request() {
	global $DB, $USER, $UTIL, $MESSAGE, $text;

	$_REQUEST['json'] = stripslashes($_REQUEST['json']);
	$c = json_decode($_REQUEST['json']);

	if (isset($c->now)) {

		//CHECK THAT VIEWING USER IS ALLOWED VIEW ACCOUNT TYPE
		if (!$UTIL->valid_connect($c->user2)) exit;

		$salt = date("U") . $_SESSION['me']->id . rand(1,5000);
		$c->code = substr(md5(uniqid($salt, true)),0,8);

		$q = "INSERT INTO nowrequests (user1,code,requested,mode) VALUES (" . $_SESSION['me']->id . ",'" . $c->code . "',now(),'" . $c->mode . "')";
		$DB->query($q);

		$n = '{"d":"' .  date("Y-m-d H:i:s") . '","u":"' . $_SESSION['me']->profile->nickname . '","m":"immediate_conversation","l":"/member/' . $_SESSION['me']->profile->nickname . '/c/now/' . $c->code . '"}';

		$q = "UPDATE heartbeat set message = '" . $n . "' WHERE user_id = " . $c->user2;
		$DB->query($q);

		$c->status = 'now';
		$c->user_message = $text[$_SESSION['language']]['messages']['conversation_now_request']->text;

		$response['flag'] = "conversation_now_result";
		$response['data'] = $c;

	} else {

		$p1 = $c->proposed_date1 . " " . $c->p1_hour . ":" . $c->p1_min . ":00";
		$p1 = date("Y-m-d H:i:s",strtotime($p1)-($c->user2_offset*3600));
		$p2 = (!empty($c->proposed_date2)) ? $c->proposed_date2 . " " . $c->p2_hour . ":" . $c->p2_min . ":00" : false;
		if ($p2) $p2 = date("Y-m-d H:i:s",strtotime($p2)-($c->user2_offset*3600));

		$c->confirmed = (isset($c->confirmed)) ? $c->confirmed : false;
		$c->cancelled = (isset($c->cancelled)) ? $c->cancelled : false;
		$c->declined = (isset($c->declined)) ? $c->declined : false;

		//WHICH USER IS MAKING THE REQUEST
		$m = ($c->user2 == $_SESSION['me']->id) ? 2 : 1;

		if ($c->code === "0") {
			
			//CHECK THAT VIEWING USER IS ALLOWED VIEW ACCOUNT TYPE
			if (!$UTIL->valid_connect($c->user2)) exit;

			$salt = $c->user2 . $c->proposed_date1 . $c->p1_hour . $_SESSION['me']->id . rand(1,5000);
			$c->code = substr(md5(uniqid($salt, true)),0,8);

			//CHECK CODE DOESN'T EXIST ALREADY
			//$r = true;
			//while ($r) {
				//if ($r) {
					//$salt = $c->user2 . $c->proposed_date1 . $c->p1_hour . $_SESSION['me']->id . rand(1,5000);
					//$c->code = substr(md5(uniqid($salt, true)),0,8);
					//$q = "SELECT id FROM conversation WHERE code = '" . $code . "'";
					//$r = $DB->get_var($q);
				//}
			//}

			$q = "INSERT INTO conversation (user1,user2,protime1,protime2,start,user1_message,approved,status,code,updated_by,user1_remind,new,mode) VALUES (" . $_SESSION['me']->id . "," . $c->user2 . ",'" . $p1 . "','" . $p2 . "','" . $p1 . "','" . $DB->escape($c->request_message) . "',1,'proposed','" . $c->code . "'," . $_SESSION['me']->id . "," . $c->user1_remind . ",1,'" . $c->mode . "')";
			$c->status = 'proposed'; 
			$c->user_message = $text[$_SESSION['language']]['messages']['conversation_request_id']->text . " " . $c->code;

			//SEND PM TO PARENT ACCOUNTS IF REQUESTER IS MINOR
			$parent = $UTIL->minor_check($_SESSION['me']->id);
		
			if ($parent > 0) {
				$ul = $UTIL->user_language($parent);
				$other = $UTIL->nickname_convert($c->user2);
				$child = $UTIL->nickname_convert($_SESSION['me']->id);

				$link = addslashes('<a href="/member/' . $other . '/c/' . $c->code . '">' . $other . " - " . $child . '</a>');
				$s = '{"thread":"0","recipient":"' . $parent . '","sender":"1","subject":"' . $text[$ul]['messages']['new_child_conversation_subject']->text . '","the_message":"' . $text[$ul]['messages']['new_child_conversation_body']->text . '<br><br>' . $link . '","admin":"true"}';
				$MESSAGE->send($s);
			}

		} else {

			//CHECK REQUESTING USER IS PARTY TO THE CONVERSATION
			$party = false;
			$q = "SELECT user1,user2 FROM conversation WHERE code = '" . $c->code . "'";
			$users = $DB->get_result($q);		
			if ($users->user1 == $_SESSION['me']->id) $party = true;
			if ($users->user2 == $_SESSION['me']->id) $party = true;

			//ALLOW IF USER IS PARENT OF EITHER PARTICIPANT
			$parent = $UTIL->minor_check($users->user1);
			if ($parent == $_SESSION['me']->id) $party = true;
			$parent = $UTIL->minor_check($users->user2);
			if ($parent == $_SESSION['me']->id) $party = true;

			//KILL
			if (!$party) return;

			if ($c->confirmed) {

				$q = "UPDATE conversation SET status = 'confirmed',start = protime" . $c->selected_option . ", user" . $m . "_message = '" . $c->confirm_message . "', updated_by = " . $_SESSION['me']->id . ",user1_remind = " . $c->user1_c_remind . ",user2_remind = " . $c->user2_c_remind . ",new=0,updated_at = now() WHERE code = '" . $c->code . "'";
				$c->status = 'confirmed';
				$c->user_message = $text[$_SESSION['language']]['messages']['conversation_confirmed']->text;

				//SEND PM TO PARENT ACCOUNTS IF CONFIRMER IS MINOR
				$parent = $UTIL->minor_check($_SESSION['me']->id);
		
				if ($parent > 0) {
					$ul = $UTIL->user_language($parent);
					$other_id = ($users->user1 == $_SESSION['me']->id) ? $users->user2 : $users->user1;
					$other = $UTIL->nickname_convert($other_id);
					$child = $UTIL->nickname_convert($_SESSION['me']->id);

					$link = addslashes('<a href="/member/' . $other . '/c/' . $c->code . '">' . $other . " - " . $child . '</a>');
					$s = '{"thread":"0","recipient":"' . $parent . '","sender":"1","subject":"' . $text[$ul]['messages']['new_child_conversation_subject']->text . '","the_message":"' . $text[$ul]['messages']['new_child_conversation_body']->text . '<br><br>' . $link . '","admin":"true"}';
					$MESSAGE->send($s);
				}

			} elseif ($c->cancelled) {

				//DEALS WITH SITUATION IN WHICH PROPOSER CANCELS BEFORE RESPONDENT HAS RESPONDED
				$c->confirm_message = (isset($c->confirm_message)) ? $c->confirm_message : $c->request_message;
				$q = "UPDATE conversation SET status = 'cancelled',updated_by = " . $_SESSION['me']->id . ", user" . $m . "_message = '" . $c->confirm_message . "',new=0,updated_at = now() WHERE code = '" . $c->code . "'";
				$c->status = 'cancelled';
				$c->user_message = $text[$_SESSION['language']]['messages']['conversation_cancelled']->text;

			} elseif ($c->declined) {

				$q = "UPDATE conversation SET status = 'declined',updated_by = " . $_SESSION['me']->id . ", user" . $m . "_message = '" . $c->confirm_message . "',new=0,updated_at = now() WHERE code = '" . $c->code . "'";
				$c->status = 'declined';
				$c->user_message = $text[$_SESSION['language']]['messages']['conversation_declined']->text;

			} else {

				$q = "UPDATE conversation SET protime1 = '" . $p1 . "',protime2 = '" . $p2 . "',start = '" . $p1 . "',status = 'amended',updated_by = " . $_SESSION['me']->id . ",user" . $m . "_message = '" . $DB->escape($c->request_message) . "',user1_remind = " . $c->user1_remind . ",user2_remind = " . $c->user2_remind . ",mode = '" . $c->mode . "', updated_at = now() WHERE code = '" . $c->code . "'";
				$c->status = 'amended';
				$c->user_message = $text[$_SESSION['language']]['messages']['conversation_amended']->text;
			}

		}

		//UPDATE
		$DB->query($q);

		//SEND PRIVATE MESSAGE
		////FIND RECIPIENT
		if ($c->user2 != $_SESSION['me']->id) {
			$o = $c->user2;
		} else {
			$q = "SELECT user1 FROM conversation WHERE code = '" . $c->code . "'";
			$o = $DB->get_var($q);
		}
		$ul = $UTIL->user_language($o);
		$user = $_SESSION['me']->profile->nickname;

		$link = addslashes('<a href="/member/' . $user . '/c/' . $c->code . '">' . $text[$ul]['words']['conversation']->text . ' ' . $c->code . '</a>');

		$s = '{"thread":"0","recipient":"' . $o . '","sender":"' . $_SESSION['me']->id . ' ","subject":"' . $text[$ul]['messages']['conversation_updated']->text . '","the_message":"' . $text[$ul]['messages']['conversation_updated_message']->text . $link . '"}';

		$MESSAGE->send($s);

		//BROWSER MESSAGE
		$response['flag'] = "conversation_request_result";
		$response['data'] = $c;

	}

	echo json_encode($response);
	
}
//##################################################################################
//CONVERSATION DISPLAY
//##################################################################################
function conversation($nickname=false,$code=false,$nowcode=false) {
	global $DB, $smarty, $UTIL, $USER, $text;
	
	$startcode = false;

	$user_id = $UTIL->nickname_convert($nickname);
	
	$skype = $UTIL->skype($user_id);

	$this->page($nickname);

	if ($code == 'now') {

		$q = "SELECT * FROM nowrequests WHERE code = '" . $nowcode . "'";
		
		$c = $DB->get_result($q);

		if ($c) {

			//USER 2 ACCEPTANCE //USER IS DIRECTED TO START PAGE
			if ($c->user2 < 1) {

				$q = "UPDATE nowrequests SET user2 = " . $_SESSION['me']->id . " WHERE code = '" . $nowcode . "'";
				$DB->query($q);

				$c->user2 = $_SESSION['me']->id;

				//CREATE CONVERSATION RECORD
				$q = "INSERT INTO conversation (user1,user2,start,approved,status,code,updated_by,mode) VALUES (" . $c->user1 . "," . $c->user2 . ",now(),1,'now','" . $c->code . "'," . $_SESSION['me']->id . ",'" . $c->mode . "')";
				$DB->query($q);

				//SEND NOTIFICATION
				$n = '{"d":"' .  date("Y-m-d H:i:s") . '","u":"' . $_SESSION['me']->profile->nickname . '","m":"immediate_conversation_accept","l":"/member/' . $_SESSION['me']->profile->nickname . '/c/now/' . $c->code . '"}';
				$q = "UPDATE heartbeat set message = '' WHERE user_id = " . $_SESSION['me']->id;
				$DB->query($q);

				$q = "UPDATE heartbeat set message = '" . $n . "' WHERE user_id = " . $user_id;

				$DB->query($q);


			} else {

				$q = "DELETE FROM nowrequests WHERE code = '" . $nowcode . "'";
				$DB->query($q);

				$q = "UPDATE heartbeat set message = '' WHERE user_id = " . $_SESSION['me']->id;
				$DB->query($q);

			}			

		} else {
		
			$q = "SELECT * FROM conversation WHERE code = '" . $nowcode . "'";
		
			$c = $DB->get_result($q);

		}
		
		$c->now = true;
		$startcode = $nowcode;

		

	} else {

		$q = "SELECT * FROM conversation WHERE code = '" . $code . "' AND (user1 = " . $user_id . " OR user2 = " . $user_id . ")";

		$c = $DB->get_result($q);
		
		$c->now = false;

		//CHECK REQUESTING USER IS PARTY TO THE CONVERSATION
		$party = false;
		if ($c->user1 == $_SESSION['me']->id) $party = true;
		if ($c->user2 == $_SESSION['me']->id) $party = true;
		
		//ALLOW IF USER IS PARENT OF EITHER PARTICIPANT
		$parent = $UTIL->minor_check($c->user1);
		if ($parent == $_SESSION['me']->id) $party = true;
		$parent = $UTIL->minor_check($c->user2);
		if ($parent == $_SESSION['me']->id) $party = true;

		//KILL
		if (!$party) return;

		//PAST
		if (date("U",strtotime($c->start)) < date("U",strtotime("-15 minutes"))) {
			$c->past = true;
		} else {
			$c->past = false;
		}
		//IS IT OK TO PRESENT A START NOW LINK?
		if (date("U",strtotime($c->start)) < date("U",strtotime("+10 minutes"))) {
			$c->start_now_ok = true;
		} else {
			$c->start_now_ok = false;
		}
		
		//FOR DISPLAY TO VIEWING USER
		$c->my_protime1 = $UTIL->gmt_convert($c->protime1, $_SESSION['me']->id);
		$c->my_protime2 = $UTIL->gmt_convert($c->protime2, $_SESSION['me']->id);
		//FOR PROCESSING
		$c->protime1 = $UTIL->gmt_convert($c->protime1, $user_id);
		$c->protime2 = $UTIL->gmt_convert($c->protime2, $user_id);
		$c->start = $UTIL->gmt_convert($c->start, $_SESSION['me']->id);
		$c->user1_nickname = $UTIL->nickname_convert($c->user1);
		$c->user2_nickname = $UTIL->nickname_convert($c->user2);

		//STATUS
		$c->status_en = $c->status;				
		$c->status = $text[$_SESSION['language']]['statuses'][$c->status]->text;

		

	}

	$smarty->assign("skype",$skype);
	
	$smarty->assign("startcode",$startcode);

	$smarty->assign("c",$c);

}
//##################################################################################
//END OF CLASS
//##################################################################################
}

//##################################################################################
//CONSTRUCT
//##################################################################################

$MEMBER = new MEMBER();

?>
