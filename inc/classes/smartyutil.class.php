<?php

//##################################################################################
//START OF CLASS
//##################################################################################

class SMARTYUTIL {

//##################################################################################
//EXTRACT FAQ NUMBER
//##################################################################################
function faq_no($s=false) {

	$a = explode("_",$s);
	return $a[1];

}
//##################################################################################
//CONVERT SECONDS TO HOURS MINUTES AND SECONDS
//##################################################################################
function sec_convert($secs=false) {

	$mins = false;
	$hours = false;

	if ($secs > 59) {
		$mins = floor($secs/60);
		$secs = $secs%60;
	}

	if ($mins > 59) {
		$hours = floor($mins/60);
		$mins = $mins%60;
	}

	$time = "";

	if ($hours) $time .= $hours . "h ";
	if ($mins) $time .= str_pad($mins,2,"0",STR_PAD_LEFT) . "m ";

	if (!$hours and !$mins) {
		$time = str_pad($secs,2,"0",STR_PAD_LEFT) . "s";
	}

	return $time;

}
//##################################################################################
//DISPLAY AD
//##################################################################################
function ad($tag=false) {
	global $DB;

	$q = "SELECT ad_content FROM ad WHERE ad_tag = '" . $tag . "' AND ad_status > 0";
	$ad = $DB->get_var($q);
	if ($ad) echo stripslashes($ad);

}
//##################################################################################
//TIMEZONE CALCULATOR
//##################################################################################
function directory($class=false) {

	$directory = '';
	for ($i=65; $i<=90; $i++) {
		$directory .= '<div class="' . $class . '" id="' . chr($i) . '">' . chr($i) . '</div>';
	}

	echo $directory;

}
//##################################################################################
//TIMEZONE CALCULATOR
//##################################################################################
function timezone_diff($t,$os,$p) {

	$t = strtotime($t);
	$h = '<span id="p' . $p . '_hour_local">' . date("H",strtotime("-" . $os . " hours",$t)) . "</span>:";
	$m = '<span id="p' . $p . '_min_local">' . date("i",strtotime("-" . $os . " hours",$t)) . "</span>";
	return $h . $m;

}
//##################################################################################
//TIMEZONE SELECTOR
//##################################################################################
function html_timezone($zone=false) {
	global $text;

	echo '<select name="times[zone]" id="timezone">';
	echo '<option value="auto">' . $text[$_SESSION['language']]['forms']['auto_detect_tz']->text;
	$timezone_identifiers = DateTimeZone::listIdentifiers();
	$continent = false;
	foreach( $timezone_identifiers as $value ){
	    if ( preg_match( '/^(Europe|America|Australia|Atlantic|Africa|Asia|Indian|Pacific|Antartica|Arctic)\//', $value ) ){
	    	$ex=explode("/",$value);	
	    	if ($continent!=$ex[0]){
	    		if ($continent!="") echo '</optgroup>';
	    		echo '<optgroup label="'.$ex[0].'">';
	    	}
 
	    	$city = (isset($ex[2])) ? $ex[1] . "/" . $ex[2] : $ex[1];
	    	$continent=$ex[0];
		if ($value == $zone) {
		    	echo '<option value="'.$value.'" selected>'.$city.'</option>';	    		
		} else {
		    	echo '<option value="'.$value.'">'.$city.'</option>';	    		
		}
	    }
	}

	echo '		</optgroup></select>';

}
//##################################################################################
//END OF CLASS
//##################################################################################
}

//##################################################################################
//CONSTRUCT
//##################################################################################

$SMARTYUTIL = new SMARTYUTIL();

?>
