<?php

//##################################################################################
//START OF CLASS
//##################################################################################

class ADMIN {

//##################################################################################
//ALLOWS GROUPS TO BE DELETED
//##################################################################################
function admin_groups_update() {
	global $DB, $UTIL, $smarty, $dr, $path;
	
	if ($path[3] == 'delete') {
	
		if (is_numeric($path[4])) {
						
			$q = "SELECT * FROM discussion WHERE group_id = " . $path[4];
			$discussions = $DB->get_results($q);
			foreach ($discussions as $k => $v) {
				$q = "DELETE FROM post WHERE discussion_id = " . $v->id;
				$DB->query($q);
			}
			$q = "DELETE FROM discussion WHERE group_id = " . $path[4];
			$DB->query($q);
			
			$q = "DELETE FROM groups WHERE id = " . $path[4];
			$DB->query($q);
			$q = "DELETE FROM user_to_groups WHERE group_id = " . $path[4];
			$DB->query($q);
			
		}
	}
	
	$q = "SELECT * FROM groups";
	$result = $DB->get_results($q);
	
	$groups = array();
	
	foreach ($result as $g) {
		
		$g->owner_nickname = $UTIL->nickname_convert($g->group_owner);
		
		$groups[$g->group_name] = $g; 
		
	
	}
	
	ksort($groups);
	
	$smarty->assign("groups",$groups);
	
}	
//##################################################################################
//MANAGE AD SPACES
//##################################################################################
function admin_ad_update() {
	global $DB, $UTIL, $smarty,$dr;

	$fn = false;

	if (isset($_FILES['asset']) and $_FILES['asset']['size'] > 0) {
		$fn = strtolower(str_replace(" ","_",$_FILES['asset']['name']));
		move_uploaded_file($_FILES['asset']['tmp_name'],$dr . "/images/ads/" . $fn);
	}

	if (isset($_REQUEST['ad'])) {

		$q = "UPDATE ad SET ad_content = '" . $DB->escape($_REQUEST['ad']['ad_content']) . "',ad_status = " . $_REQUEST['ad']['ad_status'] . " where id = " . $_REQUEST['ad']['id'];
		$DB->query($q);

	}

	//ASSETS
	$assets = array();
	if ($handle = opendir($dr . "/images/ads/")) {
		while (false !== ($entry = readdir($handle))) {
			if ($entry != "." && $entry != "..") {
				$assets[] = $entry;
			}
		}
		closedir($handle);
	}

	$q = "SELECT * FROM ad";
	$ads = $DB->get_results($q);

	$smarty->assign("assets",$assets);
	$smarty->assign("fn",$fn);
	$smarty->assign("ads",$ads);	

}
//##################################################################################
//FAQ MANAGEMENT
//##################################################################################
function faq() {
	global $DB, $UTIL, $smarty;

	if (isset($_REQUEST['faq_submit'])) {

		$q = "DELETE FROM faq WHERE id > 0";
		$DB->query($q);
		foreach ($_REQUEST['cat'] as $k => $v) {
			$q = "INSERT INTO faq (faq,cat) VALUES (" . $k . "," . $v . ")";
			$DB->query($q);
		}

	}

	$questions = array();

	$q = "SELECT * FROM text WHERE text_group = 'faq_questions' AND language_suffix = 'en' ORDER BY text_tag";
	$result = $DB->get_results($q);

	foreach ($result as $k => $v) {
		$questions[$UTIL->faq_no($v->text_tag)] = $v->text;
	}

	$categories = array();

	$q = "SELECT text_tag,text FROM text WHERE text_group = 'faq_categories' AND language_suffix = 'en' ORDER BY text_tag";
	$result = $DB->get_results($q);

	foreach ($result as $k => $v) {
		$categories[$UTIL->faq_no($v->text_tag)] = $v->text;
	}

	$data = array();

	$q = "SELECT * FROM faq";
	$result = $DB->get_results($q);

	foreach ($result as $k => $v) {
		$data[$v->faq] = $v->cat;
	}

	$smarty->assign("questions",$questions);
	$smarty->assign("categories",$categories);
	$smarty->assign("data",$data);

}
//##################################################################################
//CONVERSATION DETAILS
//##################################################################################
function conversation($code=false) {
	global $DB, $UTIL, $smarty;

	$q = "SELECT * FROM conversation WHERE code = '" . $code . "'";
	$c = $DB->get_result($q);

	$q = "SELECT * FROM user_agent WHERE code = '" . $code . "'";
	$agents = $DB->get_result($q);

	if ($agents) {
		$c->user1_agent = $agents->user1;
		$c->user2_agent = $agents->user2;
	} else {
		$c->user1_agent = "Skype Call";
		$c->user2_agent = "Skype Call";
	}

	$c->user1_nickname = $UTIL->nickname_convert($c->user1);
	$c->user2_nickname = $UTIL->nickname_convert($c->user2);

	$smarty->assign("c",$c);
}
//##################################################################################
//ADMIN DASHBOARD
//##################################################################################
function dashboard() {
	global $DB, $UTIL, $smarty;

	//STATS

	$s = array();

	$q = "SELECT count(*) FROM user";
	$s['total_users'] = $DB->get_var($q);
	$q = "SELECT count(*) FROM user WHERE skype != ''";
	$s['total_users_skype'] = $DB->get_var($q);
	
	$q = "SELECT count(*) FROM user WHERE joined > date_sub(now(), interval 7 day)";
	$s['total_users_7_days'] = $DB->get_var($q);
	$q = "SELECT count(*) FROM heartbeat WHERE user_id > 1 AND stamp > date_sub(now(), interval 5 minute)";
	$s['total_users_logged_in'] = $DB->get_var($q);
	
	$q = "SELECT count(*) FROM conversation WHERE start < date_add(now(), interval 24 hour) AND start > now()";
	$s['conversations_next_24_hours'] = $DB->get_var($q);
	$q = "SELECT count(*) FROM conversation WHERE start < date_add(now(), interval 24 hour) AND start > now() AND mode = 'skype'";
	$s['conversations_next_24_hours_skype'] = $DB->get_var($q);

	$q = "SELECT count(*) FROM conversation WHERE start < date_add(now(), interval 4 week) AND start > now()";
	$s['conversations_next_4_weeks'] = $DB->get_var($q);
	$q = "SELECT count(*) FROM conversation WHERE start < date_add(now(), interval 4 week) AND start > now() AND mode = 'skype'";
	$s['conversations_next_4_weeks_skype'] = $DB->get_var($q);

	$q = "SELECT count(*) FROM conversation WHERE started > date_sub(now(), interval 24 hour) AND started < now()";
	$s['conversations_last_24_hours'] = $DB->get_var($q);
	$q = "SELECT count(*) FROM conversation WHERE started > date_sub(now(), interval 24 hour) AND started < now() AND mode = 'skype'";
	$s['conversations_last_24_hours_skype'] = $DB->get_var($q);
	
	$q = "SELECT count(*) FROM conversation WHERE started > date_sub(now(), interval 4 week) AND started < now()";
	$s['conversations_last_4_weeks'] = $DB->get_var($q);
	$q = "SELECT count(*) FROM conversation WHERE started > date_sub(now(), interval 4 week) AND started < now() AND mode = 'skype'";
	$s['conversations_last_4_weeks_skype'] = $DB->get_var($q);

	$q = "SELECT count(*) FROM conversation WHERE started > date_sub(now(), interval 52 week) AND started < now()";
	$s['conversations_last_52_weeks'] = $DB->get_var($q);
	$q = "SELECT count(*) FROM conversation WHERE started > date_sub(now(), interval 52 week) AND started < now() AND mode = 'skype'";
	$s['conversations_last_52_weeks_skype'] = $DB->get_var($q);	
	
	$q = "SELECT count(*) FROM conversation WHERE duration < 1 AND started != '0000-00-00 00:00:00' AND started > DATE_SUB(now(), interval 1 hour)";
	$s['conversations_current'] = $DB->get_var($q);
	$q = "SELECT count(*) FROM conversation WHERE duration < 1 AND started != '0000-00-00 00:00:00' AND started > DATE_SUB(now(), interval 1 hour) AND mode = 'skype'";
	$s['conversations_current_skype'] = $DB->get_var($q);
	

	$smarty->assign("s",$s);

	//FEEDBACK REPORTS

	$q = "SELECT * FROM message WHERE recipient = " . $_SESSION['me']->id . " AND subject = 'Conversation Feedback' AND status = 'new' AND moderated > 0 ORDER BY sent DESC LIMIT 20";
	$inbox = $DB->get_results($q);

	foreach ($inbox as $k=>$v) {
		$q = "SELECT user1,user2 FROM conversation WHERE code = '" . $v->code . "'";
		$c = $DB->get_result($q);
		$r = ($c->user1 == $v->sender) ? $c->user2 : $c->user1;
		$inbox[$k]->res_name = $UTIL->nickname_convert($r);
		$inbox[$k]->sender_name = $UTIL->nickname_convert($v->sender);
		$inbox[$k]->today = false;
		$inbox[$k]->message_id = $v->id;
		if (date("Y-m-d",strtotime($v->sent)) == date("Y-m-d")) $inbox[$k]->today = true;
	}

	$smarty->assign("reports",$inbox);

	//RECENT CONVERSATIONS

	$q = "SELECT * FROM conversation WHERE started != '0000-00-00 00:00:00' ORDER BY id DESC LIMIT 10";
	$recents = $DB->get_results($q);

	$smarty->assign("recents",$recents);

}
//##################################################################################
//PROCESS ADMIN LOGIN
//##################################################################################
function login() {
	global $DB, $UTIL, $smarty, $USER;

	$process_message = false;

	if (isset($_REQUEST['admin_login'])) {

		if (isset($_REQUEST['username']) and isset($_REQUEST['password'])) {

			if ($_REQUEST['username'] == 'admin' and md5($_REQUEST['password']) == 'b074696160962c20e085f67f3e6eaef2') {

				$q = "SELECT * FROM user WHERE id = 1";
				$user = $DB->get_result($q);

				$_SESSION['me'] = $user;
				$_SESSION['me']->account = $USER->account_data(1);
				$_SESSION['me']->profile = $USER->getprofile(1);

				$UTIL->redirect('/admin/dashboard');

			} else {

				$process_message = "Incorrect username or password";

			}

		} else {

			$process_message = "Please enter a username and password";

		}

	}

	$smarty->assign("process_message",$process_message);

}
//##################################################################################
//MANAGE USER ACCOUNT
//##################################################################################
function user_account($user=false) {
	global $DB, $USER, $smarty, $UTIL, $language, $text;

	$user_id = $UTIL->nickname_convert($user);

	$me->profile = $USER->getprofile($user_id);
	$smarty->assign("me",$me);
	
	$q = "SELECT suspend FROM user WHERE id = " . $user_id;
	$suspend = $DB->get_var($q);
	$smarty->assign("suspend",$suspend);
	
	if (isset($_REQUEST['update_user_account_suspend'])) {
		
		if (isset($_REQUEST['account_suspend'])) {
			$suspend = 1;
		} else {
			$suspend = 0;	
		}
			
		$q = "UPDATE user SET suspend = " . $suspend . " WHERE id = " . $user_id;
		$DB->query($q);
		$smarty->assign("suspend",$suspend);		
					
	}

	if (isset($_REQUEST['update_user_account_password'])) {

		if ($_REQUEST['me']['password'] == $_REQUEST['me']['confirm_password']) {
			$q = "UPDATE user SET password = '" . crypt($_REQUEST['me']['password']) . "' WHERE id = " . $user_id;
			$DB->query($q);
			$_SESSION['me']->password = crypt($_REQUEST['me']['password']);
			$smarty->assign("process_message",$text[$_SESSION['language']]['messages']['accountupdated_ok']->text);

		} else {
			$smarty->assign("process_message",$text[$_SESSION['language']]['messages']['passwordmatch_error']->text);
		}

	}

	if (isset($_REQUEST['update_user_account_email'])) {

		if ($_REQUEST['me']['email'] == $_REQUEST['me']['confirm_email']) {

			$q = "SELECT id FROM user WHERE email = '" . $_REQUEST['me']['email'] . "'";
			$user = $DB->get_var($q);

			if ($user) {
				$smarty->assign("process_message",$text[$_SESSION['language']]['messages']['emailexists_error']->text);
			} else {
				$q = "UPDATE user SET email = '" . $_REQUEST['me']['email'] . "' WHERE id = " . $user_id;
				$DB->query($q);
				$_SESSION['me']->email = $_REQUEST['me']['email'];
				$smarty->assign("process_message","Profile updated");
	
			}
			
		} else {
			$smarty->assign("process_message",$text[$_SESSION['language']]['messages']['emailmatch_error']->text);
		}

	}

	if (isset($_REQUEST['update_user_account_rating_on'])) {

		$q = "UPDATE user SET rating_on = " . $_REQUEST['rating_on'] . " WHERE id = " . $user_id;
		$DB->query($q);
		$me->profile = $USER->getprofile($user_id);
		$smarty->assign("me",$me);

	}

	$smarty->display("admin_user_account.tpl");

}
//##################################################################################
//MANAGE USER PROFILE
//##################################################################################
function user_profile($user=false) {
	global $DB, $USER, $smarty, $UTIL, $language, $text;
	
	$user_id = $UTIL->nickname_convert($user);
	$nickname = $user;
	
	$user = $USER->account_data($user_id);
	
	$fields = false;
	$avatar = false;
	$not_unique = false;

	//SELECTED LANGUAGES
	$q = "SELECT language_id,level FROM user_to_language WHERE user_id = " . $user_id;
	$results = $DB->get_results($q);
	foreach ($results as $k=>$v) {
		$selected_languages[] = $v->language_id;
		$language_levels[$v->language_id] = $v->level;
	}
	$smarty->assign("selected_languages",$selected_languages);
	$smarty->assign("language_levels",$language_levels);

	//AVATAR UPLOAD
	if (isset($_FILES['avatar']) and $_FILES['avatar']['size'] > 0) {
		$avatar = $UTIL->avatar($user_id);
		$smarty->assign('avatar',$avatar);
	}

	//AVATAR CROP
	if (isset($_REQUEST['coords'])) {
		$avatar_cropped = $UTIL->crop_avatar($user_id);
		$smarty->assign('avatar_cropped',$avatar_cropped);
	}

	//UPDATE AVAILABILITY
	if (isset($_REQUEST['times'])) {

		$_REQUEST['times']['always'] = (isset($_REQUEST['times']['always'])) ? $_REQUEST['times']['always'] : 0; 	
		$exists = false;
		$q = "SELECT id FROM availability WHERE user_id =" . $user_id;
		$exists = $DB->get_var($q);
		$start1 = $_REQUEST['times']['period1_start']['Time_Hour'] . ":" . $_REQUEST['times']['period1_start']['Time_Minute'] . ":00";
		$end1 = $_REQUEST['times']['period1_end']['Time_Hour'] . ":" . $_REQUEST['times']['period1_end']['Time_Minute'] . ":00";
		$start2 = $_REQUEST['times']['period2_start']['Time_Hour'] . ":" . $_REQUEST['times']['period2_start']['Time_Minute'] . ":00";
		$end2 = $_REQUEST['times']['period2_end']['Time_Hour'] . ":" . $_REQUEST['times']['period2_end']['Time_Minute'] . ":00";
		
		//GMT OFFSET
		if (!empty($_REQUEST['times']['zone'])) {
			$d = new DateTimeZone($_REQUEST['times']['zone']);
			$dt = new DateTime("now", $d);
			$offset = $d->getOffset($dt)/3600;
		} else {
			$offset = 0;
		}

		if (!$exists) {
			$q = "INSERT INTO availability (user_id,start1,end1,start2,end2,zone,offset,always) VALUES (". $user_id . ",'" . $start1 . "','" . $end1 . "','" . $start2 . "','" . $end2 . "','" . $_REQUEST['times']['zone'] . "'," . $offset . "," . $_REQUEST['times']['always'] . ")";
		} else {
			$q = "UPDATE availability SET start1 = '" . $start1 . "',end1 = '" . $end1 . "', start2 = '" . $start2 . "', end2 = '" . $end2 . "', zone = '" . $_REQUEST['times']['zone'] . "',offset = " . $offset . ", always = " . $_REQUEST['times']['always'] . " WHERE user_id = " . $user_id;

		}
		$DB->query($q);
	}

	//AVATAR EXISTS?
	if ($UTIL->avatar_exists($user_id)) {
		$smarty->assign('avatar_exists',1);
	}

	if (isset($_REQUEST['languages']) and is_array($_REQUEST['languages'])) {

		//UPDATE LANGUAGES
		$q = "DELETE FROM user_to_language WHERE user_id = " . $user_id;
		$DB->query($q);

		foreach ($_REQUEST['languages'] as $k => $v) {
			$q = "INSERT INTO user_to_language (user_id,language_id,level) values (" . $user_id . "," . $v . ",'" . $_REQUEST['levels'][$v] . "')";
			$UTIL->log($q);
			$DB->query($q);
		}

		//SELECTED LANGUAGES
		$selected_languages = array();
		$language_levels = array();
		$q = "SELECT language_id,level FROM user_to_language WHERE user_id = " . $user_id;
		$results = $DB->get_results($q);
		foreach ($results as $k=>$v) {
			$selected_languages[] = $v->language_id;
			$language_levels[$v->language_id] = $v->level;
		}
		$smarty->assign("selected_languages",$selected_languages);
		$smarty->assign("language_levels",$language_levels);

	}


	if (isset($_REQUEST['me'])) {

		foreach ($_REQUEST['me'] as $k => $v) {

			$q = "SELECT * FROM user_data_field WHERE id = " . $k;
			$field = $DB->get_result($q);

		}



		//UPDATE DB OCCURS HERE
		//FIRST CLEAR ANY MULTIPLE OPTION VALUES
		$q = "UPDATE user_data SET value_opts = '' WHERE user_id = " . $user_id;
		$DB->query($q);
		//SECOND CLEAR ANY NON SYS SINGLE OPTION VALUES
		$q = "UPDATE user_data SET value_opt = '' WHERE user_id = " . $_SESSION['me']->id . " AND field_id NOT IN (SELECT id FROM user_data_field WHERE sys > 0)";
		$DB->query($q);

		//NOW REPOPULATE THE TABLE BASED ON FORM SUBMISSION
		foreach ($_REQUEST['me'] as $k => $v) {

			$q = "SELECT id FROM user_data WHERE user_id = " . $user_id . " AND field_id = " . $k;
			$id = $DB->get_var($q);

			$m = ($id) ? 'update' : 'insert';

			$q = "SELECT * FROM user_data_field WHERE id = " . $k;

			$field = $DB->get_result($q);

			//CHECK IF UNIQUE VALUE REQUIREMENT IS SATISFIED
			if ($field->uniq > 0) {
				$q = "SELECT id FROM user_data WHERE value_" . $field->data_type . " = '" . $v . "' AND user_id != " . $user_id;
				$result = $DB->get_var($q);
				if ($result) {
					$smarty->assign("process_message",$text[$_SESSION['language']]['user_data'][$field->tag]->text . ": " . $text[$_SESSION['language']]['messages']['uniquevalue_error']->text);
					$not_unique = true;
				}

			} else {

				$not_unique = false;

			}

			if (is_array($v) and $field->data_type == "opts") {
				$s = "[";
				foreach ($v as $o) {
					$s .= '"' . $o . '",';
				}
				$s = rtrim($s,",");
				$s .= "]";
				$v = $s;
			}

			if (!$not_unique) {
				$data = array($k => array("field_id" => $k, "type" => $field->data_type, "m" => $m, "value" => $v));
				$USER->putdata($user_id,$data);
			}

		}
		if ($smarty->getTemplateVars('process_message') == null) {
			$smarty->assign("process_message","Profile updated");
		}

	}

	$q = "SELECT * FROM user_data_field WHERE entry_method != 'system' AND (account = '" . $user['account_type'] . "' OR account = 'all') ORDER BY priority";
	$fields = $DB->get_results($q);

	if (!empty($fields)) {
		foreach ($fields as $k => $v) {
			if (!empty($v->options)) {
				$a = array();
				$v->options = json_decode($v->options,true);
				asort($v->options);
				foreach ($v->options as $r => $s) {
					$o = $text[$_SESSION['language']]['user_data_op'][strtolower($UTIL->systag($s))]->text;
					$a[$r] = $o;
				}
				$fields[$k]->options_keys = array_keys($v->options);
				$fields[$k]->options = $a;
			}
		}
		$smarty->assign('fields',$fields);
	}

	$q = "SELECT * FROM user_data WHERE user_id = " . $user_id;
	$data = $DB->get_results($q);

	foreach ($data as $k => $v) {
		if (!empty($v->value_opts)) {
			$data[$k]->value_opts = json_decode($v->value_opts);
		}
	}

	$user = array();
	$user['id'] = $user_id;

	foreach ($data as $k => $v) {
		$user[$v->field_id] = $v;
	}
	//GET AVAILABILITY
	$q = "SELECT * FROM availability WHERE user_id = " . $user_id;
	$av = $DB->get_result($q);
	if ($av) {
		$user['availability'] = $av;
	} else {
		$user['availability']->start1 = '00:00:00';
		$user['availability']->end1 = '00:00:00';
		$user['availability']->start2 = '00:00:00';
		$user['availability']->end2 = '00:00:00';
		$user['availability']->zone = '';
		$user['availability']->always = 1;
	}
	
	//STATISTICS
	$q = "SELECT count(*) FROM conversation WHERE user1 = " . $user_id . " OR user2 = " . $user_id . " AND duration > 0";
	$user['statistics']['data']['total_conversations'] = $DB->get_var($q);
	$q = "SELECT count(*) FROM conversation WHERE user1 = " . $user_id . " OR user2 = " . $user_id . " AND duration > 0 AND start > date_sub(now(),interval 28 day)";
	$user['statistics']['data']['total_conversations_28'] = $DB->get_var($q);

	$q = "SELECT sum(duration) FROM conversation WHERE user1 = " . $user_id . " OR user2 = " . $user_id;
	$user['statistics']['data']['total_duration'] = $DB->get_var($q);
	$q = "SELECT sum(duration) FROM conversation WHERE user1 = " . $user_id . " OR user2 = " . $user_id . " AND start > date_sub(now(),interval 28 day)";
	$user['statistics']['data']['total_duration_28'] = $DB->get_var($q);
	
	//NICKNAME
	$user['nickname'] = $nickname;
	
	//ACCOUNT
	$q = "SELECT * FROM user WHERE id = " . $user_id;
	$user['account'] = $DB->get_result($q,true);

	//CONVERSATION FEEDBACK
	$q = "SELECT message.*,conversation.user1,conversation.user2 FROM message LEFT JOIN conversation ON message.code = conversation.code WHERE (user1 = " . $user_id . " OR user2 = " . $user_id . ") AND subject = 'Conversation Feedback' ORDER BY sent DESC";
	$user['feedback'] = $DB->get_results($q,true);
	
	$smarty->assign('user',$user);
	$smarty->assign('required',false);
	$smarty->display('admin_user_profile.tpl');

}
//##################################################################################
//UPDATE LANGUAGE AVAILABILITY
//##################################################################################
function admin_language_update() {
	global $DB, $smarty, $UTIL, $all_language;

	$message = false;

	//UPDATE SITE ENABLED LANGUAGES
	if (isset($_POST['language_update'])) {

		$q = "UPDATE language SET language_active = 0";
		$DB->query($q);

		if (isset($_POST['enabled_languages'])) {

			foreach ($_POST['enabled_languages'] as $k => $v) {

				$q = "UPDATE language SET language_active = 1 WHERE language_id = " .  $v;
				$DB->query($q);

			}

		}

		//RELOAD LANGUAGES
		$all_language = $UTIL->language(0);
		$smarty->assign("all_language",$all_language);

		$language = $UTIL->language(1);
		$smarty->assign("language",$language);

	}

	//UPDATE PROFILE ENABLED LANGUAGES
	if (isset($_POST['profile_language_update'])) {

		$q = "UPDATE language SET language_profile = 0";
		$DB->query($q);

		if (isset($_POST['profile_languages'])) {

			foreach ($_POST['profile_languages'] as $k => $v) {

				$q = "UPDATE language SET language_profile = 1 WHERE language_id = " .  $v;
				$DB->query($q);

			}

		}

		//RELOAD LANGUAGES
		$all_language = $UTIL->language(0);
		$smarty->assign("all_language",$all_language);

		$language = $UTIL->language(1);
		$smarty->assign("language",$language);

	}

	//ADD A LANGUAGE
	if (isset($_POST['language_add'])) {

		//CHECK IF ALREADY EXISTS
		$q = "SELECT language_id FROM language WHERE language_suffix = '" . $_POST['language_suffix'] . "' OR language_name = '" . $_POST['language_name'] . "'";

		$result = $DB->get_var($q);

		if ($result) {
			$message = "That language already exists";
		} else {
			//ADD LANGUAGE
			$q = "INSERT INTO language (language_name,language_suffix,language_active,language_profile) VALUES ('" . $_POST['language_name'] . "','" . $_POST['language_suffix'] . "',0,1)";
			$DB->query($q);	
			$q = "SELECT * FROM static WHERE language_suffix = 'en'";
			
			$static = $DB->get_results($q);	
			foreach ($static as $k => $v) {
				$q = "INSERT INTO static (static_tag,language_suffix,static_content,static_status,static_title) VALUES ('" . $v->static_tag . "','" . $_POST['language_suffix'] . "','" . addslashes($v->static_content) . "',0,'" . $v->static_title . "')";
				$DB->query($q);	
			}

			//ADD LANGUAGE NAME AS TEXT
			if ($all_language) {
				foreach (array_keys($all_language) as $l) { 
					$q = "INSERT INTO text (text_group,text_tag,text_description,language_suffix,text) values ('languages','" . $UTIL->systag($_POST['language_name']) . "','" . $_POST['language_name'] . " language','" . $l . "','" . $_POST['language_name'] . "')";
					$result = $DB->query($q);
				}
			} 

			//ADD A VERSION FOR EACH TEXT FOR NEW LANGUAGE		
			$q = "SELECT * FROM text WHERE language_suffix = 'en'";
			
			$txt = $DB->get_results($q);
			foreach ($txt as $k => $v) {
				$q = "INSERT INTO text (language_suffix,text_description,text_tag,text_group,text,text_type) VALUES ('" . $_POST['language_suffix'] . "','" . $v->text_description . "','" . $v->text_tag . "','" . $v->text_group . "','" . $v->text . "','" . $v->text_type . "')";
				$DB->query($q);	
			}

		}

		//RELOAD LANGUAGES
		$all_language = $UTIL->language(0);
		$smarty->assign("all_language",$all_language);

		$language = $UTIL->language(1);
		$smarty->assign("language",$language);

	}

	$smarty->assign("message",$message);

}
//##################################################################################
//UPDATE THE STATIC CONTENT
//##################################################################################
function admin_content_update($language_suffix=false,$static_tag=false) {
	global $DB, $smarty, $UTIL, $all_language;

	$contents = false;
	$content = false;

	if (isset($_POST['content'])) {

		if (isset($_POST['content']['add'])) {

			foreach ($all_language as $k => $v) {

				$q = "INSERT INTO static (static_title,static_tag,static_content,language_suffix,static_menu,static_status,static_title_on,static_menu_on,static_order) VALUES ('" . $_POST['content']['static_title'] . "','" . $UTIL->systag($_POST['content']['static_title']) . "','" . $_POST['content']['static_content'] . "','" . $k  . "','" . $_POST['content']['static_menu'] . "'," . $_POST['content']['static_status'] . "," . $_POST['content']['static_title_on'] . "," . $_POST['content']['static_menu_on'] . "," . $_POST['content']['static_order'] . ")";
				$DB->query($q);

			}

		} elseif (isset($_POST['content']['update'])) {

				$q = "UPDATE static SET static_title = '" . $_POST['content']['static_title'] . "', static_content = '" . $_POST['content']['static_content'] . "', static_status = " . $_POST['content']['static_status'] . ", static_title_on = " . $_POST['content']['static_title_on'] . ", static_menu_on = " . $_POST['content']['static_menu_on'] . " WHERE id = " . $_POST['content']['id'];
				$DB->query($q);	
				
				foreach ($all_language as $k => $v) {

					$q = "UPDATE static SET static_menu = '" . $_POST['content']['static_menu'] . "', static_order = " . $_POST['content']['static_order'] . " WHERE static_tag = '" . $_POST['content']['static_tag'] . "'";
					$DB->query($q);

				}
			
		}

	}

	if ($static_tag and $language_suffix) {

		$q = "SELECT static.*,language.language_name,language.language_suffix FROM static LEFT JOIN language ON language.language_suffix = static.language_suffix WHERE static_tag = '" . $static_tag . "' AND static.language_suffix = '" . $language_suffix . "'";
		$content = $DB->get_result($q);

	} else {

		$contents = array();
		$q = "SELECT * FROM static LEFT JOIN language ON static.language_suffix = language.language_suffix WHERE language.language_active > 0 ORDER BY static.language_suffix";
		$rows = $DB->get_results($q);
		foreach ($rows as $k => $v) {
			$contents[$v->static_tag][$v->language_suffix] = $v;			
		}
	}

	//MENU OPTIONS
	$default_menus = array("top"=>"Header","bot"=>"Footer");
	$smarty->assign("default_menus",$default_menus);

	//SYSTEM LANGUAGES
	$system_languages = array();
	foreach ($all_language as $k => $v) {
		$system_languages[$v->language_id] = $v->language_name;
	}
	$smarty->assign('system_languages',$system_languages);

	$smarty->assign("content",$content);
	$smarty->assign("contents",$contents);

}
//##################################################################################
//UPDATE THE CONFIG OBJECT
//##################################################################################
function admin_config_update() {
	global $DB, $config;

	if (isset($_POST['config'])) {

		foreach (array_keys($_POST['config']) as $tag) {

			foreach (array_keys($_POST['config'][$tag]['data']) as $item) {
				$config[$tag]->data->$item->value = $_POST['config'][$tag]['data'][$item]['value'];
			}

			$jtag = json_encode($config[$tag]->data);

			$q = "UPDATE config SET data = '" . $jtag . "' WHERE tag = '" . $tag . "'";
			$DB->query($q);

		}

	}

}
//##################################################################################
//LANGUAGE TEXT UPDATE
//##################################################################################
function admin_text_update() {
	global $DB, $UTIL, $smarty, $all_language;

	if (isset($_POST['update_text']) > 0 and !empty($_POST['text'])) {
		$q = "UPDATE text SET text = '" . $_POST['text'] . "' WHERE text_id=" . $_POST['text_id'];
		$result = $DB->query($q);
	}

	if (isset($_POST['add_text']) > 0 and !empty($_POST['text'])) {

		if ($all_language) {

			$_POST['text_group'] = (!empty($_POST['new_text_group'])) ? $_POST['new_text_group'] : $_POST['text_group'];

			foreach (array_keys($all_language) as $l) { 
				$q = "INSERT INTO text (text_group,text_tag,text_description,language_suffix,text) values ('" . $_POST['text_group'] . "','" . $_POST['text_tag'] . "','" . $_POST['text_description'] . "','" . $l . "','" . $_POST['text'] . "')";
				$result = $DB->query($q);
			}
		} 

	}

	$text = $UTIL->text();
	$smarty->assign("text",$text);

	$_POST['ref_language_suffix'] = (!empty($_POST['ref_language_suffix'])) ? $_POST['ref_language_suffix'] : false;
	$_POST['trans_language_suffix'] = (!empty($_POST['trans_language_suffix'])) ? $_POST['trans_language_suffix'] : false;
	$_POST['text'] = (!empty($_POST['text'])) ? $_POST['text'] : 0;
	$_POST['selected_text_id'] = (!empty($_POST['selected_text_id'])) ? $_POST['selected_text_id'] : 0;

	$req_tag = false;
	$req_group = false;

	if (isset($_POST['req_tag'])) {
		foreach ($_POST['req_tag'] as $k => $v) {
			if ($v !== '0') {
				$req_tag = $v;
				$req_group = $k;
			}	
		}
	}

	$smarty->assign('ref_language_suffix',$_POST['ref_language_suffix']);
	$smarty->assign('trans_language_suffix',$_POST['trans_language_suffix']);
	$smarty->assign('req_tag',$req_tag);
	$smarty->assign('req_group',$req_group);
	$smarty->assign('selected_text_id',$_POST['selected_text_id']);

}
//##################################################################################
//USER DATA FIELD DELETE
//##################################################################################
function admin_data_update_delete($field_tag=false) {
	global $DB, $smarty, $all_language, $text, $UTIL;

	//PREVENT DELETION OF SYSTEM FIELDS
	if (isset($field_tag)) {
		$q = "SELECT sys FROM user_data_field WHERE tag = '" . $field_tag . "'";
		$sys = $DB->get_var($q); 
	}

	if ($sys < 1) {

		if (isset($field_tag)) {
			$q = "SELECT id FROM user_data_field WHERE tag = '" . $field_tag . "'";
			$field_id = $DB->get_var($q); 
		}

		$q = "DELETE FROM user_data_field WHERE id = " . $field_id;
		$DB->query($q);
		$q = "DELETE FROM text WHERE text_tag = '" . $field_tag . "' and text_group = 'user_data'";
		$DB->query($q);
		$q = "DELETE FROM user_data WHERE field_id = " . $field_id;
		$DB->query($q);

	} else {
		$process_message = "You cannot delete a system data field";
		$smarty->assign("process_message",$process_message);
	}

	$this->admin_data_update();

}
//##################################################################################
//USER DATA UPDATE
//##################################################################################
function admin_data_update($field_tag=false) {
	global $DB, $smarty, $all_language, $text, $UTIL;

	$message = false;
	$field = false;
	$field_id = false;

	if (isset($field_tag)) {
		$q = "SELECT id FROM user_data_field WHERE tag = '" . $field_tag . "'";
		$field_id = $DB->get_var($q); 
	}

	//DEAL WITH DELETED OPTIONS
	if (!empty($_REQUEST['field']['deleted_options'])) {
		$dopts = explode(",",rtrim($_REQUEST['field']['deleted_options'],","));

		$q = "SELECT data_type FROM user_data_field WHERE id = " . $_REQUEST['field_id'];
		$t = $DB->get_var($q);

		$q = "SELECT options FROM user_data_field WHERE id = " . $_REQUEST['field_id'];
		$opts = json_decode($DB->get_var($q),true);

		//SINGLE OPTION
		if ($t == 'opt') {
			foreach ($dopts as $do) {
				$i = array_search($do,$opts);
				$q = "DELETE FROM user_data WHERE field_id = " . $_REQUEST['field_id'] . " AND value_opt = " . $i;
			}
		//MULTI OPTION
		} elseif ($t == 'opts') {

			$q = "SELECT * FROM user_data WHERE field_id = " . $_REQUEST['field_id'];
			$rows = $DB->get_results($q);

			foreach ($rows as $k => $v) {

				$uopts = json_decode($v->value_opts);
				//echo "User options are:<br>"; print_r($uopts);
				//echo "<p>";
				foreach ($dopts as $do) {
					//echo "Looking for " . $do . " in system options<br>";
					$i = array_search($do,$opts);

					if ($i) {
						//echo "found at key " . $i . "<br>";					
						//echo "removing from user options<br>";
						//echo "Looking for " . $i . " in user options<br>";
						$x = array_search($i,$uopts);
						if ($x) {
							//echo "found at key " . $x . "<br>";					

							unset($uopts[$x]);
						}
						
					}
				}
				//echo "User options are:<br>"; print_r($uopts);
				if (isset($uopts[0])) {
					$new_opts = implode('","',$uopts);
					$new_opts = '"' . $new_opts . '"';
					$q = "UPDATE user_data SET value_opts = '[" . $new_opts . "]' WHERE id = " . $v->id;
				} else {
					$q = "DELETE FROM user_data WHERE id = " . $v->id;
				}
				$DB->query($q);
			}
		}
	}	

	//DELETE AND RE-ADD FOR EDITS
	if (isset($_REQUEST['field_id']) and is_numeric($_REQUEST['field_id'])) {
		//GET EXISTING TAG
		$q = "SELECT tag FROM user_data_field WHERE id = " . $_REQUEST['field_id'];
		$tag = $DB->get_var($q);
		//DELETE EXISTING TAG FROM TEXT
		$q = "DELETE FROM text WHERE text_tag = '" . strtolower($_REQUEST['field']['tag']) . "' AND text_group = 'user_data'";
		$DB->query($q);
		//DELETE FROM FIELDS
		$q = "DELETE FROM user_data_field WHERE id = " . $_REQUEST['field_id'];
		$DB->query($q);
	}

	if (isset($_REQUEST['add_user_data']) and is_array($_REQUEST['field'])) {

		$q = "SELECT id FROM user_data_field WHERE tag = '" . $_REQUEST['field']['tag'] . "'";
		$tag = $DB->get_var($q);

		if ($tag) {

			$message = "That tag already exists";

		} else {

			//UPDATE OCCURS HERE		
			$q = "INSERT INTO user_data_field (tag,data_type,entry_method,required,account,uniq,priority,search,hidden) VALUES ('" . strtolower($_REQUEST['field']['tag']) . "','" . $_REQUEST['field']['data_type'] . "','" . $_REQUEST['field']['entry_method'] . "','" . $_REQUEST['field']['required'] . "','" . $_REQUEST['field']['account'] . "'," . $_REQUEST['field']['uniq'] . "," . $_REQUEST['field']['priority'] . "," . $_REQUEST['field']['search'] . "," . $_REQUEST['field']['hidden'] . ")";
			$id = $DB->query($q);
			//IF EDITING THE FIELD
			if ($field_id) {
				//UPDATE USER DATA TABLE WITH NEW FIELD ID
				$q = "UPDATE user_data SET field_id = ". $id . " WHERE field_id = " . $field_id;
				$DB->query($q);
				//RE-ASSIGN FIELD ID FOR REDISPLAY OF EDITED FIELD (SEE BELOW)
				$field_id = $id;
			}
			
			foreach (array_keys($all_language) as $l) { 

				$q = "INSERT INTO text (text_group,text_tag,text_description,language_suffix,text) values ('user_data','" . strtolower($_REQUEST['field']['tag']) . "','" . $_REQUEST['field']['caption'] . "','" . $l . "','" . $_REQUEST['field']['caption'] . "')";
				$result = $DB->query($q);

			}

			if ($_REQUEST['field']['entry_method'] != 'text' and $_REQUEST['field']['entry_method'] != 'textarea') {

				if ($id and is_array($_REQUEST['field']['options'])) {
					$s = "{";
					foreach ($_REQUEST['field']['options'] as $k => $v) {
						$s .= '"' . $k . '":"' . $UTIL->systag($v) . '",';
					}
					$s = rtrim($s,",");
					$s .= "}";
					$q = "UPDATE user_data_field SET options = '" . $s . "' WHERE id = " . $id;
					$DB->query($q);

					foreach ($_REQUEST['field']['options'] as $o) {
						if (!is_numeric($o)) {
							$q = "SELECT text_id FROM text WHERE text_group = 'user_data_op' AND text_tag = '" . $UTIL->systag($o) . "' LIMIT 1";
							$t = $DB->get_var($q);

							if (!$t) {

								foreach (array_keys($all_language) as $l) {
														$q = "INSERT INTO text (text_group,text_tag,text_description,language_suffix,text) values ('user_data_op','" . $UTIL->systag($o) . "','" . $o . "','" . $l . "','" . $o . "')";
	$result = $DB->query($q);



								}

							}
						}
					}

				}

			}

		}

	}

	//GET FIELD FOR EDIT
	if ($field_id and is_numeric($field_id)) {
		$q = "SELECT * FROM user_data_field WHERE id = " . $field_id;
		$field = $DB->get_result($q);
		$field->options = json_decode($field->options,true);
		if (!empty($field->options)) asort($field->options);	
	}

	//ACCOUNT TYPES
	$q = "SELECT options FROM user_data_field WHERE tag = 'account_type'";
	$types = json_decode($DB->get_var($q),true);
	array_unshift($types, "all");
	$account_types = array();
	foreach ($types as $k => $v) {
		$account_types[$v] = $v;
	}

	$smarty->assign("account_types",$account_types);

	//FORM ELEMENTS
	$data_type = array("opt" => "Option", "opts" => "Options", "int" => "Number", "vc" => "Word or words", "txt" => "Paragraph" );
	$smarty->assign("data_type_select",$data_type);

	$entry_method = array("0" => "Select:", "system" => "System", "text" => "Text input", "textarea" => "Textarea", "radio" => "Radio buttons", "checkbox" => "Checkboxes", "select" => "Drop down menu (single choice)", "select_mul" => "Drop down menu (multiple choice)");
	$smarty->assign("entry_method_select",$entry_method);

	$default_radios = array("0" => "No", "1" => "Yes");
	$smarty->assign("default_radios",$default_radios);

	$priority = array(0,1,2,3,4,5,6,7,8,9,10);
	$smarty->assign("priority",$priority);

	//RELOAD TEXT OBJECT
	$text = $UTIL->text();

	$q = "SELECT * FROM user_data_field ORDER BY priority";
	$fields = $DB->get_results($q);

	$smarty->assign("selected_language","en");
	$smarty->assign("field",$field);
	$smarty->assign('text',$text);
	$smarty->assign('message',$message);
	$smarty->assign('fields',$fields);


}
//##################################################################################
//END OF CLASS
//##################################################################################
}

//##################################################################################
//CONSTRUCT
//##################################################################################

$ADMIN = new ADMIN();

?>
