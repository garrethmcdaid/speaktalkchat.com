<?php

//##################################################################################
//START OF CLASS
//##################################################################################

class START {

//##################################################################################
//SEND NOTIFICATIONS THAT CONVERSATION IS ABOUT TO START
//##################################################################################
function notify() {
	global $DB,$UTIL,$text;

	$conversations = array();

	$q = "SELECT * FROM conversation WHERE start = date_format(now(),'%Y-%m-%d %H:%i') AND status = 'confirmed'";
	$cs = $DB->get_results($q);

	if ($cs) {
		foreach ($cs as $k => $v) {
			$conversations[$v->code]['user1']['id'] = $v->user1;
			$conversations[$v->code]['user2']['id'] = $v->user2;
			$conversations[$v->code]['user1']['nickname'] = $UTIL->nickname_convert($v->user1);
			$conversations[$v->code]['user2']['nickname'] = $UTIL->nickname_convert($v->user2);
		}
	}

	//REMINDERS
	foreach ($conversations as $k => $v) {

		foreach ($v as $kk => $vv) {

			//SEND NOTIFICATION
			$n = '{"d":"' .  date("Y-m-d H:i:s") . '","u":"admin","m":"conversation_start","l":"/member/' . $vv['nickname'] . '/c/now/' . $k . '"}';
			$q = "UPDATE heartbeat set message = '' WHERE user_id = " . $vv['id'];
			$DB->query($q);
			$q = "UPDATE heartbeat set message = '" . $n . "' WHERE user_id = " . $vv['id'];
			$DB->query($q);

		}

	}

}
//##################################################################################
//RECORD START OF SKYPE CALL IF BOTH USERS ARE ONLINE
//##################################################################################
function skype_start() {
	global $DB, $UTIL;
	
	$s = json_decode(stripslashes($_REQUEST['json']));
	
	$q = "SELECT * FROM conversation WHERE code = '" . $s->code . "'";
	$c = $DB->get_result($q);
	
	if ($c->user1 == $_SESSION['me']->id) {
		$q = "UPDATE conversation SET started = now() WHERE code = '" . $c->code . "'";
		$result = $DB->query($q);
		return 0;
	} else {
		return 1;	
	}	
	
}
//##################################################################################
//FEEDBACK
//##################################################################################
function feedback() {
	global $DB, $UTIL, $text, $MESSAGE;

	$s = json_decode(stripslashes($_REQUEST['json']));

	$user = $_SESSION['me']->profile->nickname;

	$link = addslashes('<a href="/admin/conversation/' . $s->code . '">' . $s->code . '</a>');

	$m = '{"thread":"0","code":"' . $s->code . '","recipient":"1","sender":"' . $_SESSION['me']->id . ' ","subject":"Conversation Feedback","the_message":"Feeback has been received for the following conversation:<br><br>' . $link . '<br><br>' . $s->feedback . '"}';

	$MESSAGE->send($m);

	$response['data'] = $text[$_SESSION['language']]['messages']['feedback_updated']->text;
	$response['flag'] = 'feedback_updated';
	$response = json_encode($response);

	echo $response;

}
//##################################################################################
//CONVERSATION REVIEW
//##################################################################################
function review($code=false) {
	global $DB, $UTIL, $text, $smarty;

	$q = "SELECT * FROM conversation WHERE code = '" . $code . "'";
	$c = $DB->get_result($q);

	//CHECK IF CONVERSATION STARTED
	if ($c->started == "0000-00-00 00:00:00") {
		$UTIL->redirect("/user/dashboard");
		return;
	}

	//RECORD END DATA
	if ($c->duration < 1) {
		$duration = date("U") - date("U",strtotime($c->started));
		$q = "UPDATE conversation SET duration = " . $duration . " WHERE code = '" . $code . "'";
		$DB->query($q);
	}

	$q = "UPDATE conversation SET active = 0 WHERE code = '" . $code . "'";
	$DB->query($q);

	$users = array();

	if ($c->user1 == $_SESSION['me']->id) {
		$rating = $UTIL->rating($c->user2);
		$users['user_id'] = $c->user2;
		$users['viewer_id'] = $c->user1;
		
	} else {
		$rating = $UTIL->rating($c->user1);
		$users['user_id'] = $c->user1;
		$users['viewer_id'] = $c->user2;
	}

	$smarty->assign("rating",$rating);
	$smarty->assign("users",$users);
	$smarty->assign("code",$code);				
	$smarty->display("conversation_review.tpl");

}
//##################################################################################
//CONVERSATION DATA
//##################################################################################
function data($code=false,$user=false,$l=false) {
	global $DB, $UTIL, $text;

	$q = "SELECT * FROM conversation WHERE code = '" . $code . "'";
	$con = $DB->get_result($q);

	if (!$l) $l = "en";

	if ($con) {

		$data = new START();

		if ($user == "user1") {
			$data->c->you = $UTIL->nickname_convert($con->user1);
			$data->c->other_user = $UTIL->nickname_convert($con->user2);
		} else {
			$data->c->you = $UTIL->nickname_convert($con->user2);
			$data->c->other_user = $UTIL->nickname_convert($con->user1);
		}

		//LANGUAGE
		$data->c->stop = $text[$l]['chat']['stop']->text;
		$data->c->mute = $text[$l]['chat']['mute']->text;
		$data->c->send = $text[$l]['chat']['send']->text;
		$data->c->callingLbl = $text[$l]['chat']['calling']->text;
		$data->c->connected = $text[$l]['chat']['connected']->text;
		$data->c->disconnected = $text[$l]['chat']['disconnected']->text;
		$data->c->duration = $text[$l]['chat']['duration']->text;
		$data->c->call = $text[$l]['chat']['call']->text;
		$data->c->accept = $text[$l]['chat']['accept']->text;
		$data->c->Hangup = $text[$l]['chat']['hangup']->text;
		$data->c->firstPtyMsg = $text[$l]['chat']['first_party_message']->text;
		$data->c->cancel = $text[$l]['chat']['cancel']->text;
		$data->c->reject = $text[$l]['chat']['reject']->text;
		$data->c->inviteMsg = $text[$l]['chat']['invite_message']->text;
		$data->c->status = $text[$l]['chat']['status']->text;
		$data->c->stpVideoBtnLbl = $text[$l]['chat']['stop_video_button']->text;
		$data->c->strtVideoBtnLbl = $text[$l]['chat']['start_video_button']->text;
		$data->c->strtVideoMsg = $text[$l]['chat']['start_video_message']->text;
		$data->c->VideoQualMsg = $text[$l]['chat']['video_quality_message']->text;

	} else {
		return;
	}

	$data = json_encode($data);

	echo $data;
}
//##################################################################################
//MANAGE THE CIRRUS KEY
//##################################################################################
function key($code=false,$k=false) {
	global $DB;

	if ($k) {
		$q = "UPDATE conversation SET ckey = '" . $k . "' WHERE code = '" . $code . "'";
		$DB->query($q);
		return;
	} else {
		$q = "SELECT ckey FROM conversation WHERE code = '" . $code . "'";
		$result = $DB->get_var($q);
		if ($result) {
			$q = "UPDATE conversation SET started = now(),active = 1 WHERE code = '" . $code . "'";
			$DB->query($q);
		}
		echo $result;
		return;
	}

}
//##################################################################################
//AJAX CHECK IF USER IS ONLINE
//##################################################################################
function check($user_id=false) {
	global $DB;

	$q = "SELECT user_id FROM heartbeat WHERE user_id = " . $other_user . " and stamp > data_sub(now(), interval 60 second)";
	$o = $DB->get_var($q);

	if ($o) {
		echo 0;
		return;
	}

	echo 1;

}
//##################################################################################
//CONVERSATION LAUNCH
//##################################################################################
function launch($code=false) {
	global $DB, $smarty;

	$q = "SELECT * FROM conversation WHERE code = '" . $code . "' AND (user1 = " . $_SESSION['me']->id . " OR user2 = " . $_SESSION['me']->id . ")";
	$c = $DB->get_result($q);

	$q = "SELECT * FROM user_agent WHERE code = '" . $code . "'";
	$a = $DB->get_result($q);

	if ($c) {

		$c->language = $_SESSION['language'];

		if ($_SESSION['me']->id == $c->user1) {
			$c->isfirst = "true";
			$c->viewer = "user1";
			//RECORD USER AGENT
			if ($a) {
				$q = "UPDATE user_agent SET user1 = '" . $_SERVER['HTTP_USER_AGENT'] . "' WHERE code = '" . $code . "'";
			} else {
				$q = "INSERT INTO user_agent (code,user1) VALUES ('" . $code . "','" . $_SERVER['HTTP_USER_AGENT'] . "')";
			}
			$DB->query($q);
		} else {
			$c->isfirst = "false";
			$c->viewer = "user2";
			//RECORD USER AGENT
			if ($a) {
				$q = "UPDATE user_agent SET user2 = '" . $_SERVER['HTTP_USER_AGENT'] . "' WHERE code = '" . $code . "'";
			} else {
				$q = "INSERT INTO user_agent (code,user2) VALUES ('" . $code . "','" . $_SERVER['HTTP_USER_AGENT'] . "')";
			}
			$DB->query($q);
		}

		$smarty->assign("c",$c);

		$smarty->display("conversation_launch.tpl");

	}

}
//##################################################################################
//END OF CLASS
//##################################################################################
}

//##################################################################################
//CONSTRUCT
//##################################################################################

$START = new START();

?>
