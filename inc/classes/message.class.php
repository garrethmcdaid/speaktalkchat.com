<?php

//##################################################################################
//START OF CLASS
//##################################################################################

class MESSAGE {

//##################################################################################
//SEND A REMINDER ABOUT A CONVERSATION
//##################################################################################
function remind() {
	global $DB,$UTIL,$text;

	$conversations = array();

	//USER1
	$q = "SELECT * FROM conversation WHERE start = date_format(date_add(now(), interval user1_remind minute),'%Y-%m-%d %H:%i') AND status = 'confirmed'";
	//$q = "select * from conversation limit 1";
	$cs = $DB->get_results($q);

	if ($cs) {
		foreach ($cs as $k => $v) {
			$conversations[$v->code]['user1']['id'] = $v->user1;
			$conversations[$v->code]['user1']['ul'] = $UTIL->user_language($v->user1);
			$conversations[$v->code]['user1']['start'] = date('M d H:i',strtotime($UTIL->gmt_convert($v->start,$v->user1)));
			$conversations[$v->code]['user1']['with'] = $UTIL->nickname_convert($v->user2);
		}
	}

	//USER2
	$q = "SELECT * FROM conversation WHERE start = date_format(date_add(now(), interval user2_remind minute),'%Y-%m-%d %H:%i') AND status = 'confirmed'";
	//$q = "select * from conversation limit 1";
	$cs = $DB->get_results($q);

	if ($cs) {
		foreach ($cs as $k => $v) {
			$conversations[$v->code]['user2']['id'] = $v->user2;
			$conversations[$v->code]['user2']['ul'] = $UTIL->user_language($v->user2);
			$conversations[$v->code]['user2']['start'] = date('M d H:i',strtotime($UTIL->gmt_convert($v->start,$v->user2)));
			$conversations[$v->code]['user2']['with'] = $UTIL->nickname_convert($v->user1);
		}
	}

	//REMINDERS
	foreach ($conversations as $k => $v) {

		foreach ($v as $kk => $vv) {

			//SEND NOTIFICATION
			$n = '{"d":"' .  date("Y-m-d H:i:s") . '","u":"admin","m":"conversation_reminder","l":"/user/conversations/' . $k . '"}';
			$q = "UPDATE heartbeat set message = '' WHERE user_id = " . $vv['id'];
			$DB->query($q);
			$q = "UPDATE heartbeat set message = '" . $n . "' WHERE user_id = " . $vv['id'];
			$DB->query($q);
			
			//UNCOMMENT CONDITION TO ONLY SEND EMAIL IF NOT LOGGED IN
			//if (!$UTIL->loggedin($vv['id'])) {
				$message = array();
				$link = $k . ' | ' . $vv['start'] . ' | ' . $vv['with'];
				$message['to'] = $UTIL->get_email($vv['id']);
				$message['subject'] = $text[$vv['ul']]['email']['conversation_remind_subject']->text;
				$message['message'] = $text[$vv['ul']]['email']['conversation_remind_body']->text . "\n\n" . $link;
				$UTIL->sendemail($message);
			//}

		}

	}

}
//##################################################################################
//GET POSSIBLE RECIPIENTS
//##################################################################################
function recipients() {
	global $DB;

	$recipients = array();

	$q = "SELECT user_id,value_vc AS recipient FROM user_data LEFT JOIN user_data_field ON user_data.field_id = user_data_field.id WHERE tag = 'nickname' AND value_vc LIKE '" . $_REQUEST['name_startsWith'] . "%'";
	$r = $DB->get_results($q);

	//CHECK IF SAME ACCOUNT TYPE IF NOT ADMIN
	if ($_SESSION['me']->id > 1) {
		if ($r) {
			foreach ($r as $k => $v) {
				$q = "SELECT user_id FROM user_data WHERE field_id = 1 AND user_id = " . $v->user_id . " AND value_opt = " . $_SESSION['me']->profile->account_type;
				$u = $DB->get_var($q);
				if ($u) $recipients[] = $v;
			}
		}
	} else {
		$recipients = $r;	
	}
	
	echo json_encode($recipients);
	
}
//##################################################################################
//GET A USERS INBOX
//##################################################################################
function inbox($page=1) {
	global $DB, $UTIL, $smarty, $text;

	$pagination = false;
	$subject=false;

	$page = (!$page) ? 1 : $page;

	$page_mark = ($page > 1) ? (($page-1)*20) : 0;

	$q = "SELECT * FROM message WHERE recipient = " . $_SESSION['me']->id . " AND moderated > 0 ORDER BY sent DESC LIMIT " . $page_mark . ", 20";
	$inbox = $DB->get_results($q);

	//PAGINATION
	$q = "SELECT * FROM message WHERE recipient = " . $_SESSION['me']->id;
	$t = count($DB->get_results($q));
	if ($t >= 20) {
		$no_pages = ceil($t/20);
		for ($x = 1;$x<=$no_pages;$x++) {
			$s = ($x == $page) ? 'none' : 'underline';
			$u = ($_SESSION['me']->id > 1) ? 'user' : 'admin';
			$pagination .= '<a class="inbox_page" style="text-decoration:' . $s . ';" href="/' . $u . '/messages/' . $x . '">' . $x . '</a> | ';
		} 
	}

	foreach ($inbox as $k=>$v) {

		$inbox[$k]->sent = $UTIL->gmt_convert($v->sent,$_SESSION['me']->id);
		$inbox[$k]->sender_name = $UTIL->nickname_convert($v->sender);
		$inbox[$k]->today = false;
		$inbox[$k]->message = str_replace("[quote]",'<div class="equote">',$inbox[$k]->message);
		$inbox[$k]->message = str_replace("[/quote]",'</div>',$inbox[$k]->message);
		if (date("Y-m-d",strtotime($v->sent)) == date("Y-m-d")) $inbox[$k]->today = true;
	}
	
	//ALLOWS SUBJECT TO BE PASS IN REQUEST
	if (isset($_REQUEST['subject'])) {
		$subject = $_REQUEST['subject'];
		
		//TRANSLATE KNOW SUBJECTS
		switch ($subject) {
		
			case 'bugreport':
				$subject = $text[$_SESSION['language']]['forms']['subject_bug_report']->text;
			break;
			
			default:
				
		}
	}

	$smarty->assign("pagination",$pagination);
	$smarty->assign("subject",$subject);
	$smarty->assign("inbox",$inbox);
}
//##################################################################################
//GET INDIVIDUAL MESSAGE
//##################################################################################
function message() {

}
//##################################################################################
//MARK A MESSAGE WITH A STATUS
//##################################################################################
function mark() {
	global $DB, $UTIL, $text;
	

	$_REQUEST['json'] = stripslashes($_REQUEST['json']);
	$s = json_decode($_REQUEST['json']);
	
	if ($_SESSION['me']->is_admin) {
		$q = "SELECT * FROM message WHERE id = " . $s->message_id;
		$m = $DB->get_result($q);
		
		if ($m->status == 'new') {		
			$ul = $UTIL->user_language($m->sender);
			$ss = '{"thread":"0","recipient":"' . $m->sender . '","sender":"1","subject":"' . $text[$ul]['messages']['admin_email_read_subject']->text . '","the_message":"' . $text[$ul]['messages']['admin_email_read_body']->text . '<br><br>' . $m->subject . '<br>' . $m->sent . '","admin":"true"}';
			$this->send($ss);		
		}		
	}

	$q = "UPDATE message SET status = '" . $s->status . "' WHERE id = " . $s->message_id . " AND recipient = " . $_SESSION['me']->id;
	$DB->query($q);


}
//##################################################################################
//GET MESSAGE TO VIEW FOR MODERATION
//##################################################################################
function getmod() {
	global $DB, $UTIL;

	$_REQUEST['json'] = stripslashes($_REQUEST['json']);
	$s = json_decode($_REQUEST['json']);

	$response = array();

	$q = "SELECT message FROM message WHERE thread = " . $s->thread . " ORDER BY id LIMIT 1";
	$response['data'] = $DB->get_var($q);
	$response['flag'] = 'show_mod_message';

	echo json_encode($response);

}
//##################################################################################
//ACTION MODERATION CONFIRM OR DECLINE
//##################################################################################
function actionmod() {
	global $DB, $UTIL, $text;

	$_REQUEST['json'] = stripslashes($_REQUEST['json']);
	$s = json_decode($_REQUEST['json']);

	$response = array();
	$response['data'] = array();

	if ($s->action == 'confirm') {
		$q = "UPDATE message SET moderated = 1, sent = now() WHERE thread = " . $s->thread . " AND group_owner = " . $_SESSION['me']->id;
		$DB->query($q);
		$response['data']['update'] = $text[$_SESSION['language']]['messages']['mod_message_confirmed']->text;
	} else {
		$q = "SELECT subject FROM message WHERE thread = " . $s->thread . " ORDER BY id LIMIT 1";
		$subject = $DB->get_var($q);

		$q = "DELETE FROM message WHERE thread = " . $s->thread . " AND group_owner = " . $_SESSION['me']->id;
		$DB->query($q);

		//TELL USER THEIR MESSAGE HAS BEEN DECLINED
		$ul = $UTIL->user_language($s->sender);
		$m = '{"thread":"0","recipient":"' . $s->sender . '","sender":"1","subject":"' . $text[$ul]['messages']['subject_group_message_declined']->text . ': ' . $subject . '","the_message":"' . $text[$ul]['messages']['body_group_message_declined']->text . '"}';
		$this->send($m);

		$response['data']['update'] = $text[$_SESSION['language']]['messages']['mod_message_declined']->text;
	}

	$response['data']['thread'] = $s->thread;
	$response['flag'] = 'mod_message_update';

	echo json_encode($response);

}
//##################################################################################
//SEND A MESSAGE
//##################################################################################
function send($inject=false) {
	global $DB, $UTIL, $text, $GROUP;

	$group_owner = 0;

	$json = ($inject) ? str_replace("\r\n","<br>",$inject) : stripslashes($_REQUEST['json']);
	$s = json_decode($json);
	
	//CATCH ANY MESSAGES THAT DO NOT HAVE ADMIN FLAG SET
	if (!isset($s->admin)) $s->admin = false;
	
	//FOR TAGGING MESSAGE WITH CONVERSATION CODE IN FEEDBACK REPORTS
	$s->code = (isset($s->code)) ? $s->code : '';

	$r = (!$inject) ? $UTIL->nickname_convert($s->recipient) : $s->recipient; 

	$distribution = array();

	if (isset($s->group_id) and $s->group_id > 0) {
		$members = $GROUP->members($s->group_id,true);
		foreach ($members as $k => $v) {
			$distribution[] = $v->user_id;
		}

		$q = "SELECT group_owner FROM groups WHERE id = " . $s->group_id;
		$group_owner = $DB->get_var($q);

		//MODERATION NOT REQUIRED IF GROUP OWNER IS SENDING MESSAGE
		if ($_SESSION['me']->id == $group_owner) {
			$moderated = 1;
		} else {
			$moderated = 0;
		}

		//INFORM GROUP OWNER OF MODERATION REQUIREMENT
		if (!empty($distribution) and $moderated < 1) {

			if ($group_owner > 0) {

				$q = "SELECT max(thread) FROM message";
				$t = $DB->get_var($q) + 1;

				$ul = $UTIL->user_language($group_owner);

				$q = "INSERT INTO message (thread,recipient,sender,subject,status,message,sent,moderated,group_owner) VALUES (". $t . "," . $group_owner . ",1,'" . $DB->escape($text[$ul]['messages']['new_group_message_subject']->text) . "','new','" . $DB->escape($text[$ul]['messages']['new_group_message_body']->text) . "',now(),1,0)";

				$message_id = $DB->query($q);

				//SEND NOTIFICATION
				$n = '{"d":"' .  date("Y-m-d H:i:s") . '","u":"admin","m":"new_private_message","l":"/user/messages/"}';
				$q = "UPDATE heartbeat set message = '' WHERE user_id = " . $r;

				$DB->query($q);

				$q = "UPDATE heartbeat set message = '" . $n . "' WHERE user_id = " . $r;

				$DB->query($q);
							
			}

		}

	} else {
		$distribution[] = $r;
		$moderated = 1;
		$s->group_id = 0;
	}

	//INCREMENT THREAD COUNTER
	if ($s->thread < 1) {
		$q = "SELECT max(thread) FROM message";
		$s->thread = $DB->get_var($q) + 1;
	}
	
	foreach ($distribution as $r) {

		if ($r) {

			//IF MESSAGE FROM ADMINISTRATOR/SYSTEM
			$sender_id = ($s->admin) ? 1 : $_SESSION['me']->id;
			$sender_nickname = (isset($s->admin)) ? 'admin' : $_SESSION['me']->profile->nickname;  

			//CHECK USER IS ALLOWED TO SEND TO RECIPIENT
			if (!$s->admin) {
				if (!$UTIL->valid_connect($r)) return;
			}

			$q = "INSERT INTO message (thread,recipient,sender,subject,status,message,sent,moderated,group_owner,group_id,code) VALUES (". $s->thread . "," . $r . "," . $sender_id . ",'" . $DB->escape($s->subject) . "','new','" . $DB->escape($s->the_message) . "',now()," . $moderated . "," . $group_owner . "," . $s->group_id . ",'" . $s->code . "')";

			$message_id = $DB->query($q);

			//SEND NOTIFICATION
			$n = '{"d":"' .  date("Y-m-d H:i:s") . '","u":"' . $sender_nickname  . '","m":"new_private_message","l":"/user/messages/"}';
			$q = "UPDATE heartbeat set message = '' WHERE user_id = " . $r;

			$DB->query($q);

			$q = "UPDATE heartbeat set message = '" . $n . "' WHERE user_id = " . $r;

			$DB->query($q);

			$response['data'] = $text[$_SESSION['language']]['messages']['message_sent']->text;
			$response['flag'] = 'message_sent';

			//SEND INTERNET EMAIL
			$ul = $UTIL->user_language($r);
			$message = array();
			$message['to'] = $UTIL->get_email($r);
			$message['subject'] = $text[$ul]['email']['subject_new_private_message']->text;
			$message['message'] = $text[$ul]['email']['body_new_private_message']->text;
			$UTIL->sendemail($message);

		} else {

			$response['data'] = $text[$_SESSION['language']]['messages']['invalid_recipient']->text;
			$response['flag'] = 'message_not_sent';

		}

	}

	if ($inject) return;

	$response = json_encode($response);

	echo $response;

}
//##################################################################################
//DELETE MESSAGE
//##################################################################################
function delete() {
	global $DB, $UTIL;

	$_REQUEST['json'] = stripslashes($_REQUEST['json']);
	$s = json_decode($_REQUEST['json']);

	$q = "DELETE FROM message WHERE recipient = " . $_SESSION['me']->id . " AND thread = " . $s->thread . " AND id = " . $s->the_message;
	$DB->query($q);

	$response['data'] = $s->the_message;
	$response['flag'] = 'message_deleted';
	$response = json_encode($response);

	echo $response;

}
//##################################################################################
//END OF CLASS
//##################################################################################
}

//##################################################################################
//CONSTRUCT
//##################################################################################

$MESSAGE = new MESSAGE();

?>
