<?php

//##################################################################################
//START OF CLASS
//##################################################################################

class USER {

//##################################################################################
//PASSWORD RESET
//##################################################################################
function password_reset() {
	global $DB, $UTIL, $text, $smarty, $path, $SECURITY;

	if (!empty($path[3])) {

		$new = false;

		$q = "SELECT id,email FROM user WHERE authkey = '" . $path[3] . "'";
		$user = $DB->get_result($q);

		if ($user) {

			$new_password = $UTIL->password();

			$q = "UPDATE user SET password = '" . crypt($new_password) . "',authkey = '' WHERE id = " . $user->id;
			$DB->query($q);

			$process_message = $text[$_SESSION['language']]['messages']['password_reset_complete_ok']->text;

			$message=array();
			$message['to'] = $user->email;
			$message['subject'] = $text[$_SESSION['language']]['email']['password_reset_step2_subject']->text;
			$message['message'] = $text[$_SESSION['language']]['email']['password_reset_step2_body']->text;

			$message['message'] .= "\n\n" . $new_password;

			$UTIL->sendemail($message);

		} else {
			$process_message = $text[$_SESSION['language']]['messages']['password_reset_invalid_confirm']->text;
		}


	} else {

		$new = true;
		
		$q = "SELECT id FROM user WHERE email = '" . $_REQUEST['me']['email'] . "'";
		$user_id = $DB->get_var($q);

		if (isset($_REQUEST['me']['captcha']) and isset($_REQUEST['me']['hash'])) {
			$q = "SELECT code FROM captcha WHERE hash = '" .  $_REQUEST['me']['hash'] . "'";
			$c = $DB->get_var($q);
			if (strtolower($_REQUEST['me']['captcha']) != $c) {

				$SECURITY->do_captcha();
				$process_message = $text[$_SESSION['language']]['messages']['captcha_error']->text;

			} else {

				if ($user_id) {

					$authkey = $UTIL->authkey();
					$q = "UPDATE user SET authkey = '" . $authkey . "' WHERE id = " . $user_id;
					$DB->query($q);
					$process_message = $text[$_SESSION['language']]['messages']['password_reset_request_ok']->text;

					$message=array();
					$message['to'] = $_REQUEST['me']['email'];
					$message['subject'] = $text[$_SESSION['language']]['email']['password_reset_step1_subject']->text;
					$message['message'] = $text[$_SESSION['language']]['email']['password_reset_step1_body']->text;

					$link = 'http://' . $_SERVER['SERVER_NAME'] . '/user/password/reset/' . $authkey . "?l=" . $_SESSION['language'];

					$message['message'] .= "\n\n" . $link;

					$UTIL->sendemail($message);

					$new = false;

				} else {
					$SECURITY->do_captcha();
					$process_message = $text[$_SESSION['language']]['messages']['password_reset_invalid_email']->text;
				}


			}
		}


	}

	$smarty->assign("process_message",$process_message);
	$smarty->assign("new",$new);

	$smarty->display("user_password.tpl");

}
//##################################################################################
//RATE ANOTHER USER
//##################################################################################
function rate() {
	global $DB, $UTIL, $text;

	$_REQUEST['json'] = stripslashes($_REQUEST['json']);
	$s = json_decode($_REQUEST['json']);

	$q = "SELECT * FROM user_rating WHERE user_id = " . $s->user_id . " AND rating_user = " . $_SESSION['me']->id;
	$result = $DB->get_result($q);

	if ($result) {
		$q = "UPDATE user_rating SET rating = " . $s->rating . ",given = now() WHERE user_id = " . $s->user_id . " AND rating_user = " . $_SESSION['me']->id;
	} else {
		$q = "INSERT INTO user_rating (user_id,rating_user,rating,given) VALUES (" . $s->user_id . "," . $_SESSION['me']->id . "," . $s->rating . ",now())";
	}
	
	$DB->query($q);

	//RECOMPUTE GLOBAL RATING
	$q = "SELECT AVG(rating) FROM user_rating WHERE user_id = " . $s->user_id;
	$avg = $DB->get_var($q);
	$avg = ($avg/0.5);
	$avg = floor($avg);
	$rating = ($avg*0.5);

	$q = "UPDATE user SET rating = " . $rating . " WHERE id = " . $s->user_id;
	$DB->query($q);

	$response['data'] = $text[$_SESSION['language']]['messages']['rating_updated']->text;
	$response['flag'] = 'rating_updated';
	$response = json_encode($response);

	echo $response;

}
//##################################################################################
//LOGIN
//##################################################################################
function login() {
	global $DB, $USER, $smarty, $UTIL, $facebook, $text;

	$me = false;

	if (!$me) {
		//TEST FACEBOOK LOGIN
		$me = $this->facebook_login();	
		if ($me) {
			$UTIL->redirect('/user/dashboard');
			$_SESSION['me']->logout_url = (isset($me['logout_url'])) ? $me['logout_url'] : '/logout';
			return;
		}
	}

	if (isset($_REQUEST['me'])) {
		
		//ENDS IF NOT PASSWORD SUPPLIED
		if ($_REQUEST['me']['password'] == '') exit;

		$q = "SELECT * FROM user WHERE suspend < 1 AND email = '". $_REQUEST['me']['email'] . "' LIMIT 1";

		$user = $DB->get_result($q);

		if (crypt($_REQUEST['me']['password'], $user->password) == $user->password) {
			$q = "SELECT * FROM user_data WHERE user_id = " . $user->id;
			$data = $DB->get_result($q);

			$_SESSION['me'] = $user;
			$_SESSION['me']->account = $this->account_data($user->id);
			$_SESSION['me']->profile = $this->getprofile($user->id);
			$_SESSION['me']->logout_url = '/logout';

			//SEND EMAIL TO PARENT FOR MINORS
			if ($_SESSION['me']->profile->account_type == 2 or $_SESSION['me']->profile->account_type == 3) {
				$message = array();
				$minor = $_SESSION['me']->profile->nickname . ' | ' . $_SESSION['me']->email; 
				$message['to'] = $UTIL->get_parent_email($_SESSION['me']->id);
				$message['subject'] = $text[$_SESSION['language']]['email']['minor_login_subject']->text;
				$message['message'] = $text[$_SESSION['language']]['email']['minor_login_body']->text . "\n\n" . $minor;
				$UTIL->sendemail($message);
			}

			$UTIL->redirect('/user/dashboard');
		} else {

			$UTIL->redirect('/invalidlogin');

		}

	}

}
//##################################################################################
//REGISTER THE USER
//##################################################################################
function register($me=false) {
	global $DB, $smarty, $text, $path, $UTIL, $MESSAGE;

	$_SESSION['me'] = false;
	$process_message = false;

	//SET ACCOUNT TYPE
	if (!empty($path[2])) {
		$q = "SELECT options FROM user_data_field WHERE tag = 'account_type'";
		$accounts = json_decode($DB->get_var($q),true);
		$account = array_search($path[2],$accounts);
	} else {
		$account = false;
	}

	$smarty->assign("account",$account);

	$me = (isset($_POST['me'])) ? $_POST['me'] : false;

	if (!$me) {
		//TEST FACEBOOK LOGIN
		if (!isset($_SESSION['u18'])) {
			$me = $this->facebook_login();
		}
		//TEST SUSPENSION
		$q = "SELECT suspend FROM user WHERE email = '" . $me['email'] . "'";
		$suspend = $DB->get_var($q);
		if ($suspend > 0) $me = false;
	}

	if ($me) {
		
		$me['method'] = (isset($me['method'])) ? $me['method'] : 'form';
		$me['first_name'] = (isset($me['first_name'])) ? $me['first_name'] : '';
		$me['last_name'] = (isset($me['last_name'])) ? $me['last_name'] : '';

		if ($me['method'] == 'form') {
			if (isset($me['captcha']) and isset($me['hash'])) {
				$q = "SELECT code FROM captcha WHERE hash = '" .  $me['hash'] . "'";
				$c = $DB->get_var($q);
				if (strtolower($me['captcha']) != $c) {
					$smarty->assign("process_message",$text[$_SESSION['language']]['messages']['captcha_error']->text);
					$smarty->assign("me",$me);
					return;
				}
			}
		}

		$q = "SELECT * FROM user WHERE email = '" . $me['email'] . "'";

		$result = $DB->get_result($q);

		if (!$result) {

			//CHILD ACCOUNT CHECK FAMILY CODE
			$parent_id = 0;

			if ($me['account'] == 3) {
				if (!$UTIL->codeok($me['family_code'],$me['parent_email'])) {
					$smarty->assign("process_message",$text[$_SESSION['language']]['messages']['familycode_error']->text);
					$smarty->assign("me",$me);
					return;
				} else {
					$parent_id = $UTIL->codeok($me['family_code'],$me['parent_email']);
				}

			}

			$q = "INSERT INTO user (email,parent_id,password,joined,status,method,terms,skype) values ('" . $me['email'] . "'," . $parent_id . ",'" . crypt($me['password']) . "',now(),0,'" . $me['method'] . "'," . $me['terms'] . ",'" . $me['skype'] . "')";
			$id = $DB->query($q);

			//SEND PM TO PARENT ACCOUNT IF MINOR
			if ($me['account'] == 3) {
				$ul = $UTIL->user_language($parent_id);
				$s = '{"thread":"0","recipient":"' . $parent_id . '","sender":"1","subject":"' . $text[$ul]['messages']['new_child_registration_subject']->text . '","the_message":"' . $text[$ul]['messages']['new_child_registration_body']->text . '<br><br>' . $me['email'] . '","admin":"true"}';

				$MESSAGE->send($s);
			}

			$q = "INSERT INTO heartbeat (user_id,stamp) values (" . $id . ",now())";
			$DB->query($q);

			//DEFAULT AVAILABILITY
			//$q = "INSERT INTO availability (user_id, zone, always) VALUES (" . $id . ",'Europe/Dublin',1)";
			//$DB->query($q);

			//DEFAULT DIRECTORY
			$q = "INSERT INTO directory (user_id) VALUES (" . $id . ")";
			$DB->query($q);

			//ACCOUNT TYPE
			$data = array(1 => array("field_tag" => "account_type", "type" => "opt", "m" => "insert", "value" => $me['account']));
			$this->putdata($id,$data);

			//FAMILY ACCOUNT
			if ($me['account'] == 1) {
				$data = array(1 => array("field_tag" => "family_name", "type" => "vc", "m" => "insert", "value" => $me['family_name']));
				$this->putdata($id,$data);
				//CREATE CODE
				$code = $UTIL->familycode();
				$data = array(1 => array("field_tag" => "family_code", "type" => "vc", "m" => "insert", "value" => $code));
				$this->putdata($id,$data);
				
			}

			$message=array();
			$message['to'] = $me['email'];
			$message['subject'] = $text[$_SESSION['language']]['email']['new_user_subject']->text;
			$message['message'] = $text[$_SESSION['language']]['email']['new_user_header']->text;
			//FAMILY CODE IN EMAIL
			if ($me['account'] == 1) {
				$message['message'] .= $text[$_SESSION['language']]['email']['familycode_message']->text;
				$message['message'] .= "\n" . $code . "\n\n";

			}

			$message['message'] .= $text[$_SESSION['language']]['email']['new_user_footer']->text;
			$UTIL->sendemail($message);

			//DEFAULT LANGUAGES ENGLISH AND IRISH
			$q = "INSERT INTO user_to_language (user_id,language_id) values (" . $id . ",1)";
			$DB->query($q);
			//$q = "INSERT INTO user_to_language (user_id,language_id) values (" . $id . ",2)";
			//$DB->query($q);
			
			//FIRST AND LAST NAME
			$data = array(1 => array("field_tag" => "first_name", "type" => "vc", "m" => "insert", "value" => $me['first_name']));
			$this->putdata($id,$data);

			$data = array(1 => array("field_tag" => "last_name", "type" => "vc", "m" => "insert", "value" => $me['last_name']));
			$this->putdata($id,$data);
			
			//TEENAGER/MINOR ACCOUNT
			if ($me['account'] == 2 or $me['account'] == 3) {
				$data = array(1 => array("field_tag" => 'parent_email', "type" => "vc", "m" => "insert", "value" => $me['parent_email']));
				$this->putdata($id,$data);
			}

			$q = "SELECT * FROM user WHERE email = '" . $me['email'] . "'";

			$result = $DB->get_result($q);

		} else {

			if ($me['method'] == 'form') {
				$UTIL->redirect('/register/account/' . $path[2] . '/error');
				exit;
			}			

		}

		$_SESSION['me'] = $result;
		$_SESSION['me']->redirect = "/user/dashboard";
		$_SESSION['me']->account = $this->account_data($result->id);
		$_SESSION['me']->profile = $this->getprofile($result->id);
		$_SESSION['me']->logout_url = (isset($me['logout_url'])) ? $me['logout_url'] : '/logout';

	}

	$smarty->assign("me",$_SESSION['me']);

	return $me;


}
//##################################################################################
//USER LOGOUT
//##################################################################################
function logout() {

	$l = $_SESSION['language'];
	$_SESSION = array();
	$_SESSION['language'] = $l;

	$_POST['task'] = '';
	$_GET['task'] = '';
}
//##################################################################################
//FACEBOOK LOGIN
//##################################################################################
function facebook_login() {
	global $smarty, $facebook, $UTIL, $path;

	$me = false;
	$logout_url = false;

	try {
		$me = $facebook->api('/me');
	} catch (FacebookApiException $e) {
		$UTIL->log($e);

	}

	if ($me) {	
		//PREVENT UNDER 18 LOGIN	
		$bd = date('U',strtotime($me['birthday']));
		$n = date('U');
		
		if (($n - $bd) < 567648000) {
			$me = false;
			$_SESSION['u18'] = true;
			$UTIL->redirect('/register');
			return $me;
			exit;
		}
				
		$params = array("next" => "http://" . $_SERVER['SERVER_NAME'] . "/logout");
		$me['logout_url'] = $facebook->getLogoutUrl($params);
		$me['method'] = 'facebook';
		$me['account'] = $path[2];
		$me['terms'] = 1;
		$me['password'] = '';
		return $me;
	} else {
		return false;
	}

}


//##################################################################################
//PROFILE DATA
//##################################################################################
function account_data($user_id=false) {
	global $DB;

	$data = array();

	$q = "SELECT * FROM user_data WHERE user_id = " . $user_id;

	$user_data = $DB->get_results($q);

	foreach ($user_data as $k => $v) {

		$q = "SELECT * FROM user_data_field WHERE id = " . $v->field_id . " AND sys > 0";

		$f = $DB->get_result($q); 

		if ($f) {

			if ($f->data_type == 'opts' or $f->data_type == 'opt') {
				$o = json_decode($f->options,true);

				switch ($f->data_type) {
					case 'opt':
						$data[$f->tag] = $o[$v->value_opt];
					break;
					case 'opts':
						$data[$f->tag] = $o[$v->value_opts];
					break;

				}
			}

		}

	}
	
	return $data;

}
//##################################################################################
//ACCOUNT
//##################################################################################
function account() {
	global $DB, $USER, $smarty, $UTIL, $language, $text;

	if (isset($_REQUEST['update_user_account_password'])) {

		if ($_REQUEST['me']['password'] == $_REQUEST['me']['confirm_password']) {
			$q = "UPDATE user SET password = '" . crypt($_REQUEST['me']['password']) . "' WHERE id = " . $_SESSION['me']->id;
			$DB->query($q);
			$_SESSION['me']->password = crypt($_REQUEST['me']['password']);
			$smarty->assign("process_message",$text[$_SESSION['language']]['messages']['accountupdated_ok']->text);

		} else {
			$smarty->assign("process_message",$text[$_SESSION['language']]['messages']['passwordmatch_error']->text);
		}

	}

	if (isset($_REQUEST['update_user_account_email'])) {

		if ($_REQUEST['me']['email'] == $_REQUEST['me']['confirm_email']) {

			$q = "SELECT id FROM user WHERE email = '" . $_REQUEST['me']['email'] . "'";
			$user = $DB->get_var($q);

			if ($user) {
				$smarty->assign("process_message",$text[$_SESSION['language']]['messages']['emailexists_error']->text);
			} else {
				$q = "UPDATE user SET email = '" . $_REQUEST['me']['email'] . "' WHERE id = " . $_SESSION['me']->id;
				$DB->query($q);
				$_SESSION['me']->email = $_REQUEST['me']['email'];
				$smarty->assign("process_message",$text[$_SESSION['language']]['messages']['accountupdated_ok']->text);
			}
			
		} else {
			$smarty->assign("process_message",$text[$_SESSION['language']]['messages']['emailmatch_error']->text);
		}

	}

	if (isset($_REQUEST['update_user_account_skype'])) {
		
		if (isset($_REQUEST['me']['remove_skype'])) {

			$q = "UPDATE user SET skype = '' WHERE id = " . $_SESSION['me']->id;
			$result = $DB->query($q);			
			$_SESSION['me']->skype = false;
			
		} else {

			if ($_REQUEST['me']['skype'] == $_REQUEST['me']['confirm_skype']) {
				$q = "UPDATE user SET skype = '" . $_REQUEST['me']['skype'] . "' WHERE id = " . $_SESSION['me']->id;
				$DB->query($q);
				$_SESSION['me']->skype = $_REQUEST['me']['skype'];
				$smarty->assign("process_message",$text[$_SESSION['language']]['messages']['accountupdated_ok']->text);
			} else {
				$smarty->assign("process_message",$text[$_SESSION['language']]['messages']['skypematch_error']->text);
			}
		
		}
	}

	if (isset($_REQUEST['update_user_account_rating_on'])) {

		$q = "UPDATE user SET rating_on = " . $_REQUEST['rating_on'] . " WHERE id = " . $_SESSION['me']->id;
		$DB->query($q);
		$_SESSION['me']->profile->rating['on'] = $_REQUEST['rating_on'];
		$smarty->assign("process_message",$text[$_SESSION['language']]['messages']['accountupdated_ok']->text);

	}

}
//##################################################################################
//HEARTBEAT (RECORDS LOGGED IN USERS)
//##################################################################################
function heartbeat() {
	global $DB;


	if (isset($_SESSION['me']->id)) {

		if (isset($_REQUEST['clear'])) {
			$q = "UPDATE heartbeat SET message = '' WHERE user_id = " . $_SESSION['me']->id;
			$DB->query($q);
		}

		$q = "UPDATE heartbeat SET stamp = now() WHERE user_id = " . $_SESSION['me']->id;
		$DB->query($q);
		$q = "SELECT message FROM heartbeat WHERE user_id = " . $_SESSION['me']->id;
		$message = $DB->get_var($q);
		if ($message) {
			echo $message;
		} else {
			echo 0;
		}
		return;

	}
	echo 0;

}
//##################################################################################
//PROFILE
//##################################################################################
function profile() {
	global $DB, $USER, $smarty, $UTIL, $language, $text, $avatar_processed, $lang_processed, $avail_processed, $profile_processed;

	$fields = false;
	$avatar = false;
	$not_unique = false;

	//SELECTED LANGUAGES
	$selected_languages =array();
	$language_levels = array();
	$q = "SELECT language_id,level FROM user_to_language WHERE user_id = " . $_SESSION['me']->id;
	$results = $DB->get_results($q);
	foreach ($results as $k=>$v) {
		$selected_languages[] = $v->language_id;
		$language_levels[$v->language_id] = $v->level;
	}
	$smarty->assign("selected_languages",$selected_languages);
	$smarty->assign("language_levels",$language_levels);

	//AVATAR UPLOAD
	if (isset($_FILES['avatar']) and $_FILES['avatar']['size'] > 0) {
		$avatar = $UTIL->avatar();
		$smarty->assign('avatar',$avatar);
		$avatar_processed = true;
	}

	//AVATAR CROP
	if (isset($_REQUEST['coords'])) {
		$avatar_cropped = $UTIL->crop_avatar();
		$smarty->assign('avatar_cropped',$avatar_cropped);
		$avatar_processed = true;
	}

	//UPDATE TIMEZONE / AVAILABILITY

	if (isset($_REQUEST['json'])) {
		$_REQUEST['json'] = stripslashes($_REQUEST['json']);
		$s = json_decode($_REQUEST['json']);
		if ($s->request == 'update_tz') {
			$q = "UPDATE availability SET zone ='" . $s->timezone . "',offset = " . $s->offset . " WHERE user_id = " . $_SESSION['me']->id;
			$DB->query($q);
			//UPDATE SESSION
			$_SESSION['me']->profile = $this->getprofile($_SESSION['me']->id);
			$response['data'] = false;
			$response['flag'] = "tz_updated";
			echo json_encode($response);
		}
	}
		
	if (isset($_REQUEST['times'])) {

		$_REQUEST['times']['always'] = (isset($_REQUEST['times']['always'])) ? $_REQUEST['times']['always'] : 0; 	
		$exists = false;
		$q = "SELECT id FROM availability WHERE user_id =" . $_SESSION['me']->id;
		$exists = $DB->get_var($q);
		$start1 = $_REQUEST['times']['period1_start']['Time_Hour'] . ":" . $_REQUEST['times']['period1_start']['Time_Minute'] . ":00";
		$end1 = $_REQUEST['times']['period1_end']['Time_Hour'] . ":" . $_REQUEST['times']['period1_end']['Time_Minute'] . ":00";
		$start2 = $_REQUEST['times']['period2_start']['Time_Hour'] . ":" . $_REQUEST['times']['period2_start']['Time_Minute'] . ":00";
		$end2 = $_REQUEST['times']['period2_end']['Time_Hour'] . ":" . $_REQUEST['times']['period2_end']['Time_Minute'] . ":00";
		
		//CHECK FOR AUTO-DETECTION
		$auto = false;
		if ($_REQUEST['times']['zone'] == 'auto') $auto = true;
		
		//GMT OFFSET
		if (!$auto) {
			$d = new DateTimeZone($_REQUEST['times']['zone']);
			$dt = new DateTime("now", $d);
			$offset = $d->getOffset($dt)/3600;
			$zone = $_REQUEST['times']['zone'];
			$auto = 0;
		} else {
			$offset = $_REQUEST['auto_timezone_offset'];
			$zone = $_REQUEST['auto_timezone'];
			$auto = 1;
		}

		if (!$exists) {
			$q = "INSERT INTO availability (user_id,start1,end1,start2,end2,zone,offset,always,autodetect) VALUES (". $_SESSION['me']->id . ",'" . $start1 . "','" . $end1 . "','" . $start2 . "','" . $end2 . "','" . $zone . "'," . $offset . "," . $_REQUEST['times']['always'] . "," . $auto . ")";
		} else {
			$q = "UPDATE availability SET start1 = '" . $start1 . "',end1 = '" . $end1 . "', start2 = '" . $start2 . "', end2 = '" . $end2 . "', zone = '" . $zone . "',offset = " . $offset . ", always = " . $_REQUEST['times']['always'] . ", autodetect = " . $auto . " WHERE user_id = " . $_SESSION['me']->id;

		}
		$DB->query($q);
		
		if ($smarty->getTemplateVars('process_message') == null) {
			$smarty->assign("process_message",$text[$_SESSION['language']]['messages']['profileupdated_ok']->text);
		}

		//UPDATE SESSION
		$_SESSION['me']->profile = $this->getprofile($_SESSION['me']->id);	
		
		$avail_processed = true;
	}

	//AVATAR EXISTS?
	if ($UTIL->avatar_exists($_SESSION['me']->id)) {
		$smarty->assign('avatar_exists',1);
	}
	

	//LANGUAGES
	if (isset($_REQUEST['languages']) and is_array($_REQUEST['languages'])) {

		$q = "DELETE FROM user_to_language WHERE user_id = " . $_SESSION['me']->id;
		$DB->query($q);

		foreach ($_REQUEST['languages'] as $k => $v) {
			$q = "INSERT INTO user_to_language (user_id,language_id,level) values (" . $_SESSION['me']->id . "," . $v . ",'" . $_REQUEST['levels'][$v] . "')";
			$DB->query($q);
		}

		//SELECTED LANGUAGES
		$selected_languages = array();
		$language_levels = array();
		$q = "SELECT language_id,level FROM user_to_language WHERE user_id = " . $_SESSION['me']->id;
		$results = $DB->get_results($q);
		foreach ($results as $k=>$v) {
			$selected_languages[] = $v->language_id;
			$language_levels[$v->language_id] = $v->level;
		}

		if ($smarty->getTemplateVars('process_message') == null) {
			$smarty->assign("process_message",$text[$_SESSION['language']]['messages']['profileupdated_ok']->text);
		}

		$smarty->assign("selected_languages",$selected_languages);
		$smarty->assign("language_levels",$language_levels);

		//UPDATE SESSION
		$_SESSION['me']->profile = $this->getprofile($_SESSION['me']->id);
		
		$lang_processed = true;


	}

	//DATA
	if (isset($_REQUEST['me'])) {

		//WE INCLUDE A DEFAULT ENTRY FOR TIMEZONE / AVAILABILITY HERE, AS DATA ENTRY HERE IS 
		//REQUIRED FOR ALL NEW USERS AND IS THE ONLY PLACE WHERE WE CAN TAKE A READ OF THE JS GENERATED TIMEZONE AND OFFSET
		$q = "SELECT user_id FROM availability WHERE user_id = " . $_SESSION['me']->id;
		$availability = $DB->get_result($q);
		
		if (!$availability) {
		
			$q = "INSERT into availability (user_id,start1,end1,start2,end2,zone,offset,always,autodetect) values (" . $_SESSION['me']->id . ",'00:00:00','00:00:00','00:00:00','00:00:00','" . $_REQUEST['auto_timezone'] . "'," . $_REQUEST['auto_timezone_offset'] . ",1,1)";
									
			$DB->query($q);
			
		}
		
		
		foreach ($_REQUEST['me'] as $k => $v) {

			$q = "SELECT * FROM user_data_field WHERE id = " . $k;
			$field = $DB->get_result($q);

		}

		//UPDATE DB OCCURS HERE
		//FIRST CLEAR ANY MULTIPLE OPTION VALUES
		$q = "UPDATE user_data SET value_opts = '' WHERE user_id = " . $_SESSION['me']->id;
		$DB->query($q);
		//SECOND CLEAR ANY NON SYS SINGLE OPTION VALUES
		$q = "UPDATE user_data SET value_opt = '' WHERE user_id = " . $_SESSION['me']->id . " AND field_id NOT IN (SELECT id FROM user_data_field WHERE sys > 0)";
		$DB->query($q);


		//NOW REPOPULATE THE TABLE BASED ON FORM SUBMISSION
		foreach ($_REQUEST['me'] as $k => $v) {

			$q = "SELECT id FROM user_data WHERE user_id = " . $_SESSION['me']->id . " AND field_id = " . $k;
			$id = $DB->get_var($q);

			$m = ($id) ? 'update' : 'insert';

			$q = "SELECT * FROM user_data_field WHERE id = " . $k;

			$field = $DB->get_result($q);

			//CHECK IF UNIQUE VALUE REQUIREMENT IS SATISFIED
			if ($field->uniq > 0) {
				$q = "SELECT id FROM user_data WHERE value_" . $field->data_type . " = '" . $v . "' AND user_id != " . $_SESSION['me']->id;
				$result = $DB->get_var($q);
				if ($result) {
					$smarty->assign("process_message","<b>" . $text[$_SESSION['language']]['user_data'][$field->tag]->text . "</b>: " . $text[$_SESSION['language']]['messages']['uniquevalue_error']->text);
					$not_unique = true;
				}

			} else {

				$not_unique = false;

			}

			if (is_array($v) and $field->data_type == "opts") {
				$s = "[";
				foreach ($v as $o) {
					$s .= '"' . $o . '",';
				}
				$s = rtrim($s,",");
				$s .= "]";
				$v = $s;
			}

			if (!$not_unique) {
				$data = array($k => array("field_id" => $k, "type" => $field->data_type, "m" => $m, "value" => $v));
				$this->putdata($_SESSION['me']->id,$data);
			}

		}
		if ($smarty->getTemplateVars('process_message') == null) {
			$smarty->assign("process_message",$text[$_SESSION['language']]['messages']['profileupdated_ok']->text);
		}

		//UPDATE SESSION
		$_SESSION['me']->profile = $this->getprofile($_SESSION['me']->id);
		
		$profile_processed = true;

	}

	$q = "SELECT * FROM user_data_field WHERE entry_method != 'system' AND (account = '" . $_SESSION['me']->account['account_type'] . "' OR account = 'all') ORDER BY priority";
	$fields = $DB->get_results($q);

	if (!empty($fields)) {
		foreach ($fields as $k => $v) {
			if (!empty($v->options)) {
				$a = array();
				$v->options = json_decode($v->options,true);
				asort($v->options);
				foreach ($v->options as $r => $s) {
					$o = $text[$_SESSION['language']]['user_data_op'][strtolower($UTIL->systag($s))]->text;
					$a[$r] = $o;
				}
				$fields[$k]->options_keys = array_keys($v->options);
				$fields[$k]->options = $a;
			}
		}
		$smarty->assign('fields',$fields);
	}

	$q = "SELECT * FROM user_data WHERE user_id = " . $_SESSION['me']->id;
	$data = $DB->get_results($q);

	foreach ($data as $k => $v) {
		if (!empty($v->value_opts)) {
			$data[$k]->value_opts = json_decode($v->value_opts);
		}
	}

	$user = array();

	foreach ($data as $k => $v) {
		$user[$v->field_id] = $v;
	}
	//GET AVAILABILITY
	$q = "SELECT * FROM availability WHERE user_id = " . $_SESSION['me']->id;
	$av = $DB->get_result($q);
	if ($av) {
		if ($av->autodetect > 0) $av->zone = 'auto';
		$user['availability'] = $av;
	} else {
		$user['availability']->start1 = '00:00:00';
		$user['availability']->end1 = '00:00:00';
		$user['availability']->start2 = '00:00:00';
		$user['availability']->end2 = '00:00:00';
		$user['availability']->zone = '';
		$user['availability']->always = 1;
	}

	$smarty->assign('user',$user);

}
//##################################################################################
//CREATE A USER PROFILE OBJECT
//##################################################################################
function getprofile($id=false) {
	global $DB, $UTIL, $text;

	if (!$id) return false;

	$user = array();

	if (!is_numeric($id)) {
		$nickname = $id;
		$q = "SELECT user_id FROM user_data LEFT JOIN user_data_field ON user_data_field.id = user_data.field_id WHERE tag = 'nickname' and value_vc = '" . $id . "'";
		$id = $DB->get_var($q);
	} else {
		$q = "SELECT value_vc FROM user_data LEFT JOIN user_data_field ON user_data.field_id = user_data_field.id WHERE tag = 'nickname' AND user_data.user_id = " . $id;
		$nickname = $DB->get_var($q);
	}
	$user['id'] = $id;
	$user['nickname'] = $nickname;

	//RATING
	$user['rating'] = $UTIL->rating($id);

	//GROUPS
	$q = "SELECT groups.*,user_to_group.status FROM groups LEFT JOIN user_to_group ON groups.id = user_to_group.group_id WHERE user_to_group.user_id = " . $id . " ORDER BY groups.group_name";
	$groups = $DB->get_results($q);	

	foreach ($groups as $k=>$v) {
		$groups[$k]->user_status = $text[$_SESSION['language']]['statuses'][$v->status]->text;
	}
	$user['groups']['data'] = $groups;
	$user['groups']['field_label'] = $text[$_SESSION['language']]['headings']['user_groups']->text;

	//LANGUAGES
	$q = "SELECT language.language_id,language_name,level FROM user_to_language LEFT JOIN language ON user_to_language.language_id = language.language_id WHERE user_id = " . $id;
	$result = $DB->get_results($q);
	foreach ($result as $k => $v) {
		$result[$k]->level_en = $result[$k]->level;
		$result[$k]->level = $text[$_SESSION['language']]['user_data_op'][strtolower($result[$k]->level)]->text;
	}
	$user['languages']['data'] = $result;
	$user['languages']['field_label'] = $text[$_SESSION['language']]['headings']['languages_heading']->text;

	//AVAILABILITY
	$q = "SELECT * FROM availability WHERE user_id = " . $user['id'];
	$av = $DB->get_result($q);
	if ($av) {
		if ($av->offset > 0) $av->offset = "+" . $av->offset; 
		$user['availability']['data'] = $av;
		
	} else {
		$user['availability']['data']->always = 1;
	}
	$user['availability']['data']->always_label = $text[$_SESSION['language']]['messages']['availability_always']->text;
	$user['availability']['data']->times_label = $text[$_SESSION['language']]['messages']['availability_times']->text;
	$user['availability']['data']->zone_label = $text[$_SESSION['language']]['messages']['availability_zone']->text;

	$user['availability']['field_label'] = $text[$_SESSION['language']]['headings']['availability_heading']->text;

	//ACCOUNT TYPE
	$q = "SELECT value_opt FROM user_data LEFT JOIN user_data_field ON user_data.field_id = user_data_field.id WHERE tag = 'account_type' AND user_data.user_id = " . $id;
	$user['account_type'] = $DB->get_var($q);

	//STATISTICS
	$q = "SELECT count(*) FROM conversation WHERE user1 = " . $user['id'] . " OR user2 = " . $user['id'] . " AND duration > 0";
	$user['statistics']['data']['total_conversations'] = $DB->get_var($q);
	$q = "SELECT count(*) FROM conversation WHERE user1 = " . $user['id'] . " OR user2 = " . $user['id'] . " AND duration > 0 AND start > date_sub(now(),interval 28 day)";
	$user['statistics']['data']['total_conversations_28'] = $DB->get_var($q);

	$q = "SELECT sum(duration) FROM conversation WHERE user1 = " . $user['id'] . " OR user2 = " . $user['id'];
	$user['statistics']['data']['total_duration'] = $DB->get_var($q);
	$q = "SELECT sum(duration) FROM conversation WHERE user1 = " . $user['id'] . " OR user2 = " . $user['id'] . " AND start > date_sub(now(),interval 28 day)";
	$user['statistics']['data']['total_duration_28'] = $DB->get_var($q);
	
	//PROFILE FIELDS
	$q = "SELECT 
	user_data.field_id,
	user_data.value_int,
	user_data.value_vc,
	user_data.value_txt,
	user_data.value_opt,
	user_data.value_opts,
	user_data_field.tag,
	user_data_field.data_type,
	user_data_field.options 
  	FROM user_data LEFT JOIN user_data_field ON user_data.field_id = user_data_field.id WHERE sys < 1 AND hidden < 1 AND  user_id = ". $id . " order by priority";
	$result = $DB->get_results($q);

	$fields = array();

	foreach ($result as $k => $v) {

		$result[$k]->field_label = $text[$_SESSION['language']]['user_data'][$v->tag]->text;

		if ($v->data_type == "opt") {
			$opts = json_decode($v->options,true);
			if ($v->value_opt != '') $result[$k]->value_opt = $text[$_SESSION['language']]['user_data_op'][$opts[$v->value_opt]]->text;			
		}
		if ($v->data_type == "opts") {
			$opts = json_decode($v->options,true);
			$sopts = json_decode($v->value_opts);
			$result[$k]->value_opts = array();
			if (is_array($sopts)) {
				foreach ($sopts as $f => $g) {
					//PRESERVED INDEX OR NOT?
					//$result[$k]->value_opts[$g] = $text[$_SESSION['language']]['user_data_op'][$UTIL->systag($opts[$g])]->text;			
					$result[$k]->value_opts[] = $text[$_SESSION['language']]['user_data_op'][$UTIL->systag($opts[$g])]->text;			
				}
			} else {
				//IF USER HAD PREVIOUSLY SELECTED CATEGORY BUT THEN DESELECTED ALL OPTIONS IN THAT CATEGORY
				continue;
			}
			
		}
		$result[$k]->options = false;
		$fields[] = $result[$k];
	}	

	$user['fields'] = $fields;

	$user = (object) $user;

	return $user;	

}
//##################################################################################
//CONVERSATIONS
//##################################################################################
function conversations($user_id=false) {
	global $DB, $UTIL, $text;

	if (isset($_REQUEST['json'])) {
		$_REQUEST['json'] = stripslashes($_REQUEST['json']);
		$s = json_decode($_REQUEST['json']);
		
		if ($s->user_id != $_SESSION['me']->id) {
			if ($_SESSION['me']->id > 1) return;
		}

	} else {
		$s = false;
	}

	if ($s) {
		$user_id = $s->user_id;
	} else if (!$user_id) {
		$user_id = $_SESSION['me']->id;
		
	}
		
	$q = "SELECT conversation.*,user_data.value_vc as user2_nickname FROM conversation LEFT JOIN user_data on conversation.user2 = user_data.user_id LEFT JOIN user_data_field ON user_data_field.id = user_data.field_id WHERE user_data_field.tag= 'nickname' AND  (conversation.user1 = " . $user_id . " OR conversation.user2 = " . $user_id . ")";

	if ($s) {
		if (isset($s->after)) $q .= " AND conversation.start > " . $s->after;
		if (isset($s->before)) $q .= " AND conversation.start < " . $s->before;
	} else {
		//ALLOW 15 MIN GRACE PERIOD
		$q .= " AND conversation.status != 'now' AND conversation.start > date_sub(now(), interval 15 minute)";
	}

	$q .= " ORDER BY conversation.start DESC";

	$conversations = $DB->get_results($q);

	if ($conversations) {

		foreach ($conversations as $k => $v) {

			//WHO WITH
			$a = false;
			if ($v->user1 == $user_id) {
				$conversations[$k]->with_name = $UTIL->nickname_convert($v->user2);
				$conversations[$k]->with_id = $v->user2;
			} elseif ($v->user2 == $user_id) {
				$conversations[$k]->with_name = $UTIL->nickname_convert($v->user1);
				$conversations[$k]->with_id = $v->user1;
			}

			//OBSOLETE OR NOT (72 HOURS FOR DECLINED AND CANCELLED)
			$conversations[$k]->obsolete = false;

			if ($v->status == 'declined' or $v->status == 'cancelled') {
				if ((date("U") - date("U",strtotime($v->updated_at))) > 259200) {
					$conversations[$k]->obsolete = true;
				}
			}		

			//AVATAR EXISTS FOR OTHER USER
			$conversations[$k]->avatar_exists = $UTIL->avatar_exists($conversations[$k]->with_id);

			//ADJUST GMT TIME
			$conversations[$k]->protime1 = $UTIL->gmt_convert($conversations[$k]->protime1, $user_id);
			$conversations[$k]->protime2 = $UTIL->gmt_convert($conversations[$k]->protime2, $user_id);
			if (date('Y-m-d',strtotime($conversations[$k]->start)) == date('Y-m-d')) {
				$conversations[$k]->today = true;
				//IMMEDIATE CONVERSATIONS
				$delay = date('U',strtotime($conversations[$k]->start)) - date('U');
				if ($delay < 300) {
					$conversations[$k]->delay = 5;	
				} else if ($delay < 1800) {
					$conversations[$k]->delay = 30;
				} else {
					$conversations[$k]->delay = 0;
				}

			} else {
				$conversations[$k]->today = false;
				$conversations[$k]->delay = 0;
			}
			
			if ($v->status == 'declined' or $v->status == 'cancelled') {
				
			}
			
			//STATUS
			$conversations[$k]->css_status = $conversations[$k]->status;
			$conversations[$k]->status = $text[$_SESSION['language']]['statuses'][$v->status]->text;				

			$conversations[$k]->start = $UTIL->gmt_convert($conversations[$k]->start, $user_id);						

			//PAST
			if (date("U",strtotime($v->start)) < date("U",strtotime("-15 minutes"))) {
				$conversations[$k]->past = true;
			} else {
				$conversations[$k]->past = false;
			}

			//PAGINATION
			$total = 0;
			$page = 1;

			foreach ($conversations as $k => $v) {
				$total++;
				$conversations[$k]->page = ceil($total/10);	
			}

		}

	}

	if ($s) {
		$response['data'] = $conversations;
		$response['flag'] = "conversation_results";
		echo json_encode($response);
	} else {
		return $conversations;
	}

}
//##################################################################################
//CHECK THAT A USERS PROFILE IS COMPLETE; PREVENTS USE WITHOUT NICKNAME
//##################################################################################
function check_profile() {
	global $DB, $UTIL;

	$q = "SELECT * FROM user_data_field WHERE required > 0 AND id NOT IN (SELECT field_id FROM user_data WHERE user_id = " . $_SESSION['me']->id . ") AND (account = '" . $_SESSION['me']->account['account_type'] . "' OR account = 'all') ORDER BY priority";
		

	$fields = $DB->get_results($q);
	if (!empty($fields)) {
		$UTIL->redirect('/user/profile/required');
		return;
	}


}
//##################################################################################
//DASHBOARD
//##################################################################################
function dashboard() {
	global $DB, $smarty, $UTIL;

	//AVATAR EXISTS?
	$smarty->assign('avatar_exists',$UTIL->avatar_exists($_SESSION['me']->id));

	//BLOG FEED
	//$blogfeed = $UTIL->getxml("http://" . $_SERVER['SERVER_NAME'] . "/blog/?feed=rss2");
	//$blogfeed->items = array_slice($blogfeed->items,0,4);
	//if (is_object($blogfeed)) $smarty->assign("blogfeed",$blogfeed);	

	//CONVERSATIONS
	$conversations = $this->conversations($_SESSION['me']->id);

	if (is_array($conversations)) $smarty->assign("conversations",$conversations);
}
//##################################################################################
//DO SEARCH
//##################################################################################
function dosearch($s=false,$limit=false) {
	global $DB, $UTIL, $text,$is_admin;

	$users = array();

	//DIRECTORY AND ACCOUNT TYPE FILTER
	if (isset($s->directory)) {
		$letters = explode(",",$s->directory);
		$where = "(";
		foreach ($letters as $k => $v) {
			if (end($letters) != $v) {
				$where .= " value_vc LIKE '" . $v . "%' OR";
			} else {
				$where .= " value_vc LIKE '" . $v . "%'";
			}
		}
		$where .= ")";
		
		//MAIN QUERY
		$q = "SELECT user_id FROM user_data LEFT JOIN user ON user.id = user_data.user_id LEFT JOIN user_data_field ON user_data.field_id = user_data_field.id WHERE " . $where . " AND tag = 'nickname' AND user.suspend < 1";
		
		//ADMIN INCLUDES SUSPENDED USERS
		if ($is_admin) {
			$q = "SELECT user_id FROM user_data LEFT JOIN user ON user.id = user_data.user_id LEFT JOIN user_data_field ON user_data.field_id = user_data_field.id WHERE " . $where . " AND tag = 'nickname'";
		}
				
		$pusers = $DB->get_col($q);
		
		//CHECK IF SAME ACCOUNT TYPE
		if (!$is_admin and $_SESSION['me']->id > 1) {
			if ($pusers) {
				foreach ($pusers as $u) {
					
					$q = "SELECT user_id FROM user_data WHERE field_id = 1 AND user_id = " . $u . " AND value_opt = " . $_SESSION['me']->profile->account_type;
					$u = $DB->get_var($q);
					if ($u) $users[] = $u;
				}
			}
		} else {
			$users = $pusers;	
		}


	} else {
		//CHECK IF SAME ACCOUNT TYPE INCLUDED IN MAIN QUERY
		$q = "SELECT DISTINCT user_id FROM user_data LEFT JOIN user ON user.id = user_data.user_id WHERE field_id = 1 AND value_opt = " . $_SESSION['me']->profile->account_type . " AND user.suspend < 1";
		
		//ADMIN INCLUDES SUSPENDED USERS
		if ($is_admin) {
			$q = "SELECT DISTINCT user_id FROM user_data LEFT JOIN user ON user.id = user_data.user_id WHERE field_id = 1 AND value_opt = " . $_SESSION['me']->profile->account_type;
		}
		$users = $DB->get_col($q);
	}

	//LANGUAGE FILTER ANY LANGUAGE CRITERIUM
	//if (isset($s->language)) {
		//$pru = array();
		//foreach ($s->language as $k => $v) {
			//$q = "SELECT user_id FROM user_to_language WHERE language_id = " . $v->id . " AND level = '" . $v->level . "'";

			//$us = $DB->get_col($q);
			//if (is_array($us)) {		
				//foreach ($us as $u) {
					//$pru[] = $u;
				//}	
			//}
		//}
		//if (is_array($users)) {
			//$users = array_intersect($pru,$users);
		//} else {
			//$users = $pru;
		//}
	//}

	//LANGUAGE FILTER ALL LANGUAGE CRITERIA
	if (isset($s->language)) {
		//CREATE MASTER ARRAY TO HOLD SUB ARRAYS OF USER IDS IN EACH LANGUAGE
		$steps = array();
		foreach ($s->language as $k => $v) {
			$step = array();
			$q = "SELECT user_id FROM user_to_language WHERE language_id = " . $v->id . " AND level = '" . $v->level . "'";

			$us = $DB->get_col($q);
			if (is_array($us)) {		
				foreach ($us as $u) {
					$step[] = $u;
				}
				//ADD TO MASTER ARRAY	
				$steps[] = $step;
			} else {
				//ADD TO MASTER ARRAY IF EMPTY
				$steps[] = array();
			}				
		}

		//INTERATE THROUGH INTERSECTIONS, USING 0 INDEX AS SEED	
		$a = $steps[0];

		foreach ($steps as $step) {
			$a = array_intersect($a,$step);
			if (empty($a)) break;
		}

		//FIND INTERSECTION WITH PREVIOUS RESULTS
		if (is_array($users)) {
			$users = array_intersect($a,$users);
		} else {
			$users = $a;
		}
	}

	//FIELD FILTER
	if (isset($s->fields)) {

		$steps = array();
		foreach ($s->fields as $k => $v) {

			$step = array();

			//FIND TYPE
			$q = "SELECT data_type FROM user_data_field WHERE id = " . $k;

			$t = $DB->get_var($q);

			if ($t == "opts") {

				//SELECT BOXES
				foreach ($users as $u) {
					$q = "SELECT value_" . $t . " FROM user_data WHERE field_id = " . $k . " AND user_id = " . $u;

					$o = $DB->get_var($q);

					$opts = json_decode($o,true);
					if (is_array($opts)) {
						foreach ($v as $kk => $vv) {
							if (in_array($kk,$opts)) {
								$step[] = $u;
							}
						}
					}

					if (!empty($step)) $steps[] = $step;

				}

				if (!empty($steps)) {

					$a = $steps[0];

					foreach ($steps as $step) {
						$a = array_intersect($a,$step);
						if (empty($a)) break;
					}

				} else {
					$a = array();
				}


				if (!empty($a)) {
					$users = array_intersect($a,$users);
				} else {
					$users = array();
				}

			} 

			//THIS EFFECTIVELY MEANS SEARCH HAS TO MATCH RADIOS AND CHECKBOXES
			$steps = array();

			$step = array();

			//RADIO BUTTONS
			if ($t == "opt") {

				//WORK THROUGH EACH OF THE RADIO BUTTONS INCLUDED IN THE SEARCH
				foreach ($users as $u) {
					$q = "SELECT value_" . $t . " FROM user_data WHERE field_id = " . $k . " AND user_id = " . $u;
					
					$o = $DB->get_var($q);
				
					//CHECK IF USER HAS THIS OPTION VALUE SELECTED
					foreach ($v as $kk => $vv) {
						if ($kk === $o) {
							$step[] = $u;
						}
					}

				}

				//ADD TO TOTAL PROFILE MATCH
				if (!empty($step)) $steps[] = $step;

				if (!empty($steps)) {

					$a = $steps[0];

					foreach ($steps as $step) {
						$a = array_intersect($a,$step);
						if (empty($a)) break;
					}

				} else {
					$a = array();
				}


				if (!empty($a)) {
					$users = array_intersect($a,$users);
				} else {
					$users = array();
				}

			}

		}

	}

	//COLLATE
	if (!empty($users)) {
		$users = array_unique($users);
		$results = array();
	
		foreach ($users as $u) {

			if ($u == $_SESSION['me']->id) continue;

			$q = "SELECT user_data.user_id,user_data.value_vc AS nickname FROM user_data LEFT JOIN user_data_field ON user_data.field_id = user_data_field.id WHERE user_data.user_id = " . $u . " AND user_data_field.tag = 'nickname'";
			$r = $DB->get_result($q);

			//SKIP IF NO NICKNAME
			if (!$r) continue;
		
			//LANGUAGES
			$q = "SELECT language_name,level FROM user_to_language LEFT JOIN language ON language.language_id = user_to_language.language_id WHERE user_id = " . $u . " ORDER BY language_name";
			$result = $DB->get_results($q);

			foreach ($result as $k => $v) {
				$r->languages[$text[$_SESSION['language']]['languages'][$UTIL->systag($v->language_name)]->text] = $text[$_SESSION['language']]['user_data_op'][$UTIL->systag($v->level)]->text;
			}
			$result = false;

			//TIMEZONE
			$q = "SELECT zone FROM availability WHERE user_id = " . $u;
			$zone = $DB->get_var($q);
			if ($zone) $r->zone = $zone;


			//LOGGED IN
			$r->loggedin = $UTIL->loggedin($u);

			//SKYPE
			$r->skype = $UTIL->skype($u);

			//AVATAR
			if ($UTIL->avatar_exists($u)) {
				$r->avatar = 1;
			} else {
				$r->avatar = 0;
			}

			$results[] = $r;
		}

		if ($limit) $results = array_slice($results,0,$limit);

		//ALPHA SORT
		function cmp($a, $b)
		{
		    return strcmp(strtolower($a->nickname), strtolower($b->nickname));
		}
		usort($results,"cmp");

		//PAGINATION
		$total = 0;
		$page = 1;
		$presults = array();

		foreach ($results as $r) {
			$total++;
			$r->page = ceil($total/10);	
			$presults[] = $r;		
		}
		
		return $presults;
	} else {
		return false;
	}
}
//##################################################################################
//SEARCH WRAPPER FUNCTION
//##################################################################################
function search($query=false) {
	global $smarty, $DB, $UTIL, $text;

	if (isset($_SESSION['me']->last_search) and strlen($_SESSION['me']->last_search) > 0) {
		$last_search = $_SESSION['me']->last_search;		
	} else {
		$last_search = false;	
	}
	$smarty->assign('last_search',$last_search);
	
	//DO A SEARCH
	if (isset($_REQUEST['json'])) {
		$_REQUEST['json'] = stripslashes($_REQUEST['json']);
		$s = json_decode($_REQUEST['json']);

		switch ($s->request) {

			case 'user_search':
				$users = $this->dosearch($s);
				$response = array();
				$response['data'] = $users;
				$response['flag'] = 'user_search_results';
				$response = json_encode($response);
				//FOR BACK TO RESULTS
				$_SESSION['me']->last_search = $_REQUEST['json'];
			break;
			case 'user_detail':
				$response['data'] = $this->getprofile($s->user->nickname);
				$response['flag'] = 'user_search_detail';
				$response = json_encode($response);
				$_SESSION['me']->last_search = $_REQUEST['json'];

			break;
			case 'user_suggest':
				
				//FOR BACK TO RESULTS
				$_SESSION['me']->last_search = '{"request":"user_suggest"}';

				$o = array();
				$s = (object) $o;

				foreach ($_SESSION['me']->profile->languages['data'] as $k => $v) {
					$lid = $v->language_id;
					$s->language->$lid->id = $v->language_id;
					$s->language->$lid->level = $v->level_en;
				}
				//REMOVED UNTIL LARGER USER POPULATION EXISTS
				//foreach ($_SESSION['me']->profile->fields as $k => $v) {
					//if ($v->data_type == 'opts') {
						//foreach ($v->value_opts as $kk => $vv) {
							//$fid = $v->field_id;
							//$s->fields->$fid->$kk = 1;
						//}
					//}
				//}

				$users = $this->dosearch($s,3);
				$response = array();
				$response['data'] = $users;
				$response['flag'] = 'user_suggest_results';
				$response = json_encode($response);
			break;

		}
		echo $response;
		return;
	}

	//PROFILE FIELD
	$q = "SELECT * FROM user_data_field WHERE entry_method != 'system' AND (account = '" . $_SESSION['me']->account['account_type'] . "' OR account = 'all') and search > 0 ORDER BY priority";
	$fields = $DB->get_results($q);

	if (!empty($fields)) {
		foreach ($fields as $k => $v) {
			if (!empty($v->options)) {
				$a = array();
				$v->options = json_decode($v->options,true);
				asort($v->options);
				foreach ($v->options as $r => $s) {
					$o = $text[$_SESSION['language']]['user_data_op'][strtolower($UTIL->systag($s))]->text;
					$a[$r] = $o;
				}
				$fields[$k]->options_keys = array_keys($v->options);
				$fields[$k]->options = $a;
			}
		}
		$smarty->assign('fields',$fields);
	}


}
//##################################################################################
//RECORD USER DATA
//##################################################################################
function putdata($id,$data) {
	global $DB, $UTIL;

	//print_r($data);

	foreach ($data as $k => $v) {

		if ($v['type'] != 'int') $v['value'] = $UTIL->wrap($v['value']);

		if (!isset($v['field_id'])) {
			$q = "SELECT id FROM user_data_field WHERE tag = '" . $v['field_tag'] . "'";
			$v['field_id'] = $DB->get_var($q);
		}

		if ($v['m'] == "insert") {
			$q = "INSERT INTO user_data (user_id,field_id,value_" . $v['type'] . ") VALUES (" . $id . "," . $v['field_id'] . "," . $v['value'] . ")";
			$DB->query($q);
		} elseif ($v['m'] == "update") {
			$q = "UPDATE user_data SET value_" . $v['type'] . " = " . $v['value'] . " WHERE user_id = " . $id . " AND field_id = " . $v['field_id'];
			$DB->query($q);
		}

	}

}
//##################################################################################
//UPDATE A DIRECTORY
//##################################################################################
function update_directory($id,$action=false) {
	global $DB, $UTIL;

	if (!isset($_SESSION['me']->id)) die();

	$q = "SELECT members FROM directory WHERE user_id = " . $_SESSION['me']->id;
	$d = $DB->get_var($q);

	$d = json_decode($d);

	if (!is_array($d)) $d = array();	

	if ($action == 'add') {
		array_push($d,$id);
	} elseif ($action == 'del') {
		$n = array();
		foreach ($d as $k => $v) {
			if ($v != $id) $n[] = $v;
		}
		$d = $n;
	}

	$d = json_encode($d);
	$q = "UPDATE directory SET members = '" . $d . "' WHERE user_id = " . $_SESSION['me']->id;
	$DB->query($q);

}
//##################################################################################
//GET A USERS DIRECTORY
//##################################################################################
function get_directory() {
	global $DB, $UTIL;

	if (!isset($_SESSION['me']->id)) die();

	$response = array();
	$directory = array();

	$q = "SELECT members FROM directory WHERE user_id = " . $_SESSION['me']->id;
	$d = $DB->get_var($q);

	$d = json_decode($d,true);

	foreach ($d as $k => $v) {
	
		$n = $UTIL->nickname_convert($v);
		$l = strtoupper(substr($n,0,1));
		$directory[$l][$n]['nickname'] = $n;
		$directory[$l][$n]['avatar'] = $UTIL->avatar_exists($v);
		$directory[$l][$n]['loggedin'] = $UTIL->loggedin($v);
		$directory[$l][$n]['id'] = $v;
		
	}
	
	foreach (array_keys($directory) as $l) {
		ksort($directory[$l]);	
	}
	
	ksort($directory);
		
	$response['data'] = $directory;
	$response['flag'] = 'directory';

	echo json_encode($response);

}
//##################################################################################
//END OF CLASS
//##################################################################################
}

//##################################################################################
//CONSTRUCT
//##################################################################################

$USER = new USER();

?>
