<?php

//##################################################################################
//START OF CLASS
//##################################################################################

class CONTENT {

//##################################################################################
//PAGE OF STATIC CONTENT
//##################################################################################
function page($static_tag=false) {
	global $DB, $smarty, $UTIL;

	$q = "SELECT * FROM static WHERE static_tag = '" . $static_tag . "' and language_suffix = '" . $_SESSION['language'] . "'";
	$content = $DB->get_result($q);
	$smarty->assign("content",$content);
	$smarty->display("content_page.tpl");


}
//##################################################################################
//CHUNK OF STATIC CONTENT
//##################################################################################
function chunk($static_tag=false) {
	global $DB, $smarty, $UTIL;

	$q = "SELECT * FROM static WHERE static_tag = '" . $static_tag . "' and language_suffix = '" . $_SESSION['language'] . "'";
	$content = $DB->get_result($q);
	$smarty->assign("content",$content);
	$smarty->display("content_chunk.tpl");


}
//##################################################################################
//MENU OF STATIC PAGES
//##################################################################################
function static_menu() {
	global $DB, $smarty, $UTIL;

	$q = "SELECT * FROM static WHERE static_status < 1 AND language_suffix = '" . $_SESSION['language'] . "'";
	$menu = $DB->get_results($q);
	$smarty->assign("menu",$menu);

	$q = "SELECT * FROM static WHERE static_status < 1 AND static_menu = 'bot' AND static_menu_on > 0 AND language_suffix = '" . $_SESSION['language'] . "' ORDER BY static_order";
	$menu_bot = $DB->get_results($q);
	$smarty->assign("menu_bot",$menu_bot);


}
//##################################################################################
//GATHER FAQ DATA
//##################################################################################
function faq() {
	global $DB, $smarty, $UTIL;

	$faq = array();
	$answers = array();

	$data = array();

	$q = "SELECT * FROM faq";
	$result = $DB->get_results($q);

	foreach ($result as $k => $v) {
		$data[$v->faq] = $v->cat;
	}

	$categories = array();

	$q = "SELECT text_tag,text FROM text WHERE text_group = 'faq_categories' AND language_suffix = '" . $_SESSION['language'] . "' ORDER BY text_tag";
	$result = $DB->get_results($q);

	foreach ($result as $k => $v) {
		$categories[$UTIL->faq_no($v->text_tag)] = $v->text;
	}

	$q = "SELECT * FROM text WHERE text_group = 'faq_questions' AND language_suffix = '" . $_SESSION['language'] . "' ORDER BY text_tag";
	$questions = $DB->get_results($q);

	$q = "SELECT * FROM text WHERE text_group = 'faq_answers' AND language_suffix = '" . $_SESSION['language'] . "'ORDER BY text_tag";
	$result = $DB->get_results($q);

	foreach ($result as $k => $v) {

		$n = $UTIL->faq_no($v->text_tag);
		$answers[$n] = $v->text;

	}

	foreach ($questions as $k => $v) {

		$n = $UTIL->faq_no($v->text_tag);
		$faq[$n]['tag'] = $v->text_tag;
		$faq[$n]['q'] = $v->text;
		$faq[$n]['a'] = $answers[$n];

	}

	$smarty->assign("faq",$faq);
	$smarty->assign("data",$data);
	$smarty->assign("categories",$categories);
	$smarty->display("content_faq.tpl");

}
//##################################################################################
//END OF CLASS
//##################################################################################
}

//##################################################################################
//CONSTRUCT
//##################################################################################

$CONTENT = new CONTENT();

?>
