//###################################################################
//GLOBAL VARS
//###################################################################
var json = false;
var path = false;
var mydata = { };
var m_date = false;
var n_date = false;
var current_page = 1;


//###################################################################
//GET BASE URL
//###################################################################
function getBaseURL() {
    var url = location.href;  // entire url including querystring - also: window.location.href;
    var baseURL = url.substring(0, url.indexOf('/', 14));


    if (baseURL.indexOf('http://localhost') != -1) {
        // Base Url for localhost
        var url = location.href;  // window.location.href;
        var pathname = location.pathname;  // window.location.pathname;
        var index1 = url.indexOf(pathname);
        var index2 = url.indexOf("/", index1 + 1);
        var baseLocalUrl = url.substr(0, index2);

        return baseLocalUrl + "/";
    }
    else {
        // Root Url for domain name
        return baseURL + "/";
    }

}
//###################################################################
//MOBILE FACEBOOK LOGIN
//###################################################################
function js_fbLogin() {

	$("#facebook_mobile_login").hide();
	$("#user_login_form").hide();
	$(".ajax-loader").show();
	FB.login(function(response) { }, {scope:'email'}); 
	setTimeout("js_statusCheck()",3000);

}

function js_statusCheck() {

	if ($("#logged_in").val() < 1) {
		$("#facebook_mobile_login").fadeIn();
		setTimeout("js_statusCheck()",3000);
	} else {
		$("#facebook_mobile_login").hide();
		top.location.href = getBaseURL() + "/register/account";
	}

}

function js_statusChange(response) {

	if (response.authResponse) {

		FB.api('/me', function(response) {

			if (response.name) {
				$("#email").html(response.email);
				$("#logged_in").val(1);
			} else {
				$("#facebook_mobile_login").fadeIn();
			}

		});

	}

}
//###################################################################
//DISPLAY ASSET FOR ADMIN
//###################################################################
function js_displayAsset(asset) {

	$("#asset_display").empty();
	var h = '<img src="/images/ads/' + asset + '">';
	$("#asset_link").val(h);
	$("#asset_display").append('<br><img src="/images/ads/' + asset + '">');

}
//###################################################################
//OPEN SEPARATE CHAT WINDOW
//###################################################################
function js_openChat(url,event) {

	window.open(url, "Chat", "width=516,height=464,scrollbars=yes");
	event.preventDefault();

}
//###################################################################
//ALTER MAIN WINDOW AFTER OPENING SKYPE
//###################################################################
function js_openSkype() {

	$("#nav_menu").slideUp();
	$("#advice").hide();
	$("#retry_skype").show();
	$("#review_prompt").show();
	$("#started").val(1);
				
}
//###################################################################
//TELL SERVER THE SKYPE CALL HAS STARTED
//###################################################################
function js_startSkype() {

	if ($("#started").val() > 0) {	
	
		mydata.request = 'skype_start';
		mydata.code = $("#code").val();
		path = 'start/skype';
	
		js_ajax();
		
	} else {
		
		setTimeout("js_startSkype()",2000);
		
	}
	
}
//###################################################################
//CHECK IF CHAT IS READY
//###################################################################
function js_checkChat() {

	if ($("#viewer").val() == 'user2') {

		if (!path) path = "start/key/" + $("#code").val() + "/";

		$(".ajax-loader").show();
		$("#wait_message").fadeIn();

		$.ajax({
			type:           'post',
			cache:          false,
			dataType:	'text',
			url:            getBaseURL() + path
		})
		.done(function(response) {
			if (response.length > 0) {
				$(".ajax-loader").hide();
				$("#wait_message").hide();
				js_launchChat();
			} else {
				if ($("#timeout").val() > 15) {
					$(".ajax-loader").hide();
					$("#wait_message").hide();
					$("#cancel_message").fadeIn();
					return;
				}
				setTimeout('js_checkChat()',3000);
				$("#timeout").val(($("#timeout").val()*1)+1);
				return;
			}
		})
		.always(function() {

		})
		.fail(function() {

		});

	} else {
		js_launchChat();
	}

}
//###################################################################
//LAUNCH CHAT APP WHEN READY	
//###################################################################
function js_launchChat() {

	$("#flash_extra").fadeIn();

	<!-- For version detection, set to min. required Flash Player version, or 0 (or 0.0.0), for no version detection. --> 
	var swfVersionStr = "10.2.0";
	<!-- To use express install, set to playerProductInstall.swf, otherwise the empty string. -->
	var xiSwfUrlStr = "/inc/flash/playerproductinstall.swf";
	var flashvars = {};
	flashvars.isFirst=$("#isfirst").val();
	flashvars.userInfoURL="/start/data/" + $("#code").val() + "/" + $("#viewer").val() + "/" + $("#language").val()
	flashvars.dbURL="/start/key/" + $("#code").val() + "/";
	flashvars.resetURL="/start/key/" + $("#code").val() + "/reset";
	var params = {};
	params.quality = "high";
	params.bgcolor = "#ffffff";
	params.allowscriptaccess = "sameDomain";
	params.allowfullscreen = "false";
	var attributes = {};
	attributes.id = "ChatApp";
	attributes.name = "ChatApp";
	attributes.align = "middle";
	swfobject.embedSWF(
	"/inc/flash/videochat_v12.swf", "flashcontent", 
	"508", "415", 
	swfVersionStr, xiSwfUrlStr, 
	flashvars, params, attributes);
		<!-- JavaScript enabled so display the flashContent div in case it is not replaced with a swf object. -->
		swfobject.createCSS("#flashcontent", "display:inline-block;text-align:left;");


}
//###################################################################
//REVEAL AN ELEMENT
//###################################################################
function js_reveal(id) {

	var d_id = $("#" + id).attr("class").split(' ')[1];
	var s = $("#" + id).attr("class").split(' ')[2];

	if ($("#" + d_id).css("display") != 'none') {
		if (s == 'fadeIn' || !s) $("#" + d_id).fadeOut();
		if (s == 'slideDown') $("#" + d_id).slideUp('slow');
	} else {
		if (s == 'fadeIn' || !s) $("#" + d_id).fadeIn();
		if (s == 'slideDown') $("#" + d_id).slideDown('slow');
	}

}
//###################################################################
//HEARTBEAT FUNCTION TO INDICATE USER IS LOGGED IN
//###################################################################
function heartbeat(id) {

	var data = false;

	if (id == "close" || id == "move" || $("#heartbeat_state").val() < 1) {
		data = {
			'clear': true
		};
		$("#heartbeat_state").val("1");

	}

	$.ajax({
		type:           'post',
		cache:          false,
		url:            getBaseURL() + 'user/heartbeat',
		dataType:	'json',
		data: 		data
	})
	.always(function(response) {
		if (response.u) {
			if (!m_date) {
				m_date = new Date(response.d);
				$("#notifier_inner").append(response.u + ": <a href='" + response.l + "'>" + $("#" + response.m).val() + "</a>");
			} else {
				n_date = new Date(response.d);
				if (n_date > m_date) {
					$("#notifier_inner").append("<br>" + response.u + ": <a href='" + response.l + "'>" + $("#" + response.m).val() + "</a>");
					m_date = n_date;
				}
			}
			$("#notifier").slideDown("slow");

			var soundHandle = document.getElementById('ping');
			soundHandle.src = '/inc/media/ping.wav';
			soundHandle.play();

		} else {
			$("#notifier").slideUp("slow",function() {
				$("#notifier_inner").empty();
			});
		}
	});

	setTimeout('heartbeat()',10000);

}
//###################################################################
//AVATAR PREVIEW
//###################################################################
function avatarPreview(c) {
	var rx = 70 / c.w;
	var ry = 70 / c.h;

	$('#x1').val(c.x);
	$('#y1').val(c.y);
	$('#x2').val(c.x2);
	$('#y2').val(c.y2);
	$('#w').val(c.w);
	$('#h').val(c.h);

	$('#avatar_preview').css({
		width: Math.round(rx * $("#orig_w").val()) + 'px',
		height: Math.round(ry * $("#orig_h").val()) + 'px',
		marginLeft: '-' + Math.round(rx * c.x) + 'px',
		marginTop: '-' + Math.round(ry * c.y) + 'px'
	});
}
//###################################################################
//UPDATE LOCAL TIME INFORMATION
//###################################################################
function js_updateTime(dateText) {

	//DIFFERENCE
	var d1 = $("#p1_hour").val() - ($("#dif").val()*1);

	//PREVIOUS DAY
	if (d1 < 0) {
		d1 = 24 + d1;
		$("#p1_previousday").show();
	} else {
		$("#p1_previousday").hide();
	}

	//NEXT DAY
	if (d1 > 23) {
		d1 = d1 - 24;
		$("#p1_nextday").show();
	} else {
		$("#p1_nextday").hide();
	}

	$("#p1_hour_local").html(d1);

	//ADD LEADING ZERO
	if ($("#p1_hour_local").html().length < 2) {
		$("#p1_hour_local").html("0" + $("#p1_hour_local").html());
	}

	//MINUTES
	$("#p1_min_local").html($("#p1_min").val());

	//REPEAT FOR SECOND PROPOSED TIME

	//DIFFERENCE
	var d2 = $("#p2_hour").val() - ($("#dif").val()*1);

	//PREVIOUS DAY
	if (d2 < 0) {
		d2 = 24 + d2;
		$("#p2_previousday").show();
	} else {
		$("#p2_previousday").hide();
	}

	//NEXT DAY
	if (d2 > 23) {
		d2 = d2 - 24;
		$("#p2_nextday").show();
	} else {
		$("#p2_nextday").hide();
	}

	$("#p2_hour_local").html(d2);

	//ADD LEADING ZERO
	if ($("#p2_hour_local").html().length < 2) {
		$("#p2_hour_local").html("0" + $("#p2_hour_local").html());
	}

	//MINUTES
	$("#p2_min_local").html($("#p2_min").val());

	//INVALID TIME PROPOSAL
	//IN THIS ROUTINE WE ADD 15 MINS TO THE PROPOSED TIME FOR COMPARISON WITH THE END TIME, SO THAT THE USER DOESN'T PROPOSE A TIME RIGHT AT THE END OF THE WINDOW
	var y = false;
	var h = false;
	var m = false;

	var s1 = $("#start1").html() + ":00";
	var e1 = $("#end1").html() + ":00";
	var p1 = $("#p1_hour").val() + ":" + $("#p1_min").val() + ":00";

	h = $("#p1_hour").val()*1;
	m = ($("#p1_min").val()*1)+15;
	if (m >= 60) {
		h++;
		m = m - 60;
		m = m.toString();
		if (m.length < 2) m = "0" + m;
	}
	h = h.toString();
	if (h.length < 2) h = "0" + h;
	var p1s = h + ":" + m + ":00";

	var s2 = $("#start2").html() + ":00";
	var e2 = $("#end2").html() + ":00";
	var p2 = $("#p2_hour").val() + ":" + $("#p2_min").val() + ":00";

	h = $("#p2_hour").val()*1;
	m = ($("#p2_min").val()*1)+15;
	if (m >= 60) {
		h++;
		h.toString();
		m = m - 60;
		m = m.toString();
		if (m.length < 2) m = "0" + m;
	}
	h = h.toString();
	if (h.length < 2) h = "0" + h;
	var p2s = h + ":" + m + ":00";

	//DATE CAN ONLY BE PICKED FROM TODAY ON, BUT CHECK THAT SELECTED TIME IS NOT IN THE PAST
	//dateText IS PASSED FROM JQ DATEPICKER ONSELECT EVENT
	if (dateText) {

		//PROPOSED DATE IN LOCAL TIME ZONE
		var pd1 = $("#proposed_date1").val().split("-");
		var pd1 = new Date(pd1[0],((pd1[1]*1)-1),pd1[2],$("#p1_hour_local").html(),$("#p1_min_local").html());
		var pd2 = $("#proposed_date2").val().split("-");
		var pd2 = new Date(pd2[0],((pd2[1]*1)-1),pd2[2],$("#p2_hour_local").html(),$("#p2_min_local").html());

		//NOW PLUS X HOURS (IE TO GIVE USER A CHANCE TO RESPOND)
		var n = new Date();
		var t = new Date(n.getTime() + (24 * 60 * 60 * 1000));
		
		//IF THE USER IS NOT AN ADULT ADD 4 HOURS, ELSE JUST 1
		if ($("#user_account_type").val() == 3 || $("#me_account_type").val() == 3) {
			n.setHours(n.getHours()+4);
		} else {
			n.setHours(n.getHours()+1);
		}
		
		d1 = $("#p1_hour").val() - ($("#dif").val()*1);
		
		//TO CHECK IF PROPOSED DATE IS TODAY, OR TOMORROW, IN CONDITION 2
		var nnn = n.getFullYear() + "-" + n.getMonth() + "-" + n.getDate();
		var ttt = t.getFullYear() + "-" + t.getMonth() + "-" + t.getDate();

		var ppp1 = pd1.getFullYear() + "-" + pd1.getMonth() + "-" + pd1.getDate();
		var ppp2 = pd2.getFullYear() + "-" + pd2.getMonth() + "-" + pd2.getDate();
		
		var redeye = false;
		var eee = 24 - (n.getHours()*1);
		if ((eee + d1) > 0) {
			redeye = true;	
		}		
				
		//CONDITION 1
		//TODAY
		//IF PROPOSAL HOUR IS EARLIER THAN CURRENT HOUR BUT NOT NEXT DAY
		if (pd1 < n && d1 <= 23) {
			$("#p1_past_time").show();
			$("#conversation_request").hide();
			$("#p1_invalid_time").hide();
			y = true;
		//CONDITION 2	
		//TODAY	
		//IF PROPOSAL HOUR IS LATER THAN CURRENT HOUR BUT PREVIOUS DAY
		//IF PROPOSAL DATE IS 2MORO, BUT A PAST TIME FOR THE PROPOSING USER
		} else if ((pd1 > n && d1 < 0 && nnn == ppp1) || (pd1 > n && !redeye && ttt == ppp1)) {
			$("#p1_past_time").show();
			$("#conversation_request").hide();
			$("#p1_invalid_time").hide();
			y = true;
		} else {
			$("#p1_past_time").hide();
			$("#conversation_request").show();
		}

		d2 = $("#p2_hour").val() - ($("#dif").val()*1);
		
		redeye = false;
		eee = 24 - (n.getHours()*1);
		if ((eee + d2) > 0) {
			redeye = true;	
		}
		
		if (pd2 < n && d2 <= 23) {
			$("#p2_past_time").show();
			$("#conversation_request").hide();
			$("#p2_invalid_time").hide();
			y = true;
		} else if ((pd2 > n && d2 < 0 && nnn == ppp2) || (pd2 > n && !redeye && ttt == ppp2)) {
			$("#p2_past_time").show();
			$("#conversation_request").hide();
			$("#p2_invalid_time").hide();
			y = true;
		} else {
			$("#p2_past_time").hide();
			$("#conversation_request").show();
		}



	}	

	//USER ALWAYS AVAILABLE SO EXIT HERE
	if ($("#start1").length < 1) return;
	
	if ((p1s > e1 || p1 < s1) && (p1s > e2 || p1 < s2)) {
			if (!y) {
				$("#p1_invalid_time").show();
				$("#availability_section").css("background-color","yellow");
				$("#conversation_request").hide();
				y = true;
			}
	} else {
		$("#p1_invalid_time").hide();
		$("#availability_section").css("background-color","#ffffff");
		$("#conversation_request").show();
	}

	if ((p2s > e1 || p2 < s1) && (p2s > e2 || p2 < s2)) {
			if (!y) {
				$("#p2_invalid_time").show();
				$("#availability_section").css("background-color","yellow");
				$("#conversation_request").hide();
			}
	} else {
		$("#p2_invalid_time").hide();
		if (y == false) {
			$("#availability_section").css("background-color","#ffffff");
			$("#conversation_request").show();
		}
	}



}
//###################################################################
//SELECT A PROPOSED TIME OPTION
//###################################################################
function js_proposedOptionSelect(id) {

	if (id) {
		if (id == "option1") {
			$("#protime2").fadeOut();
		} else {
			$("#selected_option").val("2");
			$("#protime1").fadeOut();
		}
		$("#" + id).fadeOut();
		$("conversation_decline").blur();
		setTimeout('$("#conversation_confirm").fadeIn()',700);
	}

}
//###################################################################
//UPDATE TIMEZONE OFFSET FOR DAYLIGHT SAVINGS
//###################################################################
function js_updateTzOffset(u) {

	mydata.request = 'update_tz';
	mydata.offset = $("#update_timezone_offset").val();
	mydata.timezone = $("#update_timezone").val();
	path = 'user/profile/update_offset';

	js_ajax();

}
//###################################################################
//INITIATE INDIVIDUAL USER SEARCH
//###################################################################
function js_userResultDetail(u) {

	mydata.request = 'user_detail';
	mydata.user = { };
	mydata.user.nickname = u.id;
	mydata.current_page = $("#current_page").val();
	path = 'user/search/detail';

	js_ajax();

}
//###################################################################
//DISPLAY RESULT OF INDIVIDUAL USER SEARCH
//###################################################################
function js_userResultDisplay(u) {

	$("#" + u.nickname + "_detail").empty();	

	//ADMIN SEARCH
	if ($("#admin_search").length) {
		$("#" + u.nickname + "_detail").append('<a href="/admin/user/profile/' + u.nickname + '">Edit profile</a> | <a href="/admin/user/account/' + u.nickname + '">Edit account</a> | <a href="/admin/messages/sendto/' + u.nickname + '"><img align="absmiddle" src="/images/envelope.jpg"></a> | <span class="close clickable">' + $("#close_link").val() + '</span><br><br>');
	} else {
		$("#" + u.nickname + "_detail").append('<a class="highlight" href="/member/' + u.nickname + '/result">' + $("#connect").val() + '</a> | <span class="close clickable highlight">' + $("#close_link").val() + '</span><br><br>');
	}

	//LANGUAGES NOW APPEAR IN INITIAL SEARCH RESULT
	//$("#" + u.nickname + "_detail").append('<b>' + u.languages.field_label + '</b><br>');
	//$.each(u.languages.data, function (index,value) {
		//$("#" + u.nickname + "_detail").append(value.language_name + " (" + value.level + ")<br>");
	//});
	$.each(u.fields, function (index,value) {
		$("#" + u.nickname + "_detail").append('<b>' + value.field_label + '</b><br>');
		if (value.value_opt != 0) $("#" + u.nickname + "_detail").append(value.value_opt + '<br>');
		if (value.value_opts) {
			$.each(value.value_opts, function (i,v) {
				$("#" + u.nickname + "_detail").append(v + '<br>');
			});
		}
	});

	//AVAILABILITY
	$("#" + u.nickname + "_detail").append('<b>' + u.availability.field_label + '</b><br>');
	if (u.availability.data.always > 0) {
		$("#" + u.nickname + "_detail").append(u.availability.data.always_label);
	} else if (u.availability.data.start1 == '00:00:00' && u.availability.data.end1 == '00:00:00' && u.availability.data.start2 == '00:00:00' && u.availability.data.end2 == '00:00:00') {
		$("#" + u.nickname + "_detail").append(u.availability.data.always_label);
	} else {
		$("#" + u.nickname + "_detail").append(u.availability.data.times_label + "<br>");
		$("#" + u.nickname + "_detail").append(u.availability.data.start1 + " - " + u.availability.data.end1);
		if (u.availability.data.start2 != '00:00:00' && u.availability.data.end2 != '00:00:00') {
			$("#" + u.nickname + "_detail").append("<br>");
			$("#" + u.nickname + "_detail").append(u.availability.data.start2 + " - " + u.availability.data.end2);
		}
	}
	if (u.availability.data.zone) {
		$("#" + u.nickname + "_detail").append("<br>" + u.availability.data.zone_label + ": " + u.availability.data.zone + " (GMT " + u.availability.data.offset + ")<br>");
	}

	$("#" + u.nickname + "_detail").slideDown('fast');	

}
//###################################################################
//RE-PREP SEARCH FORM AND DISPLAY PREVIOUS RESULTS
//###################################################################
function js_backToResults(s) {
	
	mydata = s;
	
	if (s.request == 'user_suggest') {
		js_userSuggest();
		return;
	}

	$("#current_page").val(s.current_page);
	
	if (s.directory) {
		if (!s.nickname) {
			var l = s.directory.split(",");	
			$("#nickname_search").html(s.directory);
			for (i=0;i<=l.length;i++) {
				$("#" + l[i]).attr("class","dir_letter_selected user_search_data");
			}
		} else {
			$("#nickname").val(s.directory);
		}
	}
	
	if (s.language) {
	
		$.each(s.language, function (i,v) {
		
			$("#sl_" + v.id).prop("checked",true);	

			$("#level_" + v.id + " option").each(function(){
				if (this.value == v.level) {
					$("#level_" + v.id).val(v.level);
				}
			});
			
				
		});		
		
	}
	
	if (s.fields) {
		
		$("#data_search_options").slideDown();
	
		$.each(s.fields, function (i,v) {

			$.each(v,function(ii,vv) {
					
				$(".field_" + i).each(function() {
				
					if (this.value == ii) {
						$(this).prop("checked",true);
					}
						
				});
						
			});
				
		});
		
	}
	
	js_submitSearch();
	
}	
//###################################################################
//CLEAR RADIO BUTTONS IN SEARCH FORM
//###################################################################
function js_clearRadios(c) {

	$('.' + c).prop('checked',false);

	delete mydata.fields[$('.' + c).attr('name')]

	mydata.request = 'user_search';

	path = 'user/search/results';

}
//###################################################################
//COLLECT USER SEARCH DATA
//###################################################################
function js_userSearch(o) {

	$("#ajax_submit_error").hide();

	if (mydata.request && mydata.request == "user_detail") {
		mydata.request = false;
		path = false;
	}

	if (mydata.request && mydata.request == "user_suggest") mydata.request = 'user_search';
	if (!mydata.request) mydata.request = 'user_search';

	if (!path) path = 'user/search/results';

	var p = $(o).parent().attr("id");

	if (p == "directory") {

		$("#nickname").val('')
		if (mydata.nickname) mydata.nickname = false;

		var d = false;
		var c = $("#" + o.id).attr("class");
		if (c == "dir_letter_selected user_search_data") {
			$("#" + o.id).attr("class","dir_letter user_search_data");	
			d = $("#nickname_search").html().replace(","+o.id,"").replace(o.id+",","").replace(o.id,"");
			$("#nickname_search").html(d);
		} else {
			if ($("#nickname_search").html().length < 1) {
				$("#nickname_search").html($("#nickname_search").html() + o.id);
			} else {
				$("#nickname_search").html($("#nickname_search").html() + "," + o.id);
			}
			$("#" + o.id).attr("class","dir_letter_selected user_search_data");
		}
		mydata.directory = $("#nickname_search").html();

	} else if (o.id == "nickname") {
		
		mydata.nickname = true;
		
		$(".dir_letter_selected").attr("class","dir_letter");
		$("#nickname_search").html("");

		//OVERWRITE IF SPECIFIC NICKNAME ENTERED
		if ($("#nickname").val() != '') {
			mydata.directory = $("#nickname").val();
		} else {
			mydata.directory = $("#nickname_search").html();
		}	

	} else if (o.name == "language" || o.name == "language_level") {
		
		p = $(o).attr("level_id");

		if (!mydata.language) mydata.language = { };

		if (o.name == "language") {
			if (mydata.language[o.value]) {
				delete mydata.language[o.value];
			} else {		
				mydata.language[o.value] = { };
				mydata.language[o.value].id = o.value;
				mydata.language[o.value].level = $("#level_" + o.value).val();
			}
		} else {
			if (mydata.language[p]) mydata.language[p].level = $("#level_" + p).val();
		}
		if ($.isEmptyObject(mydata.language)) delete mydata.language;
		
	} else if (o.id == "extra_filter") {
		
		if (!mydata.filter) mydata.filter = { };
		
		var f = $(o).attr("filter");
		
		if (mydata.filter[f]) {
			mydata.filter[f] = false;
		} else {
			mydata.filter[f] = true;	
		}
		
	} else {
		o.name = o.name.replace("[]","");

		var t = $("#" + o.id).attr('type');

		if (!mydata.fields) mydata.fields = { };
			
		//CHECKBOXES
		if (t == 'checkbox') {
			if (!mydata.fields[o.name]) mydata.fields[o.name] = { };
			if (mydata.fields[o.name][o.value]) {
				delete mydata.fields[o.name][o.value];
				if ($.isEmptyObject(mydata.fields[o.name])) {
					delete mydata.fields[o.name];
				}
			} else {
				mydata.fields[o.name][o.value] = true;
			}
			if ($.isEmptyObject(mydata.fields)) delete mydata.fields;
		}
		//RADIO BUTTONS
		if (t == 'radio') {
			mydata.fields[o.name] = { };
			mydata.fields[o.name][o.value] = true;
		}

	}

	if ($.isEmptyObject(mydata.fields) && $.isEmptyObject(mydata.directory) && $.isEmptyObject(mydata.language)) {
		json = false;
	} else {
		json = JSON.stringify(mydata);
	}

	//$("#the_json").empty().html(json);

}
//###################################################################
//SUBMIT THE SEARCH TO THE SERVER
//###################################################################
function js_submitSearch() {

	var valid = false;
	if (mydata.fields) valid = true;
	if (mydata.directory) valid = true;
	if (mydata.language) valid = true;
	mydata.request = 'user_search';
	mydata.current_page = $("#current_page").val();
	path = 'user/search/results';

	if (valid) {
		$(".search_result_filter").prop("checked",false);
		js_ajax();
	} else {
		$("#ajax_submit_error").show();
		return;
	}

}
//###################################################################
//COLLECT USER SEARCH DATA
//###################################################################
function js_userSuggest() {

	$("#ajax_submit_error").hide();

	json = true;

	if (mydata.request && mydata.request == "user_search") mydata.request = false;
	if (mydata.request && mydata.request == "user_detail") mydata.request = false;
	if (!mydata.request) mydata.request = 'user_suggest';
	if (!path) path = 'user/search/results';

	js_ajax();

}
//###################################################################
//MAKE A CONVERSATION REQUEST
//###################################################################
function js_userMakeRequest(id) {

	if (id == "conversation_now") {

		mydata.user2 = $("#user2").val();
		mydata.now = true;
		mydata.mode = $("#mode").val();

	} else {

		mydata.selected_option = 1;

		if ($("#selected_option").val() == "2") {
			mydata.selected_option = 2;
		}

		if (!mydata.request) mydata.request = "conversation_request";

		if (id == "conversation_confirm" || id == "conversation_decline" || id == "conversation_cancel") {
			if (id == "conversation_confirm") mydata.confirmed = true;
			if (id == "conversation_decline") mydata.declined = true;
			if (id == "conversation_cancel") mydata.cancelled = true;
			mydata.user1_c_remind= $("#user1_c_remind").val();
			mydata.user2_c_remind= $("#user2_c_remind").val();
			mydata.confirm_message= $("#confirm_message").val();
		}

		mydata.mode = $("#mode").val();
		mydata.user2 = $("#user2").val();
		mydata.user2_offset = $("#user2_offset").val();
		mydata.proposed_date1 = $("#proposed_date1").val();
		mydata.proposed_date2 = $("#proposed_date2").val();
		mydata.p1_hour= $("#p1_hour").val();
		mydata.p1_min= $("#p1_min").val();
		mydata.p2_hour= $("#p2_hour").val();
		mydata.p2_min= $("#p2_min").val();
		mydata.request_message= $("#request_message").val();
		mydata.code= $("#code").val();
		mydata.user1_remind= $("#user1_remind").val();
		mydata.user2_remind= $("#user2_remind").val();


		$("#conversation_request").hide();

	}

	if (!path) path = 'member/' + $("#user2_nickname").val() + '/req';

	json = JSON.stringify(mydata);

	js_ajax();

}
//###################################################################
//DISPLAY REQUEST RESULT
//###################################################################
function js_userRequestResult(data) {

	if (data.status) {

		$("#response_area").empty();	

		if (data.status == "proposed") {

			$("#now_area").hide();
			$("#request_area").hide();

		} else if (data.status == "confirmed") {

			$("#conversation_confirm").hide();

		} else if (data.status == "now") {

			$("#now_area").hide();
			$("#request_area").hide();

		} else if (data.status == "declined") {

			$("#conversation_decline").hide();

		} else if (data.status == "cancelled") {

			$("#conversation_cancel").hide();

		} else if (data.status == "amended") {

		}

		$("#response_area").append(data.user_message);	
		$("#response_area").fadeIn();

	}

}
//###################################################################
//CHANGE RESULT PAGE
//###################################################################
function js_resultPage(page) {
		
	$(".search_result_row").hide();
	$(".page_" + page).show();
	$(".result_page").attr("class","result_page clickable");
	$("#page_t_" + page).attr("class","result_page clickable_nd selected_page highlight");
	$("#page_b_" + page).attr("class","result_page clickable_nd selected_page highlight");
	current_page = page;
	$("#current_page").val(page);
	($(".search_result_filter").prop("checked",false));
	
}
//###################################################################
//DISPLAY USER RESULTS
//###################################################################
function js_userResultsDisplay(data) {

	
	$("#user_search_results").empty();
	$(".user_search_results_pagination_t").empty();
	$(".user_search_results_pagination_b").empty();
	if (data) {

		$("#filters").fadeIn();
		
		$("#user_search_results").append(data.length + ' ' + $("#users_found").val() + '<div class="greybar"></div>');
		for (var x = 0; x < data.length; x++) {

			if (data[x].avatar > 0) {
				var avatar = '/content/' + data[x].user_id + '/avatar/avatar.jpg';
			} else {
				var avatar = '/content/default/avatar/avatar.jpg';
			}

			var light = 'red';
			if (data[x].loggedin) {
				light = 'green';
			}

			var r = '<div class="search_result_row page_' + data[x].page;  
			
			if (data[x].skype) r += ' skype_only';
			
			r += '">'; 

			r += '<div style="float:left;margin-right:4px;height:auto;width:auto;"><span id="' + data[x].nickname + '" class="user_search_result clickable small_avatar"><img src="' + avatar + '"></span></div>';
			
			r += '<div style="float:left;height:auto;width:230px;"><span id="' + data[x].nickname + '" class="user_search_result clickable_nd">' + data[x].nickname + '</span>';

			if (data[x].zone) r += ' (' + data[x].zone + ')';

			r += '<br><img src="/images/' + light + 'light.png">';

			if (data[x].skype) r += '&nbsp;<img src="/images/skype.png">';

			r += '</div><br style="clear:both;">';

			if (data[x].languages) {

				$.each(data[x].languages, function (index,value) {
					r += index + ' (' + value + ')<br>';				
				});

			}



			r += '<div id="' + data[x].nickname + '_detail" class="user_search_result_detail" style="display:none;"></div>'; 

			r += '<div class="greybar"></div></div>';

			$("#user_search_results").append(r); 

		}

		//PAGINATION
		var no_results = data.length/10;
		var no_pages = Math.ceil(no_results);

		if (no_pages > 1) {
			var ht = '';
			var hb = '';
			for (var n = 1; n <= no_pages; n++) {
				ht += '<span class="result_page clickable" id="page_t_' + n + '" page="' + n + '">' + n + '</span> | ';
				hb += '<span class="result_page clickable" id="page_b_' + n + '" page="' + n + '">' + n + '</span> | ';				
			}

			if (ht && hb) {
				$(".user_search_results_pagination_t").empty();
				$(".user_search_results_pagination_t").append(ht);
				$(".user_search_results_pagination_b").empty();
				$(".user_search_results_pagination_b").append(hb);
			}

			//ALLOWS USER TO GO BACK TO THE SAM E PAGE OF RESULTS
			js_resultPage($("#current_page").val());
		}		

	} else {
		$(".user_search_results_pagination_t").empty();
		$(".user_search_results_pagination_b").empty();
		$("#user_search_results").append('0 ' + $("#users_found").val() + '<div class="greybar"></div>');
	}

}
//###################################################################
//GET DIRECTORY
//###################################################################
function js_getDirectory() {

	if (!mydata.request) mydata.request = "directory_request";

	json = JSON.stringify(mydata);

	path = 'user/directory/get';
	
	js_ajax();

}
//###################################################################
//DIRECTORY UPDATE
//###################################################################
function js_userDirectoryUpdate(id,action) {

	if (!mydata.request) mydata.request = "directory_update";

	json = JSON.stringify(mydata);

	path = 'user/directory/update/' + action + "/" + id;

	js_ajax();

	//CLEAR TO ALLOW SUBSEQUENT CONVERSATION REQUESTS
	mydata.request = false;
	path = false;

	if (action == 'add') {
		$(".directory_add").hide();
		$(".directory_del").fadeIn();
	} else 	if (action == 'del') {
		$(".directory_del").hide();
		$(".directory_add").fadeIn();
	}

}
//###################################################################
//DIRECTORY DISPLAY
//###################################################################
function js_userDirectoryDisplay(data) {

	if (data == 'all') {
		$(".dir_section").slideDown();
		$(".dir_letter_selected").attr("class","dir_letter directory_select");
		$("#" + data).attr("class","dir_letter_selected directory_select");
		$("#directory_no_results").hide();
		$("#directory_results").fadeIn();
		$("#directory_refresh").fadeIn();
		return;
	}

	if (data.length) {

		$("#directory_no_results").hide();
		$("#directory_results").fadeIn();
		$(".dir_letter_selected").attr("class","dir_letter directory_select");
		$(".dir_section").slideUp();
		$("#dir_" + data).slideDown();
		$("#" + data).attr("class","dir_letter_selected directory_select");
		$("#directory_refresh").fadeOut();
		if ($("#dir_" + data).length < 1) {
			$("#directory_results").hide();
			$("#directory_no_results").fadeIn();
		}


	} else {

		var a = false;
		var entry = false;
		var light = 'red';

		$("#directory_results").empty();

		$.each(data, function (i,v) {

			entry = '<div class="dir_section" id="dir_' + i + '"><div class="dir_header">' + i + "</div>";
			$.each(v, function (ii,vv) {
					
				a = '<a href="/member/' + vv.nickname + '">';
					
				if (vv.avatar) {
					a += '<img align="top" src="/content/' + vv.id + '/avatar/avatar.jpg">';
				} else {
					a += '<img align="top" src="/content/default/avatar/avatar.jpg">';
				}
				
				a += '</a>';

				if (vv.loggedin) {
					light = 'green';
				}

				entry += '<div class="entry_item" style="margin-bottom:4px;"><span class="small_avatar">' + a + '</span> <a href="/member/' + vv.nickname + '">' + vv.nickname + '</a> <img src="/images/' + light + 'light.png"> </div>';

				light = 'red';

			})
			entry += '</div>';
			$("#directory_results").append(entry);

		});
		
		$("#directory_no_results").hide();
	}

}
//###################################################################
//GO BACK TO INBOX
//###################################################################
function js_closeMessage(m) {

	$(".inbox_message").hide();
	$(".inbox_row").fadeIn();
	$("#" + m).css("font-weight","normal");
	$("#inbox_back").fadeOut();
	$("#message_form").slideUp();
	scrollTo(0,0);

}
//###################################################################
//WORD WRAP MESSAGE REPLY
//http://james.padolsey.com/javascript/wordwrap-for-javascript/
//comment1
//###################################################################
function js_wordWrap(str, width, brk, cut) {


     if (str.length <= width) {
        str = str.replace(/<br>/g,"");
        str = str.replace(/&gt;/g,">");
        str = str.replace(/>/g,">>");
	return "\n" + brk + str;
     }

     str = str.replace(/<br>/g,"");

     brk = brk || '>';
     width = width || 75;
     cut = cut || false;

     if (!str) { return str; }

     var regex = '.{1,' +width+ '}(\\s|$)' + (cut ? '|.{' +width+ '}|.+$' : '|\\S+?(\\s|$)');

     str = str.match( RegExp(regex, 'g') ).join( brk );

     str = str.replace(/&gt;/g,">");

     return brk + str;

}
//###################################################################
//FILL REPLY FORM
//###################################################################
function js_messageReply(m) {

	$("#recipient").val($("#sender_" + m).html());
	$("#subject").val("Re: " + $("#subject_" + m).html().replace("Re: ",""));

	var content = $("#message_content_" + m).html().replace(/<br>/g,"");

	$("#the_message").val("\n\n---------------------------------------\n" + $("#sender_" + m).html() + " | " + $("#sent_date_" + m).html() + "\n---------------------------------------\n\n" + content);
	$("#transaction_update").empty();
	$("#the_message").focus();		
	$("#message_form").slideDown();
	scrollTo(0,0);


}
//###################################################################
//DELETE A MESSAGE
//###################################################################
function js_deleteMessage(m) {

	if (!mydata.request) mydata.request = "delete_message";
	mydata.the_message = m;
	mydata.thread = $("#x_" + m + "_thread").val();
	path = 'user/messages/delete';

	json = JSON.stringify(mydata);

	js_ajax();
		

}
//###################################################################
//READ A MESSAGE IN THE UI
//###################################################################
function js_readMessage(m) {

	if (!mydata.request) mydata.request = "message_read";
	mydata.message_id = m;
	mydata.status = 'read';

	json = JSON.stringify(mydata);

	path = 'user/messages/mark';

	js_ajax();

	$(".inbox_row").hide();
	$("#" + m).show();

	$("#message_" + m).fadeIn();
	$("#inbox_back").fadeIn();
	$("#inbox_back").attr("message",m);
	scrollTo(0,0);

}
//###################################################################
//SEND A MESSAGE
//###################################################################
function js_sendMessage() {

	if (!mydata.request) mydata.request = "send_message";

	mydata.recipient = $("#recipient").val();
	mydata.subject = $("#subject").val();
	mydata.thread = $("#thread").val();
	mydata.group_id = $("#group").val();
	mydata.the_message = $("#the_message").val();

	json = JSON.stringify(mydata);

	path = 'user/messages/send';
	
	js_ajax();
	
	//scrollTo(0,0);

}
//###################################################################
//CONFIRM MESSAGE SEND
//###################################################################
function js_userMessagesUpdate(response) {

	$("#transaction_update").empty();
	$("#transaction_update").html(response.data);
	if (response.flag == 'message_sent') {
		setTimeout('$("#message_form").slideUp()',1500);
		setTimeout('$("#transaction_update").empty()',1500);
		setTimeout('$("#recipient").val("")',1700);
		setTimeout('$("#subject").val("")',1700);
		setTimeout('$("#the_message").val("")',1700);									
	}

}
//###################################################################
//CONFIRM MESSAGE DELETE
//###################################################################
function js_userMessagesDelete(data) {

	$("#" + data).fadeOut();
	$("#message_" + data).fadeOut();
	$("#" + data).remove();
	$("#message_" + data).remove();
	scrollTo(0,0);

}
//###################################################################
//AUTOCOMPLETE RECIPIENT
//###################################################################
function js_getRecipient() {

	$("#recipient").autocomplete({
		source: function( request, response ) {
			$.ajax({
				url: getBaseURL() + "user/messages/data",
				dataType: "json",
				data: {
					featureClass: "P",
					style: "full",
					maxRows: 12,
					name_startsWith: request.term
				},
				success: function( data ) {
					response( $.map( data, function( item ) {
						return {
							label: item.recipient,
							value: item.recipient
						}
					}));
				}
			});
		},
		minLength: 2
	});

}
//###################################################################
//AUTOCOMPLETE GROUP NAME
//###################################################################
function js_getGroups() {

	$("#groupname").autocomplete({
		source: function( request, response ) {
			$.ajax({
				url: getBaseURL() + "user/groups/get",
				dataType: "json",
				data: {
					featureClass: "P",
					style: "full",
					maxRows: 12,
					name_startsWith: request.term
				},
				success: function( data ) {
					response( $.map( data, function( item ) {

						return {
							label: item.group_name,
							value: item.group_name,
							id: item.id
						}
			
					}));
				}
			});
		},
		select: function( event, ui ) {
			$("#invite").attr("group",ui.item.id);
		},
		minLength: 2
	});

}
//###################################################################
//CREATE A GROUP
//###################################################################
function js_createGroup() {

	if (!mydata.request) mydata.request = "create_group";

	mydata.group_name = $("#group_name").val();
	mydata.group_description = $("#group_description").val();
	mydata.group_status = $('input[name=group_status]:checked', '#group_create_form_fields').val();

	json = JSON.stringify(mydata);

	path = 'user/groups/create';
	
	js_ajax();

}
//###################################################################
//GROUP CREATE UPDATE
//###################################################################
function js_userGroupUpdate(response) {

	$("#transaction_update").empty();
	$("#transaction_update").html(response.data);
	if (response.flag == 'group_created') {
		setTimeout('$("#group_create_form").slideUp()',1500);
		setTimeout('$("#transaction_update").empty()',1500);
		setTimeout('$("#group_name").val("")',1700);
		setTimeout('$("#group_description").val("")',1700);
		setTimeout('js_userGroupSelect("my")',2000);
	}

}
//###################################################################
//GET GROUPS
//###################################################################
function js_userGroupSelect(l) {


	if (!mydata.request) mydata.request = "get_groups";

	if (isNaN(l)) {
		$(".dir_letter_selected").attr("class","dir_letter group_select");
		$("#" + l).attr("class","dir_letter_selected group_select");
	}

	mydata.l = l;

	json = JSON.stringify(mydata);

	path = 'user/groups/get';

	$("#group_create_form").slideUp();
	
	js_ajax();

}
//###################################################################
//DISPLAY GROUP RESULTS
//###################################################################
function js_userGroupResults(data) {

		var entry = false;
		var header = false;
		var st = false;
		var m = '';

		$("#group_results").empty();

		var h = $("#group").val();
		if ($(".dir_letter_selected").html()) h = $(".dir_letter_selected").html();

		header = '<div class="dir_section" id="dir_' + $(".dir_letter_selected").attr('id') + '"><div class="dir_header">' + h + "</div>";

		$("#group_results").append(header);

		$.each(data, function (i,v) {

			if (v.my_status) {
				st = ' | ' + v.my_status + ' | <span class="highlight">' + v.group_status + '</span> | <a href="/user/messages/sendtogroup/' + v.id + '"><img align="absmiddle" src="/images/envelope.jpg"></a>';
			} else {
				st = '';
			}

			entry = '<div class="entry_item" style="margin-bottom:4px;border-bottom:1px solid #e0e0e0;"> <a href="/user/groups/members/' + v.id + '">' + v.group_name + '</a> (' + v.nm + ') ' + st + '<br>' + v.group_description + '<div style="height:8px;"></div>';

			if (st == '') {
				entry += '<input group="' + v.id + '" type="button" id="join_' + v.id + '" class="user_group" action="join" value="' + $("#join").val() + '"> <div id="group_message_' + v.id + '"></div>';
			} else if (!v.owner) {
				entry += '<input group="' + v.id + '" class="user_group" type="button" id="leave_' + v.id + '" action="leave" value="' + $("#leave").val() + '"> <div id="group_message_' + v.id + '"></div>';
			}

			entry += '</div>';


			$("#group_results").append(entry);


		});


}
//###################################################################
//USER GROUP MANAGEMENT
//###################################################################
function js_userGroup(id) {

	mydata = { };	

	mydata.action = $("#" + id).attr("action");

	mydata.group_id = $("#" + id).attr("group");

	if ($("#" + id).attr("user_id")) mydata.user_id = $("#" + id).attr("user_id");
	if ($("#" + id).attr("request_id")) mydata.request_id = $("#" + id).attr("request_id");

	json = JSON.stringify(mydata);

	path = 'user/groups/action';

	js_ajax();

}
//###################################################################
//USER GROUP ACTON
//###################################################################
function js_userGroupAction(data) {

	if (data.action == 'leave' || data.action == 'join') {
		$("#" + data.action + "_" + data.group_id).hide();
		$("#group_message_" + data.group_id).empty();
		$("#group_message_" + data.group_id).html(data.the_message);

	} else if (data.action == 'invite') {

		//THIS ONLY RELATES TO THE INVITE FORM ON MEMBER PAGE
		$("#group_message_0").empty();
		$("#group_message_0").html(data.the_message);
		$("#groupname").val("");		

	} else {

		$("#request_message_" + data.request_id).empty();
		$("#request_message_" + data.request_id).html(data.the_message);
		$("#request_" + data.request_id).attr("class","actioned");
		setTimeout('$(".actioned").slideUp()',1200);
		
		if ($(".join_request").length == 0) {
			setTimeout('$("#group_requests").slideUp()',1200);
		}
		
	}
		

}
//###################################################################
//GROUP MEMBER DISPLAY
//###################################################################
function js_groupMemberDisplay(data) {

	if (data == 'all') {
		$(".dir_letter_selected").attr("class","dir_letter member_select");
		$("#" + data).attr("class","dir_letter_selected member_select");
		$(".member").css("height","35px");
		$(".member").fadeIn();
		
		return;
	}

	if (data.length) {

		$(".dir_letter_selected").attr("class","dir_letter member_select");
		$(".member").hide();
		$(".member").css("height","0px");
		$(".member_" + data).css("height","35px");
		$(".member_" + data).fadeIn();
		$("#" + data).attr("class","dir_letter_selected member_select");


	}
}
//###################################################################
//DELETE A GROUP MEMBER
//###################################################################
function js_groupMemberDelete(u,g) {

	if (!mydata.request) mydata.request = "delete_member";
	mydata.user_id = u;
	mydata.group_id = g;
	path = 'user/groups/members/delete';

	json = JSON.stringify(mydata);

	js_ajax();

}
//###################################################################
//CONFIRM DELETE A GROUP MEMBER
//###################################################################
function js_groupMemberDeleteConfirm(data) {

	$("#member_" + data).fadeOut();

}
//###################################################################
//INVITE USER TO A GROUP
//###################################################################
function js_inviteUser(id) {

	if (!mydata.request) mydata.request = "invite_user";
	mydata.user_id = id;
	mydata.group_name = $("#groupname").val();
	path = 'user/groups/invite';

	json = JSON.stringify(mydata);

	js_ajax();

}
//###################################################################
//RATE A USER
//###################################################################
function js_rate(rating) {

	if (!mydata.request) mydata.request = "rate_user";
	mydata.user_id = $("#user_id").val();
	mydata.rating = rating;
	path = 'user/rate';

	json = JSON.stringify(mydata);

	js_ajax();

}
//###################################################################
//CONFIRM RATING OF USER
//###################################################################
function js_rateConfirm(data) {

	$("#rating_response").empty();
	$("#rating_response").html(data);

}
//###################################################################
//SUBMIT FEEDBACK ABOUT A CONVERSATION
//###################################################################
function js_submitFeedback() {

	if (!mydata.request) mydata.request = "submit_feedback";
	mydata.user_id = $("#user_id").val();
	mydata.feedback = $("#feedback").val();
	mydata.code = $("#code").val();
	path = 'start/feedback';

	json = JSON.stringify(mydata);

	js_ajax();

}
//###################################################################
//CONFIRM FEEDBACK SENT
//###################################################################
function js_feedbackConfirm(data) {

	$("#feedback_response").empty();
	$("#feedback_response").html(data);
	$("#submit_review_feedback").hide();
	$("#member_review_feedback_input").slideUp('slow');

}
//###################################################################
//GET CONVERSATIONS
//###################################################################
function js_conversations() {

	if (!mydata.request) mydata.request = "get_conversations";
	mydata.user_id = $("#user_id").val();
	path = 'user/conversations/get';

	json = JSON.stringify(mydata);

	js_ajax();

}
//###################################################################
//DISPLAY GROUP RESULTS
//###################################################################
function js_conversationResults(data) {

		var entry = false;
		var cl = false;
		var p = $(".selected_page").attr("page");

		$("#conversation_results").empty();

		$.each(data, function (i,v) {

			if (v.today) {
				cl = 'conversation_entry today search_result_row page_' + data[i].page;
			} else {
				cl = 'conversation_entry nottoday search_result_row page_' + data[i].page;
			}

			if ($("#highlight").val()) {
				entry = '<div class="' + cl + '" id="' + v.code + '" style="width:auto;height:35px;margin-bottom:2px;display:none;">';
			} else {
				entry = '<div class="' + cl + '" id="' + v.code + '" style="width:auto;height:35px;margin-bottom:2px;">';
			}
			
			entry += '<div class="small_avatar" style="float:left;width:35px;height:35px;">';
			if (v.avatar_exists) {
				entry += '<img src="/content/' + v.with_id + '/avatar/avatar.jpg"><br>';
			} else {
				entry += '<img src="/content/default/avatar/avatar.jpg"><br>';
			}
			entry += '</div>';
			entry += '<div class="conversation ' + v.css_status + '" style="width:auto;margin-left:37px;">';
			entry += '<a style="width:100%;height:100%;display:block;" class="clickable_nd" href="/member/' + v.with_name + '/c/' + v.code + '">' + v.with_name + ' | ' + v.start + ' | ' + v.code;
			if (v.status != 'now') entry += ' | ' + v.status;
			entry += '</a>';

			entry += '</div>';

			entry += '</div>';

			$("#conversation_results").append(entry);


		});

		if ($("#highlight").val()) {
			$("#" + $("#highlight").val()).slideDown();
		} else {
			//PAGINATION
			var no_results = data.length/10;
			var no_pages = Math.ceil(no_results);

			if (no_pages > 1) {
				var ht = '';
				var hb = '';
				for (var n = 1; n <= no_pages; n++) {
					ht += '<span class="result_page clickable" id="page_t_' + n + '" page="' + n + '">' + n + '</span> | ';
					hb += '<span class="result_page clickable" id="page_b_' + n + '" page="' + n + '">' + n + '</span> | ';				
				}

				if (ht && hb) {
					$(".conversations_pagination_t").empty();
					$(".conversations_pagination_t").append(ht);
					$(".conversations_pagination_b").empty();
					$(".conversations_pagination_b").append(hb);
				}

				if (p) {
					js_resultPage(p);
				} else {
					js_resultPage(1);
				} 
			}		
		}


}
//###################################################################
//GET MESSAGE TO BE MODERATED
//###################################################################
function js_getModMessage(thread) {

	mydata.request = "get_mod_message";
	mydata.thread = thread;
	path = 'user/messages/getmod';

	json = JSON.stringify(mydata);

	js_ajax();

}
//###################################################################
//SHOW MESSAGE TO BE MODERATED
//###################################################################
function js_showModMessage(data) {

	$("#view_message").empty();
	$("#view_message").hide();
	$("#view_message").append(data);
	$("#view_message").fadeIn();

}
//###################################################################
//UPDATE RE ACTION TAKEN FOR MOD MESSAGE
//###################################################################
function js_updateModMessage(data) {

	$("#mod_update_" + data['thread']).append(data['update']);
	$("#mod_message_" + data['thread']).attr("class","mod_actioned");
	setTimeout('$(".mod_actioned").slideUp()',1200);

}
//###################################################################
//ACTION MODERATED MESSAGE CONFIRM OR DECLINE
//###################################################################
function js_actionModMessage(thread,action,sender) {

	mydata.request = "action_mod_message";
	mydata.thread = thread;
	mydata.action = action;
	mydata.sender = sender;
	path = 'user/messages/actionmod';

	json = JSON.stringify(mydata);

	js_ajax();

}
//###################################################################
//GET BUNDLE OF DISCUSSION POSTS
//###################################################################
function js_getPosts(d,p,t) {

	mydata.discussion = d;
	mydata.group = $("#group").val();
	mydata.page = p;
	mydata.thread_no = t;
	mydata.request = "posts";
	path = "user/groups/forum/" + $("#group").val() + "/discussion/posts";

	js_ajax();

}
//###################################################################
//GET BUNDLE OF DISCUSSIONS
//###################################################################
function js_getDiscussions(g,p) {

	mydata.group = g;
	mydata.page = p;
	mydata.request = "discussions";
	path = "user/groups/forum/" + $("#group").val() + "/discussions";

	js_ajax();

}
//###################################################################
//DISPLAY DISCUSSION POSTS
//###################################################################
function js_displayPosts(data) {

	if (data.status < 1) {
		$("#new_reply").show();
		$("#notification_panel").show();
	}

	var h = "";

	$.each(data, function (i,v) {
			
			
		if (v.thread_no) {

			h += '<div class="discussion_summary" id="post_' + v.id + '">';

			h += '<div id="thread_' + v.thread_no + '" style="text-align:right;">';
			h += '	<div class="post_header"><div id="poster_post_' + v.id + '" style="width:auto;float:left;">' + v.poster + '</div><div style="margin-left:100px;text-align:right;">' + v.created + ' | <a class="highlight" href="/user/groups/discussion/' + v.discussion_id + '/' + v.thread_no + '">#' + v.thread_no + '</a></div></div>';
			h += '</div>';

			h += '<div id="content_' + v.id + '" class="post_content">' + v.content + '</div>'; 

			h += '<div style="padding-top:10px;"><div style="width:auto;float:left;"><i>';

			if (v.edited != '0000-00-00 00:00:00') h += $("#edited").val() + ": " + v.edited;

			h += '</i></div>';

			if (data.status < 1) {

				h += '<div style="margin-left:100px;text-align:right;">';

				if (v.is_poster || data.is_admin) {

					if (data.is_admin) {
						h += '<span id="post_delete_' + v.id + '" class="reveal post_delete_confirm_' + v.id + ' fadeIn delete_something clickable">delete</span> <span id="post_delete_confirm_' + v.id + '" post="' + v.id + '" class="delete_post clickable" style="display:none;">' + $("#confirm").val() + '</span> | ';
					}
					h += '<span id="post_edit_reveal_' + v.id + '" class="reveal post_edit_' + v.id + ' slideDown clickable highlight">' + $("#edit").val() + '</span> | ';
				}

				if (v.flagged > 0) {
					h += '<img src="/images/redwarn.png" align="absbottom">';	
				} else {
					h += '<span id="post_flag_' + v.id + '" post="' + v.id + '" class="clickable highlight flag_post" thread_no="' + v.thread_no + '">' + $("#flag").val() + '</span>';
				}

				h += ' | <span id="post_reply_reveal_' + v.id + '" class="reveal post_reply_' + v.id + ' slideDown clickable highlight">' + $("#reply").val() + '</span>';
				h += '</div>';
				
				h += '<div class="post_reply" id="post_reply_' + v.id + '" style="display:none;height:auto;width:auto;">';
				h += '	<div class="greybar"></div>';
				h += '	<textarea name="post_reply_text" id="post_reply_text_' + v.id + '" style="width:99%;padding:0.5%;height:120px;" class="post_reply_text">[quote:' + v.poster + ']' + v.reply_content + '[/quote]</textarea>';
				h += '	<div style="text-align:right;">';
				h += '		<input type="button" id="post_reply_cancel_' + v.id + '" value="' + $("#cancel").val() + '" class="reveal post_reply_' + v.id + ' slideDown">';
				h += '		<input type="button" class="submit_post" post="' + v.id + '" value="' + $("#post").val() + '" post="' + v.id + '">';
				h += '	</div>';
				h += '</div>';

				if (v.is_poster) {

					h += '<div class="post_edit" id="post_edit_' + v.id + '" style="display:none;height:auto;width:auto;">';
					h += '	<div class="greybar"></div>';
					h += '	<textarea name="post_edit_text" id="post_edit_text_' + v.id + '" style="width:99%;padding:0.5%;height:120px;" class="post_edit_text">' + v.edit_content + '</textarea>';
					h += '	<div style="text-align:right;">';
					h += '		<input type="button" id="post_edit_cancel_' + v.id + '" value="' + $("#cancel").val() + '" class="reveal post_edit_' + v.id + ' slideDown">';
					h += '		<input type="button" class="edit_post" post="' + v.id + '" thread_no="' + v.thread_no + '" value="' + $("#confirm").val() + '" post="' + v.id + '">';
					h += '	</div>';
					h += '</div>';
				}

			}

			h += '</div></div>';

		}

	});

	$("#post_list").empty();
	$("#post_list").append(h);

	$("#subject").html(data.subject);
	if (data.status > 0) $("#subject").append(' <b>(' + $("#closed").val() + ')</b>');

	//NOTIFICATION SETTING
	if (data.notify) {
		$("#notify_setting").prop('checked', true);
	} else {
		$("#notify_setting").prop('checked', false);
	}

	//PAGINATION
	var no_pages = Math.ceil(data.total/10);

	if (no_pages > 1) {
		var ht = '';
		var hb = '';
		for (var n = 1; n <= no_pages; n++) {
			ht += '<span class="discussion_page clickable" id="page_t_' + n + '" page="' + n + '">' + n + '</span> | ';
			hb += '<span class="discussion_page clickable" id="page_b_' + n + '" page="' + n + '">' + n + '</span> | ';				
		}

		if (ht && hb) {
			$("#post_list_pagination_t").empty();
			$("#post_list_pagination_t").append(ht);
			$("#post_list_pagination_b").empty();
			$("#post_list_pagination_b").append(hb);
		}

		$("#page_t_" + data.page).attr("class","highlight");
		$("#page_b_" + data.page).attr("class","highlight");

	}

			


	//SPECIFIC POST SCROLL TO
	if (data.thread_no > 0) {
		$('html, body').animate({scrollTop: $("#thread_" + data.thread_no).offset().top
		     }, 1200);

	}

}
//###################################################################
//DISPLAY DISCUSSIONS
//###################################################################
function js_displayDiscussions(data) {

	var h = "";

	$.each(data, function (i,v) {

		if (v.group_id) {

			h += '<div class="discussion_summary">';
			h += '<div class="discussion_summary_col" style="width:540px;padding-left:0px;"><a href="/user/groups/forum/' + v.group_id + '/discussion/' + v.id + '">' + v.subject + '</a>';

			if (v.status > 0) h += ' | ' + $("#closed").val();

			h += '</div>';

			h += '<div class="discussion_summary_col" style="width:70px;">' + v.no_posts + '<br>&nbsp;</div>';
			h += '<div class="discussion_summary_col" style="width:140px;"><span class="highlight">' + v.created_by_nick + '</span><br>' + v.created + '</div>';
			h += '<div class="discussion_summary_col" style="width:140px;"><span class="highlight">' + v.last_by_nick + ' <a href="/user/groups/forum/' + v.group_id + '/discussion/' + v.id + '/' + v.last_post_no + '">>></a></span><br>' + v.last_post + '</div>';
			h += '<br style="clear:both;">';

			if (data.is_admin) h += '<span id="delete_' + v.id + '" class="reveal delete_discussion_' + v.id + ' fadeIn delete_something clickable">' + $("#delete").val() + '</span> <span id="delete_discussion_' + v.id + '" discussion="' + v.id + '" class="delete_discussion clickable" style="display:none;">' + $("#confirm").val() + '</span>';

			if (data.is_admin) {
				if (v.status < 1) {
					var action = $("#close").val();
					var status = 1;
				} else {
					var action = $("#open").val();
					var status = 0;
				}
				h += ' | <span id="toggle_' + v.id + '" class="reveal toggle_discussion_' + v.id + ' fadeIn delete_something clickable">' + action + '</span> <span id="toggle_discussion_' + v.id + '" discussion="' + v.id + '" status="' + status + '" class="toggle_discussion clickable" style="display:none;">' + $("#close").val() + '</span>';

			}

			h += '</div>';

		}

	});

	$("#discussion_list").empty();
	$("#discussion_list").append(h);

	//PAGINATION
	var no_pages = Math.ceil(data.total/10);

	if (no_pages > 1) {
		var ht = '';
		var hb = '';
		for (var n = 1; n <= no_pages; n++) {
			ht += '<span class="forum_page clickable" id="page_t_' + n + '" page="' + n + '">' + n + '</span> | ';
			hb += '<span class="forum_page clickable" id="page_b_' + n + '" page="' + n + '">' + n + '</span> | ';				
		}

		if (ht && hb) {
			$("#discussion_list_pagination_t").empty();
			$("#discussion_list_pagination_t").append(ht);
			$("#discussion_list_pagination_b").empty();
			$("#discussion_list_pagination_b").append(hb);
		}

		$("#page_t_" + data.page).attr("class","highlight");
		$("#page_b_" + data.page).attr("class","highlight");

	}

}
//###################################################################
//SUBMIT A DISCUSSION POST
//###################################################################
function js_submitPost(p) {

	mydata = { };
	mydata.discussion_id = $("#discussion_id").val();
	mydata.request = "submit_post";
	mydata.group = $("#group").val();
	mydata.content = $("#post_reply_text_" + p).val();
	mydata.poster = $("#poster_post_" + p).html();
	path = "user/groups/forum/" + $("#group").val() + "/discussion/submitpost";

	js_ajax();

}
//###################################################################
//SUBMIT A DISCUSSION POST
//###################################################################
function js_flagPost(p,t) {

	mydata = { };
	mydata.discussion_id = $("#discussion_id").val();
	mydata.request = "flag_post";
	mydata.group = $("#group").val();
	mydata.post = p;
	mydata.thread_no = t;
	path = "user/groups/forum/" + $("#group").val() + "/discussion/flagpost";

	js_ajax();

}
//###################################################################
//CHANGE NOTIFICATION SETTING FOR A DISCUSSION
//###################################################################
function js_notifyPost() {

	mydata = { };
	mydata.discussion_id = $("#discussion_id").val();
	mydata.request = "notify_post";
	path = "user/groups/forum/" + $("#group").val() + "/discussion/notifypost";

	js_ajax();

}
//###################################################################
//REDIRECT TO NEW POST
//###################################################################
function js_newPost(data) {

	js_getPosts(data.d,data.p,data.t);
	$("#post_reply_text_new").val('');

}
//###################################################################
//SUBMIT A NEW DISCUSSION
//###################################################################
function js_submitDiscussion(g) {

	mydata = { };
	mydata.request = "submit_discussion";
	mydata.group = g;
	mydata.subject = $("#new_discussion_subject").val();
	mydata.content = $("#post_content_new").val();
	path = "user/groups/forum/" + g + "/discussion/submitdiscussion";

	js_ajax();

}
//###################################################################
//EDIT AN EXISTING POST
//###################################################################
function js_editPost(p,t) {

	mydata = { };
	mydata.request = "edit_post";
	mydata.discussion_id = $("#discussion_id").val();
	mydata.post = p;
	mydata.thread_no = t;
	mydata.content = $("#post_edit_text_" + p).val();
	path = "user/groups/forum/" + $("#group").val() + "/discussion/editpost";

	js_ajax();

}
//###################################################################
//DELETE A DISCUSSION
//###################################################################
function js_deleteDiscussion(d) {

	mydata = { };
	mydata.request = "delete_discussion";
	mydata.discussion_id = d;
	mydata.group = $("#group").val();
	path = "user/groups/forum/" + $("#group").val() + "/discussion/deldiscussion";

	js_ajax();

}
//###################################################################
//DELETE A POST
//###################################################################
function js_deletePost(p) {

	mydata = { };
	mydata.request = "delete_post";
	mydata.post = p;
	mydata.discussion_id = $("#discussion_id").val();
	path = "user/groups/forum/" + $("#group").val() + "/discussion/delpost";

	js_ajax();

}
//###################################################################
//TOGGLE A DISCUSSION STATUS
//###################################################################
function js_toggleDiscussion(d,status) {

	mydata = { };
	mydata.request = "toggle_discussion";
	mydata.discussion_id = d;
	mydata.status = status;
	mydata.group = $("#group").val();
	path = "user/groups/forum/" + $("#group").val() + "/discussion/togglediscussion";

	js_ajax();

}
//###################################################################
//REDIRECT TO NEW DISCUSSION
//###################################################################
function js_newDiscussion(data) {

	path = "/user/groups/forum/" + data.g + "/discussion/" + data.d + "/1";
	top.location.href = getBaseURL() + path;

}
//###################################################################
//SHOW A POST AS FLAGGED
//###################################################################
function js_newFlag(data) {

	$("#post_flag_" + data.p).html(data.update);
	$("#post_flag_" + data.p).attr("class","none");

}
//###################################################################
//HANDLE AJAX RESPONSE
//###################################################################
function js_ajaxResponse(response) {

	if (response) {

		if (response.flag == 'user_search_results') {
			js_userResultsDisplay(response.data);
		} else if (response.flag == 'user_search_detail') {
			js_userResultDisplay(response.data);
		} else if (response.flag == 'user_suggest_results') {
			js_userResultsDisplay(response.data);
		} else if (response.flag == 'conversation_request_result') {
			js_userRequestResult(response.data);
		} else if (response.flag == 'conversation_now_result') {
			js_userRequestResult(response.data);
		} else if (response.flag == 'directory') {
			js_userDirectoryDisplay(response.data);
		} else if (response.flag == 'message_sent' || response.flag == 'message_not_sent') {
			js_userMessagesUpdate(response);
		} else if (response.flag == 'message_deleted') {
			js_userMessagesDelete(response.data);
		} else if (response.flag == 'group_created' || response.flag == 'group_not_created' ) {
			js_userGroupUpdate(response);
		} else if (response.flag == 'group_results') {
			js_userGroupResults(response.data);
		} else if (response.flag == 'user_group') {
			js_userGroupAction(response.data);
		} else if (response.flag == 'group_member_delete') {
			js_groupMemberDeleteConfirm(response.data);
		} else if (response.flag == 'rating_updated') {
			js_rateConfirm(response.data);
		} else if (response.flag == 'feedback_updated') {
			js_feedbackConfirm(response.data);
		} else if (response.flag == 'conversation_results') {
			js_conversationResults(response.data);
		} else if (response.flag == 'show_mod_message') {
			js_showModMessage(response.data);
		} else if (response.flag == 'mod_message_update') {
			js_updateModMessage(response.data);
		} else if (response.flag == 'post_list') {
			js_displayPosts(response.data);
		} else if (response.flag == 'discussion_list') {
			js_displayDiscussions(response.data);
		} else if (response.flag == 'new_post') {
			js_newPost(response.data);
		} else if (response.flag == 'new_discussion') {
			js_newDiscussion(response.data);
		} else if (response.flag == 'post_flagged') {
			js_newFlag(response.data);
		} else if (response.flag == 'post_edited') {
			js_newPost(response.data);
		} else if (response.flag == 'tz_updated') {
			top.location.href = getBaseURL() + "/user/dashboard";
		} else if (response.flag == 'discussion_deleted') {
			path = "/user/groups/forum/" + response.data.g;
			top.location.href = getBaseURL() + path;
		} else if (response.flag == 'post_deleted') {
			$("#post_" + response.data.p).slideUp();
		} else if (response.flag == 'discussion_toggled') {
			path = "/user/groups/forum/" + response.data.g;
			top.location.href = getBaseURL() + path;
		}

	}

}
//###################################################################
//SUBMIT AJAX REQUEST
//###################################################################
function js_ajax() {

	json = JSON.stringify(mydata);

	$(".ajax-loader").show();

	var data = {
		'json': json
		};

	$.ajax({
		type:           'post',
		cache:          false,
		dataType:	'json',
		url:            getBaseURL() + path,
		data:           data
	})
	.done(js_ajaxResponse)
	.always(function() {
		$(".ajax-loader").hide();
		$("#ajax-loader").hide();
	})
	.fail(function() {

	});
}
