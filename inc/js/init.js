$(document).ready(js_Init);

function js_Init() {

	//ADMIN
	$("#entry_method").live("change", function(event){
		js_entryMethods();		
 	});
	$(".text_group_select").live("change", function(event){
		js_submitTextGroup(this.id);		
 	});
	$(".delete_data").live("click", function(event){
		$("#delete_" + this.id).toggle();
 	});
	$(".cancel_delete").live("click", function(event){
		$("#" + this.id).parent().toggle();
 	});

	//USER
	$("#submit_login").on("click", function(event){
		$("#user_register_form").submit();
 	});

 	$("#password").keypress(function(event) {
 		if (event.which == 13) {
 			event.preventDefault();
 			$("#user_register_form").submit();
 		}
 	});
	
	$(".user_search_data").on("click", function(event){
		js_userSearch(this);
 	});
 	$(".user_search_data_select").on("change", function(event){
		js_userSearch(this);
 	});
 	$("#nickname").keyup(function(event){
		js_userSearch(this);
 	});
	$("#user_suggest").live("click", function(event){
		js_userSuggest();
 	});
	$(".user_search_result").live("click", function(event){
		js_userResultDetail(this);
 	});
	$(".close").live("click", function(event){
		$(this).parent().slideUp('fast');
 	});
	$(".submit_search").live("click", function(event){
		js_submitSearch();
 	});
	$(".time_propose").live("change", function(event){
		js_updateTime(this.id);
 	});
	$("#conversation_request").live("click", function(event){
		if ($("#proposed_date").valid()) {
			js_userMakeRequest(this.id);
			$("#review_area").slideUp();
		}				
 	});
	$("#conversation_confirm").live("click", function(event){
		js_userMakeRequest(this.id);
		$("#review_area").slideUp();		
 	});
	$("#conversation_decline").live("click", function(event){
		js_userMakeRequest(this.id);
		$("#review_area").slideUp();		
 	});
	$("#conversation_cancel").live("click", function(event){
		js_userMakeRequest(this.id);
		$("#review_area").slideUp();
 	});
	$("#conversation_now").live("click", function(event){
		js_userMakeRequest(this.id);
 	});
	$("#conversation_amend").live("click", function(event){
		$("#response_area").empty();	
		$("#review_area").fadeIn();
 	});
	$(".clear_heartbeat").live("click", function(event){
		heartbeat(this.id);
 	});
	$(".select_option").live("click", function(event){
		js_proposedOptionSelect(this.id);
 	});
	$("#directory_refresh").live("click", function(event){
		js_getDirectory();
 	});
	$(".directory_select").live("click", function(event){
		js_userDirectoryDisplay(this.id);
 	});
	$(".directory_add").live("click", function(event){
		js_userDirectoryUpdate(this.id,'add');
 	});
	$(".directory_del").live("click", function(event){
		js_userDirectoryUpdate(this.id,'del');
 	});
	$("#send_message").live("click", function(event){
		if ($("#message_form_fields").valid()) js_sendMessage();
 	});
	$(".reveal").live("click", function(event){
		js_reveal(this.id);
 	});
	$(".confirm_delete_message").live("click", function(event){
		js_deleteMessage($(this).attr("message"));
 	});
	$(".inbox_row td:not(:last-child)").live("click", function(event){
		js_readMessage($(this).closest('tr').attr("id"));
 	});
	$("#inbox_back").live("click", function(event){
		js_closeMessage($(this).attr("message"));
 	});
	$(".inbox_reply").live("click", function(event){
		js_messageReply($(this).attr("message"));
 	});
	$("#cancel_message").live("click", function(event){
		$("#recipient").val("");
		$("#subject").val("");
		$("#the_message").val("");									
 	});
	$("#active_only").live("click", function(event){
		$(".inactive").toggle('slow');
 	});
	$("#create_group").live("click", function(event){
		if ($("#group_create_form_fields").valid()) js_createGroup();
 	});
	$(".group_select").live("click", function(event){
		js_userGroupSelect(this.id);
 	});
	$(".user_group").live("click", function(event){
		if (this.id == 'invite') {
			if ($("#invite_user_form_fields").valid()) {
				js_userGroup(this.id);
			}
		} else {
			js_userGroup(this.id);
		}
 	});
	$(".member_select").live("click", function(event){
		js_groupMemberDisplay(this.id);
 	});
	$(".confirm_delete_member").live("click", function(event){
		js_groupMemberDelete($(this).attr("user_id"),$(this).attr("group_id"));
 	});
	$(".rating_submit").rating({
		callback: function(value, link){
			js_rate(value);
		}
	});
	$("#submit_review_feedback").live("click", function(event){
		if ($("#review_feedback").valid()) js_submitFeedback();
 	});
	$("#today_filter").live("click", function(event){
		$(".nottoday").slideUp();
		$(".conversations_pagination").hide();
 	});
	$("#not_today_filter").live("click", function(event){
		js_conversations();
		//$(".nottoday").slideDown();
		$(".conversations_pagination").fadeIn();
 	});
	$(".view_mod_message").live("click", function(event){
		js_getModMessage($(this).attr("thread"));
 	});
	$(".action_mod_message").live("click", function(event){
		js_actionModMessage($(this).attr("thread"),$(this).attr("action"),$(this).attr("sender"));
 	});
	$("#select_asset").live("change", function(event){
		js_displayAsset(event.target.options[event.target.selectedIndex].text);
 	});
	$("#openchat").live("click", function(event){
		js_openChat($(this).attr("href"),event);
 	});
	$("#openskype").live("click", function(event){
		js_openSkype();
 	});
	$(".result_page").live("click", function(event){
		js_resultPage($(this).attr("page"));
 	});
	$(".discussion_page").live("click", function(event){
		js_getPosts($("#discussion_id").val(),$(this).attr("page"),0);
 	});
	$(".forum_page").live("click", function(event){
		js_getDiscussions($("#group").val(),$(this).attr("page"),0);
 	});
	$(".submit_post").live("click", function(event){
		js_submitPost($(this).attr("post"));
 	});
	$(".edit_post").live("click", function(event){
		js_editPost($(this).attr("post"),$(this).attr("thread_no"));
 	});
	$("#submit_discussion").live("click", function(event){
		js_submitDiscussion($(this).attr("group"));
 	});
	$(".delete_discussion").live("click", function(event){
		js_deleteDiscussion($(this).attr("discussion"));
 	});
	$(".delete_post").live("click", function(event){
		js_deletePost($(this).attr("post"));
 	});
	$(".toggle_discussion").live("click", function(event){
		js_toggleDiscussion($(this).attr("discussion"),$(this).attr("status"));
 	});
	$(".flag_post").live("click", function(event){
		js_flagPost($(this).attr("post"),$(this).attr("thread_no"));
 	});
	$(".notify_new_post").live("click", function(event){
		js_notifyPost();
 	});
	$(".dismiss_banner").live("click", function(event){
		$(".banner_message").slideUp();
 	});
 	$("#update_offset_button").live("click", function(event){
		js_updateTzOffset();
 	});
	$("#remove_skype").live("click", function(event){
		if ($("#remove_skype").prop('checked')) {
			$("#skype").attr("class","skype");
			$("#confirm_skype").attr("class","skype");
		} else {
			$("#skype").attr("class","required skype");
			$("#confirm_skype").attr("class","required skype");			
		}
 	});
	$(".search_result_filter").live("click", function(event){
			
		var filter = $(this).attr("filter");
			
		if ($(this).prop('checked')) {	
			$(".search_result_row").hide();
			$("div." + filter + ".page_" + current_page).show();
		} else {
			$(".page_" + current_page).slideDown();
		}
		
 	});
 	$("#facebook_mobile_login").live("click", function(event){
		js_fbLogin();		
 	});	
 	
	//BLINK AND FADE OUT THE ANY PROCESS MESSAGES
	setTimeout('$(".process_message").attr("class","highlight process_message")',500);
	setTimeout('$(".process_message").attr("class","highlight2 process_message")',1000);
	setTimeout('$(".process_message").attr("class","highlight process_message")',1500);
	setTimeout('$(".process_message").attr("class","highlight2 process_message")',2000);
	setTimeout('$(".process_message").attr("class","highlight process_message")',2500);		
	setTimeout('$(".process_message").fadeOut()',3000);

	//PRELOAD PING
	var preload=new Image(); preload.src="/inc/media/ping.wav";

	//DATEPICK
	$(".datefield").datepick({
		dateFormat: 'yyyy-mm-dd',
		minDate: 0,
		onSelect: function(dateText, inst) { 
			js_updateTime(dateText);		
		}
	});


	//JCROP
	$("#avatar_orig").Jcrop({
		onChange: avatarPreview,
		onSelect: avatarPreview,
		setSelect:   [ 100, 100, 50, 50 ],
		aspectRatio: 1
	});

	//AUTOCOMPLETE SEND EMAIL FORM
	js_getRecipient();

	//AUTOCOMPLETE GROUP INVITE
	js_getGroups();

	//FORMS VALIDATION
	jQuery.validator.addMethod("noSpace", function(value, element) { 
  		return value.indexOf(" ") < 0 && value != ""; 
		}, $.format($("#spaces_not_allowed").val()));

	jQuery.validator.addMethod("noApos", function(value, element) { 
  		return value.indexOf("'") < 0 && value != ""; 
		}, $.format($("#apostrophes_not_allowed").val()));


	$("form").each(function() {
		$(this).validate({
			rules: {
				"me[password]": {
					minlength: 6
				},
				//NO SPACES IN NICKNAME FIELD
				"me[19]": {
          				noSpace: true,
          				noApos: true
      				}	
			},
			messages: {
				//REGISTRATION AND LOGIN
				"me[email]": {
					required: $.format($("#field_required").val()),
					email: $.format($("#valid_email").val())
				},
				"me[password]": {
					required: $.format($("#field_required").val()),
					minlength: $.format($("#minimum_length_6").val())
				},
				"me[confirm_password]": {
					required: $.format($("#field_required").val()),
					minlength: $.format($("#minimum_length_6").val())
				},
				"me[familyname]": {
					required: $.format($("#field_required").val())
				},
				"me[parentemail]": {
					required: $.format($("#field_required").val())
				},
				"me[familycode]": {
					required: $.format($("#field_required").val())
				},
				"me[captcha]": {
					required: $.format($("#field_required").val())
				},
				"me[terms]": {
					required: $.format($("#field_required").val())
				},
				//MESSAGES
				"recipient": {
					required: $.format($("#field_required").val())
				},
				"subject": {
					required: $.format($("#field_required").val())
				},
				"the_message": {
					required: $.format($("#field_required").val())
				},
				//GROUPS
				"groupname": {
					required: $.format($("#field_required").val())
				},
				"group_name": {
					required: $.format($("#field_required").val())
				},
				"group_description": {
					required: $.format($("#field_required").val())
				},
				//CONVERSATIONS
				"proposed_date1": {
					required: $.format($("#field_required").val())
				},
				"feedback": {
					required: $.format($("#field_required").val())
				},
				//PROFILE
				"me[19]": {
					required: $.format($("#field_required").val())
				},
				//ADMIN TEXTS
				text_description: {
					required: $.format($("#field_required").val())
				},
				text_tag: {
					required: $.format($("#field_required").val())
				},
				text: {
					required: $.format($("#field_required").val())
				}

			}	
		});
	});	

	//TINYMCE
	$('textarea.tinymce').tinymce({

		// Width

		relative_urls : false,
		convert_urls : false,

		// Location of TinyMCE script
		script_url : '/inc/js/tinymce/jscripts/tiny_mce/tiny_mce.js',

		// General options
		theme : "advanced",
		plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,advlist",

		// Theme options
		theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,paste,pastetext,pasteword,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo",
		theme_advanced_buttons2 : "link,unlink,image,media,hr,code",
		theme_advanced_buttons3 : "",
		theme_advanced_buttons4 : "",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_statusbar_location : "bottom",
		theme_advanced_resizing : false,
		theme_advanced_resizing_use_cookie : false,

		// Example content CSS (should be your site CSS)
		content_css : "/inc/css/style.css",

		// Drop lists for link/image/media/template dialogs
		template_external_list_url : "lists/template_list.js",
		external_link_list_url : "lists/link_list.js",
		external_image_list_url : "lists/image_list.js",
		media_external_list_url : "lists/media_list.js",

		// Replace values for the template plugin
		template_replace_values : {
			username : "Some User",
			staffid : "991234"
		}
	});

}
