//Add input field to admin form for user data fields
function js_addInput(p) {

	var f = $(".entry_method_option:first").attr('id');

	var n = Number($(".entry_method_option").length + 1);

	if (n < 21) {

		$("#option_text").clone().appendTo($("#" + p)).attr("id","option_text" + n);
		$("#" + f).clone().appendTo($("#" + p)).attr("id","entry_method_option_new_" + n);
		$("#entry_method_option_new_" + n).attr("name","field[options][]").val("");
		$("#option_nl").clone().appendTo($("#" + p)).attr("id","option_nl" + n);

	}
}
//Reveal extra fields on user data fields form
function js_entryMethods() {

	if ($("#entry_method").val() != "text" && $("#entry_method").val() != "textarea") {
		$("#entry_method_options").fadeIn();
	} else {
		$("#entry_method_options").hide();
	}


}
//Delete a data field option
function js_deleteOption(option) {

	$("#deleted_options").val($("#deleted_options").val() + $("#entry_method_options_" + option).val() + ",");
	$("#option_" + option).remove();

	var n = $(".delete_option").length;

	if (n < 3) {
		$(".delete_option").hide();
	} else {
		$(".delete_option").show();
	}


}

//Submit text group form
function js_submitTextGroup(id) {
	$(".text_group_select").each(function(index) {
    		if (id != this.id) {
			$("#" + this.id).val(0);
		}
	});
	$("#admin_text_update").submit();

}


